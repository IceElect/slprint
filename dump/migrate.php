<?php
define('BASEPATH', 'migrate.php');
$domain = $argv[1];
$_SERVER['HTTP_HOST'] = $domain;
require('../application/config/database.php');


$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ,
);

$dbh = new PDO($db['dsn'], $db['username'], $db['password'], $options);
$table_name = $db['dbprefix'] . 'db_status';
$result = $dbh->query("SHOW TABLES LIKE '" . $table_name . "';")->fetch();
if (empty($result)){
    $dbh->query(
       "CREATE TABLE `" . $table_name . "` ( `db_status_id` INT NOT NULL AUTO_INCREMENT , `file_name` VARCHAR(100) NOT NULL , `date` DATE NOT NULL , PRIMARY KEY (`db_status_id`)) ENGINE = InnoDB;"
    );
}

$_db_applied_migrations = $dbh->query('SELECT * FROM ' .$table_name . ' ORDER BY db_status_id DESC')->fetchAll();
$migrations = array_values(array_diff(scandir('migrations'), array('..', '.')));

$db_applied_migrations = array();

foreach ($_db_applied_migrations as $key => $value){
    $db_applied_migrations[] = $value['file_name'];
    $migration_key = array_search($value['file_name'], $migrations);
    if ($migration_key === false){
        $dbh->exec('DELETE FROM ' . $table_name . ' WHERE db_status_id = "' . $value['db_status_id'] . '"');
    }
}

if (!empty($migrations)){
    foreach ($migrations as $key => $value){
        if (!in_array($value, $db_applied_migrations)){
            $file_data = file_get_contents('migrations/'.$value);
            try{
                $dbh_result = $dbh->exec($file_data);
                $dbh->exec('INSERT INTO ' . $table_name . ' SET file_name = "' . $value . '" , `date`="' . date('Y-m-d') . '"' );
                echo "Migration: " . $value . " DONE!" . PHP_EOL;
            } catch(PDOException  $e){
                echo "Migration: " . $value . " ERROR: " . $e->getMessage() . PHP_EOL;
            }
        }
    }
}

echo "All migrations applied." . PHP_EOL;
