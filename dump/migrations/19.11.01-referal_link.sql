CREATE TABLE `soc_referal_link` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `target` int(1) NOT NULL,
  `date` date NOT NULL,
  `num_reg` int(11) NOT NULL DEFAULT 0,
  `num_visits` int(11) NOT NULL DEFAULT 0,
  `num_buy` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `soc_referal_link`
--
ALTER TABLE `soc_referal_link`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `soc_referal_link`
--
ALTER TABLE `soc_referal_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `soc_referal_link`
--
ALTER TABLE `soc_referal_link`
  ADD CONSTRAINT `soc_referal_link_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `soc_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
