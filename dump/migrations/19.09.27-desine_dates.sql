ALTER TABLE `soc_product_design` ADD `create_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `status`;
ALTER TABLE `soc_product_design` ADD `approve_date` DATETIME NULL DEFAULT NULL AFTER `create_date`;
