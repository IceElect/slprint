CREATE TABLE `soc_withdraw` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `amount` decimal(9,2) NOT NULL,
  `payment_service` varchar(40) DEFAULT NULL,
  `requisites` text NOT NULL,
  `payed_day` date DEFAULT NULL,
  `payment_sysinfo` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `soc_withdraw`
--
ALTER TABLE `soc_withdraw`
  ADD PRIMARY KEY (`id`);
  
ALTER TABLE `soc_withdraw`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;
