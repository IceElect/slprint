ALTER TABLE `soc_product` ADD `views` INT NOT NULL DEFAULT '0' AFTER `seo_description`;
ALTER TABLE `soc_product` ADD `orders` INT NOT NULL DEFAULT '0' AFTER `views`;
INSERT INTO `soc_product_design` (`id`, `product_type`, `default_color`, `category_id`, `author_id`, `objects`, `config`, `title`, `description`, `status`, `create_date`, `approve_date`) VALUES ('0', '1', '', '1', '1', '', '', '', '', '5', '2019-11-01', NULL);
ALTER TABLE `soc_product` DROP `image`;
ALTER TABLE `soc_product` DROP `designer_id`;
ALTER TABLE `soc_product` ADD FOREIGN KEY (`design_id`) REFERENCES `soc_product_design`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `soc_ci_sessions` CHANGE `user_data` `data` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `soc_ci_sessions` CHANGE `session_id` `id` VARCHAR(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0';