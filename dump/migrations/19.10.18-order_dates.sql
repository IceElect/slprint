ALTER TABLE `soc_orders` ADD `create_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `status`;
ALTER TABLE `soc_orders` ADD `approve_date` DATETIME NULL DEFAULT NULL AFTER `create_date`;
