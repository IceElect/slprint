<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Default_controller extends MX_Controller
{

    protected $aFields = array();
    public $activeModule = "";
    protected $aVars = array();
    protected $oUser;

    public $types = array(
        '',
        'Мужская футболка',
        'Женская футболка',
    );

    function __construct()
    {
        $this->load->helper('debug');
        $this->benchmark->mark('execution_time: conroller_start');
        if ($this->config->item('enable_profiler')) {
            $this->load->library('profiler');
        }
        parent::__construct();

        //$this->output->enable_profiler(TRUE);
        //$this->load->library('user_agent');

        $this->load->database();
        if ($this->config->item('core_autoupdate')){
            $this->load->library('Db_migration');
            $this->db_migration->set_file_list(array('install_model'));
            if (!$this->db_migration->is_latest('core')){
                mygoto('/install/setup/db_initialize');
            }
        };
        $this->load->library('Session');
        $this->load->helper('date');
        $this->load->library('MY_Smarty');
        $this->load->library('my_cache');
        //$this->load_variables();
        $this->load->library('Date_helper');
        $this->load->helper('url');
        $this->load->library('Translate', false);
        $this->load->library('Frontend');
        $this->load->library('messages');//before user Important !!
        $this->load->library('User');// here redirect if no login
        $this->user->assign_user($this->oUser);
        $this->load->library('Option');
        $this->load->library('Cron');
        $this->load->library('Cart');
        if (!empty($_GET['ref'])){
            $this->load->library('Partner_lib');
            $this->partner_lib->referal_save();
        }
        $this->cron->run();

        $this->cart->get_data();

        $this->my_smarty->assign('types', $this->types);

        $this->my_smarty->assign('base_url', base_url());
        $this->my_smarty->assign('oUser', $this->oUser);
        $this->my_smarty->assign('cart', $this->cart);
        $this->my_smarty->assign('time', time());

        $this->frontend->setTemplateDir('Store');
        //$this->frontend->setTemplateDir($this->option->get('template', 'default'));
        //if($this->user->is_logged())
        //    $this->user_model->setLastAction($this->user->get_user_id());

        $this->my_smarty->assign('project_path', FCPATH);
        $this->my_smarty->assign('hosted_url', $this->config->item('hosted_url'));
        $this->my_smarty->assign('base_url', base_url());
        $this->my_smarty->assign('url', current_url());
        $this->my_smarty->assign('cart_data', $this->cart->get_data());

        $keywords = 'футболка, футболки, свитшот, свитшоты, кружка, кружки, значок, значки, дизайн, создать, заказать, купить, красивый, красивые, самому, свой';
        $description = 'На SLPRINT вы можете сделать футболку, кружку, значок или коврик для мыши со своим собственным и уникальным дизайном принта за 5 минут в нашем удобном конструкторе.';

        $this->frontend->add_meta('keywords', 'keywords', $keywords);
        $this->frontend->add_meta('description', 'description', $description);

        $this->frontend->add_meta('og_url', 'og:url', current_url(), 'property');
        $this->frontend->add_meta('og_type', 'og:type', 'website', 'property');
        $this->frontend->add_meta('og_image', 'og:image', base_url('/logo.png'), 'property');
        $this->frontend->add_meta('og_site_name', 'og:site_name', "SLEvents - Организация мероприятий", 'property');

        $this->frontend->setTitle('SLPrint - Создай идеальную футболку сам');

        $this->is_test = false;
        $this->is_local = false;
        $this->is_production = false;

        if ($_SERVER['HTTP_HOST'] == 'print.ru') {
            $this->is_local = true;
            
        }
        if ($_SERVER['HTTP_HOST'] == 'print.slto.ru') {
            $this->is_production = true;
        }
        if ($_SERVER['HTTP_HOST'] == 'test.print.slto.ru') {
            $this->is_test = true;
        }

        $this->my_smarty->assign('is_test', $this->is_test);
        $this->my_smarty->assign('is_local', $this->is_local);
        $this->my_smarty->assign('is_production', $this->is_production);

        //init_variables
    }

    function check_login()
    {
        $this->user->check_login();
    }

    protected function setActiveModule($fp_module = '')
    {
        $this->activeModule = $fp_module;
        $this->permission->setActiveModule($fp_module);
        $this->frontend->aConf_add('active_module', $fp_module);
        $this->frontend->add_body_class('page_' . $fp_module);
    }


    protected function returnJson($fp_Val)
    {
        if ($this->config->item('use_php_json')) {
            echo json_encode($fp_Val);
        } else {
            $this->load->library('Json');
            echo $this->json->encode($fp_Val);
        }
    }

    protected function getValidationFields()
    {
        $result = array();
        foreach ($this->aFields as $key => $value) {
            if ($value['validate']) {
                $result['rules'][$value['field']] = $value['rules'];
                $result['fields'][$value['field']] = $value['title'];
            }
        }
        return $result;
    }

    protected function is_ajax()
    {
        $is_ajax = $this->input->get_post('ajax');
        if (!empty($is_ajax) || $this->input->is_ajax_request()) {
            return true;
        }
        return false;
    }

    protected function translit($fp_str)
    {
        return $this->translate->translit($fp_str);
    }

    protected function release_locked_page($fp_page_id, $redirect = false)
    {
        return $this->locker->release_locked_page($fp_page_id);;
    }

    protected function t($str, $default_str)
    {
        return $this->translate->t($str, $default_str);
    }

    function redirect_https()
    {
        return isset($_SERVER['HTTPS']);
    }


}

