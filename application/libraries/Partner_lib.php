<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Partner_lib
{
    var $oUser;
    var $activeModule = "";
    private $CI;
    private $partner_rate = 0.15;

    function __construct()
    {
        $this->CI =& get_instance();
    }



    function get_order_list($filter, $is_count = false, $limit = false, $order = false){
        $this->db =& $this->CI->db;
        if ($is_count) {
            $this->db->select('count(*) as num');
        } else {
            $this->db->select('o.*, SUM(op.price) * ' . $this->partner_rate. ' as payroll');
        }
        $this->db->from('orders o');
        $this->db->join('order_products op', 'o.id = op.order_id', 'left');
        $this->db->join('product p', 'p.id = op.product_id', 'left');
        $this->db->join('product_design d', 'p.design_id = d.id', 'left');
        if (!empty($filter)) {
            if (!empty($filter['search'])){
                if (!empty($filter['search']['fields'])) {
                    $_where = array();
                    foreach ($filter['search']['fields'] as $field){
                        if ($field == 'author_id'){
                            continue;
                        }
                        $_where[] = $field['field'] . ' LIKE "%' . addslashes($filter['search']['value']) . '%"';
                    }

                    $_where = '(' . implode(' OR ', $_where) . ')';
                    $this->db->where($_where);
                }
                unset($filter['search']);
            }
            $this->db->where($filter);

        }
        if (!empty($filter['author_id'])){
            $this->CI->db->where('d.author_id', $filter['author_id']);
        }
        if (!empty($limit)) {
            $this->db->limit($limit['limit'], $limit['offset']);
        }
        if (!empty($order)) {
            $this->db->order_by($order['column'], $order['dir']);
        } else {
            $this->db->order_by('o.id desc');
        }
        $this->db->group_by('o.id');
        $query = $this->db->get();
        //dump($this->db->last_query());
        if ($is_count) {
            $result = $query->result_array();
            return (!empty($result))?$result[0]['num']:0;
        } else {
            return $query->result_array();
        }

    }

    function process_order($order_id){
        $this->CI->load->model('order_model');
        $products = $this->CI->order_model->get_order_products_data($order_id);
        if (empty($products)){
            return true;
        }
        $order = $this->CI->order_model->getDataById($order_id);
        $this->CI->load->model('default_model', 'shop');
        $this->CI->shop->setTable('product');
        if (empty($order['bonus_payout'])) {
            $this->CI->load->model('user_model');
            foreach ($products as $product) {
                $user = $this->CI->user_model->getDataById($product['author_id']);
                $rate = $this->partner_rate;
                if (!empty($user['partner_rate'])){
                    $rate = $user['partner_rate'];
                }
                $bonus = round($product['price'] * $rate, 2);
                $this->CI->user_model->add_balance($product['author_id'], $bonus);

                $this->CI->shop->update(array('orders' => $product['orders'] + 1), array('id' => $product['id']));
            }
            $this->CI->order_model->update(array('bonus_payout' => 1,'approve_date' => date('Y-m-d')), array('id' => $order_id));
        }
        $this->referal_process('buy');
    }

    function get_withdraw_list($filter, $is_count = false, $limit = false, $order = false){
        $this->db =& $this->CI->db;
        if ($is_count) {
            $this->db->select('count(*) as num');
        } else {
            $this->db->select('*');
        }
        $this->db->from($this->table);
        if (!empty($filter)) {
            if (!empty($filter['search'])){
                if (!empty($filter['search']['fields'])) {
                    $_where = array();
                    foreach ($filter['search']['fields'] as $field){
                        $_where[] = $field['field'] . ' LIKE "%' . addslashes($filter['search']['value']) . '%"';
                    }

                    $_where = '(' . implode(' OR ', $_where) . ')';
                    $this->db->where($_where);
                }
                unset($filter['search']);
            }
            $this->db->where($filter);

        }

        if (!empty($limit)) {
            $this->db->limit($limit['limit'], $limit['offset']);
        }
        if (!empty($order)) {
            $this->db->order_by($order['column'], $order['dir']);
        }
        $query = $this->db->get();
        //dump($this->db->last_query());
        if ($is_count) {
            return $query->result_array()[0]['num'];
        } else {
            return $query->result_array();
        }

    }

    function referal_save(){
        if (empty($this->CI->session->ref_id)) {
            $this->CI->session->set_userdata('ref_id', $_GET['ref']);
            $this->referal_process('visit');
        }
    }

    function referal_process($action){
        $ref_id = $this->CI->session->ref_id;
        if (empty($ref_id)){
            return true;
        }
        $this->CI->load->model('default_model', 'referal_model');
        $this->CI->referal_model->setTable('referal_link');
        $ref = $this->CI->referal_model->getDataById($ref_id);
        if ($action == 'register') {
            if (!empty($ref)){
                $this->CI->referal_model->update(array('num_reg' => $ref['num_reg'] + 1), array('id' => $ref['id']));
            }
        }
        if ($action == 'visit'){
            if (!empty($ref)){
                $this->CI->referal_model->update(array('num_visits' => $ref['num_visits'] + 1), array('id' => $ref['id']));
            }
        }
        if ($action == 'buy'){
            if (!empty($ref)){
                $this->CI->referal_model->update(array('num_buy' => $ref['num_buy'] + 1), array('id' => $ref['id']));
            }
        }
        return true;
    }
}

