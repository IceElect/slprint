<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Session extends CI_Session
{

	public function get($item = ''){
		return $this->userdata($item);
	}
	public function getMainSessData($item = ''){
		return $this->userdataMain($item);
	}
	public function put($newdata = array(), $newval = ''){
		$this->set_userdata($newdata, $newval);
	}
	public function clear($newdata = array()){
		$this->unset_userdata($newdata);
	}
	public function clearAll(){
		if($this->session_name){
			$this->userdata[$this->session_name] = array();
		}
		$this->sess_update();
	}


}
