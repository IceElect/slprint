<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package      CodeIgniter
 * @author      ExpressionEngine Dev Team
 * @copyright   Copyright (c) 2006 - 2012, EllisLab, Inc.
 * @license      http://codeigniter.com/user_guide/license.html
 * @link      http://codeigniter.com
 * @since      Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Shopping Cart Class
 *
 * @package      CodeIgniter
 * @subpackage   Libraries
 * @category   Shopping Cart
 * @author      ExpressionEngine Dev Team
 * @link      http://codeigniter.com/user_guide/libraries/cart.html
 */
class My_cart extends CI_Cart {



    function add_($items = array()){
        if (empty($items['id'])){

        } else {
            $row_id = $this->generate_rowid($items);
            if (!empty($this->_cart_contents[$row_id])){
                $items['qty'] += $this->_cart_contents[$row_id]['qty'];
                $items['rowid'] = $row_id;
                $this->update($items);
            } else {
                $this->insert($items);
            }
        }
    }

    function add($items = array()){
        if (empty($items['id'])){

        } else {
            $row_id = $this->generate_rowid($items);
            $items['rowid'] = $row_id;
            $this->insert($items);
        }
    }

    public function format_number($n = '')
    {
        return ($n === '') ? '' : number_format( (float) $n, 0, '.', ' ');
    }

    function generate_rowid($item){
        if (isset($item['options']) AND count($item['options']) > 0) {
            return md5($item['id'].implode('', $item['options']) . mt_rand());
        } else {
            return md5($item['id'] . mt_rand());
        }
    }

    function discount($price, $discount, $discount_type = 0){
        if(!$discount_type){
            $price -= $discount;
        }else{
            $percent = $price / 100;
            $price = $price - ($percent * $discount);
        }

        return floor($price);
    }

    function get_data(){
        $this->CI->load->helper('number_helper');
        $this->CI->load->library('shop_lib');

        $total = $this->total();
        $count = $this->total_items();
        $total_products = $this->total();

        $promo = $this->CI->session->userdata('promo');
        if($promo)
            $total = $this->CI->shop_lib->get_price_with_discount($total, $promo['discount'], $promo['discount_type']);

        if($total < 0) $total = 0;

        $total          = $this->format_number($total);
        $total_products = $this->format_number($total_products);

        $total_text = 'На ' . $total_products . ' руб';

        if($count > 0){
            $count_text = num_decline('В корзине %n% товар%o%', $count, array('', 'а', 'ов'));
        }else{
            $count_text = 'Корзина пуста';
            $total_text = '';
        }

        return array(
            'promo' => $promo,
            'count_products' => $count,
            'count_products_text' => $count_text,
            'total' => $total,
            'total_products' => $total_products,
            'total_products_text' => $total_text,
        );
    }

}
