<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

// var_dump( sprintf( '%s закрыто с %s', 'CASINO', '21.20' ) );
// die();

class Shop_lib
{
    var $oUser;
    var $activeModule = "";
    private $CI;
    private $session;
    private $input;
    private $messages;
    private $permission;
    private $loaded_users = array();

    private $path = '';

    public $price_config = array(
        'manshort' => array(
            'cost_price' => 400,
            'start_price' => 800,
            'side_price' => 200,
            'discount' => 300,
            'discount_type' => 0,
        ),
        'womanshort' => array(
            'cost_price' => 400,
            'start_price' => 800,
            'side_price' => 200,
            'discount' => 300,
            'discount_type' => 0,
        ),
        'man_sweatshirt' => array(
            'cost_price' => 900,
            'start_price' => 1200,
            'side_price' => 200,
            'discount' => 200,
            'discount_type' => 0,
        ),
        'mug_white' => array(
            'cost_price' => 210,
            'start_price' => 400,
            'side_price' => 0,
            'discount' => 25,
            'discount_type' => 1,
        ),
        'mug_twotone' => array(
            'cost_price' => 330,
            'start_price' => 560,
            'side_price' => 0,
            'discount' => 25,
            'discount_type' => 1,
        ),
        'sign' => array(
            'cost_price' => 10,
            'start_price' => 40,
            'side_price' => 0,
            'discount' => 25,
            'discount_type' => 1,
        ),
    );

    public $type_names = array(
        1 => 'manshort',
        2 => 'womanshort',
        3 => 'mug_white',
        4 => 'mug_twotone',
        5 => 'sign',
        6 => 'man_sweatshirt',
    );

    public $available_sides = array(
        'manshort' => array('front', 'back'),
        'womanshort' => array('front', 'back'),
        'mug_white' => array('front'),
        'mug_twotone' => array('front'),
        'sign' => array('front'),
        'man_sweatshirt' => array('front', 'back'),
    );

    public $available_colors = array(
        'manshort' => array(
            'white'          => array('name' => 'white', 'code' => 'white'), 
            'black'          => array('name' => 'black', 'code' => '#27282c'),
            'gray'           => array('name' => 'gray', 'code' => '#aeafb5'),
            'blue'           => array('name' => 'blue', 'code' => '#1a479c'), // 1566
            'lightblue'      => array('name' => 'lightblue', 'code' => '#0a72cb'), // 1557
            'purple'         => array('name' => 'purple', 'code' => '#1e1f3d'), // 1575
            'green'          => array('name' => 'green', 'code' => '#17814d'), // 1561
            'palegreen'      => array('name' => 'palegreen', 'code' => '#233c40'), // 1545
            'cherry'         => array('name' => 'cherry', 'code' => '#98233b'), // 1540
            'red'            => array('name' => 'red', 'code' => '#b71115'), // 1549
            'yellow'         => array('name' => 'yellow', 'code' => '#ebd300'), // 1540
            'orange'         => array('name' => 'orange', 'code' => '#fe752f'), // 1540

            // 'paleblue'      => array('name' => 'paleblue', 'code' => '#bcc5d6'),
            // 'grayblue'      => array('name' => 'grayblue', 'code' => '#afb9d4'),
            // 'lightgrayblue' => array('name' => 'lightgrayblue', 'code' => '#b9d7cb'),
            // 'swampgreen'    => array('name' => 'swampgreen', 'code' => '#bdc2ab'),
            // 'lightpink'     => array('name' => 'lightpink', 'code' => '#e4c7c9'),
            // 'pink'          => array('name' => 'pink', 'code' => '#e7abb3'),
            // 'cream'         => array('name' => 'cream', 'code' => '#d4d1b0'),
            // 'leather'       => array('name' => 'leather', 'code' => '#e8d4b9'),
        ),
        'womanshort' => array(
            'white'          => array('name' => 'white', 'code' => 'white'), 
            'black'          => array('name' => 'black', 'code' => '#27282c'),
            'gray'           => array('name' => 'gray', 'code' => '#aeafb5'),
            'blue'           => array('name' => 'blue', 'code' => '#1a479c'), // 1566
            'lightblue'      => array('name' => 'lightblue', 'code' => '#0a72cb'), // 1557
            'purple'         => array('name' => 'purple', 'code' => '#1e1f3d'), // 1575
            'green'          => array('name' => 'green', 'code' => '#17814d'), // 1561
            'palegreen'      => array('name' => 'palegreen', 'code' => '#233c40'), // 1545
            'cherry'         => array('name' => 'cherry', 'code' => '#98233b'), // 1540
            'red'            => array('name' => 'red', 'code' => '#b71115'), // 1549
            'yellow'         => array('name' => 'yellow', 'code' => '#ebd300'), // 1540
            'orange'         => array('name' => 'orange', 'code' => '#fe752f'), // 1540
        ),
        'mug_white' => array(
            'white'         => array('name' => 'white', 'code' => 'white'),
        ),
        'mug_twotone' => array(
            'black'         => array('name' => 'black', 'code' => 'black'),
        ),
        'man_sweatshirt' => array(
            'gray'          => array('name' => 'gray', 'code' => '#cccccc'),
            'red'           => array('name' => 'red', 'code' => '#ad6c70'),
            'green'         => array('name' => 'green', 'code' => '#aaa587'),
            'blue'          => array('name' => 'blue', 'code' => '#798092'),
        ),
        'sign' => array(
            'white' => array('name' => 'white', 'code' => '#ffffff'),
        ),
    );

    function __construct()
    {
        $this->CI =& get_instance();
        $this->translate =& $this->CI->translate;
        $this->CI->load->model('shop_model');
        $this->shop_model =& $this->CI->shop_model;

        if (!isset($_SERVER) || !isset($_SERVER['HTTP_HOST']) || '' == $_SERVER['HTTP_HOST']) {
            $g_sHostname = getenv("HOSTNAME");
        } else {
            $g_sHostname = $_SERVER['HTTP_HOST'];
        }

        if (preg_match("`test\.print\.slto\.ru$`i", $g_sHostname)) {
            $this->path = '/var/www/print.slto.ru/public_html/';
            $this->path = FCPATH;
            $this->upload_path = FCPATH;
        }else{
            $this->path = '';
            $this->upload_path = FCPATH;
        }

        $config = $this->CI->config->load('prints');
        $this->prints_config = $config['config'];
    }

    function getColorByName($name, $product_type){
        $colors = $this->available_colors[$product_type];
        foreach($colors as $color){
            if($color['name'] == $name) return $color;
        }
    }

    function create_design($data, $sides, $config, $need_valid = false){
        $this->CI->load->model('default_model', 'design_model');
        $this->CI->design_model->setTable('product_design');

        $error = false;

        if($need_valid){
            $error = $this->create_design_validate($data);
        }

        if(!$error){
            $design_id = $this->CI->design_model->save(array('status' => 0), 'add');

            $design_path = $this->path . 'uploads/prints/' . $design_id;
            if (!file_exists($design_path)) {
                mkdir($design_path, 0777, true);
            }

            $data['objects'] = json_encode($sides);
            $data['config'] = json_encode($config);

            if(isset($data['tags'])){
                $tags = $data['tags'];

                $tag_list = $this->CI->design_model->get_tags_id($tags, 'shop_tag');
                if (count($tags) != count($tag_list)) {
                    $need_save = array_udiff($tags, array_values($tag_list), array($this, 'strcase'));
                    if ($need_save) {
                        $new_tags = $this->CI->design_model->add_tags($need_save, 'shop_tag');

                        $tag_list = $tag_list + $new_tags;
                    }
                }

                $this->CI->design_model->save_tags('product_design', $design_id, $tag_list, 'shop_tag');
            }
            unset($data['tags']);

            $this->CI->design_model->save($data, 'edit', $design_id);

            $this->CI->load->helper('print_helper');
            $print = new Print_object($design_id, $this->type_names[$data['product_type']], $config);

            $colors = $this->available_colors[$this->type_names[$data['product_type']]][$data['default_color']];

            $print->setSides($sides);
            $print->setColors(array($colors));
            $sources = $print->generatePrints($design_id, $this->type_names[$data['product_type']], $this->type_names[$data['product_type']]);

            return array(
                'id' => $design_id,
            );
        }else{
            return array(
                'error' => $error,
            );
        }
    }

    function strcase($a, $b)
    {
        $a = mb_strtoupper($a);
        $b = mb_strtoupper($b);
        if ($a === $b) {
            return 0;
        }
        return ($a > $b) ? 1 : -1;
    }

    function create_design_validate($data){
        if(empty($data['title'])){
            return "Введите название";
        }

        if(empty($data['tags'])){
            return "Введите теги";
        }

        if(empty($data['category_id'])){
            return "Выберите категорию";
        }
    }

    function create_design_product($design_id, $product_type, $color = false){
        $this->CI->load->model('default_model', 'design_model');
        $this->CI->design_model->setTable('product_design');

        $this->CI->load->model('default_model', 'design_sources_model');
        $this->CI->design_sources_model->setTable('product_design_sources');

        $design = $this->CI->design_model->getDataById($design_id);

        if($design){
            $this->CI->load->helper('print');

            $sides = (array) json_decode($design['objects']);
            $config = (array) json_decode($design['config']);

            // SIDES FILTER
            foreach($sides as $side_type => $side){
                if(!in_array($side_type, $this->available_sides[$product_type])){
                    unset($sides[$side_type]);
                }
            }

            // SIDES COUNT
            $sides_count = 0;
            foreach($sides as $_side){
                if(is_array($_side)) $sides_count++;
            }

            $sources = array();
            $previews = array();

            $design_path = $this->path . 'uploads/prints/' . $design_id . '/' . $product_type;

            if (!file_exists($design_path)) {
                mkdir($design_path, 0777, true);
            }

            // COLORS ARRAY
            if($color){
                $colors = array($this->getColorByName($color, $product_type));
            }else{
                $colors = $this->available_colors[$product_type];
            }
            $colors = $this->available_colors[$product_type];

            // DEFAULT COLOR
            if($product_type == $this->type_names[$design['product_type']]){
                $default_color = $this->available_colors[$product_type][$design['default_color']];
            }else{
                $default_color = array_shift($colors);
            }

            $print_data = $this->get_design_product_preview($design_id, $product_type, $default_color['name'], 'front', $design);

            $type_ids = array_flip($this->type_names);

            $data = array(
                'design_id' => $design_id,
                'product_type' => $type_ids[$product_type],
                'default_color' => $default_color['name'],
                'sides_count' => $sides_count,
                'sources' => json_encode($print_data['sources']),
                'previews' => json_encode($print_data['previews']),
            );

            $this->CI->design_sources_model->save($data, 'add');

            return $print_data;
        }

        return false;
    }

    function get_product_preview_path($design_id, $product_type, $color = false, $side = 'front', $design = false, $size = 512){
        $_data = $this->get_design_product_preview($design_id, $product_type, $color, $side, $design, $size);
        $preview = $_data['previews'][$side];

        $design_path  = 'uploads/prints/' . $design_id;
        $source_path  = $design_path . '/' . $product_type;
        $preview_folder = $source_path . '/preview/';
        $preview_path = $preview_folder . $side . '-' . $color . '-' . $size . '.jpg';
        if (!file_exists($preview_path)) {
            if (!is_dir($preview_folder)){
                mkdir($preview_folder);
            }
            $img = new \Imagick();
            $img->readImage(FCPATH . $preview);
            $img->setImageFormat('jpg');
            $img->thumbnailImage($size, $size);
            $img->setImageCompression(Imagick::COMPRESSION_JPEG);
            $img->setImageCompressionQuality(75);
            $img->writeImage($_SERVER['DOCUMENT_ROOT'] . '/' . $preview_path);
        }

        return $preview_path;
    }

    function get_design_product_preview($design_id, $product_type, $color = false, $side = 'front', $design = false, $size = 512){

        // CONVERT TYPE ID TO TYPE NAME
        if(is_numeric($product_type)){
            $product_type = $this->type_names[$product_type];
        }

        if(!$color){
            $colors = $this->available_colors[$product_type];
            $color = array_shift($colors);
            $color = $color['name'];
        }

        $print_config = $this->prints_config[$product_type];

        $design_path  = 'uploads/prints/' . $design_id;
        $source_path  = $design_path . '/' . $product_type;
        $print_path   = $source_path . '/' . $side . '.png';
        $preview_path = $source_path . '/' . $side . '-' . $color . '-preview.png';

        if(is_dir($this->upload_path . $source_path)){
            if(is_file($this->upload_path . $preview_path)){
                $result = array('sources' => array(), 'previews' => array());
                foreach($this->available_sides[$product_type] as $side_type){
                    if(is_file($this->upload_path . $source_path . '/' . $side_type . '.png')){
                        $result['sources'][$side_type] = $this->path . $source_path . '/' . $side_type . '.png';
                    }else{
                        $result['sources'][$side_type] = $this->path . $design_path . '/' . $side_type . '.png';
                    }
                    $result['previews'][$side_type] = $this->path . $source_path . '/' . $side_type . '-' . $color . '-preview.png';
                }
                return $result;
            }
        }

        if(!$design){
            $this->CI->load->model('design_model');
            $design = $this->CI->design_model->getDataById($design_id);
        }

        $this->CI->load->helper('print_helper');

        $sides = (array) json_decode($design['objects']);
        $config = (array) json_decode($design['config']);

        foreach($sides as $side_type => $side_object){
            if(!in_array($side_type, $this->available_sides[$product_type])){
                unset($sides[$side_type]);
            }
        }

        $color = $this->available_colors[$product_type][$color];

        $print = new Print_object($design_id, $product_type, $config);

        $print->setColors(array($color));
        $print->setSides($sides);

        if($product_type == $this->type_names[$design['product_type']]){
            $print_path = $design_path . '/' . $side . '.png'; 
        }

        if(!is_file(FCPATH . $print_path)){
            $sources = $print->generatePrints($design_id, $product_type, $this->type_names[$design['product_type']]);
        }else{
            $sources = array();
            foreach($this->available_sides[$product_type] as $side_type){
                if($product_type == $this->type_names[$design['product_type']]){
                    $sources[$side_type]  = $design_path . '/' . $side_type . '.png';
                }else{
                    $sources[$side_type]  = $source_path . '/' . $side_type . '.png';
                }
            }
        }
        $previews = $print->generatePreview($product_type, $side, $design_id, $this->type_names[$design['product_type']]);

        if($product_type == $this->type_names[$design['product_type']]){
            $previews = $previews[$color['name']];
        }else{
            if(!isset($previews['front']))
                $previews = array_shift($previews);
        }

        return array(
            'design_id' => $design_id,
            'sources' => $sources,
            'previews' => $previews,
        );
    }

    function check_design_product_preview($design_id, $product_type){
        $this->CI->load->model('default_model', 'design_sources_model');
        $this->CI->design_sources_model->setTable('product_design_sources');

        $type_keys = array_flip($this->type_names);

        $data = $this->CI->design_sources_model->getDataByWhere(array(
            'design_id' => $design_id, 
            'product_type' => $type_keys[$product_type],
        ), 'add');

        if(!$data) return false;
        return true;
    }

    function generate_preview_url($design_id, $product_type, $color, $side = 'front', $size = 512){
        // CONVERT TYPE ID TO TYPE NAME
        if(is_numeric($product_type)){
            $product_type = $this->type_names[$product_type];
        }

        $params = array();
        $params[] = $design_id;
        $params[] = $product_type;
        $params[] = $color;
        $params[] = $size;
        $params[] = $side;

        $link = base_url('ajax/product/preview/') . implode('/', $params) . '.jpg';

        return $link;
    }

    function update_order_status($order_id, $status_id, $user_id = false){
        $this->CI->load->model('order_model');
        $this->CI->load->library('vk_bot');

        $order = $this->CI->order_model->get_order_by_id($order_id);

        if(!$order)
            return false;

        switch($status_id){
            case 0:
                $managers = $this->CI->vk_bot->get_role('manager');

                $total = $this->CI->cart->discount($order->total, $order->discount, $order->discount_type);

                $message = "
                xD Новый заказ №".$order->id."
                На сумму ".$total." рублей
                Стоимость доставки: ".$order->delivery_price." рублей
                Имя: ".$order->fname."
                E-mail: ".$order->email."
                Телефон: ".$order->phone."
                Адрес: ".$order->address."
                Страница в админке: ".base_url() . 'admin/order/edit/' . $order->id."
                ";

                $this->CI->vk_bot->messagesSend($managers, $message);
                $this->CI->vk_bot->messagesSend($managers, $order->phone);
                break;
        }

        dump(0);
    }

    function get_product_attributes($product_id, $need_atts = false){
        $atts = $this->shop_model->get_product_attributes($product_id);

        if(!$atts){
            return array();
        }

        if(!$atts[0]->id){
            return array();
        }

        $result = array();
        foreach($atts as $key => $value){
            if($need_atts && !in_array($value->name, $need_atts)) continue;

            $new_values = array();
            $attr_values = json_decode( $value->values );
            foreach($attr_values as $v_key => $v_item){
                $v_item->title = $this->translate->t('attribute_' . $value->name . '_' . $v_item->name, $v_item->name);
                $new_values[$v_item->name] = $v_item;
            }
            $value->values = $new_values;

            $result[$key] = $value;
            // $result[$value->name] = $value;
        }

        return $result;
    }

    function get_price_with_discount($price, $discount, $discount_type = 0){
        if(!$discount_type){
            $price -= $discount;
        }else{
            $percent = $price / 100;
            $price -= $percent * $discount;
        }

        return floor($price);
    }

    function get_price($product, $values, $sides = 1){
        $atts = $this->shop_model->get_product_attributes($product->id);

        $data = array();

        $price = $product->price;

        if($product->type == 1 || $product->type == 2){
            if($sides > 1){
                $price += 200;
                $data['multi_sides_price'] = true;
            }
        }

        $price_with_discount = $this->get_price_with_discount($price, $product->discount, $product->discount_type);
        $add_price = 0;

        if(!empty($atts[0]->id) && $values){
            foreach($atts as $key => $attr){
                
                $new_values = array();
                $attr_values = json_decode( $attr->values );
                foreach($attr_values as $v_key => $v_item){
                    // $v_item->title = $this->translate->t('attribute_' . $attr->name . '_' . $v_item->name, $v_item->name);
                    $new_values[$v_item->name] = $v_item;
                }
                $attr->values = $new_values;

                if(isset($attr->values[ $values[$attr->name] ]))
                    $add_price += $attr->values[ $values[$attr->name] ]->add_price;
                // $result[$attr->name] = $attr;
            }
        }

        $data['_price'] = $price + $add_price;
        $data['_price_dis'] = $price_with_discount + $add_price;

        $data['price'] = $this->CI->cart->format_number( $price + $add_price );
        $data['price_dis'] = $this->CI->cart->format_number( $price_with_discount + $add_price );

        return $data;
    }

    function generate_price($type, $sides_count, $atts = array()){
        if(is_int($type)) $this->type_names[$type];

        $config = $this->price_config[$type];

        $price = $config['start_price'];
        $cost_price = $config['cost_price'];

        $add_price = 0;
        $add_price += $config['side_price'] * $sides_count;

        $price += $add_price;
        $cost_price += $add_price;

        return array(
            'price' => $price,
            'cost_price' => $cost_price,
            'discount' => $config['discount'],
            'discount_type' => $config['discount_type'],
        );
    }

    function countSides($sides){
        if(!is_array($sides))
            $sides = json_decode($sides);

        $count = 0;

        foreach($sides as $item){
            if(is_array($item)) $count++;
        }

        return $count;
    }

    function getChildCats($cat_id, $user = false){
        if($cat_id === 0){
            $this->shop_model->order('c.title', 'asc');
        }
        if($user){
            $this->shop_model->filter('eq', 'p.designer_id', $user->id, 'AND');
        }
        $categories = $this->shop_model->get_children_cats($cat_id);

        return $categories;
    }

    function getChildIds($cat_id){
        $categories_ids = $this->shop_model->get_tree_ids('parent_id', $cat_id);

        $categories_ids[] = $cat_id;

        return $categories_ids;
    }

    function translit($string) {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
            
            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
            ' ' => '-',
        );
        return strtr($string, $converter);
    }

}