<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Vk_bot{

    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->config = $this->CI->load->config('bot');
    }

    function _vkApi_call($method, $params = array()) {
        if (!$this->config['enabled']){
            return true;
        }
        $params['access_token'] = $this->config['app_secret']; 
        $params['v'] = "5.92"; 
        $url = "https://api.vk.com/method/" . $method . '?' . http_build_query($params);
        $curl = curl_init($url); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
        $json = curl_exec($curl); 
        curl_close($curl); 
        $response = json_decode($json, true);
        if(isset($response['response'])){
            return $response['response']; 
        }
        dump($response);
    }

    //Функция для вызова messages.send 
    function messagesSend($peer_id, $message, $attachments = array()) {
        return $this->_vkApi_call('messages.send', array( 
            'user_ids' => $peer_id, 
            'message' => $message, 
            'random_id' => rand(0, 64), 
            'attachment' => implode(',', $attachments), 
        )); 
    }



    function vkApi_messagesKeyboardSend($peer_id, $message, $keyboard = array(), $attachments = array()) { 
        return $this->_vkApi_call('messages.send', array( 
            'peer_id' => $peer_id, 
            'message' => $message, 
            'random_id' => rand(), 
            'keyboard' => json_encode((object) array(
                "one_time" => true, 
                'buttons' => $keyboard,
            ), JSON_UNESCAPED_UNICODE), 
            'attachment' => implode(',', $attachments), 
        )); 
    }

    function update_order_status($order_id, $status_id, $user_id = false){
        $this->CI->load->model('order_model');

        $managers = $this->get_role('manager');
        $order = $this->CI->order_model->get_order_by_id($order_id);

        if(!$order)
            return false;

        if($status_id < $order->status){
            return false;
        }

        switch($status_id){
            case 0:
                $managers = $this->get_role('manager');

                $total = $this->CI->cart->discount($order->total, $order->discount, $order->discount_type);

                $message = "
                xD Новый заказ №".$order->id."
                На сумму ".$total." рублей
                Стоимость доставки: ".$order->delivery_price." рублей
                Дата доставки: ".$order->delivery_date."
                Имя: ".$order->fname."
                E-mail: ".$order->email."
                Телефон: ".$order->phone."
                Адрес: ".$order->address."
                Страница в админке: ".base_url() . 'admin/order/edit/' . $order->id."
                Информация: ".base_url() . 'ajax/callback/order/' . $order->id."
                ";

                $message = preg_replace("/ {2,}/"," ",$message);

                $this->messagesSend($managers, $message);
                break;
            case 1:

                if(!in_array($user_id, $this->config['roles']['manager'])){
                    $this->messagesSend($user_id, 'У вас нет прав.');
                    return false;
                }

                if($order->manager_id > 0){
                    $this->messagesSend($user_id, 'У этого заказа уже есть менеджер.');
                    return false;
                }

                $manager_ids = array_flip($this->config['roles']['manager']);
                $uid = $manager_ids[$user_id];

                $this->CI->load->model('user_model');
                $user = $this->CI->user_model->getUserById($uid);

                $this->CI->order_model->update(array('manager_id' => $user->id, 'status' => 1), array('id' => $order_id));

                $this->messagesSend($managers, 'Пользователь '.$user->fname.' '.$user->lname.' принял заказ №'.$order_id);
                sleep(1);
                $this->messagesSend($user_id, $order->phone);
                break;
            case 2:
                $this->CI->order_model->update(array('status' => 2), array('id' => $order_id));

                $manager_ids = array_flip($this->config['roles']['manager']);
                $uid = $manager_ids[$user_id];

                if($uid != $order->manager_id){
                    $this->messagesSend($user_id, $order->manager_id);
                    $this->messagesSend($user_id, 'Вы не менеджер этого заказа.');
                    return false;
                }

                $providers = $this->get_role('provider');

                $this->messagesSend($user_id, 'Заказ №'.$order_id.' подтвержден.');

                $delivery_types = array('', 'Курьером');
                $total = $this->CI->cart->discount($order->total, $order->discount, $order->discount_type);
                $message = "
                xD Новый заказ №".$order->id."
                На сумму ".$total." рублей
                Стоимость доставки: ".$order->delivery_price." рублей
                Способ доставки: ".$delivery_types[$order->delivery_type]."
                Дата доставки: ".$order->delivery_date."
                Информация: ".base_url() . 'ajax/callback/order/' . $order->id."
                ";
                $this->CI->load->library('partner_lib');
                $this->CI->partner_lib->process_order($order_id);
                $message = preg_replace("/ {2,}/"," ",$message);

                break;
        }
    }

    function get_role($role){
        if(isset($this->config['roles'][$role])){
            return implode(',', $this->config['roles'][$role]);
        }
        return false;
    }

    function log($text){
        file_put_contents(FCPATH . 'callback.txt', $text, FILE_APPEND);
    }

}