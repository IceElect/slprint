<div class="page-main blank-page container">
    <div style="width: 100%">
        <div class="blank-content">
            <div class="blank-header">
                <h1 class="blank-title">Страница не найдена</h1>
            </div>
            <div class="blank-description" style="flex: 1">
                <p>Но вы можете перейти на <a href="/">главную страницу</a> или <a href="/construct">создать свою крутую одежду</a>.
                Или посмотрите <a href="/catalog?sort=new">каталог</a> с нашими последними изделиями.</p>
            </div>
        </div>
    </div>
</div>