<div class="page-main container product-page">
    <div class="construct-type-selector" style="margin-top: 15px">
        <select name="construct_type" id="" class="field" style="background: #d2383b;color: #fff">
            <option value="1" {if $type == 1}selected{/if}>Мужская футболка</option>
            <option value="2" {if $type == 2}selected{/if}>Женская футболка</option>
            <option value="3" {if $type == 3}selected{/if}>Кружка белая</option>
            <option value="4" {if $type == 4}selected{/if}>Кружка двухцветная</option>
            <option value="5" {if $type == 5}selected{/if}>Значок</option>
            <option value="6" {if $type == 6}selected{/if}>Свитшот</option>
        </select>
    </div>
    <div class="product-page-main page-layout page-layout--reverse">
        <div class="page-layout__main">
            <div class="thumb" style="margin-bottom: 40px">
                <div class="product-thumb-loading">
                    <!-- <i class="fal fa-spinner-third fa-spin"></i> -->
                </div>
                <div class="product-constructor">
                    <div class="constructor-controls" style="display: none;">

                        <button class="constructor-controls__button" id="constructor-control-delete-layer" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Удалить слой"><i class="fal fa-trash" aria-hidden="true"></i></button> 

                        {*<button class="constructor-controls__button constructor-controls__edit-layer constructor-controls__edit-layer--active" id="edit-layer" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Редактировать слой"><i class="fa fa-pencil" aria-hidden="true"></i></button>*}

                        {*<button class="constructor-controls__button" id="constructor-control-flip-x" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Отразить по&nbsp;горизонтали"><span class="constructor-controls__icon constructor-controls__icon--reflect-h" aria-hidden="true">&nbsp;</span></button>*}

                        {*<button class="constructor-controls__button" id="constructor-control-flip-y" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Отразить по&nbsp;вертикали"><span class="constructor-controls__icon constructor-controls__icon--reflect-v" aria-hidden="true">&nbsp;</span></button>*}

                        <button class="constructor-controls__button" id="constructor-control-center-vertical" data-toggle="tooltip" data-placement="auto" title="Центрировать по&nbsp;горизонтали">
                            <i class="fal fa-arrows-h" aria-hidden="true"></i>
                        </button>

                        <button class="constructor-controls__button" id="constructor-control-center-horizontal" data-toggle="tooltip" data-placement="auto" title="Центрировать по вертикали"><i class="fal fa-arrows-v" aria-hidden="true"></i></button>
                    </div>

                    <div class="layer-controls" style="display: none;">
                        <div class="text-controls" style="display: none;">
                            <label class="sr-only" for="text-control-text">Текст:</label>
                            <textarea class="text-controls__control text-controls__textarea" id="text-control-text" placeholder="Введите текст здесь" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Текст"></textarea>
                            <div>
                                <input type="hidden" id="text-control-text-font" value="Andika">
                                <input type="checkbox" style="display: none;" id="text-control-text-font-toggle">
                                <div for="text-control-text-font-toggle" class="text-controls__control text-controls__select text-controls__fonts-select fonts-select">
                                    <label class="fonts-select__selected-font" style="font-family: Arial;">Arial<i class="fa fa-caret-down fonts-select__fa" aria-hidden="true"></i>
                                    </label>
                                    <div style="display: none; position: fixed; top: 0px; right: 0px; bottom: 0px; left: 0px;"></div>
                                    <div class="fonts-select__list">
                                        {foreach from=$fonts item="file" key="name"}
                                            <span class="fonts-select__list-item" style="font-family: '{$name}';">{$name}</span>
                                        {/foreach}
                                        <!-- <button class="fonts-select__load-more" style="display: flex;">Больше шрифтов...</button> -->
                                    </div>
                                </div>
                            </div>
                            <div><label class="sr-only" for="text-control-text-color">Цвет шрифта</label><button class="text-controls__control text-controls__color-button bg-colorpicker " data-color="#000" id="text-control-text-color" data-toggle="tooltip" data-placement="auto" title="" value="#000000" style="background-color: rgb(0, 0, 0);" data-original-title="Цвет шрифта"></button></div>
                       </div>
                       <div class="image-controls" style="display: none;">
                          <button class="layer-controls__button" id="constructor-control-duplicate" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Дублировать слой"><i class="fa fa-clone" aria-hidden="true"></i></button>

                          <button class="layer-controls__button" id="constructor-control-equal-printfield" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Растянуть по&nbsp;области"><i class="fa fa-arrows-alt" aria-hidden="true"></i></button>

                          <button class="layer-controls__button" id="constructor-control-reset-transform" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Сбросить изменения слоя"><span class="constructor-controls__icon constructor-controls__icon--reset" aria-hidden="true">&nbsp;</span></button> 
                        </div>
                    </div>

                    <div class="product-constructor-holder"></div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="construct-panel thumb">
                <div class="construct-panel-buttons">
                    <button class="button button-color" onclick="photo.upload_popup(photo.test_callback, 'construct', 1, event)">Добавить фото</button>
                    <button class="button button-color" onclick="construct.addText(event)">Добавить текст</button>
                    <button class="button button-primary">Как пользоваться?</button>
                </div>
                <div class="construct-panel-sides">
                    {*<div class="construct-panel-side selected" data-side="front" onclick="construct.setSide('front')">
                        <img src="/constructor/{$product->type_name}-front.png" style="background-color:#ffffff;">
                    </div>
                    <div class="construct-panel-side" data-side="back" onclick="construct.setSide('back')">
                        <img src="/constructor/{$product->type_name}-back.png" style="background-color:#ffffff;">
                    </div>*}
                </div>
            </div>
        </div>
        <div class="page-layout__sidebar product-page-info fll">
            <h1 class="h2 product-page-title">{$product->name}</h1>
            <div class="product-page-description">
                {$product->description}
            </div>
            <form method="POST" action="/cart/add" data-type="construct" data-cart="add" data-product-id="{$product->id}" class="cart-add-holder">
                <div class="store-item-price">
                    {if $product->discount}<span class="price">{$cart->format_number($product->price)}</span>{/if} 
                    <span class="dis-price">{$cart->format_number($cart->discount($product->price, $product->discount, $product->discount_type))}</span> 
                    <i class="far fa-ruble-sign"></i>
                    <div class="spacer"></div>
                    {if $product->discount}
                        {if $product->discount_type == 0}
                            <span class="button button-color flr">- {$product->discount} Р</span>
                        {else}
                            <span class="button button-color flr">- {$product->discount}%</span>
                        {/if}
                    {/if}
                </div>
                <div class="store-item-price__description" style="display: none;">
                    + 200 Р за вторую сторону
                </div>
                <div class="product-page-options">
                    <input type="hidden" name="id" value="{$product->id}">
                    {foreach from=$attributes item=$attribute}
                      <div class="option-row option-row__{$attribute->name}" data-attr="{$attribute->name}">
                        <b class="row-title">{$attribute->title}</b>
                        <input type="hidden" name="attributes[{$attribute->name}]" value="-1">
                        <div class="option-variants-list">
                            {foreach from=$attribute->values key="k" item="item"}
                              <div class="option-variant">
                                <input name="attributes[{$attribute->name}]" type="radio" value="{$k}" {if $attribute->name == "color"}data-color="{$item->code}"{/if} id="option_{$attribute->name}_{$k}" {if isset($item->default)}checked{/if}>
                                <label for="option_{$attribute->name}_{$k}" {if $attribute->name == "color"}style="background: {$item->code}"{/if}>
                                  <span>{$item->title}</span>
                                  {if !empty($item->description)}<span>{$item->description}</span>{/if}
                                </label>
                              </div>
                            {/foreach}
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    {/foreach}
                    <b class="row-title">Количество</b>
                    <div class="option-row count-row">
                        <div class="count-button minus" data-count="minus">-</div>
                        <input type="text" class="field" name="count" value="1" min="1" max="5" onkeypress="enterNumber(event, this)" onkeydown="enterNumber(event, this)" onkeyup="enterNumber(event, this)">
                        <div class="count-button plus" data-count="plus">+</div>
                    </div>
                    <div class="store-item-info option-row actions-row">
                        <button type="submit" class="button button-color buy-button button-size-ml big full_width" data-text="Добавить в корзину" data-success-text="Перейти в корзину" {*onclick="cart.add('{$product->id}', event, this);"*}>Добавить в корзину</button>
                        <a class="button gray buy-button button-size-ml big full_width to-cart" href="/cart" style="display: none">Перейти в корзину</a>
                    </div>
                    <div class="form_result" style="padding-right: 0px;"></div>
                </div>
            </form>

            {*
            <div class="product-page-options">
                <div class="option-row" style="margin-right: 0px;">
                    <div class="option-variant" style="display: flex;">
                        <input name="product_type" type="radio" value="1" id="option_type_1" checked>
                        <label for="option_type_1" style="flex: 1"><span><i class="fa fa-male" style="color: #aaa;margin-right: 8px;"></i>Мужчинам</span></label>
                        <input name="product_type" type="radio" value="2" id="option_type_2">
                        <label for="option_type_2" style="flex: 1;margin-right: 0px;"><span><i class="fa fa-female" style="color: #aaa;margin-right: 8px;"></i>Женщинам</span></label>
                    </div>
                </div>
            </div>
            *}
            <div class="thumb padding">
                <b>В целях улучшения качества, изображения имеют ограничение по увеличению. Рекомендуемое разрешение изображения - 3508x4961, изображения меньшего разрешения не будут увеличиваются во весь размер.</b>
            </div>
            <div class="bans-thumb thumb padding" style="cursor: pointer;">
                <div class="bans-title" style="display: flex;align-items: center;">
                    <b>Запрещенные темы:</b>
                    <div style="flex: 1"></div>
                    <i class="fal fa-2x fa-angle-down"></i>
                </div>
                <div class="bans-content" style="display: none;">
                    <ul style="padding-left: 20px;margin-top: 10px">
                        <li>Информация, направленная на пропаганду войны, разжигание национальной, расовой или религиозной ненависти и вражды.</li>
                        <li>Информация, пропагандирующая порнографию, культ насилия и жестокости.</li>
                        <li>Информация, содержащая призывы к осуществлению террористической деятельности, содержащая другие экстремистские материалы.</li>
                        <li>Информация, пропагандирующая наркоманию, токсикоманию, способы, методы разработки наркотических средств, психотропных веществ и их прекурсоров, места их приобретения и распространения.</li>
                        <li>Информация о способах совершения самоубийства, а также призывы к совершению самоубийства.</li>
                        <li>Информация, унижающая национальное достоинство.</li>
                        <li>Информация, содержащая клевету в отношении лиц, занимающих государственные должности Российской Федерации, субъектов РФ, призывы к применению насилия в отношении указанных лиц.</li>
                        <li>Информация, содержащая рекламу алкогольной продукции, табачных изделий.</li>
                    </ul>
                    Остальные ограничения указаны в <a href="/eula">пользовательском соглашении</a>
                </div>
            </div>
        </div>
    </div>
</div>

{foreach from=$fonts item="file" key="name"}
    <link rel="stylesheet" href="/ajax/fonts/{$name}.css">
{/foreach}

<script>
    $("[name=construct_type]").change(function(e){
        var type = $(this).val();

        window.location.href = '/constructor/' + type;
    })
    $(".bans-title").click(function(e){
        e.preventDefault();
        $(".bans-content").slideToggle(300);
    })
    $(".text-controls__fonts-select").click(function(e){
        e.preventDefault();
        $(this).toggleClass('opened');
    })
    $(function(){
        construct.init('{$product->type_name}');
        $('.option-variant:first input[name="attributes[color]"]').click();
    })
</script>