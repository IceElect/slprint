<style>
    .contact{
        font-size: 20px;
        display: flex;
    }
    .contact-col{
        display: flex;
        align-items: center;
    }
    .contact .spacer{
        flex: 1;
    }
    @media screen and (max-width: 640px){
        .contact{
            display: block;
            position: relative;
            margin-top: -20px;
        }
        .contact-col{
            display: block;
            margin-top: 10px;
        }
        .contact-col > div{
            margin-top: 8px;
        }
    }
</style>
<div class="page-main container">
    <h1 class="h2">О магазине</h1>
    <div class="thumb padding" style="margin-top: 0px">
        <p>
            Считаешь, что качество и скорость несовместимы с низкой ценой?<br>
            Ищешь, где можно недорого приобрести значки, кружки или футболки со своим принтом? 
            Либо выбрать принт по любимой тематике из более 100 уже существующих? 
            Тогда ты попал по адресу, добро пожаловать в SLPrint — место, где мечты становятся явью! 
        </p>
        <p>
            Более года мы производим и продаём разнообразный мерч с принтами, которые клиенты могут разместить сами с помощью удобного и многофункционального онлайн - конструктора макетов, либо выбрать из обширной базы принтов по всевозможным тематикам, будь то фильмы, игры и многое другое. <br>
            Мы работаем только с проверенными поставщиками — именно поэтому нам доверяют и нас выбирают. 
        </p>

        <p>
            Для наших постоянных клиентов у нас имеется гибкая система скидок и приятные бонусы :3 
            Также мы постоянно проводим разные прикольные конкурсы и розыгрыши — так что скорее подписывайся на группу вконтакте!
        </p>
    </div>
    {*<div class="users-list users-list-tiles">
    	{foreach from=$contacts item=$user}
    	<div class="user user-tile block">
    		<div class="avatar big">
    			{get_avatar u=$user size=320}
    		</div>
    		<div class="info thumb padding">
    			<a href="{$hosted_url}@{$user->id}" class="name">{$user->fname} {$user->lname}</a>
                <span>{$user->role}</span>
    			<div class="buttons-group">
					<a href="{$hosted_url}im/{$user->id}" class="button button-color full_width">Сообщение</a>
					<a href="{$hosted_url}@{$user->id}" class="button button-color"><i class="fa fa-user"></i></a>
					<button class="button gray"><i class="fa fa-chevron-down"></i></button>
    			</div>
    		</div>
    	</div>
    	{/foreach}
    </div>*}
    <div class="thumb">
        <div class="thumb-toggler" style="text-align: center;">
            <div class="title">Контакты</div>
        </div>
        <div class="inner">
            <div class="contact">
                <div class="contact-col">
                    <div style="margin-right: 15px"><i class="fa fa-fw fa-phone-square" style="font-size: 1.1em"></i> +7(495)005-72-48</div>
                    <div style="margin-right: 15px"><i class="fa fa-fw fa-envelope-square" style="font-size: 1.1em"></i> print@slto.ru</div>
                    <div><i class="far fa-fw fa-clock" style="font-size: 1.1em"></i> Пн-вс, 10:00-21:00</div>
                </div>
                <div class="spacer"></div>
                <div class="contact-col">
                    <a href="https://vk.com/sl__print"><i class="fab fa-vk" style="font-size: 1.1em"></i></a>
                    <a href="https://www.instagram.com/sl__print" style="margin-left: 15px"><i class="fab fa-instagram" style="font-size: 1.1em"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div style="height: 50px"></div>
</div>