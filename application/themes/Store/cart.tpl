{if $cart->contents()}
    <div class="page-main container">
        <div class="page-layout page-layout--reverse">
            <div class="alert-block" id="order-error" style="display: none;"></div>
            <div class="page-layout__sidebar" style="padding-left: 20px;padding-right: 0px;">
                <div class="cart-total">
                    Итого <span data-cart="total-count">{decline number=$cart->total_items() text="%n% товар%o%" eArray=['', 'а', 'ов']}</span> на сумму :
                    <div class="price"><span data-cart="total">{$cart_data.total}</span><span class="rub2"> руб.</span></div>
                    <div class="price-text">Стоимость заказа указана без учета доставки</div>
                </div>
                <div class="hidden" id="promo" style="display: block;">
                    <div class="promo-form fl-l">
                        <input type="text" name="promo" placeholder="Промокод" class="field">
                        <button onclick="cart.promo(event, this);" class="button gray">Применить</button>
                    </div>

                    <div class="promo-text"></div>

                    <p class="promo-error hidden"></p>
                    <div class="clearfix"></div>
                </div>
                <form action="/cart/order" method="post" data-act="order" data-type="ajax">
                    <div class="thumb">
                        <div class="thumb-inner">
                            <div class="form-row clearfix">
                                <div class="form-input-wrap input-wrap-text">
                                    <input type="text" name="fname" placeholder="Имя" class="field">
                                </div>
                            </div>
                            <div class="form-row clearfix">
                                <div class="form-input-wrap input-wrap-text">
                                    <input type="text" name="phone" placeholder="Номер телефона" class="field">
                                </div>
                            </div>
                            <div class="form-row clearfix">
                                <div class="form-input-wrap input-wrap-text">
                                    <input type="text" name="email" placeholder="E-mail" class="field">
                                </div>
                            </div>
                            <div class="form-row clearfix">
                                <div class="form-input-wrap input-wrap-text">
                                    <input type="text" name="address" placeholder="Адрес" class="field">
                                </div>
                            </div>
                            <div class="form-row clearfix">
                                <div class="form-input-wrap input-wrap-select">
                                    <select id="pay_type" type="select" name="pay_type" class="field">
                                        <option value="0" disabled selected>Способ оплаты</option>
                                        <option value="1">Наличными</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row clearfix">
                                <div class="form-input-wrap input-wrap-select">
                                    <select id="delivery_type" type="select" name="delivery_type" onchange="cart.calcTotal(event);" class="field">
                                        <option value="0" disabled selected>Способ доставки</option>
                                        <option value="1">Курьером</option>
                                        <option value="2">Самовывоз</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row clearfix">
                                <div class="form-input-wrap input-wrap-select">
                                    <select id="delivery_date" type="select" name="delivery_date" class="field">
                                        <option value="0" disabled selected>Дата доставки</option>
                                        {foreach from=$delivery_dates item="item"}
                                            <option value="{$item.date}">{$item.text}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="form-row clearfix">
                                <div class="form-input-wrap input-wrap-static">
                                    <div style="padding: 7px 10px;"><b>Доставка <b data-cart="delivery_price">300</b>р. <a href="/delivery">Подробнее</a></b></div>
                                </div>
                            </div>
                        </div>
                        <div class="buttons-group">
                            <button type="submit" name="submit" value="1" class="button button-color button-size-ml full_width" data-text="Оформить заказ">Оформить заказ</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="page-layout__main">
                <div class="order-items">
                    {foreach $cart->contents() as $key => $product}
                        <div class="thumb cart-item" data-rowid="{$product.rowid}">
                            <div class="thumb-title">
                                <span href="/product/{$product.url}" class="thumb-title-inner">{$product.name}</span>
                                <div class="spacer" style="flex: 1"></div>
                                <div class="actions">
                                    <a href="#" class="icon with-bg" onclick="cart.delete(event)"><i class="fal fa-fw fa-times"></i></a>
                                </div>
                            </div>
                            <div class="cart-item-inner">
                                <a class="cart-item-image" onclick="popup.show('cart_screenshots/{$key}');">
                                    <img src="/{$product.previews.front}" alt="{$product.name}">
                                </a>
                                <div class="cart-item-info">
                                    <div class="cart-item-info-inner">
                                        <a href="/product/{$product.url}" class="cart-item-info-title">
                                            <!-- {$product.name} -->
                                            <span>{$product.art}</span>
                                        </a>
                                        <div class="cart-item-info-attributes">
                                            {foreach from=$product.all_options key="key" item="attribute"}
                                            {if $attribute->name != "color"}
                                                <div class="cart-item-info-{$attribute->name}">
                                                    <select name="{$attribute->name}" id="{$attribute->name}" class="field">
                                                        {foreach from=$attribute->values key="k" item="a"}
                                                            <option value="{$k}" {if $product.options[$attribute->name] == $k}selected{/if}>{$a->title}</option>
                                                        {/foreach}
                                                    </select>
                                                </div>
                                            {/if}
                                            {/foreach}
                                        </div>
                                    </div>
                                </div>
                                <div class="cart-item-price">
                                    {if $product.discount}<span class="price-was">{$product.was_price}р</span>{/if}
                                    <span class="price">{$product.price}р</span>
                                </div>
                            </div>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>

    {if $cart_data.promo}
        <script>
            $(function(){
                cart.set_promo('{$cart_data.total}', '{$cart_data.promo.text}');
            })
        </script>
    {/if}

{else}
<div class="page-main blank-page container">
    <div style="width: 100%">
        <div class="blank-content">
            <div class="blank-row">
                <div style="display: flex;flex-direction: column;">
                    <div class="blank-header">
                        <h1 class="blank-title">Ваша корзина пуста</h1>
                    </div>
                    <div class="blank-description" style="flex: 1">
                        <p>Зато вы можете пойти на <a href="/">главную страницу</a> или <a href="/construct">создать свою крутую одежду</a>.
                        Или посмотрите <a href="/catalog?sort=new">каталог</a> с нашими последними изделиями.</p>
                    </div>
                    <a href="/catalog?sort=popular" class="button button-size-ml gray" style="line-height: 46px;">Выбрать в магазине</a>
                </div>
                <div class="spacer"></div>
                <div class="thumb" style="display: flex;flex-direction: column;">
                    <div class="thumb-inner" style="flex: 1;height: 100%;">
                        <p><b>Создать товар в нашем конструкторе</b></p>
                        <p>В нашем конструкторе вы можете создать товар на ваш вкус. Но для начала советуем ознакомится с нашим <a href="/eula">пользовательским соглашением</a></p>
                    </div>
                    <div class="buttons-group" style="flex: inherit">
                        <a href="/constructor" class="button button-color button-size-ml full_width" style="line-height: 46px;">Создать товар</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{/if}