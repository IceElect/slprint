<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>

    <!-- <link rel="stylesheet" href="css/bootstrap-grid.css"> -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="/{$path}order/css/bootstrap.min.css">
    <link rel="stylesheet" href="/{$path}order/css/style.css">
</head>
<body style="background: #f5f8fa">

    <div class="">
        <div class="container">
            <h1 class="mt-5 h2">Заказ №{$order.id} <span class="badge badge-secondary">{$statuses[$order.status]}</span></h1>
            
            <div class="card mt-3 mb-5">
                <div class="card-header">Информация о заказе</div>
                <div class="card-body" style="background: #fff">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <strong>Заказчик</strong>
                                </div>
                                <div class="col-md-8">
                                    {$order.fname}
                                </div>
                            </div>
                            <div class="row mt-1">
                                <div class="col-md-4">
                                    <strong>Номер телефона</strong>
                                </div>
                                <div class="col-md-8">
                                    {$order.phone}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <strong>Дата доставки</strong>
                                </div>
                                <div class="col-md-8">
                                    {$order.delivery_date}
                                </div>
                            </div>
                            <div class="row mt-1">
                                <div class="col-md-4">
                                    <strong>Способ доставки</strong>
                                </div>
                                <div class="col-md-8">
                                    {$order.delivery_type}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card">
                <div class="card-header">Содержимое заказа</div>
            </div>
            {foreach from=$products key="k" item="product"}
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#">{$product->name}</a>
                            </div>
                            <div class="col-md-6 text-center">
                                <div class="row">
                                    {foreach from=$product->attributes key="key" item="attribute"}
                                        <div class="col" style="white-space: nowrap;"><b>{$attribute->title}:</b> <span>{$product->options[$attribute->name]}</span></div>
                                    {/foreach}
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-7">
                                <div class="row">
                                    {foreach from=$product->previews item="item"}
                                        <div class="col">
                                            <a href="/{$item}" class="btn btn-block btn-primary mb-2" download>Скачать</a>
                                            <img src="/{$item}" class="img-fluid" alt="">
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="row">
                                    {foreach from=$product->sources item="item"}
                                        <div class="col">
                                            <a href="/{$item}" class="btn btn-block btn-success mb-2" download>Скачать</a>
                                            <img src="/{$item}" class="img-fluid" alt="">
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {/foreach}

            <div class="mt-5"></div>

        </div>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
    <script src="/{$path}order/js/bootstrap.min.js"></script>
</body>
</html>