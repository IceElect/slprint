<style>
	body{
		background: #dcf0fb;
	}
</style>

<div class="page-main container">
	<div class="alert-block">
		<b>Хотите заработать?</b> Мы принимаем дизайны футболок и готовы платить за хорошие принты.<br> Пишите: <a href="https://vk.com/electwc">https://vk.com/electwc</a> или на почту <a href="mailto:a@slto.ru">a@slto.ru</a>
	</div>
	{*<div class="banner">
		<img src="/images/banners/b1.jpg" alt="SLPrint - Весенняя акция">
	</div>*}
	<div class="store-tiles">
		<div class="store-tiles-col">
			<a href="/constructor/" class="store-tile thumb big-tile">
				<div class="tile-image">
					<img src="/images/tile-shirt.png" alt="">
				</div>
				<span class="tile-text">Конструктор футболок</span>
			</a>
		</div>
		<div class="store-tiles-col">
			<a href="/constructor/6" class="store-tile thumb">
				<div class="tile-image">
					<img src="/images/tile-sweatshirt.png" alt="">
				</div>
				<span class="tile-text">Конструктор свитшотов</span>
			</a>
			<a href="/constructor/4" class="store-tile thumb">
				<div class="tile-image">
					<img src="/images/tile-cup.png" alt="">
				</div>
				<span class="tile-text">Конструктор кружек</span>
			</a>
			<a href="/constructor/5" class="store-tile thumb">
				<div class="tile-image">
					<img src="/images/tile-badge.png" alt="">
				</div>
				<span class="tile-text">Конструктор значков</span>
			</a>
			<div class="store-tile thumb tile-buttons" id="tile-buttons">
				<div class="tile-buttons-inner">
					<a href="/delivery" class="button button-color gray">Доставка</a>
					<a href="/about" class="button button-color gray">О магазине</a>
					<a href="/partner/start" class="button button-color">Дизайнерам</a>
					<a href="/eula" class="button button-color">Соглашение</a>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(function(){
			tiles();

			setInterval(tiles, 100);
		})

		function tiles(){
			$("#tile-buttons").css('height', $(".store-tiles-col .store-tile:not(.big-tile):first-child").outerHeight());
		}
	</script>

	<div class="store-seperator">
		<span></span>
		<span></span>
		<span></span>
	</div>

	<section class="">
		<div class="catalog-types buttons-group">
			<a href="/catalog?type=manshort" class="button big full_width"><i class="fa fa-fw fa-male"></i> Мужские футболки</a>
			<a href="/catalog?type=womanshort" class="button big full_width"><i class="fa fa-fw fa-female"></i> Женские футболки</a>
			<a href="/catalog?type=man_sweatshirt" class="button big full_width"><span><i class="fa fa-fw fa-star"></i> Свитшоты <i class="marker-hit">ХИТ</i></span></a>
		</div>
	</section>

	<div class="store-seperator"></div>

	<section class="catalog">
		<div class="page-title">
			<h2 class="page-title-inner">Хиты продаж</h2>
			<div class="spacer"></div>
			<a href="/catalog?sort=new" class="button button-color">Показать все</a>
		</div>
		<div class="store-products">
			{include file="shop/products_list.tpl" products=$hits}
		</div>
	</section>

	{*<div class="store-seperator">
		<span></span>
		<span></span>
		<span></span>
	</div>

	<section class="catalog">
		<div class="page-title">
			<h2 class="page-title-inner">Новинки</h2>
			<div class="spacer"></div>
			<a href="/catalog?sort=new" class="button button-color">Показать все</a>
		</div>
		<div class="store-products">
			{include file="shop/products_list.tpl" products=$products}
		</div>
	</section>*}

	<div class="buttons-group">
		<a href="/catalog" class="button button-color button-size-ml full_width">Показать весь каталог</a>
	</div>

	<div class="store-seperator"></div>

	<section class="catalog">
		<div class="page-title">
			<h2 class="page-title-inner">Популярные категории</h2>
		</div>
		<div class="store-categories">
			{foreach from=$categories item="category"}
			<a href="/catalog?category_id={$category->id}" class="store-categories-item">
				<div class="store-categories-item__cover" style="background-image: url('{$category->cover}')"></div>
				<span class="store-categories-item__title"><span>{$category->title}</span></span>
			</a>
			{/foreach}
		</div>
	</section>

	<div class="store-seperator">
		<span></span>
		<span></span>
		<span></span>
	</div>

	<div class="store-advant-list">
		<div class="store-advant-item">
			<div class="store-advant-item__icon">
				<i class="fal fa-award"></i>
			</div>
			<div class="store-advant-item__title">Лучшее качество</div>
			<div class="store-advant-item__desc">Мы используем только лучшую ткань, цена оправдана дважды</div>
		</div>
		<div class="store-advant-item">
			<div class="store-advant-item__icon">
				<i class="fal fa-truck"></i>
			</div>
			<div class="store-advant-item__title">Быстрая доставка</div>
			<div class="store-advant-item__desc">Наши курьеры доставят вам заказ на следующий день</div>
		</div>
		<div class="store-advant-item">
			<div class="store-advant-item__icon">
				<i class="fal fa-thumbs-up"></i>
			</div>
			<div class="store-advant-item__title">Положительные отзывы</div>
			<div class="store-advant-item__desc">Наши клиенты всегда остаются довольными</div>
		</div>
		<div class="store-advant-item">
			<a href="/constructor" class="store-advant-item__icon">
				<i class="fal fa-palette"></i>
			</a>
			<a href="/constructor" class="store-advant-item__title">Удобный конструктор</a>
			<div class="store-advant-item__desc">Если Вы не нашли принт у нас в каталоге, Вы можете <a href="/constructor">сделать его сами</a></div>
		</div>
	</div>

	<div class="store-seperator"></div>

</div>