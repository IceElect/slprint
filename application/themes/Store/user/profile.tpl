<div class="page-main container">
	<div class="page-layout page-layout--reverse">
		<div class="page-layout__main">
			<section class="content tab-content current">
				{$block}
			</section>
		</div>
		<div class="page-layout__sidebar">
			<div class="thumb tabs">
				<a href="/profile" class="tab {if $tab == 'account'}current{/if}">Аккаунт</a>
				{*<a href="#main" class="tab">Мои заказы</a>*}
				{*<a href="#main" class="tab">Мои купоны</a>*}
				<a href="/users/logout" class="tab">Выход</a>
			</div>
			<div class="thumb tabs">
				{if $oUser->print_designer}
					<a href="/catalog/{$oUser->login}" class="tab">Мой магазин</a>
					<a href="/partner/design/create" class="tab {if $tab == 'agreement'}product_create{/if}">Создать товар</a>
					<a href="/partner/design" class="tab {if $tab == 'agreement'}product{/if}">Мои товары</a>
					<a href="/partner/faq" class="tab {if $tab == 'agreement'}product{/if}">Вопрос-ответ</a>
				{else}
					<a href="/partner/agreement" class="tab {if $tab == 'agreement'}current{/if}">Создать свой товар</a>
				{/if}
			</div>
			{*
			<div class="thumb tabs">
				<a href="" class="tab">Вопросы</a>
				<a href="" class="tab">Инструкции</a>
				<a href="" class="tab">Служба поддержки</a>
			</div>
			*}
		</div>
	</div>
</div>