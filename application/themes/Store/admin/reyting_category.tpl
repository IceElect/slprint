{strip}
    <div id="dialog-confirm" title="{translate code="dialog_delete_title" text="Удаление"}">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>

        <div style="padding-left:20px;">
            {translate code="dialog_delete_reyting_cat_confirm" text="Удалить категорию со всеми под категориями?"}
        </div>
    </div>
    <div>
        <a href="/admin_reyting_cat/edit"
           class="add_category_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
    <span class="ui-button-text">
    {translate code="form_add_category" text="Добавить категорию"}
    </span>
        </a>
    </div>
    <div>
        {nocache}
            <ul class="sortable ui-sortable">
                {foreach $cat_list as $key => $cat}
                    <li data-id="ele-{$cat->id}" class="ui-state-default ui-sortable-handle">
                        <div>
                            <span class="ui-icon"></span>
                            {$cat->name}
                            <a href="javascript:void(0)" onclick="deleteLink(this)"
                               class="fright ui-state-hover buttons-edit">{translate code="form_delete" text="Удалить"}</a>
                            <a href="{$aConf.base_url}{$aConf.active_module}/edit/{$cat->id}"
                               class="fright ui-state-hover buttons-edit">{translate code="form_edit" text="Изменить"}</a>
                        </div>
                        {if (!empty($cat->children))}
                            {include file="admin/reyting_category_tree.tpl" list=$cat->children}
                        {/if}
                    </li>
                {/foreach}
            </ul>
        {/nocache}
    </div>
    <div class="row box-btn form_actions">
        <button type="submit" name="submit" value="1" class="btn btn_save">
            <i class="fa fa-floppy-o"></i>{translate code="form_save" text="Сохранить"}
        </button>
        <a class="btn btn_cancel" href="{$aConf.base_url}{$aConf.active_module}">
            <i class="fa fa-times"></i>{translate code="form_cancel" text="Отмена"}
        </a>
    </div>
    <script type="text/javascript">
        {literal}
        $(function () {
            $('.sortable').nestedSortable({
                maxLevels: 10,
                handle: 'div',
                items: 'li',
                isTree: true,
                startCollapsed: true,
                toleranceElement: '> div',
                listType: 'ul',
                cursor: 'move',
                attribute: 'data-id',
                opacity: .6,
                excludeRoot: true
            });
            $('.sortable .ui-icon').on('click', function () {
                $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
            })
            $('.btn_save').click(function () {
                hide_messages();
                var cat_order = $('ul.sortable').nestedSortable('toArray');
                $.ajax({
                    type: 'POST',
                    url: '{/literal}{$aConf.base_url}{$aConf.active_module}{literal}/save_cat_order/',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            show_message('{/literal}{translate code="form_msg_cat_order_save" text="Категории сохранены"}{literal}', 'success');
                        }
                    },
                    data: {'ajax': 1, 'cat_order': cat_order},
                    async: false
                });
            });


        });


        function deleteLink(elem) {
            $("#dialog-confirm").dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "{/literal}{translate code="form_delete" text="Удалить"}{literal}": function () {
                        $(elem).parent().parent().remove();
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

        {/literal}
    </script>
{/strip}