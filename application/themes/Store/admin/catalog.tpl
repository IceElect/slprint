<div class="content-wrap">
    <div class="container">
        <div class="admin-category-list">
        {foreach from=$categories key=i item=category}
            <div class="thumb admin-category-tile" data-id="{$category->id}">
                <a href="{$aConf.base_url}admin/catalog/{$category->name}" class="thumb-cover">
                    {assign var="cover" value="{$project_path}{$category->cover}"}
                    {if file_exists($cover) && !empty($category->cover)}
                        <img src="{$aConf.base_url}{$category->cover}" alt="Нет фото" width="100%">
                    {else}
                        <span class="no-cover"><i class="fa fa-camera"></i></span>
                    {/if}
                </a>
                <div class="thumb-title">
                    <a href="{$aConf.base_url}admin/catalog/{$category->name}" class="thumb-title-inner spacer">{$category->title}</a>
                    
                    <div class="actions">
                        <a href="{$aConf.base_url}admin/catalog{$editAction}/{$category->id}" class="ft-edit"></a>
                        <a href="{$aConf.base_url}admin/catalog{$delAction}/{$category->id}" data-id="{$category->id}" class="del-cat ft-x"></a>
                    </div>
                </div>
            </div>
        {/foreach}
        </div>

        <div class="col-md-12">
            {include file="admin/default_grid.tpl" aFields=$aFields aData=$aData pagination=$pagination get_table_only=true}
        </div>
    </div>
</div>

<script>
    {literal}
    $(document).on('click', '.del-cat', function(e){
        e.preventDefault();

        var id = $(this).data('id');
        var url = $(this).attr('href');
        $.post(url, {oper: 'del', id: [id]}, function(data){
            data = JSON.parse(data);
            if(data.response){
                $(".admin-category-tile[data-id="+id+"]").remove();
            }
        })
    })
    {/literal}
</script>