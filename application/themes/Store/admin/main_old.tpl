<!DOCTYPE html>
<html lang="en">
<head>
    <title>{$pageTitle}</title>

    <meta charset="UTF-8">
    {include file="sys/meta.tpl"}
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>
<body>
    {include file="sys/js_css_inc.tpl" place="header"}
    {include file="sys/js_inc.tpl" place="init"}

    <div class="popup_layer"></div>

    {$content}

    <!-- <div class="page-main container">
        <div class="page-main__row">
            <div class="page-main__header">
                <div class="page-main__title">
                    <h1 class="page-main__title-text title-h1_big">Список каналов</h1>
                </div>
            </div>
        </div>
    </div> -->

    {include file="sys/js_inc.tpl" place="footer"}
</body>
</html>