{strip}
{nocache}
    <div id="dialog-confirm" title="{translate code="dialog_delete_title" text="Удаление"}">
        <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
        <div style="padding-left:20px;">
            {translate code="dialog_delete_order_product_confirm" text="Удалить товар из заказа ?"}
        </div>
    </div>
    <div class="form_wrapper">
        <form action="" method="post" id="frm" enctype="multipart/form-data">
            <div class="boxedit">
                <div class="form-horizontal table">
                    {include file="sys/messages.tpl"}
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3"><span class="required">*</span> {translate code='required_fields' text='Обязательные поля'}</div>
                    </div>
                    {if $action ne 'add'}
                        <div class="form-group field_wrap_id">
                            <label class="control-label col-md-2 col-xs-2">
                                ID:
                            </label>
                            <div class="control-label col-xs-10 value">
                                {$nId}
                            </div>
                        </div>
                    {/if}
                    <div class="form-group">
                        <label for="status" class="control-label col-md-3"><span class="required">*</span> {translate code='status' text='Статус'}:</label>
                        <div class="col-md-9 field_wrap_select">
                            <select name="status" id="status" >
                                {if (!empty($aFields.status.options))}
                                    {foreach from=$aFields.status.options key="option_key" item="option" }
                                        <option value="{$option_key}"  {if (!empty($order.status) && $order.status == $option_key) } selected {/if} >{$option}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fname" class="control-label col-md-3"><span class="required">*</span> {translate code='fname' text='Имя'}:</label>
                        <div class="col-md-9 field_wrap_text">
                            <input type="text" name="fname" id="fname" class="form-control text " value="{if (!empty($order.fname))}{$order.fname}{/if}" maxlength="100" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lname" class="control-label col-md-3"><span class="required">*</span> {translate code='lname' text='Фамилия'}:</label>
                        <div class="col-md-9 field_wrap_text">
                            <input type="text" name="lname" id="lname" class="form-control text " value="{if (!empty($order.lname))}{$order.lname}{/if}" maxlength="100" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="def_3" class="control-label col-md-3"><span class="required">*</span> Email:</label>
                        <div class="col-md-9 field_wrap_text">
                            <input type="email" name="email" id="def_3" class="form-control text " value="{if (!empty($order.email))}{$order.email}{/if}" maxlength="100" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="control-label col-md-3"><span class="required">*</span> {translate code='city' text='Город'}:</label>
                        <div class="col-md-9 field_wrap_text">
                            <input type="text" name="city" id="city" class="form-control" value="{if (!empty($order.city))}{$order.city}{/if}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="def_4" class="control-label col-md-3"><span class="required">*</span> {translate code='phone_number' text='Номер телефона'}:</label>
                        <div class="field_wrap_text col-md-9">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">+38</span>
                                <input type="phone" maxlength="" name="phone" id="def_4" class="form-control text " value="{if (!empty($order.phone))}{$order.phone}{/if}" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pay_type" class="control-label col-md-3"><span class="required">*</span> {translate code='pay_type' text='Способ оплаты'}:</label>
                        <div class="col-md-9 field_wrap_select">
                            <select class="select form-control" id="pay_type" name="pay_type">
                                <option {if (!empty($order.pay_type) && $order.pay_type == 1)}selected{/if} value="1">Наложенный платёж</option>
                                <option {if (!empty($order.pay_type) && $order.pay_type == 2)}selected{/if} value="2">Наличный расчёт</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="delivery_type" class="control-label col-md-3"><span class="required">*</span> {translate code='delivery_type' text='Способ доставки'}:</label>
                        <div class="col-md-9 field_wrap_select">
                            <select class="select_d_type form-control" id="delivery_type" name="delivery_type">
                                <option {if (!empty($order.delivery_type) && $order.delivery_type == 1)}selected{/if} value="1">Самовывоз</option>
                                <option {if (!empty($order.delivery_type) && $order.delivery_type == 2)}selected{/if} value="2">Новая Почта</option>
                                <option {if (!empty($order.delivery_type) && $order.delivery_type == 3)}selected{/if} value="3">Доставка курьером</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group hidden dtype_2">
                        <label for="post_office" class="control-label col-md-3"><span class="required">*</span> {translate code='post_office' text='Отделение'}:</label>
                        <div class="col-md-9 field_wrap_select">
                            <input type="text" name="post_office" class="form-control" id="post_office" value="{if (!empty($order.post_office))}{$order.post_office}{/if}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="def_5" class="control-label col-md-3">{translate code='comment' text='Комментарий'}:</label>
                        <div class="col-md-9 field_wrap_textarea">
                            <textarea name="comment" class="textarea form-control text" rows="4">{if (!empty($order.comment))}{$order.comment}{/if}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12 menu_add_product_block">
                                <div class="form-group">
                                    <label for="add_product">{translate code="add_product" text="Добавить Продукт"}</label>
                                    <div class="field_wrap_select">
                                        <select id="add_product" class="new_product margin-right-6">
                                                {foreach $product_list as $product}
                                                    <option value="{$product->id}">{$product->name}</option>
                                                {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <a class="add_product_button ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" href="#">
                                    <span class="ui-button-text">
                                        {translate code="form_add" text="Добавить"}
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        <table width="100%" class="order_products">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>{translate code='cart_name' text='Название' }</th>
                                    <th>{translate code='cart_count' text='Кол-во' }</th>
                                    <th>{translate code='cart_price' text='Цена' }</th>
                                    <th>{translate code='cart_summ' text='Сумма' }</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            {if !empty($order.products)}
                                {foreach $order.products as $key => $product}
                                    <tr class="product_row">
                                        <td>
                                            <img src="/userfiles/products/{$product.image}" height="80">
                                            <input name="product[{$product.id}][id]" class="order_product_id" type="hidden" value="{$product.id}">
                                            <input name="product[{$product.id}][product_id]" class="product_id" type="hidden" value="{$product.product_id}">
                                        </td>
                                        <td class="product_name">{$product.name}</td>
                                        <td><input name="product[{$product.id}][number]" class="product_number form-control" onkeyup="recalc_order();" onchange="recalc_order();" type="number" min="1" value="{$product.number}" autocomplete="off"></td>
                                        <td><input name="product[{$product.id}][price]" class="product_price form-control" onkeyup="recalc_order();" onchange="recalc_order();" type="number" min="1" value="{$product.price}" autocomplete="off"><span class="currency">Р</span></td>
                                        <td><span class="product_subtotal">{$cart->format_number($product.number * $product.price)}</span><span class="currency">Р</span></td>
                                        <td><a href="#" onclick="order_product_delete_item(event);"><i class="fa fa-close"></i></a></td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" class="cart_total_label">{translate code='cart_total_label' text='Итого' }</td>
                                    <td>
                                    <span class="cart_total">{$cart->format_number($total)}</span><span class="currency">Р</span>
                                    </td>
                                </tr>
                                {*
                                <tr>
                                    <td colspan="4" class="cart_total_label">{translate code='cart_res_delivery' text='Доставка' }</td>
                                    <td>
                                        <span class="delivery_price">{$cart->format_number($order.delivery_price)}</span>
                                        <span class="currency">Р</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="cart_total_label">{translate code='cart_total_label' text='Итого' }</td>
                                    <td>
                                        <span class="order_total">{$cart->format_number($total + $order.delivery_price)}</span>
                                        <span class="currency">Р</span>
                                    </td>
                                </tr>
                                *}
                            </tfoot>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group box-btn form_actions">
                <div class="col-md-10 col-md-offset-2">
                    <button type="submit" name="submit" value="1" class="btn btn-success ui-widget ui-state-default">
                        <i class="fa fa-floppy-o"></i> {translate code="form_save" text="Сохранить"}
                    </button>
                    <a class="btn btn-danger btn_cancel" href="{$aConf.base_url}{$aConf.active_module}">
                        <i class="fa fa-times"></i>{translate code="form_cancel" text="Отмена"}
                    </a>
                </div>
            </div>
        </form>
        <table id="example" class="hidden">
            <tr class="product_row new_row">
                <td>
                    <img src="" height="80">
                    <input name="product[replace_me][id]" class="order_product_id" type="hidden" value="replace_me">
                    <input name="product[replace_me][product_id]" class="product_id" type="hidden" value="">
                </td>
                <td class="product_name"></td>
                <td><input name="product[replace_me][number]" class="product_number form-control" onkeyup="recalc_order();" onchange="recalc_order();" type="number" min="1" value="1" autocomplete="off"></td>
                <td><input name="product[replace_me][price]" class="product_price form-control" onkeyup="recalc_order();" onchange="recalc_order();" type="number" min="1" value="" autocomplete="off"><span class="currency">Р</span></td>
                <td><span class="product_subtotal"></span><span class="currency">Р</span></td>
                <td><a href="#" onclick="order_product_delete_item(event);"><i class="fa fa-close"></i></a></td>
            </tr>
        </table>
    </div>
    
{/nocache}
    <script type="text/javascript">
        {literal}
        $(function () {
            $('.add_product_button').click(addProduct);
        });
        
        var new_product = 1;
        function addProduct(e) {
            e.preventDefault();
            var product_id = jQuery('#add_product').val();
            $.ajax({
                type: 'POST',
                url: aConf['base_url'] + '{/literal}{$aConf.active_module}{literal}/get_product/' + product_id,
                dataType: 'json',
                data: {'ajax': 1},
                success: function (data) {
                    var html = jQuery('#example').html();
                    html = html.replace(/replace_me/g, 'new_' + new_product);
                    jQuery('.order_products').append(html);
                    new_product = new_product + 1;
                    var new_row = jQuery('.order_products .new_row');
                    new_row.removeClass('new_row');
                    new_row.find('.product_price').val(data.price);
                    new_row.find('.product_id').val(data.product_id);
                    new_row.find('img').attr('src','/userfiles/products/' + data.image);
                    new_row.find('.product_name').html(data.name);
                    recalc_order();
                },
                async: false
            });
        }
        
        function order_product_delete_item(event){
            event = event || window.event;
            event.preventDefault();
            var el = event.target;
            jQuery(el).parents('tr').remove();
            recalc_order();
        }
        
        function recalc_order(){
            var total = 0;
            var delivery = parseFloat(aConf['default_delivery']);
            jQuery('.order_products .product_row').each(function(it,el){
                var subtotal = parseFloat(jQuery(el).find('.product_number').val())*parseFloat(jQuery(el).find('.product_price').val());
                jQuery(el).find('.product_subtotal').html(subtotal.toFixed(2));
                total = total + subtotal;
            });
            jQuery('.cart_total').html(total.toFixed(2));
            if (total > -1000){
                delivery = 0;
            }
            total = total + delivery;
            jQuery('.delivery_price').html(delivery.toFixed(2));
            jQuery('.order_total').html(total.toFixed(2));
        }
        {/literal}
    </script>
{/strip}