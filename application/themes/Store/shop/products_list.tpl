{foreach from=$products item="item"}
	<a href="/product/{$item->url}" class="store-item">
		<div class="store-item-thumb ">
			<div class="product-thumb-loading">
				<i class="fal fa-spinner-third fa-spin"></i>
			</div>
			<img src="{product_img design_id=$item->design_id type=$item->type_name color=$item->default_color side="front"}" alt="{$item->name}" title="{$item->name} купить" data-side="1">
			{if isset($item->previews.back)}
				<img src="{product_img design_id=$item->design_id type=$item->type_name color=$item->default_color side="back"}" alt="{$item->name}" title="{$item->name} купить" data-side="2" style="opacity: 0">
			{/if}
			{if $item->discount}
				{if $item->discount_type == 0}
					<span class="store-item-discount button button-color">- {$item->discount} Р</span>
				{else}
					<span class="store-item-discount button button-color">- {$item->discount}%</span>
				{/if}
			{/if}
		</div>
		<div class="store-item-title">{$item->name}</div>
		<span class="store-item-cat">{translate code="type_`$item->type_name`" text=$item->type_name}</span>
		<div class="store-item-info">
			<!-- <a href="/product/{$item->url}" class="buy-button button"><i class="fa fa-search" style="margin-right: 0px;"></i></a> -->
			<div class="store-item-price">
				{if $item->discount}
					<span class="price">{$cart->format_number($item->price)}</span>
				{/if}
				<span class="dis-price">{$cart->format_number($cart->discount($item->price, $item->discount, $item->discount_type))}</span> 
				<i class="far fa-ruble-sign"></i>
			</div>
		</div>
	</a>
{/foreach}