<div class="page-main container" data-product-id="{$product->id}">
	<div class="product-page" 
		data-design-id="{$product->design_id}" 
		data-product-id="{$product->id}" 
		data-product-type="{$product->type_name}">
		<div class="product-page-main page-layout">
			<div class="page-layout__main product-page-image-container fl-r">
				<div class="product-page-image thumb">
					<div class="product-thumb-loading">
						<i class="fal fa-spinner-third fa-spin"></i>
					</div>
					{if isset($previews.back)}<button class="product-image-flip icon with-bg md-icon" onclick="flipSide(event)"><i class="fal fa-sync"></i></button>{/if}
					<img src="{product_img design_id=$product->design_id type=$product->type_name color=$product->default_color side="front" size=1024}" data-side="front" alt="">
					{if isset($previews.back)}<img src="{product_img design_id=$product->design_id type=$product->type_name color=$product->default_color side="back" size=1024}" data-side="back" alt="" style="display: none">{/if}
				</div>
				{*
				<div class="product-page-additional-images">
					<div class="additional-thumb thumb">
						<img src="/images/shirt-1.png" alt="">
					</div>
					<div class="additional-thumb thumb">
						<img src="/images/shirt-1.png" alt="">
					</div>
					<div class="additional-thumb thumb">
						<img src="/images/shirt-1.png" alt="">
					</div>
				</div>
				*}
			</div>
			<div class="page-layout__sidebar product-page-info fl-l">
				<div class="product-page-info-inner">
					<h1 class="h2 product-page-title">{translate code="type_`$product->type_name`" text=$product->type_name} {$product->name}</h1>
					<div class="product-page-params">
						<a class="param-category" href="/catalog?category_id={$product->cat_id}">{$product->cat_title}</a>
						<div class="spacer"></div>
					</div>
					<p class="product-page-description">
						{$product->description}
					</p>
					<form method="POST" action="/cart/add" data-cart="add" data-product-id="{$product->id}" class=" cart-add-holder">
						<div class="store-item-price">
							{if $product->discount}<span class="price">{$cart->format_number($product->price)}</span>{/if} 
							<span class="dis-price">{$cart->format_number($cart->discount($product->price, $product->discount, $product->discount_type))}</span> 
							<i class="far fa-ruble-sign"></i>
							<div class="spacer"></div>
							{if $product->discount}
								{if $product->discount_type == 0}
									<span class="button button-color flr">- {$product->discount} Р</span>
								{else}
									<span class="button button-color flr">- {$product->discount}%</span>
								{/if}
							{/if}
						</div>
						<div class="product-page-options">
							<input type="hidden" name="id" value="{$product->id}">
							{foreach from=$attributes item=$attribute}
								<div class="option-row option-row__{$attribute->name}" data-attr="{$attribute->name}">
									<b class="row-title">{$attribute->title}</b>
									<input type="hidden" name="attributes[{$attribute->name}]" value="-1">
									<div class="option-variants-list">
										{foreach from=$attribute->values key="k" item="item"}
					                      <div class="option-variant">
					                        <input name="attributes[{$attribute->name}]" type="radio" value="{$k}" {if $attribute->name == "color"}data-color="{$item->code}"{/if} id="option_{$attribute->name}_{$k}" {if isset($item->default)}checked{/if}>
					                        <label for="option_{$attribute->name}_{$k}" {if $attribute->name == "color"}style="background: {$item->code}"{/if}>
					                          <span>{$item->title}</span>
					                          {if !empty($item->description)}<span>{$item->description}</span>{/if}
					                        </label>
					                      </div>
					                    {/foreach}
									</div>
									<div class="clearfix"></div>
									{if $attribute->about_popup}
										<span onclick="popup.show('{$attribute->about_popup}');" class="option-about">{$attribute->about_text}</span>
									{/if}
								</div>
							{/foreach}
							<b class="row-title">Количество</b>
							<div class="option-row count-row">
								<div class="count-button minus" data-count="minus">-</div>
								<input type="text" class="field" name="count" value="1" min="1" max="5" onkeypress="enterNumber(event, this)" onkeydown="enterNumber(event, this)" onkeyup="enterNumber(event, this)">
								<div class="count-button plus" data-count="plus">+</div>
							</div>
							<div class="store-item-info option-row actions-row">
								<button type="submit" class="button button-color buy-button button-size-ml big full_width" data-text="Добавить в корзину" data-success-text="Перейти в корзину" {*onclick="cart.add('{$product->id}', event, this);"*}>Добавить в корзину</button>
								<a class="button gray buy-button button-size-ml big full_width to-cart" href="/cart" style="display: none">Перейти в корзину</a>
								<a type="button" class="fav-button {if !$oUser->id}login-button{/if}" onclick="shop.productFav('{$product->id}', event, this);">
									<i class="{if $product->is_liked}fas fa-heart{else}far fa-heart{/if}"></i>
								</a>
							</div>
							<div class="form_result"></div>
						</div>
						<div class="tags-holder">
							<ul class="tags">
								{foreach from=$tags item="tag" key="tag_id"}
									<li><a href="">{$tag|lower|capitalize}</a></li>
								{/foreach}
							</ul>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="thumb user-tabs">
		<ul>
			<li><a href="javascript:void(0)" class="user-tab user-tab-sel" data-type="relation"><font style="vertical-align: inherit;">Похожие товары</font></a></li>
			<li><a href="javascript:void(0)" class="user-tab" data-type="reviews"><font style="vertical-align: inherit;">Отзывы </font><span><font style="vertical-align: inherit;">{$reviews|@count}</font></span></a></li>
		</ul>
	</div>
	<div class="clearfix"></div>
	<div class="product-page-tabs tabs-content">
		<section class="thumb padding" style="margin-top: 0px; display: block;" data-tab="relation">
			<div class="store-products" style="margin-bottom: 0px">
				{include file="shop/products_list.tpl" products=$simular}
			</div>
		</section>
		<section class="thumb padding" style="margin-top: 0px;" data-tab="reviews">
			<div class="send-comment {if !$oUser->id}login-button{/if}">
	            <div class="send-post-form send-comment-form" data-product-id="{$product->id}">
	                <div class="avatar middle">
	                    {get_avatar u=$oUser}
	                </div>
	                <div class="send-form-area" contenteditable="true" placeholder="Написать отзыв"></div>
	                <div class="buttons">
	                    <button class="fl-r send-button icon fal fa-paper-plane" onclick="shop.addComment('{$product->id}');"></button>
	                </div>
	            </div>
	            <div class="send-comment-result">Ваш отзыв отправлен на модерацию</div>
	        </div>
	        <div class="clearfix" style="clear: both"></div>
	        <div class="comments-list">
	        	{foreach from=$reviews item="comment"}
	            <div class="comment">
				    <div class="avatar middle">
				        {get_avatar u_id=$comment->author_id u_av=$comment->avatar}
				    </div>
				    <div class="content">
				        <div class="info">
				            <a href="#" class="name"> {$comment->fname} {$comment->lname}</a>
				            <span><abbr title="{$comment->date|date_format:"%Y-%m-%d %H:%I:%S"}" class="time">сегодня</abbr></span>
				            <span class="spacer"></span>
				            <div class="actions">
				                <button class="icon fa icon-dot-3"></button>
				            </div>
				        </div>
				        <div class="text">{$comment->content}</div>
				    </div>
				</div>
				{/foreach}
	        </div>
		</section>
	</div>
</div>
<script>
	var side = 'front';
	function flipSide(e){
		e.preventDefault();

		var now_side = (side == 'front')?'back':'front';
		$("img[data-side='"+side+"']").hide();
		$("img[data-side='"+now_side+"']").show();
		side = now_side;

		return false;
	}

	$(".user-tabs ul li a").click(function(e){
		e.preventDefault();

		var tab_id = $(this).data('type');

		$(".user-tabs ul li a").removeClass('user-tab-sel');
		$(this).addClass('user-tab-sel');

		$(".product-page-tabs section").hide();
		$(".product-page-tabs section[data-tab="+tab_id+"]").show();
	})
</script>