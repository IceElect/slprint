<div class="container">
        <section class="catalog">
            <div class="catalog-layout">
                <div class="catalog-layout__sidebar fll">
                    {if $catalogUser}
                    <div class="catalog-categories-list">
                        <div class="buttons clearfix">
                            <a href="http://slto.ru/im/{$catalogUser->id}" class="button button-size-ml button-color big full_width" style="font-weight: bold;">Написать</a>
                        </div>
                    </div>
                    <div class="catalog-designer">
                        <div class="avatar-circle"><img src="{get_avatar u=$catalogUser link=true}" alt="{$catalogUser->fname} {$catalogUser->lname}"></div>
                        <div class="catalog-designer__name">{$catalogUser->fname} {$catalogUser->lname}</div>
                        <div class="catalog-designer__description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia nemo sed voluptatibus dolore aliquid maiores.</div>
                    </div>
                    {/if}
                    <div class="catalog-categories-list">
                        {if !$catalogUser}
                        <div class="buttons clearfix">
                            <a href="/constructor" class="button button-size-ml button-color big full_width" style="font-weight: bold;">Сделать свой дизайн</a>
                        </div>
                        {/if}
                        <ul>
                            <li>
                                <label><a href="{generate_query param=category_id value=false}"><span class="list-item__name">Все товары</span></a></label>
                            </li>
                            {foreach from=$categories item="item"}
                            {if $item->product_count > 0}
                            <li {if $category->id == $item->id || $category->parent_id == $item->id}class="list-item--active"{/if} data-cat-id="{$item->id}">
                                <label{if $item->children_count > 0} onclick="shop.showChildCats({$item->id}, event);"{/if}>
                                    {if $item->children_count > 0}
                                    <svg class="list-item__arrow" viewBox="0 0 48 48"><use xlink:href="#icon-arrow"><g id="icon-arrow"><path d="M12,3l3-3L36,24,15,48l-3-3L29.92,24Z"></path></g></use></svg>
                                    {/if}
                                    <a href="{generate_query param=category_id value=$item->id}"><span class="list-item__name">{$item->title}</span><span class="list-item__counter">{$item->product_count}</span></a>
                                </label>
                                {if $item->children_count > 0}
                                <ul class="list-item__child">
                                    <li class="loading"><i class="fa fa-spin fa-refresh"></i></li>
                                </ul>
                                {/if}
                            </li>
                            {/if}
                            {/foreach}
                        </ul>
                    </div>
                </div>
                <div class="catalog-layout__main store-products-holder">
                    <div class="store-products-header">
                        
                        <div class="filter-list">
                            <div class="filter-item filter-item--sort">
                                <input id="filter_input--type" name="filter_input" class="filter-input" type="checkbox" value="filter_input--type">
                                <label class="filter-link" for="filter_input--type">
                                    <svg class="filter-arrow" viewBox="0 0 48 48">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow"><g id="icon-arrow"><path d="M12,3l3-3L36,24,15,48l-3-3L29.92,24Z"></path></g></use>
                                    </svg>
                                    <svg class="filter-icon filter-icon-hit" viewBox="0 0 48 48">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-hit"></use>
                                    </svg>
                                    {$types[$filter_type]}
                                </label>
                                <div class="filter-menu filter-menu--sort">
                                    <div class="filter-menu-content">
                                        <svg class="filter-menu-corner" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 23.1 13.8" xml:space="preserve"><path style="fill:#FFFFFF;" d="M0,12.8c1,0,1.9-0.5,2.4-1.3l6.7-9.3c1.2-1.7,3.7-1.7,4.9,0l6.7,9.3c0.6,0.8,1.5,1.3,2.4,1.3"></path></svg>
                                        <ul class="filter-menu-list">
                                            {foreach from=$types item="name" key="type"}
                                                <li class="filter-menu-item">
                                                    <a class="filter-menu-link {if $type == $sort}filter-menu-link--selected{/if}" href="{generate_query param=type value=$type}" title="{$name}">
                                                        <svg class="filter-menu-icon" viewBox="0 0 48 48">
                                                            <use xlink:href="#icon-new"></use>
                                                        </svg>
                                                        {$name}
                                                    </a>
                                                </li>
                                            {/foreach}
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-item filter-item--sort">
                                <input id="filter_input--sort" name="filter_input" class="filter-input" type="checkbox" value="filter_input--sort">
                                <label class="filter-link" for="filter_input--sort">
                                    <svg class="filter-arrow" viewBox="0 0 48 48">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow"><g id="icon-arrow"><path d="M12,3l3-3L36,24,15,48l-3-3L29.92,24Z"></path></g></use>
                                    </svg>
                                    <svg class="filter-icon filter-icon-hit" viewBox="0 0 48 48">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-hit"></use>
                                    </svg>
                                    {$sort_types[$sort]}
                                </label>
                                <div class="filter-menu filter-menu--sort">
                                    <div class="filter-menu-content">
                                        <svg class="filter-menu-corner" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 23.1 13.8" xml:space="preserve"><path style="fill:#FFFFFF;" d="M0,12.8c1,0,1.9-0.5,2.4-1.3l6.7-9.3c1.2-1.7,3.7-1.7,4.9,0l6.7,9.3c0.6,0.8,1.5,1.3,2.4,1.3"></path></svg>
                                        <ul class="filter-menu-list">
                                            {foreach from=$sort_types item="name" key="type"}
                                                <li class="filter-menu-item">
                                                    <a class="filter-menu-link {if $type == $sort}filter-menu-link--selected{/if}" href="{generate_query param=sort value=$type}" title="{$name}">
                                                        <svg class="filter-menu-icon" viewBox="0 0 48 48">
                                                            <use xlink:href="#icon-new"></use>
                                                        </svg>
                                                        {$name}
                                                    </a>
                                                </li>
                                            {/foreach}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="spacer"></div>
                        {if $catalogUser}
                        <h1 class="catalog-header__title">Товары пользователя {$catalogUser->login}</h1>
                        {/if}
                        <!-- /Sort products -->
                        <div class="clearfix"></div>
                    </div>
                    <div class="store-products">
                        {include file="shop/products_list.tpl" products=$products}
                    </div>
                    <div class="store-products-footer">
                        <div class="spacer"></div>
                        <div class="pagination">
                            <span class="pagination-toggler pagination-toggler--active" style="display: none;" hidden>
                                <a href="{generate_query param=page value=1}">1</a>
                            </span>
                            {foreach from=$pagination.pages item="page"}
                                {if isset($page->is_seperator)}
                                    <div class="pagination-toggler pagination-toggler--separator">...</div>
                                {else}
                                    <span class="pagination-toggler {$page->class}">
                                        <a href="{$page->url}">{$page->text}</a>
                                    </span>
                                {/if}
                            {/foreach}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                </div>
            <div class="clearfix"></div>
        </section>
</div>
<script>
    var selected_category = {$category->id};
</script>