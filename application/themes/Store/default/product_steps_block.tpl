<script>
    var steps_i = 0;
    function steps_add(title, desc, res){
        $("#steps_list").append('<div class="body-nest"><div class="form-group "><div class="col-lg-3 col-sm-3 control-label"><label for="steps_'+steps_i+'_title">Ступень</label></div><div class="col-sm-6 field_wrap_text"><input id="steps_'+steps_i+'_title" type="text" name="steps_block['+steps_i+'][title]" value="'+title+'" class="form-control"></div></div><div class="form-group "><div class="col-lg-3 col-sm-3 control-label"><label for="steps_0_desc">Описание</label></div><div class="col-sm-6 field_wrap_text"><textarea name="steps_block['+steps_i+'][description]" id="steps_0_desc" class="form-control">'+desc+'</textarea></div></div><div class="form-group "><div class="col-lg-3 col-sm-3 control-label"><label for="steps_'+steps_i+'_time">Ресурс</label></div><div class="col-sm-6 field_wrap_text"><input id="steps_'+steps_i+'_time" type="text" name="steps_block['+steps_i+'][res]" value="'+res+'" class="form-control"></div></div></div>');
        steps_i++;
    }
</script>
<div class="nest nest-hide" data-linkid="product_steps">
    <div class="title-alt">
        <h6>Этапы очистки</h6>
        <div class="titleClose">
            <a class="gone" href="#elementClose">
                <span class="entypo-cancel"></span>
            </a>
        </div>
        <div class="titleToggle">
            <a class="nav-toggle-alt" href="#element">
                <span class="entypo-up-open"></span>
            </a>
        </div>
    </div>
    <div class="body-nest" id="steps_list">
        
    </div>
    <div class="body-nest">
        <div class="form-group">
            <div class="col-lg-3 col-sm-3 control-label"></div>
            <div class="col-sm-6 field_wrap_text add-steps-form">
                <b class="form-control-feedback error-holder" hidden>{translate code="error_empty_block_id" text="Введитите id элемента"}</b>
                <button type="button" class="btn btn-success" onclick="steps_add('', '', '');">
                    <i class="fa fa-plus"></i> <span>Добавить элемент</span>
                </button>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    {if isset($_post.steps_block)}
        {$steps = $_post.steps_block}
    {else}
        {if isset($aData.custom_steps_block)}
            {$steps = $aData.custom_steps_block|json_decode:1}
        {else}
            {$steps = array()}
        {/if}
    {/if}
    <script>
        {foreach from=$steps item=steps_item}
            {if !empty($steps_item.title)}
            {if empty($steps_item.description)}{$steps_item.description = ''}{/if}
            {if empty($steps_item.res)}{$steps_item.res = ''}{/if}
            steps_add('{$steps_item.title}', '{$steps_item.description|strip}', '{$steps_item.res}');
            {/if}
        {/foreach}
    </script>
</div>