<script>
    var advantages_i = 0;
    function advantage_add(title, desc){
        $("#advantages_list").append('<div class="body-nest"><div class="form-group "><div class="col-lg-3 col-sm-3 control-label"><label for="advantages_'+advantages_i+'_title">Преимущество</label></div><div class="col-sm-6 field_wrap_text"><input id="advantages_'+advantages_i+'_title" type="text" name="advantages['+advantages_i+'][title]" value="'+title+'" class="form-control"></div></div><div class="form-group "><div class="col-lg-3 col-sm-3 control-label"><label for="advantages_'+advantages_i+'_desc">Подробнее</label></div><div class="col-sm-6 field_wrap_text"><textarea name="advantages['+advantages_i+'][description]" id="advantages_'+advantages_i+'_desc" class="form-control">'+desc+'</textarea></div></div></div>');
        advantages_i++;
    }
</script>
<div class="nest nest-hide" data-linkid="product_collection">
    <div class="title-alt">
        <h6>Преимущества</h6>
        <div class="titleClose">
            <a class="gone" href="#elementClose">
                <span class="entypo-cancel"></span>
            </a>
        </div>
        <div class="titleToggle">
            <a class="nav-toggle-alt" href="#element">
                <span class="entypo-up-open"></span>
            </a>
        </div>
    </div>
    <div class="body-nest" id="advantages_list">
        
    </div>
    <div class="body-nest">
        <div class="form-group">
            <div class="col-lg-3 col-sm-3 control-label"></div>
            <div class="col-sm-6 field_wrap_text add-advantages-form">
                <b class="form-control-feedback error-holder" hidden>{translate code="error_empty_block_id" text="Введитите id элемента"}</b>
                <button type="button" class="btn btn-success" onclick="advantage_add('', '');">
                    <i class="fa fa-plus"></i> <span>Добавить элемент</span>
                </button>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    {if isset($_post.advantages)}
        {$advantages = $_post.advantages}
    {else}
        {if isset($aData.custom_advantages)}
            {$advantages = $aData.custom_advantages|json_decode:1}
        {/if}
    {/if}
    <script>
        {foreach from=$advantages item=advantage_item}
            {if !empty($advantage_item.title)}
                {if empty($advantage_item.description)}{$advantage_item.description = ''}{/if}
                advantage_add('{$advantage_item.title}', '{$advantage_item.description|strip}');
            {/if}
        {/foreach}
    </script>
</div>