<script>
    var collection_i = 0;
    function collection_add(title, desc, time){
        $("#collection_list").append('<div class="body-nest"><div class="form-group "><div class="col-lg-3 col-sm-3 control-label"><label for="collection_'+collection_i+'_title">Название</label></div><div class="col-sm-6 field_wrap_text"><input id="collection_'+collection_i+'_title" type="text" name="collection['+collection_i+'][title]" value="'+title+'" class="form-control"></div></div><div class="form-group "><div class="col-lg-3 col-sm-3 control-label"><label for="collection_0_desc">Описание</label></div><div class="col-sm-6 field_wrap_text"><textarea name="collection['+collection_i+'][description]" id="collection_0_desc" class="form-control">'+desc+'</textarea></div></div><div class="form-group "><div class="col-lg-3 col-sm-3 control-label"> <label for="collection_'+collection_i+'_time">Срок службы</label></div><div class="col-sm-6 field_wrap_text"><input id="collection_'+collection_i+'_time" type="text" name="collection['+collection_i+'][time]" value="'+time+'" class="form-control"></div></div></div>');
        collection_i++;
    }
</script>
<div class="nest nest-hide" data-linkid="product_collection">
    <div class="title-alt">
        <h6>Комлектация</h6>
        <div class="titleClose">
            <a class="gone" href="#elementClose">
                <span class="entypo-cancel"></span>
            </a>
        </div>
        <div class="titleToggle">
            <a class="nav-toggle-alt" href="#element">
                <span class="entypo-up-open"></span>
            </a>
        </div>
    </div>
    <div class="body-nest" id="collection_list">
        
    </div>
    <div class="body-nest">
        <div class="form-group">
            <div class="col-lg-3 col-sm-3 control-label"></div>
            <div class="col-sm-6 field_wrap_text add-collection-form">
                <b class="form-control-feedback error-holder" hidden>{translate code="error_empty_block_id" text="Введитите id элемента"}</b>
                <button type="button" class="btn btn-success" onclick="collection_add('', '', '');">
                    <i class="fa fa-plus"></i> <span>Добавить элемент</span>
                </button>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    {if isset($_post.collection)}
        {$collection = $_post.collection}
    {else}
        {if isset($aData.custom_collection)}
            {$collection = $aData.custom_collection|json_decode:1}
        {/if}
    {/if}
    <script>
        {foreach from=$collection item=collection_item}
            {if !empty($collection_item.title)}
                {if empty($collection_item.description)}{$collection_item.description = ''}{/if}
                collection_add('{$collection_item.title}', '{$collection_item.description|strip}', '{$collection_item.time}');
            {/if}
        {/foreach}
    </script>
</div>