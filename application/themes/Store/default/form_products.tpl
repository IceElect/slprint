<div class="nest">
    <div class="title-alt">
        <h6>С этим покупают</h6>
        <div class="titleClose">
            <a class="gone" href="#elementClose">
                <span class="entypo-cancel"></span>
            </a>
        </div>
        <div class="titleToggle">
            <a class="nav-toggle-alt" href="#element">
                <span class="entypo-up-open"></span>
            </a>
        </div>
    </div>
    <div class="body-nest">
        <section class="col-xs-12">
            <ul class="products">
                {if (!empty($aData.products))}
                {foreach $aData.products as $key => $product}
                    <li>{$product}</li>
                {/foreach}
            {/if}
            </ul>
            <p class="form-text text-muted">{translate code='error_tags_limit' text='Максимальное количество тэгов 10'}</p>
            <script>
                {literal}
                    $('.products').tagit({
                        availableTags: {/literal}{$products}{literal},
                        fieldName: 'product[]',
                        tagLimit: 10,
                        allowSpaces: true,
                        onTagLimitExceeded: function(){
                            console.log(jQuery(this).parent());
                            jQuery(this).parent().children('.error').show();
                        },
                        onTagRemoved: function(){
                            jQuery(this).parent().children('.error').hide();
                        }
                    });
                {/literal}
            </script>
        </section>
        <div class="clearfix"></div>
    </div>
</div>