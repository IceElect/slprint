<div class="thumb">
    <div class="thumb-title">
        <span class="thumb-title-inner">Теги</span>
    </div>
    <div class="thumb-inner">
        <ul class="tags">
            {if (!empty($aData.shop_tags))}
                {foreach $aData.shop_tags as $key => $tag}
                    <li>{$tag}</li>
                {/foreach}
            {/if}
        </ul>
        <p class="form-text text-muted">{translate code='error_tags_limit' text='Максимальное количество тэгов 10'}</p>
        <script>
            {literal}
                $('.tags').tagit({
                    availableTags: {/literal}{$shop_tags}{literal},
                    fieldName: 'shop_tag[]',
                    tagLimit: 10,
                    allowSpaces: false,
                    onTagLimitExceeded: function(){
                        jQuery(this).parent().children('.error').show();
                    },
                    onTagRemoved: function(){
                        jQuery(this).parent().children('.error').hide();
                    }
                });
            {/literal}
        </script>
    </div>
</div>