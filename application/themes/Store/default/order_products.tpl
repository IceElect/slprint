<table class="table order-products">
	<thead>
		<tr>
			<th></th>
			<th>Название</th>
			<th>Цена</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$products key="k" item="product"}
			<tr>
				<td width="100px">
					<img src="/{$product->previews->front}" alt="{$product->name}" style="max-width: 100%">
				</td>
				<td>
					<div><b>{$product->name}</b></div>
					<span>{$product->vendor}</span>
					<div style="display: flex;width: 100%;">
						{foreach from=$product->attributes key="key" item="attribute"}
                        <div class="cart-item-info-{$attribute->name}" style="flex: 1">
                            <select name="{$attribute->name}" id="{$attribute->name}" class="field">
                                {foreach from=$attribute->values key="k" item="a"}
                                    <option value="{$k}" {if $product->options[$attribute->name] == $k}selected{/if}>{$a->title}</option>
                                {/foreach}
                            </select>
                        </div>
                        {/foreach}
					</div>
				</td>
				<td width="100px" style="padding-bottom: 14px;vertical-align: bottom;"><input type="number" class="field" value="{$product->price}" name="price" min="1"></td>
				{*<td>{$product->price_without_disc}р</td>*}
				<td></td>
			</tr>
		{/foreach}
	</tbody>
</table>