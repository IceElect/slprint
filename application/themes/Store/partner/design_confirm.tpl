<div class="main-col">
    <h2>Создано {$products_list|@count} товар(ов) с вашим макетом</h2>
    <!-- <div class="thumb padding"> -->
        <aside style="margin: 0">
            <ol style="padding-left: 22px">
                <li>Посмотрите все ваши созданные товары.</li>
                <li>Отключите товары, к&nbsp;которым ваш макет не&nbsp;подходит.</li>
                <li>Помните, не&nbsp;на&nbsp;всех продуктах один и&nbsp;тот&nbsp;же макет будет выглядеть одинаково красиво.</li>
                <li>Для некоторых продуктов вы можете самостоятельно указать стоимость, <b>без учёты скидки магазина и промокодов</b>.</li>
                <li>Нажмите кнопку «Отправить на&nbsp;модерацию» внизу страницы.</li>
            </ol>
        </aside>
    <!-- </div> -->

    <div class="thumb">
        
    </div>
    <form method="post" action="/partner/design/moder/{$design.id}">
        <div class="thumb" style="margin-top: 0">
            <div class="design-preview-list">
                {foreach from=$products_list item="item"}
                <div class="item mb-1 design-preview--active" data-design-show="false" data-design-tkey="{$item.type_name}">
                    <label class="design-preview__label" for="{$item.type_name}">
                        <input class="design-preview__checkbox" name="approve[{$item.type_name}]" id="{$item.type_name}" type="checkbox" data-design-tkey="{$item.type_name}" checked="checked">
                        <span class="design-preview__title">{$item.title}</span>
                    </label>
                    <div class="design-preview__holder">
                        <div class="design-preview__image">
                            <div class="design-preview__image-preloader">
                                <i class="fa fa-spinner fa-spin"></i>
                                Генерация превью
                            </div>
                            <img class="design-preview__image-element" src="" style="display: none;">
                        </div>
                    </div>
                    <div class="design-preview__price pl-1 pr-1">
                        <input type="number" min="{$item.price}" name="price[]" class="form-control" placeholder="Стоимость: {$item.price}р">
                        <div class="pt-1 pb-1"><b>Стоимость по умолчанию: {$item.price} руб</b></div>
                    </div>
                </div>
                {/foreach}
            </div>
        </div>
        <div class="buttons-group">
            <button type="submit" id="send_moder" class="btn btn-primary big full_width" disabled>Отправить на модерацию</button>
        </div>
    </form>
</div>
<script>
    {literal}
    var design_id = '{/literal}{$design.id}{literal}';

    function isInViewport(element) {
        var rect = element.getBoundingClientRect();
        var html = document.documentElement;
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom-300 <= (window.innerHeight || html.clientHeight) &&
            rect.right <= (window.innerWidth || html.clientWidth)
        );
    }

    function loadPreviews(){
        $(".design-preview-list .item").each(function(index, el){
            if(isInViewport(el) && $(el).data('design-show') != "true"){
                $(el).data('design-show', "true");

                var type_name = $(el).data('design-tkey');
                $.post('/partner/generate_preview', {design_id: design_id, type_name: type_name}, function(data){
                    data = JSON.parse(data);

                    $(el).addClass('item-loaded');

                    if($(".design-preview-list .item:not(.item-loaded)").length < 1){
                        $("#send_moder").attr('disabled', false);
                    }

                    var preview = data.previews[Object.keys(data.previews)[0]];
                    $(el).find(".design-preview__image-preloader").hide();
                    $(el).find(".design-preview__image-element").attr("src", '/' + preview).show();
                })
            }
        })
    }

    $(window).scroll(loadPreviews)
    $(function(){
        loadPreviews();
    })
    {/literal}
</script>