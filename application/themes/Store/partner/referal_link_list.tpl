<div class="card">
    <div class="card-body">
        <div class="row mb-1">
            <div class="col-md-12">
                <a class="btn btn-primary pull-right" href="/partner/referal_link_edit">Создать Ссылку</a>
            </div>
        </div>
        <table id="referal_link_list" class="table table-bordered table-striped cf" style="width:100%">
            <thead>
            <tr>
                <th>Дата</th>
                <th>Название</th>
                <th>Цель</th>
                <th>Ссылка</th>
                <th>Кол-во Переходов</th>
                <th>Кол-во Регстраци</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<script>
    {literal}
    $(document).ready(function() {
        var clipboard = new ClipboardJS('.copy_link');

        $(document).delegate('.copy_link', 'click', function(e, el){
            e.preventDefault();

        });

        $('#referal_link_list').DataTable({
            responsive:true,
            stateSave:true,
            "processing": true,
            "serverSide": true,
            "searchDelay": 600,
            "ajax": {
                "url": "/partner/referal_link_list",
                "data": function (d) {
                    d.filter = {/literal}{$filter|json_encode}{literal};
                }
            },
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
                /*
                 "lengthMenu": "Показывать _MENU_ строк на странице",
                 "zeroRecords": "Ничего не найдено",
                 "info": "Показана страница _PAGE_ из _PAGES_",
                 "infoEmpty": "Нет записей",
                 "infoFiltered": "(отфильтровано из _MAX_ записей)",
                 "search": "Поиск",
                 "paginate": {
                 "next": "Дальше",
                 "previous": "Назад"
                 }*/
            },
            "columns": [
                {
                    "data":'date',
                    "render": datatable_render_date
                },
                {"data":'name'},
                {"data":'target'},
                {
                    "data":'target',
                    'render': function(data, type, full, meta){
                        var link = document.location.origin;

                        if (data == 'Мой Магазин'){
                            link +=  $('#main-menu-navigation li:last-child a').attr('href');
                        } if (data== '1') {
                            //link += '/users/register';
                            link += '/';
                        } else {
                            link += '/';
                        }
                        link += '?ref=' + full.id;
                        return  '<a href="#" class="copy_link" title="Копировать" data-clipboard-text="' + link + '">' + link + '</a>';
                    }
                },
                {"data":'num_visits'},
                {"data":'num_reg'},
                {
                    "data":'id',
                    'bSortable' : false,
                    'render': function(data, type, full, meta){
                        return  '<a href="/partner/referal_link_edit/' + data + '" >Редактироать</a>';
                    }
                }
            ]
        });

    });
    function copy_to_clipboard(e, el) {

    }
    {/literal}
</script>
