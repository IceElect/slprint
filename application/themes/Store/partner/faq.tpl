<div class="accordion" id="accordionFaq">
	{$i = 0}
	{foreach from=$faq key="question" item="answer"}
	{$i = $i + 1}
    <div class="card">
        <div class="card-header" id="headingOne">
            <h2 class="mb-0">
                <button class="btn btn-block text-left btn-link text-wrap" style="white-space: normal;line-height: 1.5;padding: 0px" type="button" data-toggle="collapse" data-target="#collapse{$i}" aria-expanded="true" aria-controls="collapse{$i}">{$question}</button>
            </h2>
        </div>
        <div id="collapse{$i}" class="collapse {if $i == 1}show{/if}" aria-labelledby="heading{$i}" data-parent="#accordionFaq">
            <div class="card-body">
                {$answer}
            </div>
        </div>
    </div>
    {/foreach}
</div>