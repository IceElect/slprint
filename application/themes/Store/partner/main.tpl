<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>{$pageTextTitle}</title>
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/partner/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="/assets/partner/vendors/css/charts/chartist.css">
    <link rel="stylesheet" type="text/css" href="/assets/partner/vendors/css/charts/chartist-plugin-tooltip.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN CHAMELEON  CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/partner/css/app-lite.css">
    <!-- END CHAMELEON  CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/partner/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/assets/partner/css/core/colors/palette-gradient.css">
    <!-- <link rel="stylesheet" type="text/css" href="/{$path}partner/"> -->
    <link rel="stylesheet" type="text/css" href="/assets/partner/css/pages/timeline.min.css">
    <!-- END Page Level CSS-->
    
    <!-- BEGIN Custom CSS-->
    {include file="sys/js_css_inc.tpl" place="header"}
    <!-- END Custom CSS-->
  </head>
  <body class="vertical-layout vertical-menu 2-columns menu-expanded" data-open="click" data-menu="vertical-menu" data-color="bg-chartbg" data-col="2-columns">
    <div class="popup_layer"></div>

    <!-- BEGIN VENDOR JS-->
    <script src="/assets/partner/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="/assets/partner/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
    <script src="/assets/partner/vendors/js/charts/chartist-plugin-tooltip.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->

    {include file="sys/js_css_inc.tpl" place="init"}
    {include file="sys/js_inc.tpl" place="init"}

    <!-- fixed-top-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow {*fixed-top*} navbar-semi-light">
      <div class="navbar-wrapper">
        <div class="navbar-container content">
          <div class="collapse navbar-collapse show" id="navbar-mobile">
            <ul class="nav navbar-nav mr-auto float-left">
              <li class="nav-item d-block d-md-none"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
              <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
            </ul>
            <ul class="nav navbar-nav float-right">         
              {*
              <li class="dropdown dropdown-language nav-item"><a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flag-icon flag-icon-us"></i><span class="selected-language"></span></a>
                <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                  <div class="arrow_box"><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-us"></i> English</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-cn"></i> Chinese</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-ru"></i> Russian</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-fr"></i> French</a><a class="dropdown-item" href="#"><i class="flag-icon flag-icon-es"></i> Spanish</a></div>
                </div>
              </li>
              *}
            </ul>
            <ul class="nav navbar-nav float-right">
              <li class="nav-item d-block"><a class="nav-link" href="https://vk.com/im?sel=-177498527" target="_blank"><i class="ficon ft-mail"></i></a></li>
              <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="avatar">{get_avatar u=$oUser}<i></i></span></a>
                <div class="dropdown-menu dropdown-menu-right">
                  <div class="arrow_box_right">
                    <a class="dropdown-item" href="#">
                      <span class="avatar avatar-online">
                        <img src="{get_avatar u=$oUser link=true}" alt="avatar">
                        <span class="user-name text-bold-700 ml-1">{$oUser->fname}</span>
                      </span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/users/logout">
                      <i class="ft-power"></i> Выход
                    </a>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true" data-img="/assets/partner/images/backgrounds/02.jpg">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">       
          <li class="nav-item mr-auto"><a class="navbar-brand" href="/"><img class="brand-logo" src="/logo.png"/>
              <h3 class="brand-text">Partner zone</h3></a></li>
          <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
      </div>
      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
          {foreach from=$menu item='item'}
              <li {if isset($item.active) or (base_url('/partner`$item.link`') == current_url())}class="active"{/if}>
                  <a href="/partner{$item.link}">
                      <i class="{$item.icon}"></i>
                      <span class="menu-title" data-i18n="">{$item.title}</span>
                  </a>
              </li>
          {/foreach}
          <li>
              <a href="/catalog/{$oUser->login}">
                  <i class="ft-shopping-cart"></i>
                  <span class="menu-title" data-i18n="">Мой магазин</span>
              </a>
          </li>
          {*
          <li class=" nav-item"><a href="charts.html"><i class="ft-pie-chart"></i><span class="menu-title" data-i18n="">Charts</span></a>
          </li>
          <li class=" nav-item"><a href="icons.html"><i class="ft-droplet"></i><span class="menu-title" data-i18n="">Icons</span></a>
          </li>
          <li class=" nav-item"><a href="cards.html"><i class="ft-layers"></i><span class="menu-title" data-i18n="">Cards</span></a>
          </li>
          <li class=" nav-item"><a href="buttons.html"><i class="ft-box"></i><span class="menu-title" data-i18n="">Buttons</span></a>
          </li>
          <li class=" nav-item"><a href="typography.html"><i class="ft-bold"></i><span class="menu-title" data-i18n="">Typography</span></a>
          </li>
          <li class=" nav-item"><a href="tables.html"><i class="ft-credit-card"></i><span class="menu-title" data-i18n="">Tables</span></a>
          </li>
          <li class=" nav-item"><a href="form-elements.html"><i class="ft-layout"></i><span class="menu-title" data-i18n="">Form Elements</span></a>
          </li>
          <li class=" nav-item"><a href="https://themeselection.com/demo/chameleon-admin-template/documentation"><i class="ft-book"></i><span class="menu-title" data-i18n="">Documentation</span></a>
          </li>
          *}
        </ul>
      </div>
      {* <a class="btn btn-danger btn-block btn-glow btn-upgrade-pro mx-1" href="https://themeselection.com/products/chameleon-admin-modern-bootstrap-webapp-dashboard-html-template-ui-kit/" target="_blank">Download PRO!</a> *}
      <div class="navigation-background"></div>
    </div>

    <div class="app-content content">
      <div class="content-wrapper">
        {if (!isset($hide_header))}
        <div class="content-wrapper-before"></div>
          <div class="content-header row">
              <div class="content-header-left col-md-6 col-12 mb-2">
                  <h3 class="content-header-title">{$pageTextTitle}</h3>
              </div>
              {*<div class="content-header-right col-md-6 col-12">
                  <div class="breadcrumbs-top float-md-right">
                      <div class="breadcrumb-wrapper mr-1">
                          <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                              <li class="breadcrumb-item active">Icons</li>
                          </ol>
                      </div>
                  </div>
              </div>*}
          </div>
          <div class="content-body">
        {/if}
            {$content}
        {if (!isset($hide_header))}
          </div>
        {/if}
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
      <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">2019  &copy; Copyright <a class="text-bold-800 grey darken-2" href="{base_url()}" target="_blank">PrintBand</a></span>
        <ul class="list-inline float-md-right d-block d-md-inline-blockd-none d-lg-block mb-0">
          <li class="list-inline-item"><a class="my-1" href="{base_url()}" target="_blank"> В магазин</a></li>
        </ul>
      </div>
    </footer>

    <!-- BEGIN CHAMELEON  JS-->
    <script src="/assets/partner/js/core/app-menu-lite.js" type="text/javascript"></script>
    <script src="/assets/partner/js/core/app-lite.js" type="text/javascript"></script>
    <!-- END CHAMELEON  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="/assets/partner/js/scripts/pages/dashboard-lite.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->


    {include file="sys/js_css_inc.tpl" place="footer"}
    {include file="sys/js_inc.tpl" place="footer"}

  </body>
</html>