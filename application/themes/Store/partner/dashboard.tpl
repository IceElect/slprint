<div class="content-wrapper-before"></div>
<div class="content-header row"></div>
<div class="content-body">
    <!-- Chart -->
    <div class="row match-height">
        <div class="col-12">
            <div class="">
                <div id="gradient-line-chart1" class="height-250 GradientlineShadow1"></div>
            </div>
        </div>
    </div>
    <!-- Chart -->
    <!-- statistic -->
    <div class="row match-height">
        <div class="col-xl-3 col-lg-6 col-md-12">
            <div class="card text-center bg-white">
                <div class="card-body">
                    <h4 class="card-title">Просмотров</h4>
                    <p class="card-text" style="font-size: 32px;font-weight: bold;line-height: 1em;">0</p>
                    <div class="card-footer pb-0">
                        <span>+0 за сегодня</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-12">
            <div class="card text-center bg-white">
                <div class="card-body">
                    <h4 class="card-title">Конверсия</h4>
                    <p class="card-text" style="font-size: 32px;font-weight: bold;line-height: 1em;">0%</p>
                    <div class="card-footer pb-0">
                        <span>+0% за сегодня</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-12">
            <div class="card text-center bg-white">
                <div class="card-body">
                    <h4 class="card-title">Заказы</h4>
                    <p class="card-text" style="font-size: 32px;font-weight: bold;line-height: 1em;">0</p>
                    <div class="card-footer pb-0">
                        <span>+0 за сегодня</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-12">
            <div class="card text-center bg-white">
                <div class="card-body">
                    <h4 class="card-title">Текущий баланс</h4>
                    <p class="card-text" style="font-size: 32px;font-weight: bold;line-height: 1em;">{$oUser->balance|@intval} руб</p>
                    <div class="card-footer" style="padding-bottom: 0px;">
                        {*<span class="float-left">+15р за сегодня</span>*}
                        <span class="float2-right" style="padding-bottom: 0px;"><a href="/partner/withdraw_list" class="card-link">Вывести</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- statistic -->
    {*
    <!-- eCommerce statistic -->
    <div class="row">
        <div class="col-xl-4 col-lg-6 col-md-12">
            <div class="card pull-up ecom-card-1 bg-white">
                <div class="card-content ecom-card2 height-180">
                    <h5 class="text-muted danger position-absolute p-1">Progress Stats</h5>
                    <div>
                        <i class="ft-pie-chart danger font-large-1 float-right p-1"></i>
                    </div>
                    <div class="progress-stats-container ct-golden-section height-75 position-relative pt-3  ">
                        <div id="progress-stats-bar-chart"></div>
                        <div id="progress-stats-line-chart" class="progress-stats-shadow"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-6 col-md-12">
            <div class="card pull-up ecom-card-1 bg-white">
                <div class="card-content ecom-card2 height-180">
                    <h5 class="text-muted info position-absolute p-1">Activity Stats</h5>
                    <div>
                        <i class="ft-activity info font-large-1 float-right p-1"></i>
                    </div>
                    <div class="progress-stats-container ct-golden-section height-75 position-relative pt-3">
                        <div id="progress-stats-bar-chart1"></div>
                        <div id="progress-stats-line-chart1" class="progress-stats-shadow"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-12">
            <div class="card pull-up ecom-card-1 bg-white">
                <div class="card-content ecom-card2 height-180">
                    <h5 class="text-muted warning position-absolute p-1">Sales Stats</h5>
                    <div>
                        <i class="ft-shopping-cart warning font-large-1 float-right p-1"></i>
                    </div>
                    <div class="progress-stats-container ct-golden-section height-75 position-relative pt-3">
                        <div id="progress-stats-bar-chart2"></div>
                        <div id="progress-stats-line-chart2" class="progress-stats-shadow"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ eCommerce statistic -->
    *}
    {*
    <!-- Statistics -->
    <div class="row match-height">
        <div class="col-xl-4 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="heading-multiple-thumbnails">Multiple Thumbnail</h4>
                    <a class="heading-elements-toggle">
                    <i class="la la-ellipsis-v font-medium-3"></i>
                    </a>
                    <div class="heading-elements">
                        <span class="avatar">
                        <img src="/{$path}partner/theme-assets/images/portrait/small/avatar-s-2.png" alt="avatar">
                        </span>
                        <span class="avatar">
                        <img src="/{$path}partner/theme-assets/images/portrait/small/avatar-s-3.png" alt="avatar">
                        </span>
                        <span class="avatar">
                        <img src="/{$path}partner/theme-assets/images/portrait/small/avatar-s-4.png" alt="avatar">
                        </span>
                    </div>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <h4 class="card-title">Content title</h4>
                        <p class="card-text">Jelly beans sugar plum cheesecake cookie oat cake soufflé.Tootsie roll bonbon liquorice tiramisu pie powder.Donut sweet
                            roll marzipan pastry cookie cake tootsie roll oat cake cookie.Jelly beans sugar plum cheesecake cookie oat cake soufflé. Tart lollipop carrot cake sugar plum. 
                        </p>
                        <p class="card-text">Sweet roll marzipan pastry halvah. Cake bear claw sweet. Tootsie roll pie marshmallow lollipop chupa chups donut fruitcake
                            cake.Jelly beans sugar plum cheesecake cookie oat cake soufflé. Tart lollipop carrot cake sugar plum. Marshmallow
                            wafer tiramisu jelly beans.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <h4 class="card-title">Recent products</h4>
                        <h6 class="card-subtitle text-muted">Carousel Card With Header & Footer</h6>
                    </div>
                    <div id="carousel-area" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-area" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-area" data-slide-to="1"></li>
                            <li data-target="#carousel-area" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img src="/{$path}partner/theme-assets/images/carousel/08.jpg" class="d-block w-100" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img src="/{$path}partner/theme-assets/images/carousel/03.jpg" class="d-block w-100" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img src="/{$path}partner/theme-assets/images/carousel/01.jpg" class="d-block w-100" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carousel-area" role="button" data-slide="prev">
                        <span class="la la-angle-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-area" role="button" data-slide="next">
                        <span class="la la-angle-right icon-next" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <div class="card-body">
                        <a href="#" class="card-link">Card link</a>
                        <a href="#" class="card-link">Another link</a>
                    </div>
                </div>
                <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                    <span class="float-left">2 days ago</span>
                    <span class="tags float-right">
                    <span class="badge badge-pill badge-primary">Branding</span>
                    <span class="badge badge-pill badge-danger">Design</span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    *}
</div>


<script>
    /*************************************************
    *               Line gradient chart               *
    *************************************************/

    var lineGradientChart1 = new Chartist.Line('#gradient-line-chart1', {
        labels: [1, 2, 3, 4, 5, 6, 7],
        series: [
            {
                name: 'Заказы',
                data: [0, 0, 0, 0, 0, 0, 0]
            },
            {
                name: 'Просмотров',
                data: [0, 0, 0, 0, 0, 0, 0]
            },
            {
                name: 'Конверсия',
                data: [0, 0, 0, 0, 0, 0, 0]
            },
            {
                name: 'Баланс',
                data: [0, 0, 0, 0, 0, 0, 0]
            },
        ]
    }, {
            fullWidth: true,
            onlyInteger: true,
            axisY: {
                low: 0,
                scaleMinSpace: 50,
            },
            axisX: {
                showGrid: false
            },
            lineSmooth: Chartist.Interpolation.simple({
                divisor: 2
            }),
            plugins: [
                Chartist.plugins.tooltip({
                    appendToBody: true,
                    pointClass: 'ct-point'
                })
            ]
        });
</script>