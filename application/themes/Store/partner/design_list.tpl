<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <a href="/partner/design_create" class="btn mb-1 btn-md-block btn-secondary btn-bg-gradient-x-purple-blue"><i class="ft-plus" style="line-height: 20px;float: left;margin-right: 8px;font-weight: bold;font-size: 20px;"></i> Создать дизайн</a>
                        <a href="/partner/faq" class="btn mb-1 btn-md-block btn-secondary pull-right">Вопрос-ответ</a>
                    </div>
                </div>
                <table id="design_list" class="table table-bordered table-striped cf" style="width:100%">
                    <thead>
                        <tr>
                            <th data-priority="1">ID</th>
                            <th data-priority="4">Превью</th>
                            <th data-priority="1">Название</th>
                            <th data-priority="3">Статус</th>
                            <th data-priority="3">Заказан</th>
                            <th data-priority="2"></th>
                        </tr>
                    </thead>
                    <tbody>
                    {*foreach from=$list item=$row }
                        <tr>
                            <td>{$row.id}</td>
                            <td><img src="/upload/product/preview/{$row.id}/{$row.product_type}/{$row.default_color}/1024/front.png"  ></td>
                            <td>{$row.title}</td>
                            <td>{$row.status}</td>
                            <td>
                                <a href="/partner/design_edit/{$row.id}">Редактировать</a>
                                <a href="/partner/design_delete/{$row.id}">Удалить</a>
                            </td>
                        </tr>
                    {/foreach*}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
{literal}

var statuses = [
    'Ожидает',
    'На модерации',
    'Опубликован',
    'Отклонен',
    'Удален'
];

$(document).ready(function() {
    var table = $('#design_list').DataTable({
        responsive:true,
        stateSave:true,
        "processing": true,
        "serverSide": true,
        "searchDelay": 600,
        "bLengthMenu" : false,
        "bLengthChange" : false,
        "ajax": {
            "url": "/partner/design",
            "data": function (d) {
                d.filter = {/literal}{$filter|json_encode}{literal};
            }
        },
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
            /*
            "lengthMenu": "Показывать _MENU_ строк на странице",
            "zeroRecords": "Ничего не найдено",
            "info": "Показана страница _PAGE_ из _PAGES_",
            "infoEmpty": "Нет записей",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск",
            "paginate": {
                "next": "Дальше",
                "previous": "Назад"
            }*/
        },
        "columns": [
            {"data":'id'},
            {
                "data":'id',
                "searchable": false,
                "orderable": false,
                "render": function(data, type, full, meta){
                    return '<img src="/ajax/product/preview/' + full.id +'/manshort/' + full.default_color + '/185/front.png"  >';
                }
            },
            {"data":'title'},
            {
                "data":'status',
                render: function(data, type, full, meta){
                    return statuses[data];
                }
            },
            {"data":'orders'},
            {
                "data":'id',
                "class" : 'actions',
                "searchable": false,
                "orderable": false,
                "render": function(data, type, full, meta){
                    var result = '';
                    if(full.status == 0){
                        result += '<a class="btn btn-md-block" href="/partner/design_confirm/' + full.id + '">Отправить на модерацию</a>';
                    }
                    if(full.status == 0){
                        result += '<a class="btn btn-md-block" href="/partner/design/edit/' + full.id + '">Редактировать</a>';
                    }
                    if(full.status == 2 || full.status == 3){
                        result += '<a class="btn btn-md-block" href="/partner/design/view/' + full.id + '">Информация</a>';
                    }
                    result += '<a class="btn btn-md-block" data-remove="' + full.id + '" href="/partner/design_delete/' + full.id + '">Удалить</a>';
                    return result;
                }
            }
        ]
    });

    $(document).on('click', '[data-remove]', function(e){
        e.preventDefault();

        $.post($(this).attr('href'), function(data){
            table.ajax.reload();
        })

        return false;
    })
});
{/literal}
</script>


