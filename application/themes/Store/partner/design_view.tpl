<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">

                <div class="design-preview-list">
                    {foreach from=$sources item="item"}
                    <div class="item mb-1 design-preview--active" data-design-show="false" data-design-tkey="{$item.type_name}">
                        {*
                        <label class="design-preview__label" for="{$item.type_name}">
                            <input class="design-preview__checkbox" name="approve[{$item.type_name}]" id="{$item.type_name}" type="checkbox" data-design-tkey="{$item.type_name}" checked="checked">
                            <span class="design-preview__title">{$design.title}</span>
                        </label>
                        *}
                        <div class="design-preview__holder">
                            <div class="design-preview__image">
                                <div class="design-preview__image-preloader">
                                    <i class="fa fa-spinner fa-spin"></i>
                                    Генерация превью
                                </div>
                                <img class="design-preview__image-element" src="" style="display: none;">
                            </div>
                        </div>
                        <div class="design-preview__price pl-1 pr-1">
                            <button class="btn btn-primary btn-block design_send" data-id="{$item.id}" onclick="popup.show('export_printovaya', {literal}{{/literal}cat: 1{literal}}{/literal})">Добавить на Принтовую</button>
                        </div>
                    </div>
                    {/foreach}
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    {literal}
    var design_id = '{/literal}{$design.id}{literal}';

    function isInViewport(element) {
        var rect = element.getBoundingClientRect();
        var html = document.documentElement;
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom-300 <= (window.innerHeight || html.clientHeight) &&
            rect.right <= (window.innerWidth || html.clientWidth)
        );
    }

    function loadPreviews(){
        $(".design-preview-list .item").each(function(index, el){
            if(isInViewport(el) && $(el).data('design-show') != "true"){
                $(el).data('design-show', "true");

                var type_name = $(el).data('design-tkey');
                $.post('/partner/generate_preview', {design_id: design_id, type_name: type_name}, function(data){
                    data = JSON.parse(data);

                    $(el).addClass('item-loaded');

                    if($(".design-preview-list .item:not(.item-loaded)").length < 1){
                        $("#send_moder").attr('disabled', false);
                    }

                    var preview = data.previews[Object.keys(data.previews)[0]];
                    $(el).find(".design-preview__image-preloader").hide();
                    $(el).find(".design-preview__image-element").attr("src", '/' + preview).show();
                })
            }
        })
    }

    $(window).scroll(loadPreviews)
    $(function(){
        loadPreviews();
        $('.design_send').click(function(){
            $.ajax({
                type: 'POST',
                url: '/partner/design/send/' + $(this).data('id'),
                dataType: 'json',
                success: function(data) {
                    alert('sent');
                },
                data: {'ajax':1},
                async: true
            });
        });
    })
    {/literal}
</script>