<div class="card">
    <div class="card-body">
        <div class="row mb-1">
            <div class="col-md-12">
                <a class="btn btn-primary pull-right" href="/partner/withdraw_create">Вывести</a>
            </div>
        </div>
        <table id="withdraw_list" class="table table-bordered table-striped cf" style="width:100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Дата</th>
                <th>Статус</th>
                <th>Система</th>
                <th>Дата выплаты</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<script>
    {literal}
    $(document).ready(function() {
        $('#withdraw_list').DataTable({
            responsive:true,
            stateSave:true,
            "processing": true,
            "serverSide": true,
            "searchDelay": 600,
            "ajax": {
                "url": "/partner/withdraw_list",
                "data": function (d) {
                    d.filter = {/literal}{$filter|json_encode}{literal};
                }
            },
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
                /*
                 "lengthMenu": "Показывать _MENU_ строк на странице",
                 "zeroRecords": "Ничего не найдено",
                 "info": "Показана страница _PAGE_ из _PAGES_",
                 "infoEmpty": "Нет записей",
                 "infoFiltered": "(отфильтровано из _MAX_ записей)",
                 "search": "Поиск",
                 "paginate": {
                 "next": "Дальше",
                 "previous": "Назад"
                 }*/
            },
            "columns": [
                {"data":'id'},
                {
                    "data":'date',
                    "render": datatable_render_date
                },
                {
                    "data":'status',
                    "render": datatable_render_order_status
                },
                {
                    "data":'payment_service'
                },
                {
                    "data":'payed_date',
                    "render": datatable_render_date
                }
            ]
        });
    });

    {/literal}
</script>
