<div class="card">
    <div class="card-body">
        <table id="order_list" class="table table-bordered table-striped cf" style="width:100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Дата</th>
                <th>Статус</th>
                <th>Бонус</th>
                <th>Начислено</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<script>
    {literal}
    $(document).ready(function() {
        $('#order_list').DataTable({
            responsive:true,
            stateSave:true,
            "processing": true,
            "serverSide": true,
            "searchDelay": 600,
            "bLengthMenu" : false,
            "bLengthChange" : false,
            "ajax": {
                "url": "/partner/order_list",
                "data": function (d) {
                    d.filter = {/literal}{$filter|json_encode}{literal};
                }
            },
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
                /*
                 "lengthMenu": "Показывать _MENU_ строк на странице",
                 "zeroRecords": "Ничего не найдено",
                 "info": "Показана страница _PAGE_ из _PAGES_",
                 "infoEmpty": "Нет записей",
                 "infoFiltered": "(отфильтровано из _MAX_ записей)",
                 "search": "Поиск",
                 "paginate": {
                 "next": "Дальше",
                 "previous": "Назад"
                 }*/
            },
            "columns": [
                {"data":'id'},
                {
                    "data":'date',
                    "render": datatable_render_date
                },
                {
                    "data":'status',
                    "render": datatable_render_order_status
                },
                {"data":'payroll'},
                {
                    "data":'bonus_payout',
                    "render": function(data, type, full, meta) {
                        if (data == 1){
                            return 'Да';
                        }
                        return 'Еще нет';
                    }
                }
                /*,
                {
                    "data":'id',
                    "class" : 'actions',
                    "searchable": false,
                    "orderable": false,
                    "render": function(data, type, full, meta){
                        var result = '';
                        result += '<a href="/partner/design_edit/' + full.id + '">Редактировать</a>';
                        result += '<a href="/partner/design_delete/' + full.id + '">Удалить</a>';
                        return result;
                    }
                }*/
            ]
        });
    });

    {/literal}
</script>


