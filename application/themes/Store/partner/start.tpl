<div class="page-main container">
    <div class="partner-page">
        <div class="partner-start">
            <div class="partner-start_image">
                <img src="/images/partner-start-designer.png" alt="SLPrint - конструктор футболок">
                <div class="partner-start_title">Создавай свои товары</div>
            </div>
            <div class="partner-start_info">
                <div class="thumb partner-start_features">
                    <ul>
                        <li>Используй конструктор для создания товаров.</li>
                        <li>Получай % с <b>каждой</b> продажи.</li>
                        <li>Стань популярным дизайнером с нами.</li>
                        <li>Твое портфолио — это и есть бизнес.</li>
                    </ul>
                </div>
                <div class="partner-start_buttons buttons-group">
                    <a href="/partner/agreement" class="button button-color button-size-ml full_width {if !$oUser->id}login-button{/if}">Начать продавать</a>
                </div>
            </div>
        </div>

        <div class="questions-list">
            {$i=0}
            {foreach from=$faq key="question" item="answer"}
            {$i=$i+1}
            <div class="questions-block thumb {if $i == 1}collapsed{/if}">
                <div class="questions-block__header">
                    {$question}
                </div>
                <div class="questions-block__body">
                    <div>{$answer}</div>
                </div>
            </div>
            {/foreach}
        </div>

    </div>
</div>

<script>
    $(".questions-block .questions-block__header").on('click', function(e){
        e.preventDefault();

        var block = $(this).parent();
        var collapsed = $(block).hasClass('collapsed');

        $(".questions-block").removeClass('collapsed');

        if(collapsed){
            block.removeClass('collapsed');
        }else{
            block.addClass('collapsed');
        }
    })
</script>