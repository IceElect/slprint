<section class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="thumb page-layout__main" style="position:relative;padding-bottom: 100%;margin-bottom: 90px;">
                    <div class="product-constructor">
                        <div class="constructor-controls" style="display: none;">

                            <button class="constructor-controls__button" id="constructor-control-delete-layer" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Удалить слой"><i class="fal fa-trash" aria-hidden="true"></i></button> 

                            {*<button class="constructor-controls__button constructor-controls__edit-layer constructor-controls__edit-layer--active" id="edit-layer" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Редактировать слой"><i class="fa fa-pencil" aria-hidden="true"></i></button>*}

                            {*<button class="constructor-controls__button" id="constructor-control-flip-x" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Отразить по&nbsp;горизонтали"><span class="constructor-controls__icon constructor-controls__icon--reflect-h" aria-hidden="true">&nbsp;</span></button>*}

                            {*<button class="constructor-controls__button" id="constructor-control-flip-y" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Отразить по&nbsp;вертикали"><span class="constructor-controls__icon constructor-controls__icon--reflect-v" aria-hidden="true">&nbsp;</span></button>*}

                            <button class="constructor-controls__button" id="constructor-control-center-vertical" data-toggle="tooltip" data-placement="auto" title="Центрировать по&nbsp;горизонтали">
                                <i class="fal fa-arrows-h" aria-hidden="true"></i>
                            </button>

                            <button class="constructor-controls__button" id="constructor-control-center-horizontal" data-toggle="tooltip" data-placement="auto" title="Центрировать по вертикали"><i class="fal fa-arrows-v" aria-hidden="true"></i></button>
                        </div>

                        <div class="layer-controls" style="display: none;">
                            <div class="text-controls" style="display: none;">
                                <label class="sr-only" for="text-control-text">Текст:</label>
                                <textarea class="text-controls__control text-controls__textarea" id="text-control-text" placeholder="Введите текст здесь" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Текст"></textarea>
                                <div>
                                    <input type="hidden" id="text-control-text-font" value="Andika">
                                    <input type="checkbox" style="display: none;" id="text-control-text-font-toggle">
                                    <div for="text-control-text-font-toggle" class="text-controls__control text-controls__select text-controls__fonts-select fonts-select">
                                        <label class="fonts-select__selected-font" style="font-family: Arial;">Arial<i class="fa fa-caret-down fonts-select__fa" aria-hidden="true"></i>
                                        </label>
                                        <div style="display: none; position: fixed; top: 0px; right: 0px; bottom: 0px; left: 0px;"></div>
                                        <div class="fonts-select__list">
                                            {foreach from=$fonts item="file" key="name"}
                                                <span class="fonts-select__list-item" style="font-family: '{$name}';">{$name}</span>
                                            {/foreach}
                                            <!-- <button class="fonts-select__load-more" style="display: flex;">Больше шрифтов...</button> -->
                                        </div>
                                    </div>
                                </div>
                                <div><label class="sr-only" for="text-control-text-color">Цвет шрифта</label><button class="text-controls__control text-controls__color-button bg-colorpicker " data-color="#000" id="text-control-text-color" data-toggle="tooltip" data-placement="auto" title="" value="#000000" style="background-color: rgb(0, 0, 0);" data-original-title="Цвет шрифта"></button></div>
                           </div>
                           <div class="image-controls" style="display: none;">
                              <button class="layer-controls__button" id="constructor-control-duplicate" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Дублировать слой"><i class="fa fa-clone" aria-hidden="true"></i></button>

                              <button class="layer-controls__button" id="constructor-control-equal-printfield" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Растянуть по&nbsp;области"><i class="fa fa-arrows-alt" aria-hidden="true"></i></button>

                              <button class="layer-controls__button" id="constructor-control-reset-transform" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Сбросить изменения слоя"><span class="constructor-controls__icon constructor-controls__icon--reset" aria-hidden="true">&nbsp;</span></button> 
                            </div>
                        </div>

                        <div class="product-constructor-holder"></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="construct-panel thumb">
                    <div class="construct-panel-buttons col-md-6">
                        <button class="button button-color" onclick="photo.upload_popup(photo.test_callback, 'construct', 1, event)">Добавить фото</button>
                        <button class="button button-color" onclick="construct.addText(event)">Добавить текст</button>
                        <button class="button button-primary">Как пользоваться?</button>
                    </div>
                    <div class="construct-panel-sides col-md-6">
                        {*<div class="construct-panel-side selected" data-side="front" onclick="construct.setSide('front')">
                            <img src="/constructor/{$product->type_name}-front.png" style="background-color:#ffffff;">
                        </div>
                        <div class="construct-panel-side" data-side="back" onclick="construct.setSide('back')">
                            <img src="/constructor/{$product->type_name}-back.png" style="background-color:#ffffff;">
                        </div>*}
                    </div>
                </div>
            </div>
            <div class="page-layout__sidebar col-md-6">
                
                <form method="POST" action="/partner/add" data-type="construct" data-dynamic-price="false" data-cart="add" class="cart-add-holder">
                    <input type="hidden" name="id" value="{$product->id}">
                    <div class="product-page-options">
                        <div class="form-group row">
                            <label for="title" class="col-lg-2 col-sm-2 col-form-label">Название</label>
                            <div class="col-sm-8 field_wrap_text">
                                <input type="text" name="title" id="title" class="form-control text " value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="category_parent" class="col-lg-2 col-sm-2 col-form-label">Категория</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="category" id="category_parent" style="background: #fff">
                                    <option value="0">-</option>
                                    {foreach from=$categories item="cat"}
                                        <option value="{$cat->id}">{$cat->title}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="form-group row" id="category_child_row" style="display: none;">
                            <label for="category_child_holder" class="col-lg-2 col-sm-2 col-form-label">Подкатегория</label>
                            <div class="col-sm-8" id="category_child_holder">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-sm-2 col-form-label">Теги</label>
                            <div class="col-sm-8 field_wrap_text">
                                <input type="text" name="shop_tag[]" class="form-control tagit">
                            </div>
                        </div>
                        {foreach from=$attributes item=$attribute}
                          <div class="option-row option-row__{$attribute->name}" data-attr="{$attribute->name}">
                            <b class="row-title">{$attribute->title}</b>
                            <input type="hidden" name="attributes[{$attribute->name}]" value="-1">
                            <div class="option-variants-list">
                                {foreach from=$attribute->values key="k" item="item"}
                                  <div class="option-variant">
                                    <input name="attributes[{$attribute->name}]" type="radio" value="{$k}" {if $attribute->name == "color"}data-color="{$item->code}"{/if} id="option_{$attribute->name}_{$k}" {if isset($item->default)}checked{/if}>
                                    <label for="option_{$attribute->name}_{$k}" {if $attribute->name == "color"}style="background: {$item->code}"{/if}>
                                      <span>{$item->title}</span>
                                      {if !empty($item->description)}<span>{$item->description}</span>{/if}
                                    </label>
                                  </div>
                                {/foreach}
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        {/foreach}
                        <div class="store-item-info option-row actions-row mt-1">
                            <button type="submit" class="btn btn-secondary buy-button big full_width" data-text="Создать">Создать</button>
                        </div>
                        <div class="form_result" style="padding-right: 0px;"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

{foreach from=$fonts item="file" key="name"}
    <link rel="stylesheet" href="/ajax/fonts/{$name}.css">
{/foreach}

<script>
    $(function(){
        construct.init('{$product->type_name}');
        $('.option-variant:first input[name="attributes[color]"]').click();
    })
</script>
<script type="text/javascript">
    $("#category_parent").change(function(e){
        e.preventDefault();

        $("#category_child_row").hide();
        $("#category_child_holder").html("");

        cart.stateDisable();

        var cat = $(this).find('option:selected').val();
        $.post('/partner/get_child_cats/' + cat, function(data){
            var cats = JSON.parse(data);

            if(cats.length > 0 && cat != 0){
                $("#category_child_row").show();
                $("#category_child_holder").append('<select class="form-control" name="category" id="category_child" style="background: #fff"></select>');
                $.each(cats, function(key, item){
                    $("#category_child").append('<option value="'+item.id+'">'+item.title+'</option>');
                })
                cart.stateEnable();
            }else{
                cart.stateEnable();
                $("#category_child_row").hide();
            }
        })
    })
    $(".text-controls__fonts-select").click(function(e){
        e.preventDefault();
        $(this).toggleClass('opened');
    })
</script>
<script>
    {literal}
        $(function(){
            $('.tagit').tagit({
                availableTags: {/literal}{$shop_tags}{literal},
                fieldName: 'shop_tag[]',
                tagLimit: 30,
                allowSpaces: false,
                onTagLimitExceeded: function(){
                    jQuery(this).parent().children('.error').show();
                },
                onTagRemoved: function(){
                    jQuery(this).parent().children('.error').hide();
                }
            });
        })
    {/literal}
</script>