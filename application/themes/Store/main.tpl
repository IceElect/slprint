<!DOCTYPE html>
<html lang="en">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
    <title>{$pageTitle}</title>

    <meta charset="UTF-8">
    {include file="sys/meta.tpl"}
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <link rel="canonical" href="{$base_url}">
</head>
<body>
    {include file="sys/js_css_inc.tpl" place="header"}
    {include file="sys/js_inc.tpl" place="init"}

    <div class="popup_layer"></div>

    <div class="topline">
        <div class="topline-container container">
            <div class="topline-menu">
                <a href="/" class="topline-link topline-menu__item">Принт</a>
                <a href="https://events.slto.ru" class="topline-link topline-menu__item">Мероприятия</a>
            </div>
            <div class="topline-user">
                {if $user->is_logged()}
                <a href="/partner/dashboard" class="topline-user-block" style="margin-right: 20px">
                    <span class="topline-user-name">Партнерский кабинет</span>
                </a>
                <a href="/profile" class="topline-user-block">
                    <div class="topline-user-avatar avatar-circle" style="background-image: url({get_avatar u=$oUser link=true});"></div>
                    <span class="topline-user-name">{$oUser->fname} {$oUser->lname}</span>
                </a>
                {else}
                <a href="#" class="topline-user-block login-button">
                    <span class="topline-user-name">Войти в аккаунт</span>
                </a>
                {/if}
            </div>
        </div>
    </div>
    <div class="header">
        <div class="header-container container">
            <div class="header-top">
                <div href="#" id="header-toggle-nav" class="header-burger">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <a href="/" class="header-logo">
                    <span class="header-logo-project">PRINT</span>
                    <div class="header-logo-icon">BAND</div>
                </a>
                <div class="header-middle">
                    <div class="header-middle-wrap">
                        <form action="/search" class="header-search">
                            <div class="header-search-box">
                                <input type="text" class="header-search-input" placeholder="Найти товар">
                            </div>
                            <button class="button button-color button-size-ml header-search-submit">
                                <span class="button-text">ИСКАТЬ</span>
                            </button>
                        </form>
                    </div>
                </div>
                <a href="/constructor" class="header-button tablet-show">
                    <div class="header-button-icon">
                        <i class="fa fa-palette"></i>
                    </div>
                </a>
                <a href="tel:+74950057248" class="header-contact">
                    <div class="header-contact-box">
                        <div class="header-contact-icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="header-contact-text">
                            <span class="header-contact-text-data">+7 (495) 005-72-48</span> <span class="header-contact-text-desc">Пн - Вс, 10:00 - 22:00</span>
                        </div>
                    </div>
                </a>
                <a href="/cart" class="header-cart {if !isset($is_cart) && $cart_data.count_products > 0}header-cart--active{/if}">
                    <div class="header-cart-box">
                        <div class="header-cart-icon">
                            <i class="fa fa-shopping-cart"></i>
                            <span class="header-cart-counter">{$cart_data.count_products}</span>
                        </div>
                        <div class="header-cart-text">
                            <span class="header-cart-text-lots" data-cart="count-text">{$cart_data.count_products_text}</span> <span class="header-cart-text-cost" data-cart="total-products-text">{$cart_data.total_products_text}</span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="header-bottom">
                <div class="header-catalog-button">
                    <a href="/constructor" class="button button-color">Свой дизайн</a>
                </div>
                <nav class="header-nav">
                    <a href="/constructor" class="header-nav-item">Конструктор</a>
                    <a href="/partner/start" class="header-nav-item">Дизайнерам</a>
                    <a href="/about" class="header-nav-item">О магазине</a>
                    <a href="/delivery" class="header-nav-item">Доставка</a>
                    <a href="#" onclick="return false;" class="header-nav-item"></a>
                    {if $user->is_logged()}
                        <a href="/profile" class="header-nav-item tablet-show">{$oUser->fname} {$oUser->lname}</a>
                        <a href="/partner/dashboard" class="header-nav-item tablet-show">Партнерский кабинет</a>
                        <a href="/users/logout" class="header-nav-item tablet-show">Выход</a>
                    {else}
                        <a href="#" onclick="popup.show('login');toggleMobileMenu();return false;" class="header-nav-item header-toggle-nav tablet-show">Войти в аккаунт</a>
                    {/if}
                </nav>
            </div>
        </div>
    </div>

    {$content}

    <!-- <div class="page-main container">
        <div class="page-main__row">
            <div class="page-main__header">
                <div class="page-main__title">
                    <h1 class="page-main__title-text title-h1_big">Список каналов</h1>
                </div>
            </div>
        </div>
    </div> -->

    <script>var cart_count = {$cart_data.count_products};</script>
    {include file="sys/js_inc.tpl" place="footer"}

    <script>
        $(".login-button").click(function(e){
            e.preventDefault();
            popup.show('login');
        })
    </script>

    {if $is_production}
    {literal}
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-134453072-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-134453072-1');
    </script>
    <div style="display: none">
        <!--LiveInternet counter--><script type="text/javascript">
        document.write("<a href='//www.liveinternet.ru/click' "+
        "target=_blank><img src='//counter.yadro.ru/hit?t44.6;r"+
        escape(document.referrer)+((typeof(screen)=="undefined")?"":
        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
        screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
        ";h"+escape(document.title.substring(0,150))+";"+Math.random()+
        "' alt='' title='LiveInternet' "+
        "border='0' width='31' height='31'><\/a>")
        </script><!--/LiveInternet-->
    </div>
    {/literal}
    {/if}
</body>
</html>