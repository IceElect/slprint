<div class="popup_bg"></div>
<div class="popup block" data-id="product_add" width="600px">
    <div class="popup_header_holder">
        <div class="popup_header module_title">Добавление товара <button class="md-icon close" onclick="popup.hide('product_add');">close</button></div>
    </div>
    <div class="popup_content module_content">
		<form method="post" action="/admin/products/add" class="form form-horizontal" data-act="product_add" data-type="ajax">
			<input type="hidden" name="act" value="product_add">
			<div class="form-row clearfix">
				<div class="form-label">
					<label for="cat_id">Категория</label>
				</div>
				<div class="form-input-wrap">
					<select name="cat_id" id="cat_id" class="field">
						{foreach from=$cat_tree key="option_key" item="option" }
                            <option value="{$option_key}" {if $cat_id == $option_key} selected {/if}>{$option}</option>
                        {/foreach}
					</select>
				</div>
			</div>
			<div class="form-row clearfix">
				<div class="form-label">
					<label for="type">Тип товара</label>
				</div>
				<div class="form-input-wrap">
					<select name="type" id="type" class="field">
                            <option value="1">Мужская футболка</option>
                            <option value="2">Женская футболка</option>
                            <option value="4">Белая кружка</option>
					</select>
				</div>
			</div>
			<div class="form-row clearfix">
				<div class="form-label">
					<label for="name">Название товара</label>
				</div>
				<div class="form-input-wrap">
					<input type="text" name="name" id="name" class="field">
				</div>
			</div>
			<div class="form-row clearfix">
				<div class="form-label"></div>
				<div class="form-input-wrap">
					<button type="submit" name="submit" value="1" class="button">Создать</button>
					<button type="button" class="button gray" onclick="popup.hide('product_add');">Отмена</button>
					<span class="form_result" style="line-height: 28px;margin-left: 3px"></span>
				</div>
			</div>
		</form>
        <div class="clearfix"></div>
    </div>
</div>