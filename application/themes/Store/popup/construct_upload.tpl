<div class="popup_bg"></div>
<div class="popup block" data-id="construct_upload">
    <div class="popup_header_holder">
        <div class="popup_header module_title">
            Загрузить изображение <button class="md-icon close" onclick="popup.hide('construct_upload');"><i class="fal fa-times ft-x"></i></button>
        </div>
    </div>
    <div class="popup_content module_content">
        <b>Ограничения</b>
        <ul>
            <li>Разрешение изображения должно быть не менее 3000x3000 пикселей</li>
            <li>Файл должен иметь размер не более 20 МБ и расширения PNG, JPEG или GIF</li>
        </ul>

        <div class="clearfix"></div>

        <div class="file-upload text-center">
            <input id="file" type="file" style="display: none;" onchange="upload.onFile('construct', 0, this.files);" size="28" accept="image/jpeg,image/png,image/gif,image/heic,image/heif">
            <label for="file" class="button button-color btn btn-secondary">Загрузить файл</label>
        </div>
        
        <div class="load-info">
            <span id="message"></span>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!--<script src="/views/Social/js/drop_load.js"></script>-->