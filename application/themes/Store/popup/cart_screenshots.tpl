<div class="popup_bg"></div>
<div class="popup block" data-id="cart_screenshots">
    <div class="popup_header_holder">
        <div class="popup_header module_title">{$product.name} <button class="md-icon close" onclick="popup.hide('cart_screenshots');">close</button></div>
    </div>
    <div class="popup_content module_content">
        {foreach from=$product.previews item="preview"}
        	<!-- <img src="{$preview}" width="100%" alt=""> -->
        {/foreach}
    </div>
</div>