<div class="popup_bg"></div>
<div class="popup block" data-id="login" width="600px">
    <div class="popup_header_holder">
        <div class="popup_header module_title">Войти в аккаунт <button class="md-icon close" onclick="popup.hide('avatar_upload');"><i class="fal fa-times"></i></button></div>
    </div>
    <div class="popup_content module_content">
		<form method="post" action="/user/login" class="form" data-act="login" data-type="ajax">
			<input type="hidden" name="act" value="login">
			<fieldset>
				<label for="login">Логин/E-mail</label>
				<input type="text" name="user_email" id="login" class="field">
			</fieldset>
			<fieldset>
				<label for="pass">Пароль</label>
				<input type="password" name="user_password" id="pass" class="field">
			</fieldset>
			<fieldset>
				<button type="submit" name="submit" value="1" class="button button-color">Войти</button>
				<a class="button button-primary" href="{$hosted_url}?destination" onclick="popup.show('register');return false;">Регистрация</a>
                <a class="button" href="https://oauth.vk.com/authorize?client_id=7176300&display=popup&scope=friends,email,status,stats&response_type=code&state={$base_url}&redirect_uri={$base_url}users/vk_auth"><i class="fab fa-vk"></i></a>
				<span class="form_result" style="line-height: 28px;margin-left: 3px"></span>
			</fieldset>
		</form>
        <div class="clearfix"></div>
    </div>
</div>