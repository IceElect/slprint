<div class="popup_bg"></div>
<div class="popup block" data-id="login">
    <div class="popup_header_holder">
        <div class="popup_header module_title">Добавить в заказ <button class="md-icon close" onclick="popup.hide('add_after');">close</button></div>
    </div>
    <div class="popup_content module_content thumb cart-item" style="margin-top: 0px">
        <div class="cart-item-inner">
            <a href="/product/{$product.url}" class="cart-item-image">
                <img src="/{$product.previews[0]}" alt="{$product.name}">
            </a>
            <div class="cart-item-info">
                <div class="cart-item-info-inner">
                    <a href="/product/{$product.url}" class="cart-item-info-title">
                        {$product.name}
                        <span>{$product.art}</span>
                    </a>
                    <div class="cart-item-info-attributes">
                        {foreach from=$product.all_options key="key" item="attribute"}
                        <div class="cart-item-info-{$attribute->name}">
                            <select name="{$attribute->name}" id="{$attribute->name}" class="field">
                                {foreach from=$attribute->values key="k" item="a"}
                                    <option value="{$k}" {if isset($product.options[$attribute->name])}{if $product.options[$attribute->name] == $k}selected{/if}{/if}>{$a}</option>
                                {/foreach}
                            </select>
                        </div>
                        {/foreach}
                    </div>
                </div>
            </div>
            <div class="cart-item-price">
                {if $product.discount}<span class="price-was">{$product.was_price}р.</span>{/if}
                <span class="price">{$product.price}р.</span>
            </div>
        </div>
    </div>
    <a href="#" class="button full_width big">Добавить</a>
</div>