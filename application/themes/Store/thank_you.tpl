<div class="page-main blank-page container">
    <div style="width: 100%">
        <div class="blank-content">
            <div class="blank-row">
                <div style="display: flex;flex-direction: column;">
                    <div class="blank-header">
                        <h1 class="blank-title">Это успех, друг!</h1>
                    </div>
                    <div class="blank-description" style="flex: 1">
                        <p>Ваш заказ успешно оформлен. О любом изменении статуса мы будем информировать вас по электронной почте. Спасибо!</p>
                    </div>
                    <a href="/" class="button button-color full_width">На главную</a>
                </div>
                <div class="spacer"></div>
                {*
                <div class="thumb" style="display: flex;flex-direction: column;">
                    <div class="thumb-inner" style="flex: 1;height: 100%;">
                        <p><b>Получите скидку на следующий заказ</b></p>
                        <p>Вы можете получить индивидуальный купон на скидку по завершению этого заказа, если создадите аккаунт.</p>
                    </div>
                    <div class="buttons-group" style="flex: inherit">
                        <a href="#" class="button button-size-ml full_width">Создать аккаунт</a>
                    </div>
                </div>
                *}
                <div class="thumb" style="display: flex;flex-direction: column;">
                    <div class="thumb-inner" style="flex: 1;height: 100%;">
                        <p><b>Наши менеджеры связутся с вами в течении 30 минут</b></p>
                        <div class="spacer"></div>
                        <!-- <p>Наши контакты:</p> -->
                        <div>
                            <i class="far fa-fw fa-clock" style="font-size: 1.5em;vertical-align: middle;"></i> 
                            <b style="vertical-align: middle;">Пн-вс, 10:00-21:00</b>
                        </div>
                        <div style="margin-top: 8px;">
                            <i class="fa fa-fw fa-phone-square" style="font-size: 1.5em;vertical-align: middle;"></i> 
                            <b style="vertical-align: middle;">+7(495)005-72-48</b>
                        </div>
                        <div style="margin-top: 8px;">
                            <i class="fa fa-fw fa-envelope-square" style="font-size: 1.5em;vertical-align: middle;"></i>
                            <b style="vertical-align: middle;">print@slto.ru</b>
                         </div>
                    </div>
                </div>
            </div>
            {*
            <section class="blank-row">
                <div class="thumb blank-order">
                    <div class="thumb-inner">
                        
                    </div>
                </div>
            </section>
            
            <section style="margin-top: 40px">
                <h2>Добавите в заказ кружку со скидкой 10%</h2>
                <div class="store-products">
                    <div class="store-item thumb">
                        <a href="/product/first-product" class="store-item-thumb ">
                            <img src="/uploads/prints/10/front-preview.png" alt="Лучшая футболка" title="Лучшая футболка купить">
                            <span class="store-item-discount button">-10%</span></a>
                        <a href="/product/first-product" class="store-item-title">Это правда кружка!</a>
                        <span class="store-item-cat">Кружки</span>
                        <div class="store-item-info">
                            <a href="#" onclick="popup.setURL('/cart/');popup.show('add_after', {literal}{id: 1, count: 1}{/literal});return false;" class="buy-button button"><i class="fa fa-plus"></i> Добавить</a>
                            <div class="store-item-price">
                                <span class="price">300</span> 
                                <span class="dis-price">270</span> 
                                <i class="fa fa-rub"></i>
                            </div>
                        </div>
                    </div>
                    <div class="store-item thumb">
                        <a href="/product/first-product" class="store-item-thumb ">
                            <img src="/uploads/prints/10/front-preview.png" alt="Лучшая футболка" title="Лучшая футболка купить">
                            <span class="store-item-discount button">-10%</span></a>
                        <a href="/product/first-product" class="store-item-title">Это правда кружка!</a>
                        <span class="store-item-cat">Кружки</span>
                        <div class="store-item-info">
                            <a href="/product/first-product" class="buy-button button"><i class="fa fa-plus"></i> Добавить</a>
                            <div class="store-item-price">
                                <span class="price">300</span> 
                                <span class="dis-price">270</span> 
                                <i class="fa fa-rub"></i>
                            </div>
                        </div>
                    </div>
                    <div class="store-item thumb">
                        <a href="/product/first-product" class="store-item-thumb ">
                            <img src="/uploads/prints/10/front-preview.png" alt="Лучшая футболка" title="Лучшая футболка купить">
                            <span class="store-item-discount button">-10%</span></a>
                        <a href="/product/first-product" class="store-item-title">Это правда кружка!</a>
                        <span class="store-item-cat">Кружки</span>
                        <div class="store-item-info">
                            <a href="/product/first-product" class="buy-button button"><i class="fa fa-plus"></i> Добавить</a>
                            <div class="store-item-price">
                                <span class="price">300</span> 
                                <span class="dis-price">270</span> 
                                <i class="fa fa-rub"></i>
                            </div>
                        </div>
                    </div>
                    <div class="store-item thumb">
                        <a href="/product/first-product" class="store-item-thumb ">
                            <img src="/uploads/prints/10/front-preview.png" alt="Лучшая футболка" title="Лучшая футболка купить">
                            <span class="store-item-discount button">-10%</span></a>
                        <a href="/product/first-product" class="store-item-title">Это правда кружка!</a>
                        <span class="store-item-cat">Кружки</span>
                        <div class="store-item-info">
                            <a href="/product/first-product" class="buy-button button"><i class="fa fa-plus"></i> Добавить</a>
                            <div class="store-item-price">
                                <span class="price">300</span> 
                                <span class="dis-price">270</span> 
                                <i class="fa fa-rub"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            *}
        </div>
    </div>
</div>
<script>
    $(function(){
        popup.setURL('/cart/');
    })
</script>