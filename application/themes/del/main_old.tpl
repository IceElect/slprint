<!DOCTYPE html>
<html lang="en">
<head {if isset($post)}prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#"{/if}>
	<meta charset="UTF-8">
	<title>{$pageTitle}</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="canonical" href="{$base_url}">

	{include file="sys/meta.tpl"}

	
	<!--<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-->
	<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
</head>
<body>
	{include file="sys/js_css_inc.tpl" place="header"}
	<!-- <style>
    	body{
    		abackground: #efefef;
    	}
    	.popup_layer{
			width: calc( 100% - 68px );
		}
    	.page-content{
			width: calc( 100% - 88px );
			margin-right: 10px;
		}
    	.left-wrap .app-bar{
    		background: #bbcddb;
    		background: #222;
    	}
    	.app-bar .avatar {
		    background: #3f51b5;
		    background: rgba(0, 0, 0, 0.2);
		}
    	.main-col{
    		max-width: 1200px;
    	}
    	@media screen and (max-width: 1024px){
    		
    	}
    	@media screen and (max-width: 800px){
    		.main-col{
    			max-width: 100%;
    		}
    		.main-col .sidebar{
    			width: 100%;
    		}
    		.page-content{
				width: 100%;
				margin-right: 0px;
			}
			.thumbs{
				width: 100%;
			}
			.thumbs .thumb{
				display: block;
			}
			.thumbs .thumb .image-holder{
				width: 100%;
			}
			.thumb .image-holder .time{
				left: 0px;
			}
			.thumb .content-holder .info{
				position: relative;
				bottom: auto;
			}
    	}
		div.send-form-area{
			background: #f4f4f4;
			border: none;
		}

		.send-post-form .send-form-area{
			padding-right: 45px;
		}

		.send-post-form .avatar{
			padding-left: 0px;
		}

		.tooltip:after{
			background-color: #222!important;
		}
		.tooltip.tip-right:before{
			border-color: transparent #222 transparent transparent;
		}
		.app-bar-count{
			color: #fff;
			width: 18px;
			height: 18px;
			font-size: 11px;
			background: #d2383b;
			line-height: 16px;
			position: absolute;
			text-align: center;
			border-radius: 50px;
			margin-right: -5px;
			margin-bottom: -5px;
			border: .5px solid #222;
			left: 50%;
			top: 50%;
		}
		.app-bar-count:empty{
			display: none;
		}
    </style> -->

	{include file="sys/js_conf_inc.tpl"}
	<!--
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="/{$path}scripts/jquery-3.2.1.min.js"></script>
-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	{if isset($post)}
	<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
	<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
	{/if}
	{include file="sys/js_inc.tpl" place="init"}
	{include file="sys/js_inc.tpl" place="header"}

	<div class="popup_layer"></div>

	<div class="main">
		<div class="fll left-wrap">
			<!-- begin app-bar -->
			<div class="app-bar full-viewport-height main-flex fll">
				<nav role="navigation" class="left-nav simple-scrollbar">
					<ul class="flex-fill">
						<li class="tooltip tip-right" data-tip="Главная">
							<a href="/" class="app-bar-link">
								<span class="md-icon">home</span>
								<span class="app-bar-text">Главная</span>
							</a>
						</li>
						<li class="tooltip tip-right" data-tip="Каталог">
							<a href="/catalog" class="app-bar-link">
								<span class="md-icon">assignment</span>
								<span class="app-bar-text">Каталог</span>
							</a>
						</li>
						<li class="tooltip tip-right" data-tip="Конструктор">
							<a href="/constructor/" class="app-bar-link">
								<span class="md-icon">color_lens</span>
								<span class="app-bar-text">Конструктор</span>
							</a>
						</li>
						<li class="mobile-hide spacer"></li>
						<li class="tooltip tip-right" data-tip="Корзина">
							<a href="/cart" class="app-bar-link">
								<span class="md-icon">shopping_cart</span>
								<span class="app-bar-text">Корзина</span>
								<span class="app-bar-count" data-cart="count">{if $cart_count > 0}{$cart_count}{/if}</span>
							</a>
						</li>
						<li class="tooltip tip-right mobile-show mobile-hide {if !$oUser->id}login-button{/if}">
							<a href="/profile" class="app-bar-link">
								<span class="md-icon">account_circle</span>
								<span class="app-bar-text">Профиль</span>
							</a>
						</li>
						<li class="tooltip tip-right mobile-hide {if !$oUser->id}login-button{/if}" data-tip="{if $user->is_logged()}{$oUser->fname} {$oUser->lname}{else}Войти в аккаунт{/if}">
							<a href="/profile" class="app-bar-link avatar">
								{get_avatar u=$oUser}
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<!-- end app-bar -->

			<!-- begin app-content -->
			<!--
			<div class="app-content full-viewport-height fll">
				<div class="loader">
					<a><i class="fa fa-circle-o-notch fa-spin fa-3x"></i></a>
				</div>
				<div class="app-content-wrap">
					<div class="search">
						<div class="app-top">
							<div class="search-flex">
								<form action="#" class="search-form">
									<div class="group">
										<input class="field text-field search-field" type="text" placeholder="Поиск">
										<button class="icon icon-search search-but"></button>
									</div>
								</form>
								<button class="icon icon-pencil app-icon search-outer"></button>
							</div>
						</div>
					</div>
					<div class="tab-content"></div>
				</div>
			</div>
			<!-- end app-content -->
		</div>
		<div class="page-content">
			{$content}
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>

	<script>var cart_count = {$cart_count};</script>
	{include file="sys/js_inc.tpl" place="footer"}

	<script>
		var sidebar = '{$sidebar}';
		//load_sidebar(sidebar);
		jQuery(document).on('click', '[data-type="actions"]', function(e){
			e.preventDefault();
			var list = jQuery(this).parent().find('.actions-menu');
			if(jQuery('body').width()<480){
				list.width(jQuery(this).parent().parent().width());
			}
			if(list.hasClass('show')){
				list.removeClass('show');
			}else{
				list.addClass('show');
				if(!list.hasClass('loaded')){
					jQuery.post('/ajax/user/actions/'+jQuery(this).attr('data-user'), {}, function(data){
						list.addClass('loaded');
						data = eval('('+data+')');
						list.html(data.html);
					})
				}
				jQuery('.actions-menu').removeClass('show');
				list.toggleClass('show');
			}
		})
		jQuery(document).on('click', '.tree-header', function(e){
			var o = jQuery(this).find('.icon');
			var list = jQuery(this).parent().find(' > ul');
			if(o.hasClass('icon-down-dir')){
				list.hide();
				o.addClass('icon-right-dir').removeClass('icon-down-dir');
			}else{
				list.show();
				o.removeClass('icon-right-dir').addClass('icon-down-dir');
			}
		})
		jQuery(document).on('click', '.sapp-bar li a', function(e){
			e.preventDefault();
			var o = jQuery('.left-wrap .app-content');
			if(o.hasClass('show')){
				if(sidebar !== jQuery(this).attr('data-id'))
					return false;
				jQuery('body').css('overflow', 'auto');
				width = o.find('.app-content-wrap').width();
				o.animate({ left: '-'+parseInt(width + 20) }, 200).removeClass('show');
			}else{
				if(jQuery('body').width() < 480){
					jQuery('body').css('overflow', 'hidden');
					o.animate({ left: '0px' }, 200).addClass('show');
				}else if(jQuery('body').width() > 480 && jQuery('body').width() < 768){
					o.animate({ left: '42px' }, 200).addClass('show');
				}
			}
		})
		window.onresize = function(e) {
			var o = jQuery('.left-wrap .app-content');
			if(o.hasClass('show')){
				if(jQuery('body').width() < 480){
					o.animate({ left: '0px' }, 200).addClass('show');
				}else{
					o.animate({ left: '42px' }, 200).addClass('show');
				}
			}
		}

		$(".login-button").click(function(e){
			e.preventDefault();
			popup.show('login');
		})
	</script>
	{literal}
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134453072-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-134453072-1');
	</script>
	<div style="display: none;">
		<!--LiveInternet counter--><script type="text/javascript">
		document.write("<a href='//www.liveinternet.ru/click' "+
		"target=_blank><img src='//counter.yadro.ru/hit?t44.6;r"+
		escape(document.referrer)+((typeof(screen)=="undefined")?"":
		";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
		screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
		";h"+escape(document.title.substring(0,150))+";"+Math.random()+
		"' alt='' title='LiveInternet' "+
		"border='0' width='31' height='31'><\/a>")
		</script><!--/LiveInternet-->
	</div>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
	   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
	   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
	   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	   ym(52365232, "init", {
	        id:52365232,
	        clickmap:true,
	        trackLinks:true,
	        accurateTrackBounce:true,
	        webvisor:true
	   });
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/52365232" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
	{/literal}
</body>
</html>