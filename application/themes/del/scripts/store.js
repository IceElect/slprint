var order_form = $(".order-form:not(.form-block)");

function enterNumber(e, el){
    if (!/^[0-9]+$/.test(el.value)) { 
        el.value = el.value.substring(0,el.value.length-1);
        return false;
    }
    if(el.value < 1){
        el.value = 1;
    }
    if(el.value > 5){
        el.value = 5;
        return false;
    }
    if(el.value == null || el.value == "")
        el.value = 1;
}

$(document).on('click', '[data-count]', function(e){
    var field = $(this).parent().find('input');
    var field_val = field.val();
    var action = $(this).attr('data-count');

    if(action == 'plus'){
        field.val(parseInt(field_val)+1);
    }else{
        if(field_val > 1)
            field.val(parseInt(field_val)-1);
    }
})

function show_message(msg, msgClass) {
    var msg_div = $('<div/>').addClass('message').addClass(msgClass).html(msg);
    if ($('.message_holder').length) {
        $('.message_holder').show();
        $('.message_holder').append(msg_div);
    } else {
        $('#main').prepend($('<div/>').addClass('message_holder'));
        $('.message_holder').append(msg_div);
    }
}

function hide_messages() {
    $('.message_holder').hide('slow');
    $('.message_holder').html('');
}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$(order_form).parent().on('submit', function(e){
    e.preventDefault();
    var $that = jQuery(this),
    formData = new FormData($that.get(0));
    jQuery.ajax({
      url: $that.attr('action'),
      type: $that.attr('method'),
      contentType: false,
      processData: false,
      data: formData,
      dataType: 'html',
      beforeSend: function(){
          jQuery("body").addClass('loading');
          $that.find('.loading').show();
      },
      success: function(data){
        $that.find('.loading').hide();
        jQuery("body").removeClass('loading');
        $(order_form).find(".error").html("");
        var data = eval('('+data+')');
        if(data.response){
            $("body").addClass('page-blur');
            $(".modal-blur").show();
            $(".order-success").css('visibility', 'visible');
        }else{
            $(order_form).find(".error-summary").html(data.error);
        }
      }
    });
});

$(function(){
    var categories_list = $(".catalog-categories-list ul");
    if(categories_list.length > 0){
        var current_category = categories_list.find(".list-item--active");
        if(current_category.length > 0){
            shop.showChildCats(current_category.data('cat-id'), window.event);
        }
    }
})

function Shop(){
    var self = {};

    self.showChildCats = function(id, e){
        var list = $(".catalog-categories-list ul");
        var current = list.find('[data-cat-id="'+id+'"]');
        var children = current.find('ul');

        if(current.hasClass('list-item--active')){
            e.preventDefault();
            current.removeClass('list-item--active');
            return;
        }

        if(children.length > 0){
            e.preventDefault();
            list.find(".list-item--active").removeClass("list-item--active");

            current.addClass('list-item--active');
            children.html('<li class="loading"><i class="fa fa-spin fa-refresh"></i></li>');

            $.post('/ajax/product/get_child_cats/' + id, {url: window.location.href}, function(data){
                data = JSON.parse(data);

                if(data.length > 0){
                    children.html("");
                    var sclass = (selected_category == id)?"list-item--selected":"";
                    children.append('<li class="'+sclass+'"><a href="/catalog/?category_id='+id+'"><span class="list-item__name">Посмотреть все</span></a></li>');
                    $.each(data, function(key, item){
                        var sclass = (selected_category == item.id)?"list-item--selected":"";
                        children.append('<li class="'+sclass+'"><a href="'+item.url+'"><span class="list-item__name">'+ item.title +'</span><span class="list-item__counter">'+ item.product_count +'</span></a></li>');
                    })
                }

                selected_category = id;
            })

            return false;
        }
    }

    self.addComment = function(id){
        var text = $("[data-product-id="+id+"] .send-comment .send-form-area").html();
        var data = {text: text};
        var form = $("[data-product-id="+id+"] .send-comment-form");
        $("[data-product-id="+id+"] .send-comment .send-comment-result").hide();

        if(this.answer)
            data.answer = this.answer;

        $.ajax({
            type: 'POST',
            url: '/ajax/product/addComment/'+id,
            data: data,
            beforeSend: function () {
                form.addClass('form-loading');
                form.find('.send-button').attr('disabled', 'disabled');
            },
            success: function (data){
                data = JSON.parse(data);

                form.removeClass('form-loading');
                form.find('.send-button').attr('disabled', false);

                if(data.response != false){
                    //$("[data-product-id="+id+"] .comments-list").prepend(data.post.data);

                    $("[data-product-id="+id+"] .comments-count").html(parseInt($("[data-product-id="+id+"] .comments-count").html())+1);

                    $("[data-product-id="+id+"] .send-comment .send-form-area").html("");
                    $("[data-product-id="+id+"] .send-comment .send-comment-result").show();
                    $("abbr.time").timeago();
                }else{
                    if('error' in data)
                        error(data.error);
                }
            },
            error: function(){
                error('Ошибка 12');
            }
        });

        return false;
    }

    self.productFav = function(id, e, el){
        e.preventDefault();
        var icon = $(el).find('i');
        var is_fav = ($(icon).html() == 'favorite');

        $.post('/ajax/product/fav/' + id, function(data){
            data = JSON.parse(data);

            if(data.response){
                if(!is_fav){
                    $(icon).html('favorite');
                }else{
                    $(icon).html('favorite_bordera');
                }
            }
        })
    }

    return self;
}

var shop = new Shop;

$('[data-cart="add"]').on('submit', function(e){
    e.preventDefault();

    var id = $(this).attr('data-product-id');
    
    cart.add(id, e, $(this));

    return false;
})

function Cart(){
    var self = {};

    self.init = function(){
        self.eform = $("[data-cart=add]");
        self.ebutton = self.eform.find('button.buy-button');
        self.eblock_err = self.eform.find('.form_result');

        self.is_construct = (self.eform.data('type') == 'construct');

        self.id = self.eform.find('[name=id]').val();
        var attributes_o = self.eform.find('.option-variants-list:not(.not-dinamic) .option-variant');

        attributes_o.find('input').on('change', function(e){
            e.preventDefault();
            self.calcPrice();

            var attribute_name = $(this).parents('.option-row').data('attr');

            if(attribute_name == 'color'){

                var color = $(this).data('color');
                var color_name = $(this).val();

                if(self.is_construct){
                    construct.changeColor(color, color_name);
                }else{
                    self.changeColor(color, color_name);
                }
            }
        })

        // $(document).on('change', 'input[name="attributes[color]"]', function(e){
        //     e.preventDefault();

        //     var color = $("input[name='attributes[color]']:checked").data('color');
        //     var color_name = $("input[name='attributes[color]']:checked").val();

        //     if(self.props.background_type == 'image'){
        //         self.config.background_color_name = color_name;
        //     }else{
        //         self.config.background_color = color;
        //         self.config.background_color_name = color_name;
        //         $(".canvas-container").css('background-color', color);
        //     }
        // })
    }

    self.setCount = function(count){
        if(count == 0) count = '';
        $("[data-cart='count']").html(count);
    };

    self.calcPrice = function(){
        //console.log(attributes);

        if(self.eform.data('dynamic-price') == false){
            return false;
        }

        self.eblock_err.html('');
        self.stateDisable();

        var data = self.eform.serializeArray();

        if(self.is_construct){
            var constructData = construct.export();
            var sides = constructData.sides;
            var sides_count = 0;

            $.each(sides, function(key, item){
                if(item.length > 1){
                    sides_count++;
                }
            })

            if(sides_count < 1) sides_count = 1;

            data.push({name: 'sides_count', value: sides_count});
        }

        $.post('/cart/calcPrice', data, function(data){
            data = JSON.parse(data);

            self.eform.find('span.price').html(data.price);
            self.eform.find('span.dis-price').html(data.price_dis);

            if(data.multi_sides_price){
                self.eform.find('.store-item-price__description').show();
            }

            self.stateEnable();
        });
    }

    self.changeColor = function(color, color_name){
        var product = $('.product-page');
        var thumb_container = $('.product-page-image');

        var back_thumb = $('.product-page-image img[data-side="back"]');
        var front_thumb = $('.product-page-image img[data-side="front"]');

        back_thumb.attr('src', '');
        front_thumb.attr('src', '');

        var design_id = $(product).data('design-id');
        var product_type = $(product).data('product-type');

        if(back_thumb.length > 0){
            var url = '/ajax/product/preview/' + design_id + '/' + product_type + '/' + color_name + '/' + '1024' + '/' + 'back.png';
            back_thumb.attr('src', url);
        }

        if(front_thumb.length > 0){
            var url = '/ajax/product/preview/' + design_id + '/' + product_type + '/' + color_name + '/' + '1024' + '/' + 'front.png';
            front_thumb.attr('src', url);
        }
    }

    self.CdToFd = function(cd, fd, pre){
        $.each(cd, function(side_key, side) {
            var side_pre = pre + "[" + side_key + "]";
            fd.append(side_pre, false);
            if(side.length > 1){
                $.each(side, function(obj_key, obj){
                    var obj_pre = side_pre + "[" + obj_key + "]";
                    $.each(obj, function(param_key, param){
                        var param_pre = obj_pre + "[" + param_key + "]";

                        if(typeof param !== "function" && typeof param !== undefined && typeof param !== "object"){
                            fd.append(param_pre, param);
                        }
                    })
                })
            }
        })
    }

    self.CdConfToFd = function(cd, fd, pre){
        $.each(cd, function(param_key, param){
            var param_pre = pre + "[" + param_key + "]";

            if(typeof param !== "function" && typeof param !== undefined && typeof param !== "object"){
                fd.append(param_pre, param);
            }
        })
    }

    self.add = function(id, e, el){
        e.preventDefault();

        var button = $(el).find('.buy-button');
        var err_block = $(el).find('.form_result');
        var success_btn = $(el).find('.button.to-cart');

        var formData = new FormData(el.get(0));

        if(self.is_construct){
            var constructData = construct.export();
            self.CdToFd(constructData.sides, formData, 'sides');
            self.CdConfToFd(constructData.config, formData, 'config');
            //formData.append('sides', constructData.sides);
            //formData.append('config', JSON.stringify(constructData.config));

            formData.append('construct', 1);

            var sides = constructData.sides;
            var sides_count = 0;

            $.each(sides, function(key, item){
                if(item.length > 1){
                    sides_count++;
                }
            })

            if(sides_count < 1) sides_count = 1;

            formData.append('sides_count', sides_count);
        }
        
        jQuery.ajax({
          url: el.attr('action'),
          type: "POST",
          contentType: false,
          processData: false,
          data: formData,
          beforeSend: function(){
            $(err_block).html("");
            self.stateDisable();
          },
          success: function(data){
            data = JSON.parse(data);
            self.stateEnable();
            if(data.response){

                if('redir' in data){
                    document.location.href = data.redir;
                }

                cart_count++;
                self.setCount(cart_count);
                $(button).hide();
                $(success_btn).show();
                $(button).html($(button).data('success-text'));
            }else{
                $(err_block).html(data.error);
                $(button).html($(button).data('text'));
            }
          }
        });

        return false;
    }

    self.addAfter = function(){

    }

    self.delete = function(event){
        event = event || window.event;
        event.preventDefault();
        var el = event.target;
        var tr = jQuery(el).parents('.cart-item');
        $.ajax({
            type: 'POST',
            url: '/cart/edit_cart',
            dataType: 'json',
            data: {'ajax': 1, 'action':'delete', 'id': tr.attr('data-rowid')},
            success: function (data) {
                tr.remove();
                $('[data-cart="total"]').html(data.total);
                $('[data-cart="num"]').html(data.total_items);
                if (jQuery('.cart_row').length == 0){
                    jQuery('.cart_place_order').hide();
                }

                cart_count--;
                self.setCount(cart_count);
            },
            async: false
        });
    }

    self.stateDisable = function(){
        self.ebutton.attr("disabled", "disabled");
        self.ebutton.html('<i class="fa fa-spin fa-refresh"></i>');;
    }
    self.stateEnable = function(){
        self.ebutton.attr("disabled", false);
        self.ebutton.html(self.ebutton.data('text'));
    }

    self.promo = function(e, el){
        var form = $(el).parents('.promo-form');
        var holder = $(el).parents('#promo');

        var price = $('[data-cart="total"]');
        var eresult = holder.find('.promo-text');

        var code = form.find('input[name="promo"]').val();

        $.post('/cart/promo', {promo: code}, function(data){
            data = JSON.parse(data);

            if(data.response){
                form.hide();

                price.html(data.total);

                eresult.html(data.text);
                eresult.show();
            }else{
                form.show();
                eresult.hide();

                form.addClass('error');
            }
        })
    }

    self.set_promo = function(total, text){
        var form = $('.promo-form');
        var holder = $('#promo');

        var price = $('[data-cart="total"]');
        var eresult = holder.find('.promo-text');

        form.hide();

        price.html(total);

        eresult.html(text);
        eresult.show();
    }

    self.calcTotal = function(e, el){
        e.preventDefault();

        var form = $('form[data-act="order"]');
            delivery_type = form.find("#delivery_type option:selected").val();

        self.ebutton = form.find('.button.big');

        self.stateDisable();

        $.post('/cart/calcTotal', {delivery_type: delivery_type}, function(data){
            data = JSON.parse(data);

            $('[data-cart="total"]').html(data.total);
            $('[data-cart="total-count"]').html(data.total_items_text);
            $('[data-cart="delivery_price"]').html(data.delivery_price);

            self.stateEnable();
        })
    }

    return self;
}

$(document).on('keyup', ".form-row.has-error input", function(e){
    e.preventDefault();
    $(this).parents('.form-row').removeClass('has-error');
})

var cart = new Cart;
cart.init();