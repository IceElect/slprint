<?php
/**
 * Smarty plugin
 *
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {translate} function plugin
 *
 * Type:     function<br>
 * Name:     translate<br>
 * Date:     Aug 10, 2014<br>
 * Purpose:  translate text via tranlate library
 * Examples: {translate text="Войти" code="login_button"}
 * Output:   Войти
 * Params:
 * <pre>
 * - code        - (required) - translate code
 * - text        - (required) - translate text
 * </pre>
 *
 * @author Jekson (jekson.com.ua)
 * @version 1.0
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string
 * @uses CI translate library()
 */
function smarty_function_product_img($params, $template)
{

    $design_id = '';
    $type = '';
    $color = false;
    $side = 'front';
    $design = false;
    $size = 360;
    foreach($params as $_key => $_val) {
        switch ($_key) {
            case 'design_id':
            case 'type':
            case 'color':
            case 'side':
            case 'design':
            case 'size':
                $$_key = $_val;
                break;
        }
    }

    if (empty($design_id)) {
        trigger_error("product_img: missing 'design_id' parameter", E_USER_NOTICE);
        return false;
    }

    if (empty($type)) {
        trigger_error("product_img: missing 'type' parameter", E_USER_NOTICE);
        return false;
    }
    $CI =& get_instance();
    $CI->load->library('Shop_lib');
    $uri = '/' . $CI->shop_lib->get_product_preview_path($design_id, $type, $color, $side, $design, $size);
    return $uri;


}

?>