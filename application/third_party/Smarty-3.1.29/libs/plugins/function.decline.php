<?php
/**
 * Smarty plugin
 *
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {translate} function plugin
 *
 * Type:     function<br>
 * Name:     birthday<br>
 * Date:     Sen 17, 2017<br>
 * Purpose:  
 * Examples: {birthday date="0000-00-00 00:00:00"}
 * Output:   
 * Params:
 * <pre>
 * - date        - (required) - birthday date
 * </pre>
 *
 * @author Elect (slto.ru/elect)
 * @version 1.0
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string
 * @uses CI translate library()
 */
function smarty_function_decline($params, $template)
{

    $text = '';
    $number = '';
    $eArray = array();
    foreach($params as $_key => $_val) {
        switch ($_key) {
            case 'text':
            case 'number':
            case 'eArray':
                $$_key = $_val;
                break;
        }
    }

    $CI =& get_instance();
    $CI->load->helper('number_helper');

    echo num_decline($text, $number, $eArray);
}

?>