<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Default_model extends CI_Model
{


    var $table;
    var $translate_table = 'translate';

    /**
     * Constructor
     *
     * @return    void
     */
    function __construct()
    {
        parent::__construct();
        $this->table = "block";
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    function getDataCountbyFilter()
    {
        $sql = "SELECT
                   count(*) as count
            FROM " . $this->db->dbprefix.$this->table;
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result->count;
    }

     function getNextId($prevId){
        $this->db->select('id');
        $this->db->from($this->table);
        $this->db->where('id > '.$prevId);
        $this->db->limit(1, 0);

        $query = $this->db->get();
        $result = $query->row();

        if(!$result)
            return false;
        
        return $result->id;
    }

    function getDataByWhere($fp_aFilter)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($fp_aFilter);
        //dump($this->db->last_query(),0);
        $query = $this->db->get();
        //dump($this->db->last_query());
        return $query->result();
    }

    function datatable($fp_aFilter, $is_count = false, $limit = false, $order = false){
        if ($is_count) {
            $this->db->select('count(*) as num');
        } else {
            $this->db->select('*');
        }
        $this->db->from($this->table);
        if (!empty($fp_aFilter)) {
            if (!empty($fp_aFilter['search'])){
                if (!empty($fp_aFilter['search']['fields'])) {
                    $_where = array();
                    $search_value = addslashes($fp_aFilter['search']['value']);
                    foreach ($fp_aFilter['search']['fields'] as $field){
                        if ($field['type'] != 'select') {
                            $_where[] = $field['field'] . ' LIKE "%' . $search_value . '%"';
                        } else {
                            $o_list = array();
                            foreach ($field['options'] as $o_key => $o_value){
                                if (strripos($o_value, $search_value) !== false){
                                    $o_list[] = $o_key;
                                }
                            }
                            if (!empty($o_list)){
                                $_where[] = $field['field'] . ' IN (' . implode(',', $o_list) . ')';
                            }
                        }
                    }

                    $_where = '(' . implode(' OR ', $_where) . ')';
                    $this->db->where($_where);
                }
                unset($fp_aFilter['search']);
            }
            $this->db->where($fp_aFilter);

        }
        if (!empty($limit)) {
            $this->db->limit($limit['limit'], $limit['offset']);
        }
        if (!empty($order)) {
            $this->db->order_by($order['column'], $order['dir']);
        }
        $query = $this->db->get();
        //dump($this->db->last_query());
        if ($is_count) {
            return $query->result_array()[0]['num'];
        } else {
            return $query->result_array();
        }
    }

    function getDataByOrWhere($fp_aFilter)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->or_where($fp_aFilter);
        //dump($this->db->last_query(),0);
        $query = $this->db->get();
        //dump($this->db->last_query());
        return $query->result();
    }

    function getDataForSelect($field, $is_translabel = false, $prefix = false, $lang = false){
        $display_field = $field;
        $data = $this->get_ordered_all_data($field);
        $result = array();
        if (!empty($data)){
            foreach($data as $key => $row){
                $result[$row->id] = $row->{$display_field};
            }
        }
        return $result;
    }

    function getDatabyFilter($fp_aFilter, $fp_sidx, $fp_sord)
    {
        $this->db->select('*');
        if (!empty($fp_aFilter['filters']->rules)) {
            foreach ($fp_aFilter['filters']->rules as $key => $value) {
                $this->set_filter_function($this->db, $value->op, $value->field, $value->data, $fp_aFilter['filters']->groupOp);
            }
        }
        $this->db->order_by($fp_sidx, $fp_sord);
        if (!isset($fp_aFilter['limit'])) {
            $query = $this->db->get_where($this->table, $fp_aFilter['fields']);
        } else {
            $query = $this->db->get_where($this->table, $fp_aFilter['fields'], $fp_aFilter['limit'], $fp_aFilter['offset']);
        }
        //dump($this->db->last_query());
        return $query->result();
    }

    function check_duplicate($fp_params)
    {
        foreach ($fp_params as $key => $value) {
            //if (!empty($value)) {
                if ($key == 'id') {
                    $key = $key . ' !=';
                }
                $this->db->where($key, $value);
            //}
        }
        $this->db->from($this->table);
        $this->db->select('*');
        $res = $this->db->count_all_results();
        return $res ? true : false;
    }

    function getDataById($fp_nId, $table = false)
    {
        if (!$fp_nId) return false;
        if (!$table) {
            $table = $this->table;
        }
        $query = $this->db->get_where($table, array('id' => $fp_nId));
        $data = $query->row_array();
        //dump($this->db->last_query(),0);
        return $data;
    }

    function getDataByIdList($fp_rowId)
    {
        if (!$fp_rowId) return false;
        foreach ((array)$fp_rowId as $k => $id) {
            $this->db->or_where('id', $id);
        }
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }

    function getAllData($select = '*')
    {
        $this->db->select($select)->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    function save($aData, $subact = 'edit', $fp_nId = '')
    {
        if ($subact == 'add') {
            $res = $this->db->insert($this->table, $aData);
            $res = $this->db->insert_id();
        } else {
            if (!$fp_nId) return false;
            $res = $this->db->update($this->table, $aData, "id = '" . $fp_nId . "'");
        }
        if (!$res) {
            
            return false;
        } else {
            if ($subact == 'add') {
                return $res;
            } else {
                return $fp_nId;
            }
        }
    }

    function saveIfNotExists($where, $data){
        $this->db->select('id')
            ->from($this->table)
            ->where($where);
        $query = $this->db->get();
        $res = $query->result();
        if(!isset($res[0])){
            return $this->save($data, 'add');
        }
        return $res[0]->id;
    }

    function isValidLogin($fp_sLogin, $fp_nId = '')
    {
        if (!$fp_sLogin) return false;

        $this->db->from($this->table);
        $this->db->where('login', $fp_sLogin);
        if ($fp_nId) {
            $this->db->where('id !=', $fp_nId);
        }
        $res = $this->db->count_all_results();
        //dump($this->db->last_query());
        return $res ? false : true;
    }

    function isValidId($fp_nId)
    {
        if (!$fp_nId) return false;
        $this->db->from($this->table);
        $this->db->where('id', $fp_nId);
        $res = $this->db->count_all_results();
        return $res ? true : false;
    }

    /**
     * Delete data by id
     *
     * @param    array $fp_rowId = array('Key_Row' => 'ID')
     * @return    void
     */
    function del($fp_rowId)
    {
        foreach ((array)$fp_rowId as $k => $id) {
            $this->db->or_where('id', $id);
        }
        $this->db->from($this->table);
        $this->db->delete($this->table);
        //dump($this->db->last_query());
        return true;
    }

    function delWhere($where)
    {
        if (empty($where)) return false;
        $this->db->where($where);
        $this->db->from($this->table);
        $result = $this->db->delete($this->table);
        //dump($this->db->last_query());
        return $result;
    }

    function inc_view($item_type, $item_id){
        $data = array(
            'ip' => $this->input->ip_address(),
            'date' => time(),
            'user_id' => $this->user_id,
            $item_type . '_id' => $item_id,
        );

        return $this->db->insert($item_type . '_views', $data);
    }


    /**
     * update multiple rows
     *
     * @param $data = array()
     * @return void
     */
    function updateRow($data)
    {
        $this->db->where($data['where']);
        $res = $this->db->update($this->table, $data['update']);
        return $res;
    }

    function increment($where, $column, $on = 1){
        $this->db->where($where);
        $this->db->set($column, $column.'+'.$on, FALSE);
        $this->db->update($this->table);
    }


    /**
     * update multiple rows
     *
     * @param $data = array()
     * @param $where = array()
     * @return void
     */
    function update($data, $where)
    {
        $this->db->where($where);
        $res = $this->db->update($this->table, $data);
        //dump($this->db->last_query());
        return $res;
    }

    function get_screenshots($item_col, $item_id){
        $this->db
            ->select('*')
            ->from($item_col.'_screenshots')
            ->order_by('weight', 'ASC')
            ->where($item_col.'_id', $item_id);
        $query = $this->db->get();
        $result = $query->result();
        $return = array('preview' => array(), 'config' => array());
        foreach($result as $key => $value){
            $return['preview'][] = $value->image_path;
            $return['config'][] = (object) array('caption' => $key.'.png', 'size' => $value->size, 'key' => $value->weight);
        }

        return $return;
    }

    function save_screenshots($item_col, $item_id, $screenshots_list)
    {
        $this->db->where($item_col.'_id', $item_id);
        //$this->db->delete($item_col.'_screenshots');
        if (!empty($screenshots_list)) {
            foreach ($screenshots_list as $key => $screen) {
                $this->db->insert($item_col.'_screenshots', $screen);
            }
        }
    }

    function get_tags($item_col, $item_id, $table = 'tag')
    {
        $this->db
            ->select('ts.*')
            ->from($item_col . '_' .  $table . ' t')
            ->join($table . ' ts', ' ts.id = t.'.$table . '_id', 'left')
            ->where_in($item_col . '_id', $item_id);
        $query = $this->db->get();
        $result = $query->result();
        $return = array();
        if (!empty($result)) {
            foreach ($result as $key => $row) {
                $return[$row->id] = $row->name;
            }
        }
        return $return;
    }

    function get_tags_id($list, $table = 'tag')
    {
        $this->db
            ->select('*')
            ->from($table)
            ->where_in('name', $list);
        $query = $this->db->get();
        $result = $query->result();
        $return = array();
        if (!empty($result)) {
            foreach ($result as $key => $row) {
                $return[$row->id] = $row->name;
            }
        }
        //dump($this->db->last_query());
        return $return;
    }

    function save_tags($item_col, $item_id, $tags_list, $tags_table = 'tag')
    {
        $this->db->where($item_col . '_id', $item_id);
        $this->db->delete($item_col . '_' . $tags_table);
        if (!empty($tags_list)) {
            foreach ($tags_list as $key => $tag) {
                $this->db->insert($item_col . '_' . $tags_table, array($item_col.'_id' => $item_id, $tags_table.'_id' => $key));
            }
        }
    }

    function add_tags($tag_list, $table = 'tag')
    {
        $result = array();
        foreach ($tag_list as $tag) {
            $res = $this->db->insert($table, array('name' => ucfirst($tag)));
            $result[$this->db->insert_id()] = ucfirst($tag);
        }
        return $result;
    }

    function get_categories($item_col, $item_id)
    {
        $this->db
            ->select('*')
            ->from($item_col.'_categories')
            ->where_in($item_col.'_id', $item_id);
        $query = $this->db->get();
        $result = $query->result();
        $return = array();
        if (!empty($result)) {
            foreach ($result as $key => $row) {
                $return[] = $row->category_id;
            }
        }
        return $return;
    }
    function save_categories($item_col, $item_id, $categories_list)
    {
        $this->db->where($item_col.'_id', $item_id);
        $this->db->delete($item_col.'_categories');
        if (!empty($categories_list)) {
            foreach ($categories_list as $key => $cat) {
                $this->db->insert($item_col.'_categories', array($item_col.'_id' => $item_id, 'category_id' => $cat));
            }
        }
    }


    /**
     *
     * get all data from table
     * order by function param $order
     * @param $order = string;
     */
    function get_ordered_all_data($order)
    {
        $this->db->select('*')->from($this->table)->order_by($order);
        $query = $this->db->get();
        return $query->result();

    }

    function get_translate_ordered_all_data($field_name, $prefix, $lang)
    {
        $this->db->select('d.*,t.tr_code,t.tr_text')->from($this->table . ' d')->order_by('t.tr_text');
        $this->db->join($this->translate_table . ' t', 't.tr_code = concat("' . $prefix . '", d.' . $field_name . ') AND t.lang_id = \'' . $lang . '\'', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    function get_tree($parent_id_column = 'parent_id', $parent_id = 0, $order = '')
    {
        $this->db->select('*')->from($this->table)->where($parent_id_column, $parent_id);

        if (!empty($order)) {
            $this->db->order_by($order);
        }

        $query = $this->db->get();
        $result = $query->result();
        $branch = array();
        if (!empty($result)) {
            foreach ($result as $key => $row) {
                $branch[$key] = $row;
                $branch[$key]->children = $this->get_tree($parent_id_column, $row->id, $order);
            }
        }
        return $branch;
    }

    function get_tree_ids($parent_id_column = 'parent_id', $parent_id = 0, $arr = array())
    {
        $this->db->select('*')->from('product_cat')->where($parent_id_column, $parent_id);

        $query = $this->db->get();
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $key => $row) {
                $arr[] = $row->id;
                $arr = $this->get_tree_ids($parent_id_column, $row->id, $arr);
            }
        }
        return $arr;
    }

    function get_parents_tree($id, $url = '/category/', $parent_id_column = 'parent_id'){
        $response = array('breadcrumbs' => array());
        $level = 0;

        $cat = $this->getDataByWhere(array('id' => $id));
        @$cat = $cat[0];
        while($cat){
            $response['breadcrumbs'][base_url($url.$cat->name)] = $cat->title;

            $cat = $this->getDataByWhere(array('id' => $cat->parent_id));
            @$cat = $cat[0];
            $level++;
        }

        $response['breadcrumbs'] = array_reverse($response['breadcrumbs']);

        return $response['breadcrumbs'];
    }

    function get_tree_child($parent_id_column, $order, $parent_id)
    {
        $this->db->select('*')->from($this->table)->where($parent_id_column . ' IS NULL', NULL, false);
        $query = $this->db->get();
        $result = $query->result();
    }

    function filter($op, $field, $data, $groupOp = 'AND'){
        $this->set_filter_function($this->db, $op, $field, $data, $groupOp = 'AND');
    }

    function order($by, $type){
        $this->db->order_by($by, $type);
    }

    function limit($limit, $offset = 0){
        $this->db->limit($limit, $offset);
    }

    function set_filter_function(&$cur_db, $op, $field, $data, $groupOp = 'AND')
    {
        if(!$cur_db){
            $cur_db = $this->db;
        }

        $where = 'where';
        $like = 'like';
        $not_like = 'not_like';
        $in = 'where_in';
        $not_in = 'where_not_in';
        if ($groupOp != 'and' && $groupOp != 'AND') {
            $where = 'or_where';
            $like = 'or_like';
            $not_like = 'or_not_like';
            $in = 'or_where_in';
            $not_in = 'or_where_not_in';
        }
        switch ($op) {
            case 'eq':
                $cur_db->$where($field, $data);
                break;
            case 'lt':
                $cur_db->$where($field . ' <', $data);
                break;
            case 'le':
                $cur_db->$where($field . ' <=', $data);
                break;
            case 'gt':
                $cur_db->$where($field . ' >', $data);
                break;
            case 'ge':
                $cur_db->$where($field . ' >=', $data);
                break;
            case 'ne':
                $cur_db->$where($field . ' !=', $data);
                break;
            case 'bw':
                $cur_db->$like($field, $data, 'after');
                break;
            case 'bn':
                $cur_db->$not_like($field, $data, 'after');
                break;
            case 'ew':
                $cur_db->$like($field, $data, 'before');
                break;
            case 'en':
                $cur_db->$not_like($field, $data, 'before');
                break;
            case 'cn':
                $cur_db->$like($field, $data, 'both');
                break;
            case 'nc':
                $cur_db->$not_like($field, $data, 'both');
                break;
            case 'nu':
                $cur_db->$where($field, NULL);
                break;
            case 'in':
                $cur_db->$in($field, $data);
                break;
            case 'ni':
                $cur_db->$not_in($field, $data);
                break;
        }
    }

}