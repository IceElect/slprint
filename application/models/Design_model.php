<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Design_model extends Default_model
{

    public $statuses = array(
        0 => 'Ожидает',
        1 => 'На модерации',
        2 => 'Опубликован',
        3 => 'Отклонен',
    );
    public $user_id = 0;
    function __construct()
    {
        parent::__construct();
        $this->table = 'product_design';
        $this->source_table = 'product_design_sources';
    }

    function _prepare_previews($product){
        $product->sources = json_decode( $product->sources );
        $product->previews = json_decode( $product->previews );

        if(!isset($product->previews->front)){
            if($product->default_color){
                $product->previews = (array) $product->previews->{$product->default_color};
            }else{
                $product->previews = (array) $product->previews;
                $product->previews = (array) array_shift($product->previews);
            }
        }else{
            $product->previews = (array) $product->previews;
        }

        return $product;
    }

    function datatable($fp_aFilter, $is_count = false, $limit = false, $order = false){
        $this->db->from($this->table . ' d');
        if ($is_count) {
            $this->db->select('count(*) as num');
        } else {
            $this->db->select('d.*, p.type, c.title as category_name, p.type, p.type_name, dp.default_color, dp.sources, dp.previews, SUM(p.orders) as orders');
            $this->db->join('product p', 'p.design_id = d.id', 'left');
            $this->db->join('product_design_sources dp', 'dp.design_id = d.id AND dp.product_type = d.product_type', 'left');
            $this->db->join('product_cat c', 'c.id = p.cat_id', 'left');
            // $this->db->join('product_likes l', 'l.product_id = p.id AND l.user_id = \''.$this->user_id.'\'', 'left');
            // $this->db->join('product_views v', 'v.product_id = p.id', 'left');
            $this->db->group_by('d.id');
        }
        
        $this->db->where('d.status != 5');

        if (!empty($fp_aFilter)) {
            if (!empty($fp_aFilter['search'])) {
                if (!empty($fp_aFilter['search']['fields'])) {
                    $_where = array();
                    foreach ($fp_aFilter['search']['fields'] as $field){
                        if($field['field'] == 'id') $field['field'] = 'd.id';
                        if($field['field'] == 'title') $field['field'] = 'd.title';
                        if($field['field'] == 'status') $field['field'] = 'd.status';
                        if($field['field'] == 'product_type') $field['field'] = 'd.product_type';
                        if($field['field'] == 'default_color') $field['field'] = 'd.default_color';
                        $_where[] = $field['field'] . ' LIKE "%' . addslashes($fp_aFilter['search']['value']) . '%"';
                    }

                    $_where = '(' . implode(' OR ', $_where) . ')';
                    $this->db->where($_where);
                }
                unset($fp_aFilter['search']);
            }
            $this->db->where($fp_aFilter);

        }
        if (!empty($limit)) {
            $this->db->limit($limit['limit'], $limit['offset']);
        }
        if (!empty($order)) {
            if($order['column'] == 'id') $order['column'] = 'd.id';
            $this->db->order_by($order['column'], $order['dir']);
        }
        $query = $this->db->get();
        //dump($this->db->last_query());
        if ($is_count) {
            return $query->result_array()[0]['num'];
        } else {
            $result = $query->result();
            $products = array();
            foreach($result as $key => $design){
                $design = $this->_prepare_previews($design);
                $design->status_name = $this->statuses[$design->status];

                $products[] = (array) $design;
            }

            return $products;
        }
    }

    function getSource($design_id, $product_type, $with_products = true){
        $this->db->select('s.*, p.id, p.type_name')
            ->from($this->source_table . ' s')
            ->join('product p', 'p.design_id = s.design_id AND p.type = s.product_type', 'left')
            ->where(array('s.design_id' => $design_id))
            ->where(array('s.product_type' => $product_type));

        if(!$with_products){
            $this->db->where('p.id IS NULL');
        }

        $query = $this->db->get();
        return $query->row();
    }

    function getSources($id, $with_products = true){
        $this->db->select('s.*, p.id, p.type_name')
            ->from($this->source_table . ' s')
            ->join('product p', 'p.design_id = s.design_id AND p.type = s.product_type', 'left')
            ->where(array('s.design_id' => $id));

        if(!$with_products){
            $this->db->where('p.id IS NULL');
        } else {
            $this->db->where('p.id > 0');
        }

        $query = $this->db->get();
        return $query->result();
    }

    function setSourceStatus($design_id, $product_type, $status){

        $this->update(array('status' => $status), array('id' => $design_id));

        $this->db->where(array('design_id' => $design_id, 'product_type' => $product_type));
        $res = $this->db->update($this->source_table, array('status' => $status));
        //dump($this->db->last_query());
        return $res;
    }

    function setSourcePrice($design_id, $product_type, $price){
        $this->db->where(array('design_id' => $design_id, 'product_type' => $product_type));
        $res = $this->db->update($this->source_table, array('price' => $price));
        //dump($this->db->last_query());
        return $res;
    }

    function delDesign($design_id){

        $this->update(array('status' => 5), array('id' => $design_id));

        $this->db->where(array('design_id' => $design_id));
        $res = $this->db->update('product', array('show' => 0));

        $this->db->where(array('design_id' => $design_id));
        $res = $this->db->update($this->source_table, array('status' => 5));
        //dump($this->db->last_query());
        return $res;
    }
}