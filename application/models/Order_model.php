<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends Default_model
{
    /**
     * Constructor
     *
     * @return    void
     */
    function __construct()
    {
        parent::__construct();
        $this->table = 'orders';
        $this->order_products_table = 'order_products';
        $this->products_table = 'product';
        $this->design_table = 'product_design';
    }

    function get_order_products($id){
        $this->db->select('op.id, p.*, p.id as product_id, p.price, p.name, op.number, op.price, op.sources, op.previews, op.price_without_disc, op.options')->from($this->products_table. ' p');
        $this->db->join($this->order_products_table . ' op', 'p.id = op.product_id', 'left');
        $this->db->join($this->table . ' o', 'o.id = op.order_id', 'left');
        // $this->db->join('product_design d', 'p.design_id = d.id', 'left');
        $this->db->where('o.id', $id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function get_order_products_data($id, $filter = array()){
        $this->db->select('op.*, d.author_id')
            ->from($this->order_products_table . ' op');
        $this->db->join($this->products_table. ' p', 'p.id = op.product_id', 'left');
        $this->db->join($this->design_table . ' d', 'p.design_id = d.id', 'left');
        $this->db->where('op.order_id', $id);
        if (!empty($filter['author_id'])){
            $this->db->where('d.author_id', $filter['author_id']);
        }
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function get_partner_order_list($filter){
        $this->CI->db
            ->select('o.*')
            ->from('orders o')
            ->join('order_products op', 'o.id = op.order_id', 'left')
            ->join('product p', 'p.id = op.product_id', 'left')
            ->join('product_design d', 'p.design_id = d.id', 'left');
        $this->CI->from($this->table);
        if (!empty($filter['user_id'])){
            $this->CI->where('d.author_id', $filter['user_id']);
            unset($filter['user_id']);
        }
        $this->CI->or_where($filter);
        dump($this->CI->db->last_query(),0);
        $query = $this->CI->db->get();
        //dump($this->db->last_query());
        return $query->result();
    }

    function save_cart_products($order_id, $products_list)
    {
        if (!empty($products_list)) {
            foreach ($products_list as $key => $value) {
                $this->db->insert($this->order_products_table, array(
                    'order_id' => $order_id,
                    'product_id' => $value['product_id'],
                    'price_without_disc' => $value['was_price'],
                    'price' => $value['price'],
                    'number' => $value['qty'],
                    'options' => json_encode($value['options']),
                    'sources' => json_encode($value['sources']),
                    'previews' => json_encode($value['previews']),
                ));
            }
        }
    }

    function add_products($order_id, $products_list)
    {
        $result = array();
        foreach ($products_list as $key => $value) {
//            if(!empty($))
            $res = $this->db->insert($this->order_products_table, array('order_id' => $order_id, 'product_id' => $value['id'], 'number' => $value['qty']));
            $result[$this->db->insert_id()] = $product;
        }
        return $result;
    }

    function get_order_by_id($order_id, $user_id = false){
        $this->db->select('o.*, SUM(op.price) as total');

        $this->db->from($this->table . ' o');
        $this->db->where('o.id', $order_id);

        $this->db->join('order_products op', 'op.order_id = o.id', 'left');
        $this->db->group_by('o.id');

        $query = $this->db->get();
        //dump($this->db->last_query());
        $result = $query->row();

        if (!empty($result)){
            $result->products = $this->get_order_products($order_id);
        }
        return $result;
    }

    function update_order_status($order_id, $status, $user_id = false){
        $this->db->where('id', $order_id);
        if ($user_id){
            $this->db->where('user_id', $user_id);
        }
        $this->db->set('status', $status);
        $res = $this->db->update($this->table);
        return $res;
    }

    function save_product($aData, $subact = 'edit', $fp_nId = ''){
        if ($subact == 'add') {
            $res = $this->db->insert($this->order_products_table, $aData);
            $res = $this->db->insert_id();
        } else {
            if (!$fp_nId) return false;
            $res = $this->db->update($this->order_products_table, $aData, "id = '" . $fp_nId . "'");
        }
        if (!$res) {
            return false;
        } else {
            if ($subact == 'add') {
                return $res;
            } else {
                return $fp_nId;
            }
        }
    }

    function getDatabyFilter($fp_aFilter, $fp_sidx, $fp_sord)
    {
        $this->db->select('o.*, SUM(op.price) as total');
        if (!empty($fp_aFilter['filters']->rules)) {
            foreach ($fp_aFilter['filters']->rules as $key => $value) {
                $this->set_filter_function($this->db, $value->op, $value->field, $value->data, $fp_aFilter['filters']->groupOp);
            }
        }

        $this->db->from($this->table . ' o');
        $this->db->where($fp_aFilter['fields']);

        $this->db->join('order_products op', 'op.order_id = o.id', 'left');

        $this->db->group_by('o.id');

        $this->db->order_by($fp_sidx, $fp_sord);
        if (isset($fp_aFilter['limit'])) {
            $this->db->limit($fp_aFilter['limit'], $fp_aFilter['offset']);
        }
        $query = $this->db->get();
        //dump($this->db->last_query());
        return $query->result();
    }

    function del_product($fp_rowId){
        foreach ((array)$fp_rowId as $k => $id) {
            $this->db->or_where('id', $id);
        }
        $this->db->delete($this->order_products_table);
        //dump($this->db->last_query());
        return true;
    }

}