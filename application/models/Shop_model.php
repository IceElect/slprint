<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Shop_model extends Default_model
{
    public $user_id = 0;
    function __construct()
    {
        parent::__construct();
        $this->table = 'product';
        $this->category_table = 'product_cat';
        $this->attribute_table = 'shop_attribute';
        $this->manufacturer_table = 'product_manuf';
        $this->product_attributes_table = 'product_attributes';
        $this->product_images_table = 'product_screenshots';
        $this->comment_table = 'product_comments';
        $this->translate_table = 'translate';
    }

    function _prepare_previews($product){
        $product->sources = json_decode( $product->sources );
        $product->previews = json_decode( $product->previews );

        if(!isset($product->previews->front)){
            if($product->default_color){
                $product->previews = (array) $product->previews->{$product->default_color};
            }else{
                $product->previews = (array) $product->previews;
                $product->previews = (array) array_shift($product->previews);
            }
        }else{
            $product->previews = (array) $product->previews;
        }

        return $product;
    }

    function get_products($page = 1, $limit = 24, $only_count = false){
        $offset = ($page -1) * $limit;

        $select = 'p.*, s.image_path as image, c.title as category_name, COUNT(DISTINCT l.id) as is_liked, COUNT(DISTINCT v.id) as views_count, p.type, p.type_name, dp.default_color, dp.sources, dp.previews';

        if($only_count)
            $select = 'COUNT(DISTINCT p.id) as count';

        $this->db->select($select)->from($this->table. ' p');

        if(!$only_count){
            $this->db->join('product_screenshots s', 's.product_id = p.id AND s.weight = 0', 'left');
            $this->db->join('product_likes l', 'l.product_id = p.id AND l.user_id = \''.$this->user_id.'\'', 'left');
            $this->db->join('product_views v', 'v.product_id = p.id', 'left');
            $this->db->join('product_design d', 'p.design_id = d.id', 'left');
            $this->db->join('product_design_sources dp', 'dp.design_id = d.id AND dp.product_type = p.type', 'left');
            $this->db->join('product_cat c', 'c.id = p.cat_id', 'left');
            $this->db->group_by('p.id');
        }
        $this->db->where('p.show', 2);

        if(!$only_count){
            $this->db->limit($limit, $offset);
        }

        $query = $this->db->get();

        if($only_count){
            $result = $query->row();
            return $result->count;
        }else{
            $result = $query->result();
        }

        $products = array();
        foreach($result as $key => $product){
            $product = $this->_prepare_previews($product);

            $products[] = $product;
        }

        return $products;
    }

    function set_discount(){
        $this->db->where('p.discount != 0');
    }

    function set_filters(){
        $filters = array();

        if($this->input->get('price_min') && $this->input->get('price_max')){
            $filters['price_min'] = $this->input->get('price_min');
            $filters['price_max'] = $this->input->get('price_max');
            $this->db->where('p.price BETWEEN '.$filters['price_min'].' AND '.$filters['price_max'].'');
        }

        if($this->input->get('sort_by')){
            $filters['sort_by'] = $this->input->get('sort_by');
            switch($filters['sort_by']){
                case 'price_desc':
                    $this->db->order_by('p.price', 'desc');
                    break;
                case 'price_asc':
                    $this->db->order_by('p.price', 'asc');
                    break;
                case 'popularity':
                    $this->db->order_by('views_count', 'desc');
                    break;
            }
        }else{
            $filters['sort_by'] = 'popularity';
            $this->db->order_by('views_count', 'desc');
        }

        return $filters;
    }

    function view($data){
        $this->setTable('product_views');
        $this->save($data, 'add');
    }

    function get_children_cats($id){
        $cats = array($id);
        $this->db->select('c.*, COUNT(DISTINCT cc.id) as children_count, COUNT(DISTINCT p.id) as product_count')
            ->from('product_cat c')
            ->join('product_cat cc', 'cc.parent_id = c.id', 'left')
            ->join('product p', 'p.cat_id = c.id OR p.cat_id = cc.id', 'left')
            ->where('c.parent_id', $id)
            ->order_by('c.weight', 'asc')
            ->group_by('c.id');
        $query = $this->db->get();
        $result = $query->result();

        foreach($result as $cat){
            $cats[] = $cat->id;
        }

        return $result;
    }

    function get_product_by_id($id){
        $this->db->select('p.*, m.title as manufacturer, s.image_path as image, COUNT(l.id) as is_liked, COUNT(v.id) as views_count, COUNT(DISTINCT likes.product_id) as likes_count, p.type, p.type_name, dp.sources, dp.previews')->from($this->table . ' p');
        $this->db->join($this->manufacturer_table . ' m', 'p.manufacturer_id = m.id', 'left');
        $this->db->join('product_screenshots s', 's.product_id = p.id AND s.weight = 0', 'left');
        $this->db->join('product_likes l', 'l.product_id = p.id  AND l.user_id = \''.$this->user_id.'\'', 'left');
        $this->db->join('product_likes likes', 'likes.product_id = p.id', 'left');
        $this->db->join('product_views v', 'v.product_id = p.id', 'left');
        $this->db->join('product_design d', 'p.design_id = d.id', 'left');
        $this->db->join('product_design_sources dp', 'dp.design_id = p.design_id AND dp.product_type = p.type', 'left');
        $this->db->where('p.show != 1');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        $product = $query->row();

        $product->sources = json_decode( $product->sources );
        $product->previews = json_decode( $product->previews );

        return $product;
    }

    function get_product_by_url($url){
        $this->db->select('p.*, m.title as manufacturer, s.image_path as image, c.name as cat_name, c.title as cat_title, COUNT(l.id) as is_liked, COUNT(v.id) as views_count, COUNT(DISTINCT likes.product_id) as likes_count, dp.default_color, p.type, p.type_name, dp.sources, dp.previews')->from($this->table . ' p');
        $this->db->join($this->manufacturer_table . ' m', 'p.manufacturer_id = m.id', 'left');
        $this->db->join('product_screenshots s', 's.product_id = p.id AND s.weight = 0', 'left');
        $this->db->join('product_likes l', 'l.product_id = p.id  AND l.user_id = \''.$this->user_id.'\'', 'left');
        $this->db->join('product_likes likes', 'likes.product_id = p.id', 'left');
        $this->db->join('product_cat c', 'c.id = p.cat_id', 'left');
        $this->db->join('product_views v', 'v.product_id = p.id', 'left');
        $this->db->join('product_design d', 'p.design_id = d.id', 'left');
        $this->db->join('product_design_sources dp', 'dp.design_id = d.id AND dp.product_type = p.type', 'left');
        $this->db->where('p.show != 1');
        $this->db->where('p.url', $url);
        $query = $this->db->get();
        $product = $query->row();

        // $product->sources = (array) json_decode( $product->sources );
        // $product->previews = json_decode( $product->previews );

        // if(!isset($product->previews->front)){
        //     $product->previews = (array) $product->previews;
        //     $product->previews = (array) array_shift($product->previews);
        // }

        return $product;
    }

    function get_category_by_id($id){
        $this->db->select('c.*, COUNT(DISTINCT cc.id) as children_count, COUNT(DISTINCT p.id) as product_count')
            ->from('product_cat c')
            ->join('product_cat cc', 'cc.parent_id = c.id', 'left')
            ->join('product p', 'p.cat_id = c.id OR p.cat_id = cc.id', 'left')
            ->where('c.id', $id)
            ->group_by('c.id');
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function get_category_by_name($name){
        if(empty($name)) return false;
        $this->db->select('*')->from($this->category_table);
        if(!empty($name)){
            $this->db->where('name', $name);
        }else{
            $this->db->where('parent_id', 0);
        }
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function get_products_by_cats($cats, $limit = false, $where = false){
        $ids = '';
        $seperator = '';
        foreach($cats as $key => $_id){
            $ids .= $seperator."'".$_id."'";
            $seperator = ',';
        }
        $this->db->select('p.*, s.image_path as image, count(v.id) as views_count, COUNT(l.id) as is_liked, p.type, p.type_name, dp.sources, dp.previews')->from($this->table.' p');
        $this->db->join('product_screenshots s', 's.product_id = p.id AND s.weight = 0', 'left');
        $this->db->join('product_views v', 'v.product_id = p.id', 'left');
        $this->db->join('product_likes l', 'l.product_id = p.id AND l.user_id = \''.$this->user_id.'\'', 'left');
        $this->db->join('product_design d', 'p.design_id = d.id', 'left');
        $this->db->join('product_design_sources dp', 'dp.design_id = d.id AND dp.product_type = p.type', 'left');
        $this->db->where('p.cat_id IN('.$ids.')');
        $this->db->group_by('p.id');
        if(!isset($this->admin))
            $this->db->where('p.show != 1');
        if($where)
            $this->db->where($where);
        $this->db->group_by('p.id');
        if($limit)
            $this->db->limit($limit, 0);
        $query = $this->db->get();
        $result = $query->result();

        $products = array();
        foreach($result as $key => $product){
            $product->sources = json_decode( $product->sources );
            $product->previews = json_decode( $product->previews );

            $products[] = $product;
        }

        return $products;
    }

    function get_products_by_cat($cat_id){
        $this->db->select('*')->from($this->table);
        $this->db->where('cat_id', $cat_id);
        $this->db->where('show', 1);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function get_products_by_manufacturer($manufacturer_id){
        $this->db->select('*')->from($this->table);
        $this->db->where('manufacturer_id', $manufacturer_id);
        $this->db->where('show', 1);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function get_category_list($parent_id = 0){
        $this->db->select('c.*, count(DISTINCT p.id) as products_count')
            ->from($this->category_table . ' c')
            ->join($this->table . ' p', 'c.id = p.cat_id', 'left')
            ->where('parent_id', $parent_id)
            ->group_by('c.id');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function get_simular_products($tags, $product_id = false, $design_id = false){

        if(!$tags){
            return array();
        }

        $select = 'p.*, s.image_path as image, c.name as category_name, COUNT(l.id) as is_liked, p.type, p.type_name, dp.default_color, dp.sources, dp.previews, ';
        $select .= ($design_id)?'(case when (p.design_id = 103) THEN 100 ELSE count(DISTINCT st.shop_tag_id) END) as simular_rank':'count(DISTINCT st.shop_tag_id) as simular_rank';

        $tags_row = implode(",", $tags);
        $this->db->select($select)
            ->from('product_shop_tag st')
            ->join($this->table . ' p', 'p.id = st.product_id', 'left')
            ->where_in('st.shop_tag_id', $tags)
            ->order_by('simular_rank', 'desc')
            ->group_by('p.id');

        $this->db->join('product_screenshots s', 's.product_id = p.id AND s.weight = 0', 'left');
        $this->db->join('product_likes l', 'l.product_id = p.id AND l.user_id = \''.$this->user_id.'\'', 'left');
        $this->db->join('product_design d', 'p.design_id = d.id', 'left');
        $this->db->join('product_design_sources dp', 'dp.design_id = d.id AND dp.product_type = p.type', 'left');
        $this->db->join('product_cat c', 'c.id = p.cat_id', 'left');

        if($product_id){
            $this->db->where('p.id !=', $product_id);
        }

        $query = $this->db->get();
        $result = $query->result();
        
        $products = array();
        foreach($result as $key => $product){
            $product->sources = json_decode( $product->sources );
            // $product->previews = json_decode( $product->previews );

            // if($product->default_color){
            //     $product->previews = (array) $product->previews->{$product->default_color};
            // }else{
            //     if(!isset($product->previews->front)){
            //         $product->previews = (array) $product->previews;
            //         $product->previews = (array) array_shift($product->previews);
            //     }
            // }

            $products[] = $product;
        }
        return $products;
    }

    function get_by_id($id){
        $this->db->select('p.*, s.image_path as image, c.name as category_name, COUNT(l.id) as is_liked, p.type, p.type_name, dp.sources, dp.previews')->from($this->table.' p');
        $this->db->join('product_screenshots s', 's.product_id = p.id AND s.weight = 0', 'left');
        $this->db->join('product_likes l', 'l.product_id = p.id AND l.user_id = \''.$this->user_id.'\'', 'left');
        $this->db->join('product_design d', 'p.design_id = d.id', 'left');
        $this->db->join('product_design_sources dp', 'dp.design_id = d.id AND dp.product_type = p.type', 'left');
        $this->db->join('product_cat c', 'c.id = p.cat_id', 'left');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        $product = $query->row();

        $product->sources = json_decode( $product->sources );
        $product->previews = json_decode( $product->previews );

        return $product;
    }

    function get_product_screenshots($id){
        $this->db->select('*')->from($this->product_images_table);
        $this->db->where('product_id', $id);
        $this->db->order_by('weight','asc');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function get_product_attributes_($id){
        $this->db->select('a.*, pa.values')->from($this->product_attributes_table . ' pa');
        $this->db->join($this->attribute_table . ' a', 'pa.attribute_id = a.id', 'left');
        $this->db->where('pa.product_id', $id);
        $this->db->order_by('a.order','asc');
        $query = $this->db->get();
        $data = $query->result();

        return $data;
    }

    function get_product_attributes($id){
        $this->db->select('a.*, pa.values')->from($this->table . ' p');
        $this->db->join($this->product_attributes_table . ' pa', 'pa.product_id = p.id OR pa.product_type = p.type', 'left');
        $this->db->join($this->attribute_table . ' a', 'a.id = pa.attribute_id', 'left');
        $this->db->where('p.id', $id);
        $this->db->order_by('a.order','asc');
        $query = $this->db->get();
        $data = $query->result();

        return $data;
    }

    function get_manufacturer_by_url($url){
        $this->db->select('*')->from($this->manufacturer_table);
        $this->db->where('url', $url);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function get_comments($id, $page = 1, $limit = 20){
        $offset = ($page -1) * $limit;

        $this->db->select('c.*, u.fname, u.lname, u.avatar');
        $this->db->from($this->comment_table . ' c')
        ->join('users u', "u.id = c.author_id", 'left');
        $this->db->where('c.product_id', $id);
        $this->db->where('c.status', 1)
        ->order_by('date', 'desc')
        ->limit($limit, $offset);

        //$offset = ($this->page - 1) * $this->limit;

        //dump($this->db->last_query(),0);
        $query = $this->db->get();
        //dump($this->db->last_query());
        return $query->result();
    }

    function add_comment($product_id, $aData){
        $result = $this->db->insert($this->comment_table, $aData);
        $result = $this->db->insert_id();

        return $result;
    }

    function update_comment($comment_id, $aData){
        $this->db->where(array('id' => $comment_id));
        $res = $this->db->update($this->comment_table, $aData);
        return $res;
    }

    function get_homepage_products(){
        $this->db->select('p.*, s.image_path as image, c.title as category_name, COUNT(l.id) as is_liked, p.type, p.type_name, dp.default_color, dp.sources, dp.previews')->from($this->table. ' p');
        $this->db->join('product_screenshots s', 's.product_id = p.id AND s.weight = 0', 'left');
        $this->db->join('product_likes l', 'l.product_id = p.id AND l.user_id = \''.$this->user_id.'\'', 'left');
        $this->db->join('product_design d', 'p.design_id = d.id', 'left');
        $this->db->join('product_design_sources dp', 'dp.design_id = d.id AND dp.product_type = p.type', 'left');
        $this->db->join('product_cat c', 'c.id = p.cat_id', 'left');
        $this->db->group_by('p.id');
        $this->db->where("p.index_page = 1 OR p.is_new = 1");
        $this->db->where('p.show', 2);
        $this->db->limit(16, 0);
        $query = $this->db->get();
        $result = $query->result();

        $products = array();
        foreach($result as $key => $product){
            $product->sources = json_decode( $product->sources );
            $product->previews = json_decode( $product->previews );

            if($product->default_color){
                $product->previews = (array) $product->previews->{$product->default_color};
            }else{
                if(!isset($product->previews->front)){
                    $product->previews = (array) $product->previews;
                    $product->previews = (array) array_shift($product->previews);
                }
            }

            $products[] = $product;
        }

        return $products;
    }

    function search($aSearch = false){
        $sql = 'SELECT p.*, s.image_path as image, c.title as category_name, COUNT(v.id) as views_count, COUNT(l.id) as is_liked, p.type, p.type_name';
        $sql_from = ' FROM soc_' . $this->table . ' p
                LEFT JOIN soc_' . $this->category_table . ' c ON c.id = p.cat_id
                LEFT JOIN soc_product_screenshots s ON s.product_id = p.id AND s.weight = 0
                LEFT JOIN soc_product_views v ON v.product_id = p.id
                LEFT JOIN soc_product_likes l ON l.product_id = p.id AND l.user_id = \''.$this->user_id.'\'
                LEFT JOIN soc_' . $this->manufacturer_table . ' m ON m.id = p.manufacturer_id
                LEFT JOIN soc_product_design d ON p.design_id = d.id
                LEFT JOIN product_design_sources dp ON dp.design_id = d.id AND dp.product_type = p.type
                WHERE 1 = 1
        ';
        $sql .= $sql_from;
        $sql_count = 'SELECT COUNT(*) as number FROM fw_' . $this->table;
        $sql_cond = '';
        if ($aSearch){
            if (!empty($aSearch['search'])){
                $search = $this->db->escape_str($aSearch['search'], true);
                $search = explode(' ', $search);
                $sql_cond .= ' AND (';
                $_sql = array();
                foreach ($search as $key => $value){
                    $_where = ' (p.name LIKE ("%' . $value . '%")';
                    if (empty($aSearch['category'])){
                        $_where .= ' OR c.title LIKE ("%' . $value . '%")';
                    }
                    if (empty($aSearch['manufacturer'])){
                        $_where .= ' OR m.title LIKE ("%' . $value . '%")';
                    }
                    $_sql[] = $_where . ')';
                }
                $sql_cond .= implode(' OR ', $_sql) . ') ';
            }
            if (!empty($aSearch['category'])){
                $sql_cond .= ' AND c.id IN (' . implode(',',$aSearch['category']) . ')';
            }
            if (!empty($aSearch['manufacturer'])){
                $sql_cond .= ' AND m.id IN (' . implode(',',$aSearch['manufacturer']) . ')';
            }
            if (!empty($aSearch['price']['c'])){
                $sql_cond .= ' AND p.price BETWEEN ' . $aSearch['price']['min'] . ' AND ' . $aSearch['price']['max'] . ' ';
            }
            $sql .= $sql_cond;
            //$this->db->where('id', 1);
        } else {
            //$this->db->order_by('p.name','asc');
        }
        $sql .= ' GROUP BY p.id ';
        //dump($sql,0);
//        $query = $this->db->get();
        $query = $this->db->query($sql);
        $result = $query->result();
        $query = $this->db->query($sql_count);
        $result_count = $query->row_array();
        $query = $this->db->query('SELECT min(price) as min, max(price) as max FROM fw_' . $this->table);
        $result_count_2 = $query->row_array();

        return array(
            'list' => $result,
            'counter' => array_merge($result_count, $result_count_2),
        );
    }

    function get_recommended($pid){
        $this->db->select('p.*, s.image_path as image, c.title as category_name, COUNT(v.id) as views_count, COUNT(l.id) as is_liked, p.type, p.type_name, dp.sources, dp.previews')
            ->from('product_product pp')
            ->join('product p', 'pp.product_id = p.id', 'left')
            ->join('product_views v', 'v.product_id = p.id', 'left')
            ->join('product_likes l', 'l.product_id = p.id AND l.user_id = \''.$this->user_id.'\'', 'left')
            ->join('product_screenshots s', 's.product_id = p.id AND s.weight = 0', 'left')
            ->join('product_design d', 'p.design_id = d.id', 'left')
            ->join('product_design_sources dp', 'dp.design_id = d.id AND dp.product_type = p.type', 'left')
            ->join('product_cat c', 'c.id = p.cat_id', 'left')
            ->where('pp.item_id', $pid)
            ->group_by('p.id');

        $query = $this->db->get();
        return $query->result();
    }

}