<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promo_model extends Default_model
{


    /**
     * Constructor
     *
     * @return    void
     */
    function __construct()
    {
        parent::__construct();
        $this->table = 'shop_promo';
    }

    function get($code){
        $this->db->select('*')->from($this->table);
        $this->db->where('code', $code);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

}