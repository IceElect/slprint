<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_ticket extends Default_Controller {
    private $response = array('response' => false, 'html' => '');
    function __construct(){
        parent::__construct();
        $this->load->model('user_model');

        $this->user_id = $this->session->userdata('user_id');
    }

    function button($id = false){
        if(!$this->user->is_logged())
            exit;

        $response = true;

        $promo = $this->input->post('promo');

        //$id = $this->input->post('id');
        $this->load->model('Ticket_model', 'ticket');
        $this->ticket->user_id = $this->oUser->id;

        $ticket = $this->ticket->getUserTicket($id);

        $this->load->library('robokassa');

        $price = $ticket->price;

        if($promo){
            $promo = $this->robokassa->check_promo($promo, $this->user_id);
            if($promo){
                $price = ($promo->discount>$price)?0:($price-$promo->discount);
                $this->response['text'] = "Вы применили промокод на сумму " . $promo->discount . " рублей";
            }else{
                $response = false;
                $this->response['error'] = 'Данный промокод не существует или уже был использован';
            }
        }


        $url = $this->robokassa->set_payment($ticket->payment_id, $price, "PCR", "Покупка билета ".$ticket->name);

        if($url && $response)
            $this->response['response'] = true;

        $this->response['url'] = $url;
        $this->response['html'] = '<a href="'.$url.'" class="button" id="pay-button">Оплатить билет</a>';

        echo $this->frontend->returnJson($this->response);
    }

}