<?php
class screenshots_model extends Default_model
{

    function __construct()
    {
        parent::__construct();
        $this->item = 'video';
        $this->table = 'video_screenshots';
    }

    function setItem($item){
        $this->item = $item;
        $this->table = $item.'_screenshots';
    }

    function remove($item, $weight){
        if( $this->delWhere(array($this->item.'_id' => $item, 'weight' => $weight)) )
            return true;
        return false;
    }
}