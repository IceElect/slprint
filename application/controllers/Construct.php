<?php

class Construct extends Default_controller
{
    function __construct()
    {
        parent::__construct();
        $this->setActiveModule('construct');
        $this->load->model('Shop_model', 'shop');
        $this->load->library('Shop_lib');
    }

    function background($type){
        header('Content-type: image/svg+xml');

        if(isset($_GET['colors'])){
            $codes = array(
                "white" => "#fff",
                "black" => "#28292d",
                "yellow" => "#ecd400",
                "blue" => "#0a71cc",
                "red" => "#b50f13",
                "dark-blue"  => "#2d377e",
                "orange" => "#f76500",
            );
            $colors = array();
            $aColors = explode(",", $this->input->get('colors'));

            foreach($aColors as $key => $value){
                if(isset($codes[$value]))
                    $value = $codes[$value];

                $colors["color" . $key] = $value;
            }

            $this->my_smarty->assign('colors', $colors);
            $tpl = $this->frontend->fetch('construct/' . $type);

            echo $tpl['data'];
        }
    }

    function index($type = 1){

        $this->load->model('shop_model', 'product_model');
        $product = $this->product_model->get_by_id(-$type);

        $this->frontend->add_js('assets/js/fabric.min.js', 'footer');
        $this->frontend->add_js('assets/js/lodash.min.js', 'footer');
        $this->frontend->add_js('assets/js/constructor.js', 'footer');

        $this->frontend->add_css('assets/css/construct.css');

        $attributes = $this->shop_lib->get_product_attributes(-$type);

        $config = $this->config->load('prints');
        $fonts = $config['fonts'];

        $this->my_smarty->assign('type', $type);
        $this->my_smarty->assign('fonts', $fonts);
        $this->my_smarty->assign('product', $product);

        $this->my_smarty->assign('attributes', $attributes);
        $this->frontend->view('construct');
    }

    function upload_photo(){
        $aData = $this->input->get();
        $response = array('response' => false);

        $data = array('author' => $this->oUser->id, 'date' => date('Y-m-d H:i:s'));

        if (!empty($_FILES['photo']['name'])){
            $this->load->library('uploader');

            $path = 'uploads/construct/';
            $file_name = uniqid();
            $this->uploader->set_upload_config(array(
                    'max_size' => 0,
                    'file_name' => $file_name,
                    'upload_path' => $path,
                    'allowed_types' => 'gif|jpg|png|jpeg',
                    'create_folder' => true,
            ));
            //$this->uploader->set_field_title($this->translate->t('post_image', 'Изображение'));//variant_image
            $image_data = $this->uploader->run('photo');
            if ($image_data['error']) {
                //$is_validate = false;
                dump($image_data);
                $response['error'] = $image_data['error'];
                $this->messages->add_tmessages_list('error', $image_data['data'], 'photo');
            } else {
                $data['src'] = '/' . $path . $image_data['data']['file_name'];
                // $id = $this->photo_model->addPhoto($data);
                $id = 1;
                if($id){
                    $response['photo_id'] = $id;
                    $response['src'] = $data['src'];
                    //$response['path'] = $data['src'];
                    $response['response'] = true;
                }
            }
        }

        echo json_encode($response);

    }

    function _getColors($type){
        switch($type){
            case 'shirt':
            break;
        }
    }

    function create($type = 'manshort'){

        //dump($this->input->post());
        $this->load->model('default_model', 'design_model');
        $this->design_model->setTable('product_design');
        
        $this->load->helper('print');

        $sides = $this->input->post('sides');
        $config = $this->input->post('config');

        $design_id = $this->design_model->save(array('status' => 0), 'add');

        $sources = array();
        $previews = array();

        $design_path = FCPATH . 'uploads/prints/' . $design_id;

        if (!file_exists($design_path)) {
            mkdir($design_path, 0777, true);
        }

        foreach($sides as $side_type => $side){
            $objects = $side['objects'];

            $print = new Print_object('manshort', $side, $config);
            $workarea = array_shift($objects);
            $print->setWorkarea($workarea);
            $print->setObjects($objects);

            $source_path = $print->close($design_id, $side_type);

            $preview_path = $print->generatePreview('manshort', $side_type, $design_id);

            $sources[] = $source_path;
            $previews[] = $preview_path;
        }

        $this->design_model->save(array(
            'sources' => json_encode($sources),
            'previews' => json_encode($previews),
            'status' => 1,
        ), 'edit', $design_id);
    }

    function addText($thumb){
        $font = FCPATH . 'arial.ttf';

        $text_color = imagecolorallocate($thumb, 233, 14, 91);
        imagettftext($thumb, 100, 0, 100, 100, $text_color, $font, "This is example");
    }

    function addImage($thumb){
        $path = FCPATH . 'uploads/construct/' . '5b8dba3516423.png';
        //$path = FCPATH . 'uploads/construct/' . '5b911766cac74.jpg';
        $img = imagecreatefrompng($path);
        $background = imagecolorallocate($img, 0, 0, 0);
        imagecolortransparent($img, $background);
        imagealphablending($img, false);
        imagesavealpha($img, true);

        imagecopy($thumb, $img, 300, 300, 0, 0, 510, 510);
    }
}