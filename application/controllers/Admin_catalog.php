<?php

class Admin_catalog extends Default_controller
{
    function __construct()
    {
        parent::__construct();
        $this->frontend->used_backend();
        $this->setActiveModule('admin/catalog');
        $this->frontend->setTitle($this->translate->t('admin_product_cat_title', 'Категории товаров'));
        // $this->menu->register_menu_link('admin_product_cat', 'admin_product_cat', 'admin_product_cat', 'view', 'admin_menu');

        $this->load->model('product_model');
        $this->product_model->admin = true;

        $this->load->model('default_model', 'category_model');
        $this->category_model->setTable('product_cat');

        $this->load->model('default_model', 'design_model');
        $this->design_model->setTable('product_design');

        // $this->load->model('default_model','manufacturer_model');
        // $this->manufacturer_model->setTable('product_manuf');
    }

    function get_cats_ids($array = array(), $ids){
        foreach($ids as $key => $_id){
            $array[] = $_id->id;
            $_id = $_id->children;
            $array = $this->get_cats_ids($array, $_id);
        }

        return $array;
    }

    function index($url = '', $parent = 0)
    {
        $this->permission->check_action_redirect('view');

        $this->setActiveModule('admin/products');
        $this->my_smarty->assign('activeModule', 'admin/catalog');

        $category = $this->product_model->get_category_by_name($url);
        if(!$category)
            $category = (object) array('id' => 0);

        $breadcrumbs = $this->category_model->get_parents_tree($category->id);

        $_ids = $this->category_model->get_tree('parent_id', $category->id, '');
        $ids = array($category->id);
        $ids = $this->get_cats_ids($ids, $_ids);
        $categories = $this->category_model->getDataByWhere(array('parent_id' => $category->id));

        $this->load->library('fields');
        $this->setFieldsProduct($category->id, $parent);
        $left = array(
            'buttons' => array(
                array('text' => 'Добавить категорию', 'url' => base_url() . 'admin/catalog/add/' . $category->id),
                array('text' => 'Добавить товар', 'url' => '#', 'onclick' => 'add_product('.$category->id.', event)')
            ),
        );
        $this->fields->grid($left, array('op' => 'in', 'field' => 'cat_id', 'data' => $ids), false);

        $this->my_smarty->assign('categories', $categories);
        $this->frontend->breadcrumbs($breadcrumbs);
        $this->frontend->view('catalog');
    }

    // edit menu
    function edit($fp_nId = '')
    {
        $this->permission->check_action_redirect('edit');
        if (!empty($fp_nId)) {
            // $this->locker->use_locker();
        }
        $this->load->library('fields');
        $this->fields->addTab();
        $this->setFields($fp_nId, false);
        $this->fields->setTable('product_cat');
        $this->fields->setMessage('wrong_id', 'product_cat_wrong_id', 'Категории с таким id не найдено.');
        $this->fields->setMessage('edit_save', 'product_cat_edit_save', 'Категория обновлена.');
        $this->fields->setMessage('add_save', 'product_cat_add_save', 'Категория добавлена.');
        $this->fields->form($fp_nId);
    }

    function add($parent = 0)
    {
        $this->permission->check_action_redirect('edit');
        if (!empty($fp_nId)) {
            $this->locker->use_locker();
        }
        $this->load->library('fields');
        $this->fields->addTab();
        $this->setFields(0, $parent);
        $this->fields->setTable('product_cat');
        $this->fields->setMessage('wrong_id', 'product_cat_wrong_id', 'Категории с таким id не найдено.');
        $this->fields->setMessage('edit_save', 'product_cat_edit_save', 'Категория обновлена.');
        $this->fields->setMessage('add_save', 'product_cat_add_save', 'Категория добавлена.');
        $this->fields->form('');
    }

    function del()
    {
        $this->permission->check_action_redirect('delete');
        $this->load->library('fields');
        $this->fields->setTable('product_cat');
        $return = $this->fields->del();
        return $return;
    }


    function setFields($selected, $parent = false)
    {
        $aCats = array((object) array('id' => 0, 'name' => 'dir', 'parent_id' => 0, 'title' => 'Корень'));
        $aCats[0]->children = $this->category_model->get_tree();

        $cats = $this->catTree($aCats, $selected);

        $this->fields->setTable('product_cat');

        $this->fields->addField_id();
        $this->fields->addField_text(array(
            'field' => 'title',
            'title' => $this->translate->t('field_product_cat_title', 'Название'),
            'rules' => 'trim|strtolower|required|max_length[255]',
        ));
        $this->fields->addField_text(array(
            'field' => 'name',
            'title' => $this->translate->t('field_product_cat', 'Системное имя'),
            'rules' => 'trim|strtolower|required|alpha_dash|max_length[255]|callback_unique[name]',
        ));
        $this->fields->addField_text(array(
            'field' => 'description',
            'title' => $this->translate->t('field_product_desc', 'Описание'),
            'rules' => 'trim|max_length[255]',
        ));
        $this->fields->addField_image(array(
            'field' => 'cover',
            'rules' => 'trim',
            'multiple' => false,
            'title' => 'Фото',
            'id_as_name' => true,
            'resize_width' => 720,
        ), array(
            'file_name' => uniqid(),
            'upload_path' => 'uploads/category',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'allowed_types' => '*',
            'create_folder' => true,
        ));
        $parent_field = array(
            'field' => 'parent_id',
            'rules' => 'trim',
            'table_width' => 100,
            'title' => $this->translate->t('field_product_cat_parent', 'Родительская категория'),
            'options' => $cats,
            'value' => $parent,
        );
        if($parent) $parent_field['value'] = $parent;
        $this->fields->addField_select($parent_field);
        /*$this->fields->addField_select(array(
            'field' => 'parent_id',
            'rules' => 'trim',
            'table_width' => 100,
            'title' => $this->translate->t('field_product_cat_parent', 'Родительская категория'),
            'options' => $this->design_model->getDataForSelect('id'),
            'value' => $parent,
        ));*/
        $this->fields->addField_text(array(
            'field' => 'weight',
            'type' => 'number',
            'title' => $this->translate->t('field_product_cat_weight', 'Вес'),
            'rules' => 'trim',
        ));
        $this->fields->addField_text(array(
            'field' => '',
            'form_show' => false,
            'form_disable' => true,
            'validate' => false,
            'title' => $this->translate->t('field_empty', ' '),
            'view_callback' => array($this, 'category_buttons_callback'),
            'sortable' => false,
            'table_width' => '30',
        ));
    }

    function category_buttons_callback($row)
    {
        return '<a href="'. base_url() . $this->activeModule . '/' . $row->id . '" title="'.$this->translate->t('category_view_child', 'Просмотреть').'"><i class="fa fa-eye"></i></a>';
    }

    /**
     * @param $cats
     * @param int $selected
     * @param bool $array
     * @param int $level
     * @return bool
     */
    function catTree($cats, $selected = 0, $array = false, $level = 0){
        foreach($cats as $key => $value){
            $array[$value->id] = str_repeat('-', $level) . ' ' . $value->title;

            //if($selected !== 0 && $selected == $value->id) continue;

            if(count($value->children) > 0){
                $array = $this->catTree($value->children, $selected, $array, $level+1);
            }
        }

        return $array;
    }

    protected function setFieldsProduct($parent, $selected)
    {
        $aCats = $this->category_model->get_tree();
        $cats = $this->catTree($aCats);

        $this->fields->setTable('product');

        //$this->fields->useOnePage();
        $this->fields->addField_check();
        $this->fields->addField_id();
        $this->fields->addField_text(array(
            'field' => 'name',
            'title' => $this->translate->t('field_name', 'Название'),
            'rules' => 'trim|required|max_length[100]|callback_unique[name]',
        ));


        /*$this->fields->addField_checkbox(array(
            'field' => 'show',
            'title' => $this->translate->t('field_show', 'Активен'),
            'table_align' => 'center',
            'table_width' => '80',
            'view_callback' => array($this, 'active_view_callback'),
        ));*/
        $this->fields->addField_text(array(
            'field' => 'url',
            'title' => $this->translate->t('field_page_url', 'URL'),
            'rules' => 'trim|required|alpha_dash|max_length[100]|callback_unique[url]',
            'prefix' => base_url() . 'products/',
        ));


        // $this->fields->addField_select(array(
        //     'field' => 'cat_id',
        //     'table_align' => 'center',
        //     'title' => $this->translate->t('field_cat', 'Категория'),
        //     'options' => $cats,
        //     'tab' => 2,
        // ));
        // $this->fields->addField_select(array(
        //     'field' => 'manufacturer_id',
        //     'table_align' => 'center',
        //     'title' => $this->translate->t('field_manufacturer', 'Производитель'),
        //     'options' => $this->manufacturer_model->getDataForSelect('title'),
        //     'tab' => 2,
        // ));


        $this->fields->addField_text(array(
            'field' => 'store',
            'title' => $this->translate->t('field_store', 'Склад'),
            'rules' => 'trim|required|integer',
            'suffix' => 'шт',
            'tab' => 3,
        ));
        $this->fields->addField_text(array(
            'field' => 'price',
            'title' => $this->translate->t('field_price', 'Цена'),
            'rules' => 'trim|required|integer',
            'suffix' => 'Р',
            'tab' => 3,
        ));


        $this->fields->addField_select(array(
            'field' => 'show',
            'rules' => 'trim',
            'table_width' => 100,
            'title' => $this->translate->t('field_status', 'Статус'),
            'options' => array(
                '1' => $this->translate->t('tag_status_new', 'Неактивен'),
                '2' => $this->translate->t('tag_status_approved', 'Активен'),
                '3' => $this->translate->t('tag_status_denied', 'Заблокирован'),
            ),
            'tab' => 'sidebar',
        ));

        $this->fields->addField_text(array(
            'field' => '',
            'form_show' => false,
            'form_disable' => true,
            'validate' => false,
            'title' => $this->translate->t('field_empty', ' '),
            'view_callback' => array($this, 'product_buttons_callback'),
            'sortable' => false,
            'table_width' => '30',
        ));

        // $this->fields->additionalFields('product');
    }

    function product_buttons_callback($row)
    {
        $html = '<a href="'. base_url() .'admin/products/edit/'.$row->id.'"><i class="fa fa-pencil"></i></a>';
        return $html;
    }

}
