<?php

class Admin_promo extends Default_controller
{
    function __construct()
    {
        parent::__construct();
        $this->frontend->used_backend();
        $this->setActiveModule('admin/promo');

        $this->frontend->setTitle($this->translate->t('admin_promo_title', 'Промокоды'));
    }


    function index()
    {
        $this->permission->check_action_redirect('view');
        $this->load->library('fields');
        $this->fields->setSearchField('name');
        $this->setFields();
        $left = array(
            'buttons' => array(
                array('text' => 'Добавить промокод', 'url' => base_url() . 'admin/promo/edit/')
            ),
        );
        $this->fields->grid($left);
    }

    function add(){
        $this->permission->check_action_redirect('edit');
        $this->frontend->add_js('js/jquery/jquery.mjs.nestedSortable.js');

        $action = 'add';
        $this->frontend->setTextTitle($this->translate->t('vote_'. $action .'_form_title', 'Создание товара'));

        $this->permission->check_action_redirect('edit');
        if (!empty($fp_nId)) {
            // $this->locker->use_locker();
        }
        $this->load->library('fields');
        $this->fields->addTab();
        $this->fields->setHook('after_sidebar', 'form_tags');
        $this->fields->setHook('after_sidebar', 'form_products');
        $this->fields->addTab(array('title' => 'Дополнительные поля', 'id' => 'custom'));
        $this->fields->addTab(array('title' => 'Фотографии', 'id' => 'photo'));
        $this->fields->addTab(array('title' => 'Категории', 'id' => 'category'));
        $this->fields->addTab(array('title' => 'Цены', 'id' => 'price'));
        $this->fields->addTab(array('title' => 'SEO', 'id' => 'seo'));
        $this->setFields($cat);
        $this->fields->setMessage('wrong_id', 'products_wrong_id', 'Продукт с таким id не найден.');
        $this->fields->setMessage('edit_save', 'products_edit_save', 'Продукт обновлен.');
        $this->fields->setMessage('add_save', 'products_add_save', 'Продукт добавлен.');
        $this->fields->add_hook('after_save', array($this, '_after_save'));
        $this->fields->form('');
    }

    function edit($fp_nId = '')
    {
        $this->permission->check_action_redirect('edit');
        if (!empty($fp_nId)) {
            // $this->locker->use_locker();
        }

        if (!empty($fp_nId)) {
            $action = 'edit';
            $this->frontend->setTextTitle($this->translate->t('promo_'. $action .'_form_title', 'Редактирование промокода'));
        } else {
            $action = 'add';
            $this->frontend->setTextTitle($this->translate->t('promo_'. $action .'_form_title', 'Создание промокода'));
        }

        $this->permission->check_action_redirect('edit');
        if (!empty($fp_nId)) {
            // $this->locker->use_locker();
        }
        $this->load->library('fields');
        $this->fields->addTab();
        $this->setFields();
        $this->fields->setMessage('wrong_id', 'promo_wrong_id', 'Промокод с таким id не найден.');
        $this->fields->setMessage('edit_save', 'promo_edit_save', 'Промокод обновлен.');
        $this->fields->setMessage('add_save', 'promo_add_save', 'Промокод добавлен.');
        $this->fields->add_hook('after_save_', array($this, '_after_save'));
        $this->fields->form($fp_nId);
    }

    function _after_save($params){
        $id = $params['id'];
        $aData = $params['aData'];
    }

    function del()
    {
        $this->permission->check_action_redirect('delete');
        $this->load->library('fields');
        $this->fields->setTable('shop_promo');
        $return = $this->fields->del();
        return $return;
    }


    protected function setFields()
    {
        $this->fields->setTable('shop_promo');

        //$this->fields->useOnePage();
        $this->fields->addField_check();
        $this->fields->addField_id();
        $this->fields->addField_text(array(
            'field' => 'code',
            'title' => $this->translate->t('field_code', 'Промокод'),
            'rules' => 'trim|required|max_length[255]',
        ));

        $this->fields->addField_select(array(
            'field' => 'discount_type',
            'title' => $this->translate->t('field_discount_type', 'Тип скидки'),
            'options' => array(
                0 => 'Сумма',
                1 => 'Процент',
            ),
        ));

        $this->fields->addField_text(array(
            'type' => 'number',
            'field' => 'discount',
            'title' => $this->translate->t('field_discount', 'Скидка'),
            'rules' => 'trim|integer',
            //'suffix' => '%',
        ));

        $this->fields->addField_text(array(
            'field' => '',
            'form_show' => false,
            'form_disable' => true,
            'validate' => false,
            'title' => $this->translate->t('field_empty', ' '),
            'view_callback' => array($this, 'buttons_callback'),
            'sortable' => false,
            'table_width' => '30',
        ));
    }

    function active_view_callback($row, $field)
    {
        if ($field) {
            $class = "checkbox_view_on";
            $text = $this->translate->t('field_checkbox_on', 'вкл');
        } else {
            $class = "checkbox_view_off";
            $text = $this->translate->t('field_checkbox_off', 'выкл');
        }
        return '<span class="' . $class . '">' . $text . '</span>';
    }

    function buttons_callback($row)
    {
        $html = '<a href="'. base_url() .'admin/promo/edit/'.$row->id.'"><i class="fa fa-pencil"></i></a>';
        return $html;
    }

}
