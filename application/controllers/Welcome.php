<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends Default_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('blog_model', 'blog');
		$posts = $this->blog->getDataByWhere('p.category_id = 5');
		$this->my_smarty->assign('posts', $posts);
		$this->frontend->view('info');
	}

	public function about(){
		$contacts = array();
		$aContacts = array(
			array('id' => 1, 'role' => 'Разработчик'),
			array('id' => 2, 'role' => 'Менеджер, тестировщик'),
			array('id' => 5, 'role' => 'Менеджер, финансы, доставка'),
			array('id' => 234, 'role' => 'Логистика'),
		);

		$this->load->model('user_model');

		foreach($aContacts as $key => $contact){
			$user = $this->user_model->getUserById($contact['id']);
			if($user){
				$user->role = $contact['role'];
				$contacts[] = $user;
			}
		}
		//$you = $this->user->get_anon_user();
		//$you->fname = "Здесь могли бы быть";
		//$you->lname = "Вы";
		//$you->role = "&nbsp;";
		//$contacts[] = $you;

		$this->my_smarty->assign('roles', $contacts);
		$this->my_smarty->assign('contacts', $contacts);

		$this->frontend->view('about');
	}

	function delivery(){
		$this->frontend->view('delivery');
	}

	function eula(){
		$this->frontend->view('eula');
	}

	public function gallery(){
		$this->load->model('ticket_model');

		$events = $this->ticket_model->getEvents(2);

		$this->my_smarty->assign('events', $events);
		$this->frontend->view('gallery');
	}

	public function album(){
		$response = array('response' => false);
		$aData = $this->input->post();

		if($aData['id']){
			$this->load->model('photo_model');

			$photos = $this->photo_model->getPhotos($aData['id']);

			$this->my_smarty->assign('photos', $photos);
			$response = $this->frontend->fetch('user/album');
		}

		echo json_encode($response);
	}
}
