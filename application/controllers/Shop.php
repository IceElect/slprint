<?php

class Shop extends Default_controller
{

    function __construct()
    {
        parent::__construct();
        $this->setActiveModule('shop');
        $this->load->model('Shop_model', 'shop');
        $this->shop->setTable('product');
        $this->shop->user_id = $this->oUser->id;
        $this->load->library('Shop_lib');
    }

    function index(){
        $this->shop->order('p.id', 'desc');
        $this->shop->filter('eq', 'p.is_hit', 1, 'AND');
        $hits = $this->shop->get_products(1, 15);


        $this->shop->order('products_count', 'desc');
        $this->shop->limit(8, 0);
        $categories = $this->shop->get_category_list();

        // $this->shop->order('p.id', 'desc');
        // $this->shop->filter('eq', 'p.type', 6, 'AND');
        // $products = $this->shop->get_products(1, 15);

        $this->my_smarty->assign('hits', $hits);
        $this->my_smarty->assign('categories', $categories);
        // $this->my_smarty->assign('products', $products);
    	$this->frontend->view('index');
    }

    function genPreviewPrint($design_id, $product_type_ = false){
        $this->load->helper('print');
        $this->load->library('shop_lib');

        $this->load->model('design_model');
        $design = $this->design_model->getDataById($design_id);

        $sides = (array) json_decode($design['objects']);
        $config = (array) json_decode($design['config']);

        $product_type = $design['product_type'];
        if(is_numeric($product_type)){
            $product_type = $this->shop_lib->type_names[$product_type];
        }
        if($product_type_){
            $product_type = $product_type_;
        }

        foreach($sides as $side_type => $side_object){
            if(!in_array($side_type, $this->shop_lib->available_sides[$product_type])){
                unset($sides[$side_type]);
            }
        }

        $print = new Print_object($design_id, $product_type, $config);

        $print->setColors($this->shop_lib->available_colors[$product_type]);
        $print->setSides($sides);

        $sources  = $print->generatePrints($design_id, $product_type, $this->shop_lib->type_names[$design['product_type']]);
        $previews = $print->generatePreview($product_type, 'front', $design_id, $this->shop_lib->type_names[$design['product_type']]);
    }

    function view($url){
        $product = $this->shop->get_product_by_url($url);
        // dump($product);
        if($product->id){
            $attributes = $this->shop_lib->get_product_attributes($product->id);
            $screenshots = $this->shop->get_product_screenshots($product->id);
            $reviews = $this->shop->get_comments($product->id);
            $tags = $this->shop->get_tags('product', $product->id, 'shop_tag');

            $this->shop->limit(15, 0);
            $simular = $this->shop->get_simular_products(array_keys($tags), $product->id, $product->design_id);

            $this->shop->inc_view('product', $product->id);
            $this->shop->update(array('views' => $product->views + 1), array('id' => $product->id));

            $preview_data = $this->shop_lib->get_design_product_preview($product->design_id, $product->type_name, $product->default_color);

            $this->my_smarty->assign('sources', $preview_data['sources']);
            $this->my_smarty->assign('previews', $preview_data['previews']);

            $this->my_smarty->assign('tags', $tags);
            $this->my_smarty->assign('reviews', $reviews);
            $this->my_smarty->assign('product', $product);
            $this->my_smarty->assign('simular', $simular);
            $this->my_smarty->assign('attributes', $attributes);
            $this->my_smarty->assign('screenshots', $screenshots);

            $this->frontend->setTitle($this->translate->t('type_' . $product->type_name, $product->type_name) . ' ' . $product->name . ' - SLPrint');

            $this->frontend->view('shop/product');
        }else{
            show_404();
        }
    }

    function catalog($user = false){

        $this->load->model('user_model');

        if($user){
            $user = $this->user_model->getUserByLogin($user);
        }

        $limit = 24;

        $params = $this->input->get();

        $page = (isset($params['page']))?$params['page']:1;
        $sort = (isset($params['sort']))?$params['sort']:'popular';
        $type = (isset($params['type']))?$params['type']:'manshort';
        $category_id = (isset($params['category_id']))?$params['category_id']:0;

        $categories = $this->shop_lib->getChildCats(0, $user);
        $category = $this->shop->get_category_by_id($category_id);
        if(!$category){
            $category = (object) array(
                'id' => 0,
                'parent_id' => 0,
                'name' => 'catalog',
                'title' => 'Каталог товаров',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis aliquam in aspernatur, officiis fugiat distinctio soluta.',
            );
        }

        $children_ids = $this->shop_lib->getChildIds($category->id);

        if($user){
            $this->shop->filter('eq', 'p.designer_id', $user->id, 'AND');
        }

        $this->set_filters($params, $children_ids);
        $count = $this->shop->get_products(0, 0, 1);

        switch($sort){
            case 'new':
                $this->shop->order('p.id', 'desc');
            break;
            case 'popular':
            default:
                $this->shop->order('views_count, p.id', 'desc');
            break;
        }

        if($user){
            $this->shop->filter('eq', 'p.designer_id', $user->id, 'AND');
        }

        $this->set_filters($params, $children_ids);
        $products = $this->shop->get_products($page, $limit);

        $sort_types = array(
            'popular' => 'По популярности',
            'new' => 'По новизне',
        );

        $types = array(
            'manshort' => 'Мужские футболки',
            'womanshort' => 'Женские футболки',
            'man_sweatshirt' => 'Свитшоты',
        );

        $title_items = array();
        $title_items[] = 'Каталог';
        if($category){
            $title_items[] = ($user)?$user->login:$category->title;
        }
        $title_items[] = $types[$type];
        $title_items[] = 'PrintBand';

        $this->frontend->setTitle(implode(' - ', $title_items));

        $this->load->helper('url_helper');

        $pagination = $this->get_pagination($params, $count, $page, $limit);

        $this->my_smarty->assign('sort', $sort);
        $this->my_smarty->assign('filter_type', $type);
        $this->my_smarty->assign('pagination', $pagination);

        $this->my_smarty->assign('category', $category);
        $this->my_smarty->assign('products', $products);
        $this->my_smarty->assign('categories', $categories);
        $this->my_smarty->assign('sort_types', $sort_types);
        $this->my_smarty->assign('types', $types);
        $this->my_smarty->assign('catalogUser', $user);

        $this->frontend->view('catalog');
    }

    function set_filters($params, $children_ids){
        $type = (isset($params['type']))?$params['type']:'manshort';

        $this->shop->filter('in', 'cat_id', $children_ids, 'AND');
        $this->shop->filter('eq', 'p.type_name', $type, 'AND');
    }

    function get_pagination($params, $count, $page = 1, $limit = 24){
        $response = array();

        $response['cur_page'] = $page;
        $response['pages_count'] = ceil($count / $limit);

        $response['pages'] = array();

        if($page > 3){
            $response['pages'][] = (object) array(
                'num' => 1,
                'text' => 1,
                'class' => '',
                'url' => $this->generate_pagination_url($params, 1),
            );
            $response['pages'][] = (object) array(
                'is_seperator' => true,
            );
        }

        if($page == 3){
            $response['pages'][] = (object) array(
                'num' => $page - 2,
                'text' => $page - 2,
                'class' => '',
                'url' => $this->generate_pagination_url($params, $page - 2),
            );
        }

        if(($page - 1) > 0){
            $response['pages'][] = (object) array(
                'num' => $page - 1,
                'text' => $page - 1,
                'class' => '',
                'url' => $this->generate_pagination_url($params, $page - 1),
            );
        }

        $response['pages'][] = (object) array(
            'num' => $page,
            'text' => $page,
            'class' => 'pagination-toggler--active',
            'url' => $this->generate_pagination_url($params, $page),
        );

        if($response['pages_count'] >= ($page + 1)){
            $response['pages'][] = (object) array(
                'num' => $page + 1,
                'text' => $page + 1,
                'class' => '',
                'url' => $this->generate_pagination_url($params, $page + 1),
            );
        }

        if($response['pages_count'] == ($page + 2)){
            $response['pages'][] = (object) array(
                'num' => $page + 2,
                'text' => $page + 2,
                'class' => '',
                'url' => $this->generate_pagination_url($params, $page + 2),
            );
        }

        if($response['pages_count'] - $page > 2){
            $response['pages'][] = (object) array(
                'is_seperator' => true,
            );
            $response['pages'][] = (object) array(
                'num' => $response['pages_count'],
                'text' => $response['pages_count'],
                'class' => '',
                'url' => $this->generate_pagination_url($params, $response['pages_count']),
            );
        }

        return $response;
    }

    function generate_pagination_url($params, $page){
        $params['page'] = $page;

        return current_url() . '?' . http_build_query($params);
    }

    function test_status($order_id, $status_id, $user_id = false){
        $this->load->library('vk_bot');

        $this->vk_bot->update_order_status($order_id, $status_id, $user_id);
    }

    function font_css($name){
        header("Content-type: text/css");

        $config = $this->config->load('prints');
        $config = $config['fonts'];
        
        $name = urldecode($name);

        if(!isset($config[$name])){
            return false;
        }

        $file = $config[$name];

        $response = "@font-face {
            font-family: '".$name."';
            src: url(../../fonts/".$file.");
}";

        $response = preg_replace("/ {2,}/", " ", $response);

        echo $response;
    }

}