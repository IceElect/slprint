<?php

class Admin_order extends Default_controller
{
    function __construct()
    {
        parent::__construct();
        $this->frontend->used_backend();
        $this->setActiveModule('admin/order');
        $this->frontend->setTitle($this->translate->t('admin_order_title', 'Заказы'));
        //$this->menu->register_menu_link('admin/order', 'admin_order', 'admin_order', 'view', 'admin_menu');

        $this->load->model('order_model', 'model');
        $this->model->setTable('orders');
    }

    function index()
    {
        $this->permission->check_action_redirect('view');
        $this->load->library('fields');
        $this->fields->set_default_model($this->model);
        $this->setFields();
        $left = array(
            'buttons' => array(
                array('text' => 'Добавить заказ', 'url' => base_url() . 'admin/order/edit/')
            ),
        );
        $this->fields->grid($left);
    }

    // edit menu
    function edit($fp_nId = '')
    {
        $this->permission->check_action_redirect('edit');
        if (!empty($fp_nId)) {
            // $this->locker->use_locker();
        }

        $this->load->library('shop_lib');

        $aProducts = $this->model->get_order_products($fp_nId);
        $products = array();
        foreach($aProducts as $key => $product){
            $product = (object) $product;
            $product->options = (array) json_decode( $product->options );
            $product->sources = json_decode( $product->sources );
            $product->previews = json_decode( $product->previews );
            $product->attributes = $this->shop_lib->get_product_attributes($product->id);

            $products[] = $product;
        }

        $this->load->library('fields');
        $this->fields->set_default_model($this->model);
        $this->fields->addTab();

        $this->setFields();

        $this->fields->setHook('after_form', 'order_products');
        $this->fields->add_hook('after_save', array($this, 'after_save'));

        $this->my_smarty->assign('products', $products);

        $this->fields->setMessage('wrong_id', 'mail_wrong_id', 'Заказ с таким id не найден.');
        $this->fields->setMessage('edit_save', 'mail_edit_save', 'Заказ обновлен.');
        $this->fields->setMessage('add_save', 'mail_add_save', 'Заказ добавлен.');
        $this->fields->form($fp_nId);
    }

    function after_save($vars){

        if ($vars['aData']['status'] == 7 && ($vars['aData']['status'] != $vars['oldData']['status'])){
            $this->load->library('partner_lib');
            $this->partner_lib->process_order($vars['id']);
        }
    }

    function del()
    {
        $this->permission->check_action_redirect('delete');
        $this->load->library('fields');
        $this->fields->setTable('orders');
        $return = $this->fields->del();
        return $return;
    }

    protected function setFields()
    {
        $this->fields->setTable('orders');
        $this->fields->set_default_model($this->model);

        $aManagers = $this->user_model->getDataByWhere('id IN(1,2,5)');
        $managers = array('-');
        if (!empty($aManagers)){
            foreach($aManagers as $key => $row){
                $managers[$row->id] = $row->fname . ' ' . $row->lname;
            }
        }

        $file_name = empty($id) ? uniqid() : $id;

        $this->fields->addField_check();
        $this->fields->addField_id();
        $this->fields->useOnePage();
        $this->fields->addField_select(array(
            'field' => 'status',
            'table_width' => 100,
            'title' => $this->translate->t('field_status', 'Статус'),
            'options' => array(
                0 => $this->translate->t('order_status_new', 'Новый'),
                1 => $this->translate->t('order_status_accepted', 'Принят'),
                2 => $this->translate->t('order_status_confirmed', 'Подтвержден'),
                3 => $this->translate->t('order_status_in_production', 'В производстве'),
                4 => $this->translate->t('order_status_shipped', 'Отправлен'),
                5 => $this->translate->t('order_status_received', 'Получен'),
                6 => $this->translate->t('order_status_issued', 'Выдан'),
                7 => $this->translate->t('order_status_complete', 'Завершен'),
                8 => $this->translate->t('order_status_canceled', 'Отменен'),
            ),
            'tab' => 'sidebar',
        ));
        $this->fields->addField_text(array(
            'field' => 'user_id',
            'view_callback' => array($this, 'user_callback'),
            'title' => $this->translate->t('field_order_user_id', 'Покупатель'),
        ));

        $this->fields->addField_text(array(
            'field' => 'fname',
            'table_show' => false,
            'title' => $this->translate->t('field_order_fname', 'Имя'),
        ));
        $this->fields->addField_text(array(
            'field' => 'email',
            'table_show' => false,
            'title' => $this->translate->t('field_order_email', 'E-mail'),
        ));
        $this->fields->addField_text(array(
            'field' => 'phone',
            'table_show' => false,
            'title' => $this->translate->t('field_order_phone', 'Номер телефона'),
        ));

        $this->fields->addField_select(array(
            'field' => 'manager_id',
            'table_width' => 100,
            'title' => $this->translate->t('field_manager', 'Менеджер'),
            'options' => $managers,
            'tab' => 'sidebar',
        ));

        $this->fields->addField_text(array(
            'field' => 'total',
            'form_show' => false,
            'disable_save' => true,
            'table_width' => '10%',
            'view_callback' => array($this, 'total_callback'),
            'title' => $this->translate->t('field_order_total', 'Сумма'),
        ));

        $this->fields->addField_text(array(
            'field' => 'address',
            'table_show' => false,
            'title' => $this->translate->t('field_order_address', 'Адрес'),
        ));

        $this->fields->addField_text(array(
            'field' => 'date',
            'table_width' => '18%',
            'title' => $this->translate->t('field_order_date', 'Дата заказа'),
        ));

        $this->fields->addField_text(array(
            'field' => '',
            'form_show' => false,
            'form_disable' => true,
            'validate' => false,
            'title' => $this->translate->t('field_empty', ' '),
            'view_callback' => array($this, 'buttons_callback'),
            'sortable' => false,
            'table_width' => '30',
        ));
    }

    function user_callback($row){
        return $row->fname;
    }

    function total_callback($row){
        return $row->total . ' руб';
    }

    function buttons_callback($row){
        $html = '<a href="'. base_url() .'admin/order/edit/'.$row->id.'"><i class="fa fa-pencil"></i></a>';
        return $html;
    }
}