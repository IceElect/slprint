<?php

class Admin_products extends Default_controller
{
    function __construct()
    {
        parent::__construct();
        $this->frontend->used_backend();
        $this->setActiveModule('admin/products');

        $this->frontend->setTitle($this->translate->t('admin_products_title', 'Продукты'));

        //$this->menu->register_menu_link('admin_products', 'admin_products', 'admin_products', 'view', 'admin_menu');

        $this->load->model('default_model','category_model');
        $this->category_model->setTable('product_cat');

        // $this->load->model('default_model','manufacturer_model');
        // $this->manufacturer_model->setTable('product_manuf');

        $this->load->model('shop_model', 'product_model');
        $this->product_model->admin = true;
    }


    function index()
    {
        $this->permission->check_action_redirect('view');
        $this->load->library('fields');
        $this->fields->setSearchField('name');
        $this->setFields();
        $left = array(
            'buttons' => array(
                array('text' => 'Добавить товар', 'url' => base_url() . 'admin/products/edit/')
            ),
        );
        $this->fields->grid($left);
    }

    function add(){
        $response = array('response' => false);

        $name = $this->input->post('name');
        $type  = $this->input->post('type');
        $cat  = $this->input->post('cat_id');

        if($name && $cat){
            $data = array(
                'name' => $name,
                'type' => $type,
                'cat_id' => $cat,
            );

            $id = $this->product_model->save($data, 'add');
            if($id){
                $response['response'] = true;
                $response['redir'] = '/admin/products/edit/' . $id;
            }else{
                $response['error'] = "Произошла ошибка при сохранении";
            }
        }else{
            $response['error'] = "Заполните обязательные поля";
        }

        echo json_encode($response);
    }

    function add_($cat = '')
    {
        $this->permission->check_action_redirect('edit');
        $this->frontend->add_js('js/jquery/jquery.mjs.nestedSortable.js');

        $action = 'add';
        $this->frontend->setTextTitle($this->translate->t('vote_'. $action .'_form_title', 'Создание товара'));

        $this->permission->check_action_redirect('edit');
        if (!empty($fp_nId)) {
            // $this->locker->use_locker();
        }
        $this->load->library('fields');
        $this->fields->addTab();
        $this->fields->setHook('after_sidebar', 'form_tags');
        $this->fields->setHook('after_sidebar', 'form_products');
        $this->fields->addTab(array('title' => 'Дополнительные поля', 'id' => 'custom'));
        $this->fields->addTab(array('title' => 'Фотографии', 'id' => 'photo'));
        $this->fields->addTab(array('title' => 'Категории', 'id' => 'category'));
        $this->fields->addTab(array('title' => 'Цены', 'id' => 'price'));
        $this->fields->addTab(array('title' => 'SEO', 'id' => 'seo'));
        $this->setFields($cat);
        $this->fields->setMessage('wrong_id', 'products_wrong_id', 'Продукт с таким id не найден.');
        $this->fields->setMessage('edit_save', 'products_edit_save', 'Продукт обновлен.');
        $this->fields->setMessage('add_save', 'products_add_save', 'Продукт добавлен.');
        $this->fields->add_hook('after_save', array($this, '_after_save'));
        $this->fields->form('');

    }

    function edit($fp_nId = '')
    {
        $this->my_smarty->assign('_post', $_POST);
        $this->permission->check_action_redirect('edit');
        $this->frontend->add_js('js/jquery/jquery.mjs.nestedSortable.js');
        if (!empty($fp_nId)) {
            // $this->locker->use_locker();
        }

        if (!empty($fp_nId)) {
            $action = 'edit';
            $this->frontend->setTextTitle($this->translate->t('vote_'. $action .'_form_title', 'Редактирование товара'));
        } else {
            $action = 'add';
            $this->frontend->setTextTitle($this->translate->t('vote_'. $action .'_form_title', 'Создание товара'));
        }

        $this->permission->check_action_redirect('edit');
        if (!empty($fp_nId)) {
            // $this->locker->use_locker();
        }
        $this->load->library('fields');
        $this->fields->addTab();
        $this->fields->setHook('after_sidebar', 'form_tags');
        // $this->fields->setHook('after_sidebar', 'form_products');
        $this->fields->addTab(array('title' => 'Дополнительные поля', 'id' => 'custom'));
        $this->fields->addTab(array('title' => 'Фотографии', 'id' => 'photo'));
        $this->fields->addTab(array('title' => 'Категории', 'id' => 'category'));
        $this->fields->addTab(array('title' => 'Цены', 'id' => 'price'));
        $this->fields->addTab(array('title' => 'SEO', 'id' => 'seo'));

        $this->setFields($fp_nId);

        $this->fields->setMessage('wrong_id', 'products_wrong_id', 'Продукт с таким id не найден.');
        $this->fields->setMessage('edit_save', 'products_edit_save', 'Продукт обновлен.');
        $this->fields->setMessage('add_save', 'products_add_save', 'Продукт добавлен.');
        $this->fields->add_hook('after_save_', array($this, '_after_save'));
        
        $this->my_smarty->assign('shop_tags', $this->fields->get_all_tags('shop_tag'));

        $this->fields->form($fp_nId);
    }

    function _after_save($params){
        $id = $params['id'];
        $aData = $params['aData'];

        $this->fields->setTable('product');
        $this->fields->save_tags($params, true, 'shop_tag');

        // $this->product_model->save(array(), 'edit', $id);
    }

    function del()
    {
        $this->permission->check_action_redirect('delete');
        $this->load->library('fields');
        $this->fields->setTable('product');
        $return = $this->fields->del();
        return $return;
    }


    protected function setFields($fp_nId, $cat = 0)
    {
        $aCats = $this->category_model->get_tree();
        $cats = $this->catTree($aCats);

        $this->load->model('default_model', 'design_model');
        $this->design_model->setTable('product_design');
        $this->design_model->order('id', 'asc');
        $aDesigns = $this->design_model->getDataForSelect('title');
        $designs = array();

        foreach($aDesigns as $design_id => $design_title){
            if($design_title){
                $designs[$design_id] = $design_title;
            }else{
                $designs[$design_id] = $design_id;
            }
        }

        $this->fields->setTable('product');

        $this->fields->setScreenshotsDir('product/' . $fp_nId);

        //$this->fields->useOnePage();
        $this->fields->addField_check();
        $this->fields->addField_id();
        $this->fields->addField_text(array(
            'field' => 'name',
            'title' => $this->translate->t('field_name', 'Название'),
            'rules' => 'trim|required|max_length[255]',
        ));
        $this->fields->addField_text(array(
            'field' => 'vendor',
            'title' => $this->translate->t('field_vendor', 'Артикул'),
            'rules' => 'trim|required',
            'table_show' => false,
        ));
        $this->fields->addField_text(array(
            'field' => 'weight',
            'title' => $this->translate->t('field_weight', 'Вес'),
            'rules' => 'trim|numeric',
            'suffix' => 'кг',
            'table_show' => false,
        ));
        $this->fields->addField_wysiwyg(array(
            'field' => 'description',
            'type' => 'wysiwyg',
            'table_show' => false,
            'title' => $this->translate->t('field_description', 'Описание'),
            'translable' => 1,
            'rules' => '',
        ));
        $this->fields->addField_text(array(
            'field' => 'url',
            'title' => $this->translate->t('field_page_url', 'URL'),
            'rules' => 'trim|required|alpha_dash|max_length[100]|callback_unique[url]',
            'prefix' => base_url() . 'products/',
        ));

        $this->fields->addField_image(array(
            'field' => 'print',
            'rules' => 'trim',
            'multiple' => false,
            'title' => 'Принт',
            'tab' => 2,
        ), array(
            'file_name' => 'print',
            'upload_path' => 'uploads/product/' . $fp_nId,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'allowed_types' => '*',
            'create_folder' => true,
        ));

        // $this->fields->addField_image(array(
        //     'field' => 'screenshots[]',
        //     'table_show' => false,
        //     'rules' => 'trim',
        //     'multiple' => true,
        //     'title' => $this->translate->t('field_actor_screenshots', 'Фотографии'),
        //     'disable_save' => true,
        //     'full_size' => true,
        //     'tab' => 2,
        // ));

        /*$this->fields->addField_checkbox(array(
            'field' => 'show',
            'title' => $this->translate->t('field_show', 'Активен'),
            'table_align' => 'center',
            'table_width' => '80',
            'view_callback' => array($this, 'active_view_callback'),
        ));*/

        $this->fields->addField_select(array(
            'field' => 'cat_id',
            'title' => $this->translate->t('field_cat', 'Категория'),
            'options' => $cats,
            'value' => $cat,
            //'tab' => 3,
        ));
        $this->fields->addField_select(array(
            'field' => 'design_id',
            'title' => $this->translate->t('field_product_design_id', 'ID дизайна'),
            'rules' => 'trim|integer',
            'options' => $designs,
        ));
        // $this->fields->addField_select(array(
        //     'field' => 'manufacturer_id',
        //     'title' => $this->translate->t('field_manufacturer', 'Производитель'),
        //     'options' => $this->manufacturer_model->getDataForSelect('title'),
        //     'tab' => 3,
        // ));


        $this->fields->addField_text(array(
            'field' => 'store',
            'title' => $this->translate->t('field_store', 'Склад'),
            'rules' => 'trim|required|integer',
            'suffix' => 'шт',
            'tab' => 4,
        ));
        $this->fields->addField_text(array(
            'field' => 'price',
            'title' => $this->translate->t('field_price', 'Цена'),
            'rules' => 'trim|required|integer',
            'suffix' => 'Р',
            'tab' => 4,
        ));
        $this->fields->addField_select(array(
            'field' => 'discount_type',
            'title' => $this->translate->t('field_discount_type', 'Тип скидки'),
            'options' => array(
                0 => 'Сумма',
                1 => 'Процент',
            ),
            'tab' => 4,
        ));
        $this->fields->addField_text(array(
            'type' => 'number',
            'field' => 'discount',
            'title' => $this->translate->t('field_price', 'Скидка'),
            'rules' => 'trim|integer',
            // 'suffix' => '%',
            'tab' => 4,
        ));


        /*
        $this->fields->addField_select(array(
            'field' => 'taste',
            'table_align' => 'center',
            'table_width' => 50,
            'title' => $this->translate->t('field_taste', 'Вкус'),
            'options' => $this->translate->get_lang_options(),
        ));*/


        $this->fields->addField_text(array(
            'field' => 'seo_title',
            'title' => $this->translate->t('field_seo_title', 'Seo заголовок'),
            'rules' => 'trim|max_length[60]',
            'table_show' => false,
            'tab' => 5,
        ));
        $this->fields->addField_text(array(
            'field' => 'seo_keywords',
            'title' => $this->translate->t('field_seo_keywords', 'Seo ключевые слова'),
            'rules' => 'trim|max_length[255]',
            'comments' => $this->translate->t('field__product_image_comments', 'Слова через запятую'),
            'table_show' => false,
            'tab' => 5,
        ));
        $this->fields->addField_textarea(array(
            'field' => 'seo_description',
            'title' => $this->translate->t('field_seo_description', 'Seo описание'),
            'rules' => 'trim',
            'table_show' => false,
            'tab' => 5,
        ));


        $this->fields->addField_select(array(
            'field' => 'show',
            'rules' => 'trim',
            'table_align' => 'center',
            'table_width' => 100,
            'title' => $this->translate->t('field_status', 'Статус'),
            'options' => array(
                0 => $this->translate->t('tag_status_draft', 'Черновик'),
                1 => $this->translate->t('tag_status_new', 'Неактивен'),
                2 => $this->translate->t('tag_status_approved', 'Активен'),
                3 => $this->translate->t('tag_status_denied', 'Заблокирован'),
            ),
            'tab' => 'sidebar',
        ));

        $this->fields->addField_select(array(
            'field' => 'type',
            'rules' => 'trim',
            'table_align' => 'center',
            'table_width' => 100,
            'title' => $this->translate->t('field_type', 'Тип товара'),
            'options' => array(
                0 => '-',
                1 => 'Мужская футболка',
                2 => 'Женская футболка',
                4 => 'Кружка',
            ),
            'tab' => 'sidebar',
        ));

        // $this->fields->addField_select(array(
        //     'field' => 'type',
        //     'rules' => 'trim',
        //     'table_align' => 'center',
        //     'table_width' => 100,
        //     'title' => $this->translate->t('field_type', 'Свойство'),
        //     'options' => array(
        //         0 => '(не установлено)',
        //         1 => 'Обратный осмос',
        //         2 => 'Магистральный',
        //         3 => 'Проточный питьевой',
        //         4 => 'Картридж',
        //         5 => 'Аксессуар',
        //     ),
        //     'tab' => 'sidebar',
        // ));

        $this->fields->addField_select(array(
            'field' => 'index_page',
            'rules' => 'trim',
            'table_align' => 'center',
            'table_width' => 100,
            'title' => $this->translate->t('field_index', 'Главная'),
            'options' => array(
                0 => 'Нет',
                1 => 'Да',
            ),
            'tab' => 'sidebar',
        ));

        // $this->fields->addField_text(array(
        //     'field' => 'custom_collection',
        //     'table_show' => false,
        //     'form_disable' => true,
        //     'form_show' => false,
        //     'rules' => '',
        //     'title' => $this->translate->t('field_actor_collection', 'Копмлектация'),
        //     'disable_save' => true,
        //     'full_size' => true,
        //     'validate' => 0,
        // ));
        // $this->fields->addField_text(array(
        //     'field' => 'custom_steps_block',
        //     'table_show' => false,
        //     'form_disable' => true,
        //     'form_show' => false,
        //     'rules' => '',
        //     'title' => $this->translate->t('field_actor_collection', 'Этапы очистки'),
        //     'disable_save' => true,
        //     'full_size' => true,
        //     'validate' => 0,
        // ));
        // $this->fields->addField_text(array(
        //     'field' => 'custom_advantages',
        //     'table_show' => false,
        //     'form_disable' => true,
        //     'form_show' => false,
        //     'rules' => '',
        //     'title' => $this->translate->t('field_actor_collection', 'Преимущества'),
        //     'disable_save' => true,
        //     'full_size' => true,
        //     'validate' => 0,
        // ));

        //$this->fields->additionalFields('product', 1);
    }

    function active_view_callback($row, $field)
    {
        if ($field) {
            $class = "checkbox_view_on";
            $text = $this->translate->t('field_checkbox_on', 'вкл');
        } else {
            $class = "checkbox_view_off";
            $text = $this->translate->t('field_checkbox_off', 'выкл');
        }
        return '<span class="' . $class . '">' . $text . '</span>';
    }

    function save_images($aSysData, $field){
        if (!empty($_FILES[$field['field']]['tmp_name'])){
            if ($field['id_as_name']){
                $file_name = empty($this->vote['id']) ? uniqid() : $this->vote['id'];
                $field['image_config']['file_name'] = $file_name;
            }
            $this->CI->uploader->set_upload_config($field['image_config']);
            $this->CI->uploader->set_field_title($field['title']);
            $image_data = $this->CI->uploader->run($field['field']);
            if ($image_data['error']) {
                $is_validate = false;
                $this->CI->messages->add_tmessages_list('error', $image_data['data'], $field['field'] . 'upload');
            } else {
                if (!empty($oldData[$field['field']]) && $oldData[$field['field']] != $image_data['data']['file_name']) {
                    @unlink($field['image_src'] . $oldData[$field['field']]);
                }
                $aData[$field['field']] = $image_data['data']['file_name'];
            }
        }
    }

    function remove_file($product){
        $this->load->model('screenshots_model');
        $this->screenshots_model->setItem('product');
        if(isset($_POST['key'])){
            $key = $this->input->post('key');
            if($this->screenshots_model->remove($product, $key))
                echo json_encode(array());return true;
        }
        echo json_encode(array('response' => false));
    }

    function remove_file2($id, $image_id)
    {
        dump($image_id);
        /*
        $this->init_model();
        $row = $this->default_model->getDataById($id);
        $this->default_model->update(array($field_name => ''), array('id' => $id));
        @unlink($this->aFields[$field_name]['image_src'] . $row[$field_name]);
        $responce = new StdClass;
        $responce->success = true;
        $this->CI->frontend->returnJson($responce);
        */

        /*
        $this->load->library('fields');
        $this->fields->setTable($this->config->item('achieves', 'tables'));
        $this->setFields();
        $this->fields->remove_field_file($id, $field_name);
        */
    }

    function catTree($cats, $array = array(), $level = 0){
        foreach($cats as $key => $value){
            $array[$value->id] = str_repeat('-', $level) . ' ' . $value->title;

            if(count($value->children) > 0){
                $array = $this->catTree($value->children, $array, $level+1);
            }
        }

        return $array;
    }

    function comment(){
        $this->setActiveModule('admin/products/comment');
        $this->frontend->setTitle($this->translate->t('admin_categories_title', 'Отзывы'));

        $this->permission->check_action_redirect('view');
        $this->load->library('fields');
        $this->fields->setEditAction('_edit');
        $this->fields->setDeleteAction('_del');

        $left = array(
            'buttons' => array(
                array('text' => 'Добавить отзыв', 'url' => base_url() . 'admin/products/comment_add/')
            ),
        );

        $this->setCommentFields();
        $this->fields->grid($left);
    }

    function comment_add(){$this->comment_edit();}
    function comment_edit($fp_nId = '')
    {
        $this->setActiveModule('admin/products/comment');
        $this->frontend->setTitle($this->translate->t('admin_categories_title', 'Отзывы'));

        $this->permission->check_action_redirect('edit');
        if (!empty($fp_nId)) {
            $this->locker->use_locker();
        }
        $this->load->library('fields');
        $this->fields->addTab();
        $this->setCommentFields();
        $this->fields->setEditAction('_edit');
        $this->fields->setMessage('wrong_id', 'page_wrong_id', 'Отзыв с таким id не найден.');
        $this->fields->setMessage('edit_save', 'page_edit_save', 'Отзыв обновлен.');
        $this->fields->setMessage('add_save', 'page_add_save', 'Отзыв добавлен.');
        $this->fields->add_hook('after_save', array($this, '_edit_after_save_category_callback'));
        $this->fields->form($fp_nId);
    }

    function comment_del()
    {
        $this->permission->check_action_redirect('delete');
        $this->load->library('fields');
        $this->fields->setTable('product_comments');
        $return = $this->fields->del();
        return $return;
    }

    function setCommentFields(){
        $this->fields->setTable('product_comments');
        $product_list = $this->product_model->getDataForSelect('name');

        $this->fields->useOnePage();
        $this->fields->addField_check();
        $this->fields->addField_id();
        $this->fields->addField_text(array(
            'field' => 'name',
            'title' => $this->translate->t('fname', 'Имя'),
            'rules' => 'trim|required|max_length[255]',
        ));
        $this->fields->addField_select(array(
            'field' => 'product_id',
            'title' => $this->translate->t('fname', 'Товар'),
            'options' => $product_list,
            'table_width' => 200,
        ));
        $this->fields->addField_textarea(array(
            'field' => 'content',
            'title' => $this->translate->t('fname', 'Текст отзыва'),
            'options' => $product_list,
        ));
        $this->fields->addField_select(array(
            'field' => 'status',
            'title' => $this->translate->t('status', 'Статус'),
            'options' => array(
                1 => 'В обработке',
                2 => 'Опубликован',
                3 => 'Отклонен',
                4 => 'Удален',
            ),
            'table_width' => 200,
            'value' => 0,
            'tab' => 'sidebar',
        ));
    }

}
