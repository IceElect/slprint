<?php

class Admin_user extends Default_controller
{
    function __construct()
    {
        parent::__construct();
        $this->frontend->used_backend();
        $this->setActiveModule('admin/order');
        $this->frontend->setTitle($this->translate->t('admin_order_title', 'Заказы'));
        //$this->menu->register_menu_link('admin/order', 'admin_order', 'admin_order', 'view', 'admin_menu');

        $this->load->model('user_model', 'model');
    }

    function index()
    {
        $this->permission->check_action_redirect('view');
        $this->load->library('fields');
        $this->fields->set_default_model($this->model);
        $this->setFields();
        $left = array(
            'buttons' => array(
                array('text' => 'Добавить пользователя', 'url' => base_url() . 'admin/order/edit/')
            ),
        );
        $this->fields->grid($left);
    }

    // edit menu
    function edit($fp_nId = '')
    {
        $this->permission->check_action_redirect('edit');
        if (!empty($fp_nId)) {
            // $this->locker->use_locker();
        }

        $this->load->library('fields');
        $this->fields->set_default_model($this->model);
        $this->fields->addTab();

        $this->setFields();

        $this->fields->setMessage('wrong_id', 'user_wrong_id', 'Пользователь с таким id не найден.');
        $this->fields->setMessage('edit_save', 'user_edit_save', 'Пользователь обновлен.');
        $this->fields->setMessage('add_save', 'user_add_save', 'Пользователь добавлен.');
        $this->fields->form($fp_nId);
    }


    function del()
    {
        $this->permission->check_action_redirect('delete');
        $this->load->library('fields');
        $this->fields->setTable('orders');
        $return = $this->fields->del();
        return $return;
    }

    protected function setFields()
    {
        $this->fields->setTable('orders');
        $this->fields->set_default_model($this->model);

        $aManagers = $this->user_model->getDataByWhere('id IN(1,2,5)');
        $managers = array('-');
        if (!empty($aManagers)){
            foreach($aManagers as $key => $row){
                $managers[$row->id] = $row->fname . ' ' . $row->lname;
            }
        }

        $file_name = empty($id) ? uniqid() : $id;

        $this->fields->addField_check();
        $this->fields->addField_id();
        $this->fields->useOnePage();

        $this->fields->addField_text(array(
            'field' => 'fname',
            'table_show' => false,
            'title' => $this->translate->t('fname', 'Имя'),
        ));

        $this->fields->addField_text(array(
            'field' => 'email',
            'title' => $this->translate->t('email', 'E-mail'),
        ));

        $this->fields->addField_text(array(
            'field' => 'partner_rate',
            'table_show' => false,
            'title' => $this->translate->t('partner_rate', 'Рейт Партнера'),
        ));


        $this->fields->addField_text(array(
            'field' => '',
            'form_show' => false,
            'form_disable' => true,
            'validate' => false,
            'title' => $this->translate->t('field_empty', ' '),
            'view_callback' => array($this, 'buttons_callback'),
            'sortable' => false,
            'table_width' => '30',
        ));
    }

    function user_callback($row){
        return $row->fname;
    }


    function buttons_callback($row){
        $html = '<a href="'. base_url() .'admin/user/edit/'.$row->id.'"><i class="fa fa-pencil"></i></a>';
        return $html;
    }
}