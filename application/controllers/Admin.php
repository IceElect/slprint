<?php

class Admin extends Default_controller
{
    function __construct()
    {
    	parent::__construct();
        $this->frontend->used_backend();
    }

    function dashboard(){
    	$this->frontend->view('dashboard');
    }
}