<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends Default_controller
{
    function __construct(){
        parent::__construct();
        $this->load->library('shop_lib');
    }

    function _translateDate($date){
        $date = strtotime($date);
        
        $months = array(
          'января',
          'февраля',
          'марта',
          'апреля',
          'мая',
          'июня',
          'июля',
          'августа',
          'сентября',
          'октября',
          'ноября',
          'декабря'
        );

        $result = date('d', $date) . ' ' . $months[date('n', $date)-1] . ', ' . date('Y', $date);
        return $result;

    }

    function _getDeliveryDates($days = 7){
        $today = date("Y-m-d");
        $dates = array();

        for($i = 1;$i <= 7;$i++){
            $_date = array();
            $_date["date"] = date('Y-m-d', strtotime($today. ' + '.$i.' days'));
            $_date["text"] = $this->_translateDate($_date["date"]);
            $dates[] = $_date;
        }

        return $dates;
    }

    public function index()
    {
        $this->frontend->add_body_class('order dark');
        $this->frontend->add_css('assets/css/doublecard.css');
        $cart_products = $this->cart->contents();

        $delivery_dates = $this->_getDeliveryDates();

        $this->my_smarty->assign('cart_data', $this->cart->get_data());
        $this->my_smarty->assign('delivery_dates', $delivery_dates);

        $this->frontend->messages_custom_place();
        $this->frontend->setTitle('Корзина');
        if(!$cart_products)
            $this->frontend->setTitle('Ваша корзина пуста');
        $this->frontend->breadcrumbs(array('/cart' => 'Оформление заказа'));
        $this->frontend->view('cart');
    }

    function order(){

        $response = array('response' => false, 'error' => false, 'errors' => array());
        $cart_products = $this->cart->contents();
        //dump($cart_products);
        if (empty($cart_products)){
            $response['error'] = 'Ваша корзина пуста';
        }
        $order_info = array();
        $delivery_price = 250;
        if ($this->cart->total() > -1000){
            //$delivery_price = 0;
        }
        if(!$response['error']){
            $this->load->model('order_model');
            $order_info = array();
            $error = false;
            $d_type = $this->input->get_post('delivery_type');
            $d_date = $this->input->get_post('delivery_date');
            $required_fields = array('fname','email', 'address', 'phone', 'delivery_date');

            $promo = $this->session->userdata('promo');

            $order_info = array(
                'user_id' => $this->oUser->id,
                'fname' => $this->input->get_post('fname', true),
                'lname' => $this->input->get_post('lname', true),
                'email' => $this->input->get_post('email', true),
                'city' => $this->input->get_post('city', true),
                'address' => $this->input->get_post('address', true),
                'phone' => $this->input->get_post('phone', true),
                'pay_type' => $this->input->get_post('pay_type', true),
                'post_office' => $this->input->get_post('post_office', true),
                'delivery_date' => $d_date,
                'delivery_type' => $d_type,
                'comment' => $this->input->get_post('comment', true),
                'date' => date('Y-m-d H:i:s'),
                'status' => 0,
            );

            if($promo){
                $order_info['discount'] = $promo['discount'];
                $order_info['discount_type'] = $promo['discount_type'];
            }

            $order_info['delivery_price'] = 300;

            switch($d_type){
                case '2':
                    // $order_info['post_office'] = $this->input->get_post('post_office');
                    // $required_fields[] = 'post_office';

                    $order_info['delivery_price'] = 0;
                break;
            }


            foreach ($required_fields as $key => $value) {
                if(empty($order_info[$value])){
                    $response['error'] = 'Заполните обязательные поля.';
                    $response['errors'][$value] = 'Заполните обязательные поля.';
                }
            }

            if (!filter_var($order_info['email'], FILTER_VALIDATE_EMAIL)){
                $response['error'] = 'Неверный формат e-mail.';
                $response['errors']['email'] = 'Неверный формат e-mail.';
            }

            if (!preg_match('/((8|\+7)-?)?\(?\d{3,5}\)?-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}((-?\d{1})?-?\d{1})?/', $order_info['phone'])){
                $response['error'] = 'Неверный формат телефона.';
                $response['errors']['phone'] = 'Неверный формат телефона.';
            }

            if($order_info['delivery_type'] == 0){
                $response['error'] = 'Выберите способ доставки.';
                $response['errors']['delivery_type'] = 'Выберите способ доставки.';
            }

            if($order_info['delivery_date'] == 0){
                $response['error'] = 'Выберите дату доставки.';
                $response['errors']['delivery_date'] = 'Выберите дату доставки.';
            }

            if(
                strtotime($order_info['delivery_date']) < strtotime(date('Y-m-d', strtotime(date('Y-m-d'). ' + 1 days'))) ||
                strtotime($order_info['delivery_date']) > strtotime(date('Y-m-d', strtotime(date('Y-m-d'). ' + 7 days')))
            ){
                $response['error'] = 'Неверная дата доставки.';
                $response['errors']['delivery_date'] = 'Неверная дата доставки.';
            }

            if($order_info['pay_type'] == 0){
                $response['error'] = 'Выберите способ оплаты.';
                $response['errors']['pay_type'] = 'Выберите способ оплаты.';
            }

            if (!$response['error']){
                if ($this->cart->total() < -1000){
                    //$order_info['delivery_price'] = 250;
                }
                $order_id = $this->order_model->save($order_info, 'add');
                $this->order_model->save_cart_products($order_id, $cart_products);
                $this->cart->destroy();
                $this->session->set_userdata('order_info', $order_info);

                $response['order_info'] = $order_info;
                $response['redir'] = '/cart/thank_you';
                $response['response'] = true;

                //$this->mail($order_id);
                $this->load->library('vk_bot');
                $this->vk_bot->update_order_status($order_id, 0);
            }
        }
        echo $this->frontend->returnJson($response);
    }

    function mail($id){
        $total = 0;

        $info = $this->order_model->get_order_by_id($id);

        if (!empty($info['products'])){
            foreach ($info['products'] as $product) {
                $total += $product['number'] * $product['price'];
            }
        }

        $payments = array(1=>'Наличными',2=>'Банковский перевод',3=>'Банковская карта',4=>'Яндекс.Деньги',5=>'Сбербанк',6=>'Qiwi кошелек',7=>'Наличными курьеру');
        $delivery = array(1=>'Курьером',2=>'Самовывоз',3=>'Транспортными компаниями');
        $statuses = array(
            1 => 'Не подвержден',
            2 => 'Новый',
            3 => 'В обработке',
            4 => 'Готов',
            5 => 'Отправлен',
            6 => 'Доставлен',
            7 => 'Отменен',
        );
        $this->my_smarty->assign('delivery', $delivery);
        $this->my_smarty->assign('payments', $payments);
        $this->my_smarty->assign('statuses', $statuses);
        $this->my_smarty->assign('total', $total);
        $this->my_smarty->assign('order', $info);

        $this->load->library('mailer');
        $config = $this->config->item('smtp','mailer');
        $address_list = $this->config->item('address','mailer');
        $subject = 'Новый заказ';
        $message = $this->frontend->get_mail_html('order');
        //$this->mailer->send_order($config['user'], $config['name'], $subject, $message);
    }
/*
    public function checkout(){

        //$this->frontend->setTitle($this->translate->t('checkout_title', 'Оплата'));
        $this->frontend->add_body_class('checkout');

        $this->frontend->view('checkout');
    }
*/
    public function checkout($order_id = false){

        if(empty($order_id)){
            mygoto('/cart');
        }
        $this->load->model('order_model');
        $this->frontend->setTitle($this->translate->t('check_order', 'Подтверждение заказа'));
        $order = $this->order_model->get_order_by_id($order_id, $this->oUser->id);
        $total = 0;
        if (!empty($order['products'])){
            foreach ($order['products'] as $product) {
                $total += $product['number'] * $product['price'];
            }
        }
        $this->frontend->add_body_class('checkout');
        $this->my_smarty->assign('total', $total);
        $this->my_smarty->assign('order', $order);
        $this->frontend->view('checkout');
    }

    public function thank_you(){
        $order_id = $this->input->get_post('order_id');
        $this->frontend->add_body_class('thank_you');
        $this->messages->add_success('order_thank_you','Спасибо за ваш заказ. Наши менедеры свяжутся с вами в ближайшее время.');
        if (!empty($order_id)){
            $this->load->model('order_model');
            $this->order_model->update_order_status($order_id, 1, $this->oUser->id);
        }
        $this->frontend->view('thank_you');
    }

    public function add(){
        $id = $this->input->post('id');
        $qty = $this->input->post('count', 1);
        $result = array('response' => false);
        if (empty($id)){
            echo $this->frontend->returnJson($result);
            return;
        }

        if($qty > 5){
            $result['error'] = "Вы выбрали слишком большое кол-во";
            echo $this->frontend->returnJson($result);
            return;
        }

        $this->load->model('shop_model', 'product_model');
        $product = $this->product_model->get_by_id($id);

        if (empty($product)){
            echo $this->frontend->returnJson($result);
            return;
        }

        $color = array();
        $attributes = $this->shop_lib->get_product_attributes($id);

        foreach($attributes as $key => $a){
            if(@!isset($_POST['attributes'][$a->name]) || $_POST['attributes'][$a->name] == -1){
                $result['error'] = "Выберите " . mb_strtolower($a->title);
                echo $this->frontend->returnJson($result);
                return;
            }
            if($a->name == 'color'){
                $color_value = $_POST['attributes'][$a->name];
                $color = $a->values[$color_value];
            }
        }

        if($this->input->post('construct')){
            $type = $this->input->post('sides');
            $sides = $this->input->post('sides');
            $config = (object) $this->input->post('config');

            $data = array(
                'title' => $this->input->post('title'),
                'category_id' => $this->input->post('category'),
                'default_color' => $config->background_color_name,
                'product_type' => $product->type,
                'author_id' => $this->oUser->id,
            );

            $design = $this->shop_lib->create_design($data, $sides, $config, false);
            $design_product = $this->shop_lib->create_design_product($design['id'], $product->type_name, $config->background_color_name);
            //$design_product = $this->shop_lib->create_design_product($design['id'], 'wshirt');
            
            $product->design_id = $design['id'];
            $product->sources = $design_product['sources'];
            $product->previews = $design_product['previews'];
        }else{
            $print_data = $this->shop_lib->get_design_product_preview($product->design_id, $product->type_name, $color->name);
            $product->previews = $print_data['previews'];
            $product->sources = $print_data['sources'];
        }

        $sides = $this->input->post('sides_count'); // Sides count

        $atts_values = (isset($_POST['attributes']))?$this->input->post('attributes'):false;
        $price_data = $this->shop_lib->get_price($product, $atts_values, $sides);

        $product->qty = $qty;
        $product->price = intval($price_data['_price']);
        $product->total_price = intval($price_data['_price_dis']);

        $p_check = (is_array($product->previews))?(isset($product->previews['front'])):(isset($product->previews->front));
        if(!$p_check){
            $product->previews = (array) $product->previews;
            if(!$color){
                $product->previews = (array) array_shift($product->previews);
            }else{
                $product->previews = (array) $product->previews[$color->name];
            }
        }

        $data = array(
            'id'            => mt_rand(),
            'qty'           => 1,
            'type'          => $product->type,
            'product_id'    => $product->id,
            'design_id'     => $product->design_id,
            'price'         => $product->total_price,
            'was_price'     => $product->price,
            'discount'      => $product->discount,
            'url'           => $product->url,
            'art'           => $product->vendor,
            'name'          => $product->name,
            'image'         => $product->image,
            'is_liked'      => $product->is_liked,
            'category_name' => $product->category_name,
            'options'       => $this->input->post('attributes'),
            'all_options'   => $attributes,
            'sources'       => $product->sources,
            'previews'      => $product->previews,
            // 'custom_steps'   => $product->custom_steps,
            // 'custom_water_temp'   => $product->custom_water_temp,
            // 'custom_performance'   => $product->custom_performance,
            /*
            'options' => array(
               'name' => ,
            ),*/
        );

        for($i=0;$i<$qty;$i++){
            $data['id'] = mt_rand();
            $this->cart->add((array) $data);
        }
        $result['response'] = true;
        $result['cart_data'] = $this->cart->get_data();
        echo $this->frontend->returnJson($result);
    }

    function add_after(){
        $id = $this->input->post('id');
        $qty = $this->input->post('count', 1);
        $result = array('response' => false);
        if (empty($id)){
            echo $this->frontend->returnJson($result);
            return;
        }

        $this->load->model('shop_model', 'product_model');
        $product = $this->product_model->get_by_id($id);

        if (empty($product)){
            echo $this->frontend->returnJson($result);
            return;
        }

        $attributes = $this->shop_lib->get_product_attributes($id);

        $sides = $this->input->post('sides_count'); // Sides count
        $price_data = $this->shop_lib->get_price($product, $attributes, $sides);

        $product->qty = $qty;
        $product->price = $price_data['price'];
        $product->total_price = $price_data['price_dis'];

        if(isset($_POST['attributes'])){
            foreach($attributes as $key => $a){
                if(@!isset($_POST['attributes'][$a->name]) || $_POST['attributes'][$a->name] == -1){
                    $result['error'] = "Выберите " . mb_strtolower($a->title);
                    echo $this->frontend->returnJson($result);
                    return;
                }
            }
        }else{
            $data = array(
                'qty'           => 1,
                'type'          => $product->type,
                'product_id'    => $product->id,
                'design_id'     => $product->design_id,
                'price'         => $product->total_price,
                'was_price'     => $product->price,
                'discount'      => $product->discount,
                'url'           => $product->url,
                'art'           => $product->vendor,
                'name'          => $product->name,
                'image'         => $product->image,
                'is_liked'      => $product->is_liked,
                'category_name' => $product->category_name,
                'options'       => array(),
                'all_options'   => $attributes,
                'sources'       => $product->sources,
                'previews'      => $product->previews,
                // 'custom_steps'   => $product->custom_steps,
                // 'custom_water_temp'   => $product->custom_water_temp,
                // 'custom_performance'   => $product->custom_performance,
                /*
                'options' => array(
                   'name' => ,
                ),*/
            );

            $this->my_smarty->assign('product', $data);
            $result = $this->frontend->fetch('popup/add_after');
        }

        echo $this->frontend->returnJson($result);
    }

    function edit_cart(){
        $action = $this->input->get_post('action');
        $id = $this->input->get_post('id');
        $qty = $this->input->get_post_def('qty', 0);
        $result = array('response' => false);
        $data = array(
            'rowid'      => $id,
            'qty'     => $qty,
        );
        $this->cart->update($data);
        $result['response'] = true;
        if ($action == 'change_qty'){
            $cart = $this->cart->contents();
            $result['subtotal'] = $this->cart->format_number($cart[$id]['subtotal']);
        }

        $total = $this->cart->total();

        $promo = $this->session->userdata('promo');
        if($promo)
            $total = $this->shop_lib->get_price_with_discount($total, $promo['discount'], $promo['discount_type']);

        $result['cart_data'] = $this->cart->get_data();
        echo $this->frontend->returnJson($result);
    }

    function calcPrice(){
        $id = $this->input->post('id');
        $qty = $this->input->post('count', 1);
        $attributes = $this->input->post('attributes');

        $this->load->model('shop_model');
        $product = $this->shop_model->get_by_id($id);
        $sides = $this->input->post('sides_count'); // Sides count
        $price_data = $this->shop_lib->get_price($product, $attributes, $sides);

        echo json_encode($price_data);
    }

    function calcTotal(){
        $response = array('response' => false);
        $delivery_type = $this->input->post('delivery_type');
        $code = $this->input->post('promo');

        $total = $this->cart->total();

        $promo = $this->session->userdata('promo');
        if($promo)
            $total = $this->shop_lib->get_price_with_discount($total, $promo['discount'], $promo['discount_type']);

        require_once(FCPATH . '/application/third_party\Smarty-3.1.29\libs\plugins/function.decline.php');

        $response['total_items'] = $this->cart->total_items();
        ob_start();
        smarty_function_decline(
            array(
                'text' => '%n% товар%o%',
                'number' => $this->cart->total_items(),
                'eArray' => array('', 'а', 'ов'),
            ), ''
        );
        $response['total_items_text'] = ob_get_contents();
        ob_end_clean();
        $response['total'] = $this->cart->format_number($total);
        $response['cart_data'] = $this->cart->get_data();

        $delivery_prices = array(0, 300, 0);

        $response['delivery_price'] = $this->cart->format_number($delivery_prices[$delivery_type]);

        echo json_encode($response);
    }

    function promo(){
        $response = array('response' => false);
        $code = $this->input->post('promo');

        $this->load->model('promo_model');
        $promo = $this->promo_model->get($code);

        $price = $this->cart->total();

        if($promo){
            $response['response'] = true;
            $response['total'] = $this->cart->format_number($this->shop_lib->get_price_with_discount($price, $promo->discount, $promo->discount_type));

            if(!$promo->discount_type){
                $response['text'] = $promo->discount . ' Р скидка по промокоду';
            }else{
                $response['text'] = $promo->discount . '% скидка по промокоду';
            }

            $this->session->set_userdata('promo', array(
                'text' => $response['text'],
                'total' => $response['total'],
                'discount' => $promo->discount,
                'discount_type' => $promo->discount_type,
            ));
        }

        echo json_encode($response);
    }

    function clear(){
        $this->cart->destroy();
    }

    function clear_promo(){
        $this->session->set_userdata('promo', false);
    }

    function createDesign($type){
        $this->load->model('default_model', 'design_model');
        $this->design_model->setTable('product_design');
        
        $this->load->helper('print');

        $sides = $this->input->post('sides');
        $sides = (array) json_decode($sides);
        $config = (array) json_decode($this->input->post('config'));

        if(!$this->design_id)
            $this->design_id = $this->design_model->save(array('status' => 0), 'add');

        $design_id = $this->design_id;

        $sources = array();
        $previews = array();

        $design_path = FCPATH . 'uploads/prints/' . $design_id . '/' . $type;

        if (!file_exists($design_path)) {
            mkdir($design_path, 0777, true);
        }

        foreach($sides as $side_type => $side){
            $side = (array) $side;
            $objects = $side['objects'];

            $print = new Print_object($type, $side, $config);
            $workarea = array_shift($objects);
            $print->setWorkarea($workarea);
            $print->setObjects($objects);

            $source_path = $print->close($type, $side_type, $design_id);

            $preview_path = $print->generatePreview($type, $side_type, $design_id);

            $sources[] = $source_path;
            $previews[] = $preview_path;
        }

        $this->design_model->save(array(
            'sources' => json_encode($sources),
            'previews' => json_encode($previews),
            'status' => 0,
        ), 'edit', $design_id);

        return array(
            'id' => $design_id,
            'sources' => $sources,
            'previews' => $previews,
        );
    }

}
