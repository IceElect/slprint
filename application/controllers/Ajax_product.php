<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_product extends Default_Controller {
	private $response = array('response' => false, 'html' => '');
	function __construct(){
		parent::__construct();
		$this->load->model('shop_model');

		$this->user_id = $this->session->userdata('user_id');
	}

	function addComment($product_id){
        if(!$this->user->is_logged())
            exit;

        $response = array('response' => false);

        $product = $this->shop_model->get_product_by_id($product_id);

        $aData = $this->input->post();
        if(isset($aData['text'])){
            if(!empty($aData['text'])){

                $aData = array(
                    'author_id' => $this->oUser->id,
                    'product_id' => $product_id,
                    'name' => $this->oUser->fname,
                    'content' => trim($aData['text']),
                    'date' => time(),
                    //'recommended' => $aData['recommended'],
                    'deleted' => 0,
                    'status' => 0,
                    'type' => 1,
                );

                $id = $this->shop_model->add_comment($product_id, $aData);
                if($id){
                    $response['response'] = true;
                    $response['comment_id'] = $id;

                    $this->load->library('vk_bot');

                    $mtext = preg_replace('/\</', "\n<", $aData['content']);
                    $mtext = strip_tags($mtext);
                    $mtext = "xD Новый отзыв: " . $mtext . "\nК товару: " . base_url() . 'product/' . $product->url;
                    
                    $mid = $this->vk_bot->messagesSend("102114911,137958427", $mtext . "\n#".$id);

                    // $mid = $this->vkApi_messagesSend(102114911, $mtext, array(
                    //     array(
                    //         (object) array(
                    //             'action' => (object) array(
                    //                 'type' => 'text', 
                    //                 'payload' => "{\"button\": \"4\"}", 
                    //                 'label' => 'Принять',
                    //             ),
                    //             "color" => "positive",
                    //         ),
                    //         (object) array(
                    //             'action' => (object) array(
                    //                 'type' => 'text', 
                    //                 'payload' => "{\"button\": \"4\"}", 
                    //                 'label' => 'Отклонить',
                    //             ),
                    //             "color" => "negative",
                    //         ),
                    //     ),
                    // ));
                }

            }else{
                $response['error'] = 'Введите ваш комментарий';
            }
        }else{
            $response['error'] = 'Введите ваш комментарий';
        }

        echo $this->frontend->returnJson($response);
    }

    function fav($product_id){
        if(!$this->user->is_logged())
            exit;

        $response = array('response' => false);

        if($this->user->is_logged()){
            $this->load->model('default_model', 'likes_model');
            $this->likes_model->setTable('product_likes');

            $data = array(
                'product_id' => $product_id, 
                'ip' => $this->input->ip_address(),
            );

            $like = $this->likes_model->getDataByWhere($data);
            if(!$like){
                $data['date'] = time();
                $data['user_id'] = $this->oUser->id;
                $id = $this->likes_model->save($data, 'add');
            }else{
                $like = $like[0];
                $this->likes_model->del($like->id);
            }
            $response['response'] = true;
        }

        echo $this->frontend->returnJson($response);
    }

    function get_child_cats($cat_id){
        $this->load->model('shop_model');
        $aCategories = $this->shop_model->get_children_cats($cat_id);

        $url = (isset($_POST['url']))?$this->input->post('url'):(base_url() . 'catalog');
        $params = parse_url($url);

        if(isset($params["query"])){
            parse_str($params["query"], $params);
        }else{$params = array();}

        $url = strtok($url, '?');

        $cats = array();
        foreach($aCategories as $cat){
            $params["category_id"] = $cat->id;
            $cat->url = $url . '?' . http_build_query($params);
            $cats[] = $cat;
        }

        echo json_encode($cats);
    }

    function preview($design_id, $product_type, $color, $side = 'front', $side_ = false){
        $this->load->library('shop_lib');

        $size = 640;

        if(is_numeric($side)){
            $size = $side;
            $side = $side_;
        }

        $data = $this->shop_lib->get_design_product_preview($design_id, $product_type, $color, $side, false, $size);
        $preview = $data['previews'][$side];

        $this->_display_img($preview, $size);

    }

    function category_preview($id, $name, $size = 300){

    }

    function _display_img($path, $size = 512){

        header('Pragma: public');
        header('Cache-Control: max-age=86400');
        header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
        header('Content-Type: image/jpeg');

        $img = new Imagick();
        $img->readImage($path);
        $img->setImageFormat('jpg');
        $img->thumbnailImage($size, $size);
        $img->setImageCompression(Imagick::COMPRESSION_JPEG);
        $img->setImageCompressionQuality(75);

        echo $img;
    }
}