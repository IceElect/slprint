<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_popup extends Default_Controller {
	private $response = array('response' => false, 'html' => '');
	function __construct(){
		parent::__construct();
		$this->load->model('user_model');

		$this->user_id = $this->session->userdata('user_id');
	}

	public function index()
	{
		$this->frontend->view('welcome');
	}

	function confirm($type){
		if(!$this->user->is_logged())
			exit;
		$this->response = $this->frontend->fetch('popup/confirm_'.$type);
		echo $this->frontend->returnJson($this->response);
	}

	function login(){
		$this->response = $this->frontend->fetch('popup/login');
		echo $this->frontend->returnJson($this->response);
	}

    function register(){
        $this->response = $this->frontend->fetch('popup/register');
        echo $this->frontend->returnJson($this->response);
    }

	function pay(){
        if(!$this->user->is_logged())
            exit;

		$id = $this->input->post('id');
		$this->load->model('Ticket_model', 'ticket');
        $this->ticket->user_id = $this->oUser->id;

		$ticket = $this->ticket->getTicket($id);

        if($ticket){
            if(($ticket->count - $ticket->t_count) > 0){
                $this->load->library('robokassa');
                $url = $this->robokassa->create_payment($this->oUser->id, $ticket->price, "PCR", "Пукупка билета ".$ticket->name);
                $oid = $this->robokassa->get_order_id();

                $hash = md5(time().$this->oUser->id);
                $tid = $this->ticket->buy($id, $hash, $oid);

                $this->my_smarty->assign('url', $url);
                $this->my_smarty->assign('tid', $tid);
                $this->my_smarty->assign('oid', $oid);

                $this->response = $this->frontend->fetch('popup/pay');
                $this->response['url'] = $url;
                $this->response['oid'] = $url;
            }
        }

		echo $this->frontend->returnJson($this->response);
	}

	function add_product(){
		$cat = $this->input->post('cat');

		$this->setActiveModule('admin/products');
		$this->permission->check_action_redirect('view');

		$this->load->model('default_model','category_model');
        $this->category_model->setTable('product_cat');

        $aCats = array((object) array('id' => 0, 'name' => 'dir', 'parent_id' => 0, 'title' => 'Корень'));

		$aCats[0]->children = $this->category_model->get_tree();
        $cats = $this->catTree($aCats);

        $this->my_smarty->assign('cat_id', $cat);
        $this->my_smarty->assign('cat_tree', $cats);

		$this->response = $this->frontend->fetch('popup/product_add');

		echo $this->frontend->returnJson($this->response);
	}

	function catTree($cats, $array = array(), $level = 0){
        foreach($cats as $key => $value){
            $array[$value->id] = str_repeat('-', $level) . ' ' . $value->title;

            if(count($value->children) > 0){
                $array = $this->catTree($value->children, $array, $level+1);
            }
        }

        return $array;
    }

    function photo_upload($type){
        $response = array('response' => false);

        $this->my_smarty->assign('album', $this->input->post('album'));

        switch ($type) {
            case 'upload':
                $response = $this->frontend->fetch('popup/photo_upload');
                break;
            case 'avatar':
                $response = $this->frontend->fetch('popup/avatar_upload');
                break;
            case 'construct':
                $response = $this->frontend->fetch('popup/construct_upload');
                break;
            default:
                # code...
                break;
        }

        echo json_encode($response);
    }

    function cloth(){
        $this->response = $this->frontend->fetch('popup/cloth');

        echo $this->frontend->returnJson($this->response);
    }

    function cart_screenshots($key){
        $cart_products = $this->cart->contents();
        $product = $cart_products[$key];

        $this->my_smarty->assign('product', $product);

        $this->response = $this->frontend->fetch('popup/cart_screenshots');

        echo $this->frontend->returnJson($this->response);
    }



    function export_printovaya(){
        // $design_id = $this->input->post('design_id');
        $source_id = $this->input->post('source_id');

        $this->load->model('design_model');
        $source = $this->design_model->getSource($source_id);
        dump($source);
    }

}