<?php

class Partner extends Default_controller
{

    function __construct()
    {
        parent::__construct();
        $this->setActiveModule('partner');
        if($this->router->fetch_method() != 'start' && $this->router->fetch_method() != 'agreement'){
            $this->frontend->used_partner();
            if(!$this->user->is_logged()){
                mygoto('/profile');
            }
        }
        $this->frontend->add_css('assets/css/partner.css');
    }

    function start(){
        $faq = $this->load->config('faq');

        $this->my_smarty->assign('faq', $faq);
        $this->frontend->view('partner/start');
    }

    function agreement(){

        if($this->oUser->print_designer == 1){
            mygoto('/partner/design/create');
        }

        if(isset($_POST['submit'])){
            $this->user_model->update_user($this->oUser->id, array('print_designer' => 1));
            mygoto('/partner/design/create');
        }

        $block = $this->frontend->fetch('partner/agreement');

        if(isset($block['data'])){
            $this->my_smarty->assign('tab', 'agreement');
            $this->my_smarty->assign('block', $block['data']);

            $this->frontend->view('user/profile');
        }
    }

    function index(){
        mygoto('/partner/dashboard');
        /*
        $this->frontend->used_partner();

        $this->frontend->add_css('/assets/partner/css/pages/dashboard-ecommerce.min.css');

        $this->frontend->view('dashboard');*/
    }

    function dashboard(){
        $this->frontend->used_partner();
        $this->frontend->add_css('/assets/partner/css/pages/dashboard-ecommerce.min.css');

        $this->my_smarty->assign('hide_header', true);
        $this->frontend->view('dashboard');
    }

    function faq(){

        $faq = $this->load->config('faq');

        $this->my_smarty->assign('faq', $faq);
        $this->frontend->setTitle('Вопрос-ответ');
        $this->frontend->view('faq');
    }

    public function design_view($id){
        $this->load->model('design_model');

        $design = $this->design_model->getDataById($id);
        $design_sources = $this->design_model->getSources($id, true);

        if (!empty($design_sources)){
            foreach ($design_sources as $key => $row){
                $design_sources[$key] = (array) $row;
            }
        }
        //dump($design_sources);

        $this->frontend->setTitle('Дизаин "' . $design['title'] . '"');

        $this->my_smarty->assign('id', $id);
        $this->my_smarty->assign('design', $design);
        $this->my_smarty->assign('sources', $design_sources);
        $this->frontend->view('design_view');
    }

    function design_send($id)
    {
        $design = $this->design_model->getDataById($id);
        //if ($design['printovaya']) {
        $design_sources = $this->design_model->getSources($id, false);
        //}
        $path = $this->shop_lib->get_product_preview_path();
        $postData = $design_sources;
        $ch = curl_init();
        $link = 'https://printovaya.ru/poster/test.php';
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        if ($response === false) $response = curl_error($ch);
        curl_close($ch);
        $result = json_decode($response);


        echo json_encode($result);
        die();
    }

    public function design(){
        $this->load->model('design_model');
/*        $list = $this->design_model->getDataByWhere(array('author_id' => $this->oUser->id));
        if (!empty($list)){
            foreach ($list as $key => $row){
                $list[$key] = (array) $row;
            }
        }
        $this->my_smarty->assign('list', (array)$list);
        $this->frontend->view('design_list');
*/

        if(!isset($_POST['order'])) $_POST['order'] = array('column' => 'id', 'DESC');

        $this->frontend->setTitle('Мои дизайны');
        $filter['author_id'] = $this->oUser->id;
        $this->load->library('fields');
        $this->setDesignFields();
        $this->fields->datatable($filter);
        $this->frontend->view('design_list');
    }

    function setDesignFields(){
        $this->fields->setTable('design');
        $this->fields->set_default_model($this->design_model);
        $this->fields->addField_id();
        $this->fields->addField_text(array(
            'field' => 'title',
            //'title' => $this->translate->t('fname', 'Имя'),
        ));

        $this->fields->addField_text(array(
            'field' => 'status',
            //'title' => $this->translate->t('email', 'E-mail'),
        ));
        $this->fields->addField_text(array(
            'field' => 'orders',
            'form_disble' => true,
            'search' => false,
            //'title' => $this->translate->t('email', 'E-mail'),
        ));
        $this->fields->addField_text(array(
            'field' => 'product_type',
        ));
        $this->fields->addField_text(array(
            'field' => 'default_color',
        ));
    }

    function design_create($type = 1){

        $this->frontend->used_partner();

        $this->load->library('shop_lib');
        $this->load->model('shop_model');
        $product = $this->shop_model->get_by_id(-$type);

        if($this->input->post('submit')){
            
        }

        $this->frontend->add_js('assets/js/store.js', 'footer');
        $this->frontend->add_js('assets/js/fabric.min.js', 'init');
        $this->frontend->add_js('assets/js/lodash.min.js', 'init');
        $this->frontend->add_js('assets/js/constructor.js', 'init');
        $this->frontend->add_js('assets/js/jquery.ui.min.js', 'footer');
        $this->frontend->add_js('assets/plugins/tagit/tag-it.js', 'footer');
        $this->frontend->add_js('assets/plugins/color-picker/color-picker.js', 'init');

        $this->frontend->add_css('assets/css/construct.css');
        $this->frontend->add_css('assets/plugins/tagit/jquery.tagit.css');
        $this->frontend->add_css('assets/plugins/color-picker/color-picker.min.css');

        // tags
        $this->load->model('default_model', 'tags_model');
        $this->tags_model->setTable('shop_tag');
        $shop_tags = $this->tags_model->getAllData();
        $aTags = array();
        foreach ($shop_tags as $key => $value) {
            $aTags[] = $value->name;
        }

        // categories
        $categories = $this->shop_model->get_children_cats(0);

        // attributes
        $attributes = $this->shop_lib->get_product_attributes(-$type, array('color'));

        $config = $this->config->load('prints');
        $fonts = $config['fonts'];

        $this->my_smarty->assign('fonts', $fonts);
        $this->my_smarty->assign('product', $product);
        $this->my_smarty->assign('shop_tags', json_encode($aTags));
        $this->my_smarty->assign('categories', $categories);

        $this->my_smarty->assign('attributes', $attributes);
        $this->frontend->setTitle('Создание дизайна');
        $this->frontend->view('construct');
    }

    function design_confirm($id){
        $this->load->library('shop_lib');

        $this->load->model('default_model', 'design_model');
        $this->design_model->setTable('product_design');

        $design = $this->design_model->getDataById($id);

        $products_list = array(
            array(
                'type_id' => 1, 
                'type_name' => 'manshort', 
                'title' => 'Мужская футболка',
            ),
            array(
                'type_id' => 2, 
                'type_name' => 'womanshort', 
                'title' => 'Женская футболка',
            ),
            array(
                'type_id' => 6, 
                'type_name' => 'man_sweatshirt', 
                'title' => 'Свитшот',
            ),
            // array('type_id' => 4, 'type_name' => 'mug_twotone', 'title' => 'Кружка двухцветная'),
            // array('type_id' => 5, 'type_name' => 'sign', 'title' => 'Значок'),
        );
        $count_sides = $this->shop_lib->countSides($design['objects']);
        foreach($products_list as $key => $item){
            $price = $this->shop_lib->generate_price($item['type_name'], $count_sides);
            $products_list[$key]['price'] = $price['price'];
        }
        $this->my_smarty->assign('design', $design);
        $this->my_smarty->assign('products_list', $products_list);
        $this->my_smarty->assign('hide_header', true);
        $this->frontend->view('design_confirm');
    }

    function design_moder($id){
        $this->load->model('shop_model');
        $this->load->model('design_model');
        $this->design_model->setTable('product_design');

        $this->load->library('shop_lib');

        $approve_price = $this->input->post('price');
        $approve_types = $this->input->post('approve');

        $design = $this->design_model->getDataById($id);
        $design_sources = $this->design_model->getSources($id, false);

        foreach($design_sources as $key => $source){
            $type_name = $this->shop_lib->type_names[$source->product_type];

            if(!isset($approve_types[$type_name])){
                $this->design_model->setSourceStatus($design['id'], $source->product_type, 3);
                continue;
            }

            $this->design_model->setSourceStatus($design['id'], $source->product_type, 1);


            $price = $approve_price[$key];
            if($price){
                $this->design_model->setSourcePrice($design['id'], $source->product_type, $price);
            }
        }

        mygoto('/partner/design');
    }

    function design_approve($id){
        $this->load->model('shop_model');
        $this->load->model('design_model');
        $this->design_model->setTable('product_design');

        $this->load->library('shop_lib');

        $approve_types = $this->input->post('approve');

        $design = $this->design_model->getDataById($id);
        $design_sources = $this->design_model->getSources($id, false);
        foreach($design_sources as $key => $source){
            $type_name = $this->shop_lib->type_names[$source->product_type];

            if(!isset($approve_types[$type_name])){
                $this->design_model->setSourceStatus($design['id'], $source->product_type, 3);
                continue;
            }

            $url = $design['id'] . '-' . $this->shop_lib->translit(mb_strtolower($design['title'])) . '-' . $type_name;
            $tags = $this->design_model->get_tags('product_design', $design['id'], 'shop_tag');

            // $title = $this->translate->t('type_' . $type_name, $type_name) . ' ' . $design['title'];

            $price_data = $this->shop_lib->generate_price($source->product_type, $source->sides_count);

            $data = array(
                'name' => $design['title'],
                'type' => $source->product_type,
                'cat_id' => $design['category_id'],
                'type_name' => $type_name,
                'designer_id' => $design['author_id'],
                'design_id' => $design['id'],
                'vendor' => $url,
                'price' => $price_data['price'],
                'cost_price' => $price_data['cost_price'],
                'discount_type' => $price_data['discount_type'],
                'discount' => $price_data['discount'],
                'show' => 2,
                'url' => $url,
                'is_new' => 1,
            );

            $product_id = $this->shop_model->save($data, 'add');

            if($product_id){
                $this->design_model->setSourceStatus($design['id'], $source->product_type, 2);
                $this->shop_model->save_tags('product', $product_id, $tags, 'shop_tag');
            }
        }

        // mygoto('/');
        mygoto('/partner/design');
    }



    function design_delete($id){
        $this->load->model('design_model');
        $this->design_model->delDesign($id);
    }

    public function add(){
        $id = $this->input->post('id');
        $result = array('response' => false);
        if (empty($id)){
            echo $this->frontend->returnJson($result);
            return;
        }

        $this->load->model('shop_model', 'product_model');
        $product = $this->product_model->get_by_id($id);

        if (empty($product)){
            echo $this->frontend->returnJson($result);
            return;
        }

        $this->load->library('shop_lib');


        // ATTRIBUTES

        $attributes = $this->shop_lib->get_product_attributes($id);

        // foreach($attributes as $key => $a){
        //     if(@!isset($_POST['attributes'][$a->name]) || $_POST['attributes'][$a->name] == -1){
        //         $result['error'] = "Выберите " . mb_strtolower($a->title);
        //         echo $this->frontend->returnJson($result);
        //         return;
        //     }
        // }


        // DESIGN

        $sides = $this->input->post('sides');
        $config = (object) $this->input->post('config');

        $tags = $this->input->post('shop_tag');

        if(!isset($config->background_color_name)){
            $config->background_color_name = 'white';
        }

        $data = array(
            'title' => $this->input->post('title'),
            'category_id' => $this->input->post('category'),
            'default_color' => $config->background_color_name,
            'product_type' => $product->type,
            'author_id' => $this->oUser->id,
            'tags' => $tags,
        );

        $design = $this->shop_lib->create_design($data, $sides, $config, true);

        if(isset($design['error'])){
            echo json_encode($design);
            return;
        }

        $url = '/partner/design/confirm/' . $design['id'];

        $result['redir'] = $url;
        $result['response'] = true;
        $result['design_id'] = $design['id'];
        echo $this->frontend->returnJson($result);
    }

    function get_child_cats($cat){
        $categories = $this->shop_model->get_children_cats($cat);

        echo json_encode($categories);
    }

    function generate_preview(){
        $this->load->library('benchmark');

        $this->benchmark->mark('code_start');
        $design_id = $this->input->post('design_id');
        $type_name = $this->input->post('type_name');

        $this->load->library('shop_lib');

        $types = array_flip($this->shop_lib->type_names);
        $type = $types[$type_name];

        if($this->shop_lib->check_design_product_preview($design_id, $type_name)){
            $design_product = $this->shop_lib->get_design_product_preview($design_id, $type_name);
        }else{
            $design_product = $this->shop_lib->create_design_product($design_id, $type_name);
        }
        $this->benchmark->mark('code_end');

        // dump($this->benchmark->elapsed_time('code_start', 'code_end'));
        // $this->load->library('Partner_lib');
        // $filter['user_id'] = $this->oUser->id;
        // $this->load->library('fields');
        // $this->setWithdrawFields();
        // $this->fields->datatable($filter);
        echo json_encode($design_product);
    }

    function order_list(){
        $this->permission->check_action_redirect('view');
        $this->load->library('Partner_lib');

        //$aData = $this->partner_lib->get_order_list($filter);

        if(!isset($_POST['order'])) $_POST['order'] = array('column' => 'id', 'DESC');

        $filter['author_id'] = $this->oUser->id;
        $this->load->library('fields');
        $this->setOrderFields();
        $this->fields->datatable($filter);

/*
        echo "<pre>";
        print_r($aData);
        echo "</pre>";*/
        //$this->my_smarty->assign('aData', $aData);
        $this->frontend->setTitle('Список заказов');
        $this->frontend->view('order_list');
    }

    function setOrderFields(){
        //$this->fields->setTable('design');
        $this->fields->set_default_model($this->partner_lib);
        $this->fields->setModelFunction('get_order_list');
        $this->fields->addField_id();

        $this->fields->addField_text(array(
            'field' => 'status',
            //'title' => $this->translate->t('email', 'E-mail'),
        ));

        $this->fields->addField_text(array(
            'field' => 'date',
            //'title' => $this->translate->t('email', 'E-mail'),
        ));
        $this->fields->addField_text(array(
            'field' => 'payroll',
            //'title' => $this->translate->t('email', 'E-mail'),
        ));
        $this->fields->addField_text(array(
            'field' => 'bonus_payout',
            //'title' => $this->translate->t('email', 'E-mail'),
        ));

    }

    function withdraw_list(){
        $this->frontend->setTitle('Запросы на вывод');
        $filter['user_id'] = $this->oUser->id;
        $this->load->library('fields');
        $this->setWithdrawFields();
        $this->fields->datatable($filter);
        $this->frontend->view('withdraw_list');
    }

    function withdraw_create(){
        $this->frontend->setTitle('Запрос на вывод');
        $this->load->library('Partner_lib');
        $filter['user_id'] = $this->oUser->id;
        $_POST['user_id'] = $this->oUser->id;
        $this->load->library('fields');
        $this->fields->addTab();
        $this->setWithdrawFields();
        $this->fields->setRedirectAfter('/partner/withdraw_list');
        $this->fields->form();
    }

    function withdraw_add_values($params){
        $params['date'] = date('Y-m-d H:i:s');
        return $params;
    }

    function setWithdrawFields(){
        $this->fields->setTable('withdraw');
        //$this->fields->set_default_model($this->partner_lib);
        //$this->fields->setModelFunction('get_withdraw_list');
        $this->fields->add_hook('before_save_values', array($this, 'withdraw_add_values'));
        $this->fields->addField_id(array(
            'form_show' => false,
        ));
        $this->fields->addField_hidden(array(
            'field' => 'user_id',
            'form_show' => false,
            'table_show' => false,
        ));
        $this->fields->addField_select(array(
            'field' => 'status',
            'form_show' => false,
            'form_disable' => true,
            'disable_save' => true,
            'title' => $this->translate->t('status', 'Статус'),
        ));
        $this->fields->addField_date(array(
            'field' => 'date',
            'table_show' => true,
            'form_show' => false,
            'form_disable' => true,
            'disable_save' => true,
            'view_callback' => false,
            //'title' => $this->translate->t('email', 'E-mail'),
        ));
        $this->fields->addField_text(array(
            'field' => 'amount',
            'type' => 'number',
            'min' => 1000,
            'max' => (int)$this->oUser->balance,
            'rules' => 'trim|required|numeric|greater_than[999]|less_than[' . (((int)$this->oUser->balance) + 1) . ']',
            'title' => $this->translate->t('amount', 'Количество руб.'),
        ));
        $this->fields->addField_select(array(
            'field' => 'payment_service',
            'title' => $this->translate->t('payment_service', 'Сервис'),
            'options' => array(
                'WebMoney',
                'Qiwi',
                'Банковская карта'
            ),
        ));
        $this->fields->addField_text(array(
            'field' => 'payed_day',
            'form_show' => false,
            'form_disable' => true,
            'disable_save' => true,
            //'title' => $this->translate->t('email', 'E-mail'),
        ));
        $this->fields->addField_textarea(array(
            'field' => 'requisites',
            'title' => $this->translate->t('requisites', 'Реквизиты'),
            //'title' => $this->translate->t('email', 'E-mail'),
        ));
    }

    function referal_link_list(){
        $this->frontend->setTitle('Реферальные ссылки');
        $this->frontend->add_js('assets/plugins/clipboard/clipboard.min.js', 'footer');
        //$this->frontend->add_css('assets/css/construct.css');
        $filter['user_id'] = $this->oUser->id;
        $this->load->library('fields');
        $this->setReferalFields();
        $this->fields->datatable($filter);
        $this->frontend->view('referal_link_list');
    }

    function setReferalFields(){
        $this->fields->setTable('referal_link');
        //$this->fields->set_default_model($this->partner_lib);
        //$this->fields->setModelFunction('get_withdraw_list');
        $this->fields->add_hook('before_save_values', array($this, 'withdraw_add_values'));
        $this->fields->addField_id(array(
            'form_show' => false,
        ));
        $this->fields->addField_hidden(array(
            'field' => 'user_id',
            'form_show' => false,
            //'table_show' => false,
        ));
        /*
        $this->fields->addField_select(array(
            'field' => 'status',
            'form_show' => false,
            'form_disable' => true,
            'disable_save' => true,
            'title' => $this->translate->t('status', 'Статус'),
        ));*/
        $this->fields->addField_date(array(
            'field' => 'date',
            'table_show' => true,
            'form_show' => false,
            'form_disable' => true,
            'disable_save' => true,
            'view_callback' => false,
            //'title' => $this->translate->t('email', 'E-mail'),
        ));

        $this->fields->addField_text(array(
            'field' => 'name',
            //'form_show' => false,
            //'form_disable' => true,
            //'disable_save' => true,
            'title' => $this->translate->t('name', 'Название'),
        ));
        $this->fields->addField_select(array(
            'field' => 'target',
            'title' => $this->translate->t('payment_service', 'Сервис'),
            'options' => array(
                'Главная',
                'Форма Регистрации',
                'Мой Магазин',
            ),
        ));
        $this->fields->addField_text(array(
            'field' => 'num_reg',
            'form_show' => false,
            'form_disable' => true,
            'disable_save' => true,
            'title' => $this->translate->t('num_reg', 'Кол-во Регстраций'),
        ));

        $this->fields->addField_text(array(
            'field' => 'num_visits',
            'form_show' => false,
            'form_disable' => true,
            'disable_save' => true,
            'title' => $this->translate->t('num_visits', 'Кол-во Переходов'),
        ));

        $this->fields->addField_text(array(
            'field' => 'num_buy',
            'form_show' => false,
            'form_disable' => true,
            'disable_save' => true,
            'table_show' => false,
            'title' => $this->translate->t('num_buy', 'Кол-во Покупок'),
        ));

    }

    function referal_link_edit($id = false){
        $this->frontend->setTitle('Реферальные ссылки');
        $filter['user_id'] = $this->oUser->id;
        $_POST['user_id'] = $this->oUser->id;
        $this->load->library('fields');
        $this->fields->addTab();
        $this->setReferalFields();
        $this->fields->setRedirectAfter('/partner/referal_link_list');
        $this->fields->form($id);
    }

}