<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_callback extends Default_Controller {
    private $response = array('response' => false, 'html' => '');
    function __construct(){
        parent::__construct();
        $this->load->model('shop_model');

        $this->api_secret = "jHGkuhaHBS";

        $this->config = $this->load->config('bot');
    }

    function vk(){
        if(0){
            $data = $_POST;
            echo "e901351c";
            die();
        }

        $this->load->library('vk_bot');

        $event = json_decode(file_get_contents('php://input'), true);

        switch ($event['type']) {
            case 'confirmation':
                echo $this->config['confirmation_token'];
                break;
            case 'message_new':
                $message = $event['object'];
                $this->vk_user_id = $message['user_id'];

                $this->message_handler($message);

                echo "ok";
                break;
        }
    }


    function message_handler($message){

        $words = explode(' ', $message['body']);

        $type = mb_substr($words[0], 0, 1);
        switch($type){
            case '#':
                if(!isset($words[1])){
                    $this->vk_bot->messagesSend($this->vk_user_id, 'Введите необходимое действие.');
                    break;
                }

                $id = $type = mb_substr($words[0], 1);

                $this->review_handler($id, $words[1]);

                break;
            case '№':

                if(!isset($words[1])){
                    $this->vk_bot->messagesSend($this->vk_user_id, 'Введите необходимое действие.');
                    break;
                }

                $id = $type = mb_substr($words[0], 1);

                $this->order_handler($id, $words[1]);

                break;
            default:
                //$this->vk_bot->messagesSend($this->vk_user_id, 'Другое');
                break;
        }

        if(0)
            $this->vk_bot->messagesSend($this->vk_user_id, $message['body']);
    }

    function review_handler($id, $act){
        $this->load->model('shop_model');

        $action = mb_substr($act, 0, 1);

        $actions_list = array(
            1 => array('П', 'п', 1), // Принять
            2 => array('О', 'о', 2), // Отклонить
        );

        if(in_array($action, $actions_list[1])){
            if($this->shop_model->update_comment($id, array('status' => 1))){
                $this->vk_bot->messagesSend($this->vk_user_id, 'Комментарий #' . $id . ' опубликован.');
            }
        }elseif(in_array($action, $actions_list[2])){
            if($this->shop_model->update_comment($id, array('status' => 2))){
                $this->vk_bot->messagesSend($this->vk_user_id, 'Комментарий #' . $id . ' отклонен.');
            }
        }else{
            $this->vk_bot->messagesSend($this->vk_user_id, 'Неизвестное действие.');
        }
    }

    function order_handler($id, $act){
        $actions_3 = array(
            1 => array('при', 'При'),
            2 => array('под', 'Под'),
            3 => array('В п', 'в п', 'Про', 'про'),
            4 => array('отп', 'Отп'),
            5 => array('пол', 'Пол'),
            6 => array('выд', 'Выд'),
            7 => array('зав', 'Зав', 'зак', 'Зак'),
            8 => array('отм', 'Отм', 'отк', 'Отк'),
        );

        $action = mb_substr($act, 0, 3);

        $status = false;
        foreach($actions_3 as $key => $item){
            if(in_array($action, $item)){
                $status = $key;
            }
        }

        if($status){
            $this->vk_bot->update_order_status($id, $status, $this->vk_user_id);
        }
        //$this->vk_bot->messagesSend($this->vk_user_id, 'Неизвестное действие.');
    }

    function order($id){
        $this->load->library('shop_lib');

        $this->load->model('shop_model');
        $this->load->model('order_model');

        $order = $this->order_model->getDataById($id);

        if(!$order){
            return false;
        }

        $aProducts = $this->order_model->get_order_products($id);
        foreach($aProducts as $key => $product){
            $product = (object) $product;
            $product->options = (array) json_decode( $product->options );
            $product->sources = json_decode( $product->sources );
            $product->previews = json_decode( $product->previews );
            $product->attributes = $this->shop_lib->get_product_attributes($product->id);

            $products[] = $product;
        }

        $statuses = array(
            0 => $this->translate->t('order_status_new', 'Новый'),
            1 => $this->translate->t('order_status_accepted', 'Принят'),
            2 => $this->translate->t('order_status_confirmed', 'Подтвержден'),
            3 => $this->translate->t('order_status_in_production', 'В производстве'),
            4 => $this->translate->t('order_status_shipped', 'Отправлен'),
            5 => $this->translate->t('order_status_received', 'Получен'),
            6 => $this->translate->t('order_status_issued', 'Выдан'),
            7 => $this->translate->t('order_status_completed', 'Выдан'),
            8 => $this->translate->t('order_status_canceled', 'Отменен'),
        );

        $this->my_smarty->assign('order', $order);
        $this->my_smarty->assign('products', $products);
        $this->my_smarty->assign('statuses', $statuses);

        $template = $this->frontend->fetch('order/provider');
        echo $template['data'];
    }

    function log($text){
        file_put_contents(FCPATH . 'callback.txt', $text, FILE_APPEND);
    }
}