<?php
$config['menu'] = array();
$config['menu'][] = array(
	'icon' => 'ft-home',
	'title' => 'Главная',
	'link' => '/',
);
$config['menu'][] = array(
	'icon' => 'ft-shopping-cart',
	'title' => 'Каталог',
	'link' => '/catalog',
);
$config['menu'][] = array(
	'icon' => 'ft-award',
	'title' => 'Промокоды',
	'link' => '/promo',
);
$config['menu'][] = array(
	'icon' => 'ft-award',
	'title' => 'Пользователи',
	'link' => '/users',
);
$config['menu'][] = array(
	'icon' => 'ft-award',
	'title' => 'Заказы',
	'link' => '/order',
);