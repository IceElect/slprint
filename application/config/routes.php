<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'shop';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin/catalog/(:any)'] = 'admin_catalog/index/$1';

$route['admin'] = 'admin/dashboard';
$route['admin/(:any)'] = 'admin_$1';
$route['admin/(:any)/(:any)'] = 'admin_$1';
$route['admin/(:any)/(:any)'] = 'admin_$1/$2';
$route['admin/(:any)/(:any)/(:num)'] = 'admin_$1/$2/$3';
$route['admin/(:any)/(:any)/(:num)/(:num)'] = 'admin_$1/$2/$3/$4';

$route['eula'] = 'welcome/eula';
$route['about'] = 'welcome/about';
$route['delivery'] = 'welcome/delivery';

$route['catalog'] = 'shop/catalog';
$route['catalog/(:any)'] = 'shop/catalog/$1';

$route['product/(:any)'] = 'shop/view/$1';
$route['constructor'] = 'construct/index/1';
$route['constructor/(:any)'] = 'construct/index/$1';

$route['ajax/fonts/(:any).css'] = 'shop/font_css/$1';

$route['profile'] = 'users/profile';
$route['user/login'] = 'users/login';
$route['user/logout'] = 'users/logout';
$route['popup/(:any)'] = 'ajax_popup/$1';
$route['ajax/(:any)/(:any)'] = 'ajax_$1/$2';
$route['ajax/(:any)/(:any)/(:any)'] = 'ajax_$1/$2/$3';

$route['ajax/product/preview/(:any)/(:any)/(:any)/(:any).png'] = 'ajax_product/preview/$1/$2/$3/$4';
$route['ajax/product/preview/(:any)/(:any)/(:any)/(:any)/(:any).png'] = 'ajax_product/preview/$1/$2/$3/$4/$5';

$route['partner/design/(:any)'] = 'partner/design_$1';
$route['partner/design/(:any)/(:num)'] = 'partner/design_$1/$2';

$route['post/(:num)-(:any)'] = 'blog/post/$1/$2';