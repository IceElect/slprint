<?php

$config['prints']['config'] = array(
    0 => array(),
    'manshort' => array(
        'background_type' => 'color', 
        'result_w' => 3508, 
        'result_h' => 4961,
        'original_w' => 500,
        'original_h' => 500,
        'workarea_w' => 264, 
        'workarea_h' => 372.5, 
        'image_ratio' => 13.28931297709924, 
        'font_ratio' => 13.28931297709924,
    ),
    'manshort' => array(
        'background_type' => 'color', 
        'result_w' => 3508, 
        'result_h' => 4961,
        'original_w' => 500,
        'original_h' => 500,
        'workarea_w' => 220, 
        'workarea_h' => 311, 
        'image_ratio' => 15.9454545, 
        'font_ratio' => 15.9454545,
    ),
    'womanshort' => array(
        'background_type' => 'color', 
        'result_w' => 3508, 
        'result_h' => 4961, 
        'original_w' => 500,
        'original_h' => 500,
        'workarea_w' => 220, 
        'workarea_h' => 311, 
        'image_ratio' => 15.9454545, 
        'font_ratio' => 15.9454545,
    ),
    'sign' => array(
        'background_type' => 'color', 
        'result_w' => 640, 
        'result_h' => 640, 
        'original_w' => 500,
        'original_h' => 500,
        'workarea_w' => 566, 
        'workarea_h' => 566, 
        'image_ratio' => 1.11, 
        'font_ratio' => 1.11,
    ),
    'mug_white' => array(
        'background_type' => 'color', 
        'result_w' => 2362, 
        'result_h' => 1122, 
        'original_w' => 500,
        'original_h' => 500,
        'workarea_w' => 400.5, 
        'workarea_h' => 190, 
        'image_ratio' => 5.89762797, 
        'font_ratio' => 5.89762797,
    ),
    'mug_twotone' => array(
        'background_type' => 'image', 
        'result_w' => 2362, 
        'result_h' => 1122, 
        'original_w' => 500,
        'original_h' => 500,
        'workarea_w' => 402, 
        'workarea_h' => 191, 
        'image_ratio' => 5.87562189, 
        'font_ratio' => 5.87562189,
    ),
    'sign' => array(
        'background_type' => 'color',
        'result_w' => 568,
        'result_h' => 568,
        'original_w' => 568,
        'original_h' => 568,
        'workarea_w' => 568,
        'workarea_h' => 568,
        'image_ratio' => 1,
        'font_ratio' => 1,
    ),
    'man_sweatshirt' => array(
        'background_type' => 'color', 
        'result_w' => 3508, 
        'result_h' => 4961, 
        'original_w' => 1000,
        'original_h' => 1000,
        'workarea_w' => 206, 
        'workarea_h' => 290, 
        'image_ratio' => 17.0291262, 
        'font_ratio' => 17.0291262,
    ), 
);

$config['prints']['fonts'] = array(
    'Arial' => 'Arial.ttf',
    'Andika' => 'Andika.ttf',
    'Lobster' => 'Lobster.ttf',
    'Ruslan Display' => 'Ruslan_Display.ttf',
    'Seymour One' => 'SeymourOne.ttf',
    'Amatic SC' => 'AmaticSC.ttf',
    'Russo One' => 'RussoOne.ttf',
    'American Captain Cyrillic' => 'AmericanCaptainCyrillic.ttf',
    'Caveat' => 'Caveat.ttf',
    'Pacifico' => 'Pacifico.ttf',
    'Bad Script' => 'BadScript.ttf',
);