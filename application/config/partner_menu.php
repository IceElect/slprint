<?php
$config['menu'] = array();
$config['menu'][] = array(
	'icon' => 'ft-home',
	'title' => 'Главная',
	'link' => '/dashboard',
);
$config['menu'][] = array(
	'icon' => 'ft-eye',
	'title' => 'Мои дизайны',
	'link' => '/design',
);
$config['menu'][] = array(
	'icon' => 'ft-upload',
	'title' => 'Реферальные Ссылки',
	'link' => '/referal_link_list',
);
$config['menu'][] = array(
	'icon' => 'ft-upload',
	'title' => 'Выплаты',
	'link' => '/withdraw_list',
);
$config['menu'][] = array(
	'icon' => 'ft-download',
	'title' => 'Заказы',
	'link' => '/order_list',
);
$config['menu'][] = array(
	'icon' => 'ft-help-circle',
	'title' => 'Вопрос-ответ',
	'link' => '/faq',
);