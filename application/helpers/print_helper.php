<?php

class Print_object{

    private $CI;
    private $type;
    private $side;
    private $fonts;
    private $config;
    private $workarea = false;
    private $size = array();
    private $workarea_size = array();
    private $preview_size = array();

    private $path = '';
    private $thumb;

    private $sizes = false;
    private $design_id = false;
    private $background_type = 'color';

    private $sizes_config = array(
        0 => array(),
        'manshort' => array(
            'background_type' => 'color', 
            'result_w' => 3508, 
            'result_h' => 4961,
            'original_w' => 500,
            'original_h' => 500,
            'workarea_w' => 264, 
            'workarea_h' => 372.5, 
            'image_ratio' => 13.28931297709924, 
            'font_ratio' => 13.28931297709924,
        ),
        'manshort' => array(
            'background_type' => 'color', 
            'result_w' => 3508, 
            'result_h' => 4961,
            'original_w' => 500,
            'original_h' => 500,
            'workarea_w' => 220, 
            'workarea_h' => 311, 
            'image_ratio' => 15.9454545, 
            'font_ratio' => 15.9454545,
        ),
        'womanshort' => array(
            'background_type' => 'color', 
            'result_w' => 3508, 
            'result_h' => 4961, 
            'original_w' => 500,
            'original_h' => 500,
            'workarea_w' => 220, 
            'workarea_h' => 311, 
            'image_ratio' => 15.9454545, 
            'font_ratio' => 15.9454545,
        ),
        'mug_white' => array(
            'background_type' => 'color', 
            'result_w' => 2362, 
            'result_h' => 1122, 
            'original_w' => 500,
            'original_h' => 500,
            'workarea_w' => 400.5, 
            'workarea_h' => 190, 
            'image_ratio' => 5.89762797, 
            'font_ratio' => 5.89762797,
        ),
        'mug_twotone' => array(
            'background_type' => 'image', 
            'result_w' => 2362, 
            'result_h' => 1122, 
            'original_w' => 500,
            'original_h' => 500,
            'workarea_w' => 402, 
            'workarea_h' => 191, 
            'image_ratio' => 5.87562189, 
            'font_ratio' => 5.87562189,
        ),
        'sign' => array(
            'background_type' => 'color',
            'result_w' => 568,
            'result_h' => 568,
            'original_w' => 568,
            'original_h' => 568,
            'workarea_w' => 568,
            'workarea_h' => 568,
            'image_ratio' => 1,
            'font_ratio' => 1,
        ),
        'man_sweatshirt' => array(
            'background_type' => 'color', 
            'result_w' => 3508, 
            'result_h' => 4961, 
            'original_w' => 1000,
            'original_h' => 1000,
            'workarea_w' => 206, 
            'workarea_h' => 290, 
            'image_ratio' => 17.0291262, 
            'font_ratio' => 17.0291262,
        ),  
    );

    function __construct($design_id, $type, $config){
        $this->CI =& get_instance();

        $this->type = $type;
        $this->initSizes($type);
        $this->font_ratio = $this->sizes['font_ratio'];
        $this->image_ratio = $this->sizes['image_ratio'];
        $this->config = (object) $config;
        $this->design_id = $design_id;

        if (!isset($_SERVER) || !isset($_SERVER['HTTP_HOST']) || '' == $_SERVER['HTTP_HOST']) {
            $g_sHostname = getenv("HOSTNAME");
        } else {
            $g_sHostname = $_SERVER['HTTP_HOST'];
        }

        if (preg_match("`test\.print\.slto\.ru$`i", $g_sHostname)) {
            $this->path = '/var/www/print.slto.ru/public_html/';
        }

        $this->font_ratio = $this->font_ratio / $this->config->mobile_scale;
        $this->image_ratio = $this->image_ratio / $this->config->mobile_scale;

        $config = $this->CI->load->config('prints');
        $this->fonts = $config['fonts'];
    }

    function initSizes($type = 1){
        $data = $this->sizes_config[$type];

        $this->preview_size = array(2560, 2560);

        if($data['result_w'] < 2560 || $data['result_h'] < 2560){ 
            $this->preview_size = array(640, 640);
        }
        if($type == 'mug'){
            $this->preview_size = array(2000, 2000);
        }

        $this->sizes = $data;
        $this->background_type = $data['background_type'];
    }

    function setWorkarea($workarea){
        $this->workarea = (array) $workarea;
    }
    function setObjects($objects){
        if(!$objects || $objects == "false") return false;

        foreach($objects as $key => $object){
            $object = (array) $object;
            switch($object['type']){
                case 'image':
                    $this->addImage($object);
                break;
                case 'text':
                    $this->addText($object);
                break;
            }
        }
    }
    function setSides($sides){
        $this->sides = (array) $sides;
    }
    function setColors($colors){
        $this->available_colors = $colors;
    }

    function addText($object){
        $background = imagecreatetruecolor($this->preview_size[0], $this->preview_size[1]);

        $x = $object['left'] - $this->workarea['left'];
        $size = ($object['fontSize']) * $object['scaleX'];
        $color = $object['fill'];
        $text = $object['text'];

        $text_rows = explode ( "\n", $text );

        $font_name = $object['fontFamily'];
        $font_name = str_replace("'", '', $font_name);
        $font = FCPATH . '/fonts/' . $this->fonts[$font_name];

        $font_size = (3/4*$size) * $this->font_ratio;

        if($color == '#000' || $color == '#000000' || $color == 'black'){
            $color = '#000001';
        }

        if($color == '#fff' || $color == '#ffffff' || $color == 'white'){
            $color = '#fffffe';
        }
        
        $color = $this->_hex2rgb($color);
        $text_color = imagecolorallocate($this->thumb, $color[0], $color[1], $color[2]);

        // $draw = new \ImagickDraw();
        // $draw->setStrokeColor(new \ImagickPixel('black'));
        // $draw->setFillColor(new \ImagickPixel('red'));
        // $draw->setStrokeWidth(1);
        // $draw->setFontSize(36);

        // // First box
        // $draw->setTextAlignment(\Imagick::ALIGN_LEFT);
        // $draw->annotation(250, 75, "First line\nSecond Line");

        // $image->drawImage($draw);

        // return;

        //imagettftext($this->thumb, $font_size, 0, $x * $this->ratio, $y * $this->ratio, $text_color, $font, $text);
        $height_tmp = $object['top'] - $this->workarea['top'] + 2;
        $height_tmp += $object['scaleX'];
        $height_tmp = $height_tmp * $this->font_ratio;

        $long_row = $text_rows[0];
        $long_row_length = mb_strlen($text_rows[0]);
        foreach($text_rows as $key => $text){
            if(mb_strlen($text) > $long_row_length){
                $long_row = $text;
                $long_row_length = mb_strlen($text);
            }
        }
        $long_row = imagettfbbox($font_size, 0, $font, $long_row);
        $box_width = abs($long_row[0])+abs($long_row[2]);

        foreach($text_rows as $key => $text){
            $dims = imagettfbbox($font_size, 0, $font, $text);

            $ascent = abs($dims[7]);
            $descent = abs($dims[1]);
            $width = abs($dims[0])+abs($dims[2]);
            $height = $ascent+$descent;

            $x = (($this->sizes['result_w']/2) - ($width/2));
            $y = ($height) + $height_tmp;

            $x = $object['left'] - $this->workarea['left'];
            $x = $x * $this->font_ratio;
            $x += ($box_width/2) - ($width/2);

            imagettftext($this->thumb, $font_size, 0, $x, $y, -$text_color, $font, $text);

            $height_tmp += ($height * 1.4) + $object['scaleX'] * $this->font_ratio;
        }
    }

    function addImage($object){
        $path_data = parse_url($object['src']);
        $path = FCPATH . $path_data['path'];

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime_type = finfo_file($finfo, $path);
        finfo_close($finfo);

        $ratio = $this->image_ratio;

        $width = $object['width'] * $object['scaleX'] * $ratio;
        $height = $object['height'] * $object['scaleY'] * $ratio;

        $top = ($object['top'] - $this->workarea['top'] ) * $ratio;
        $left = ($object['left'] - $this->workarea['left'] ) * $ratio;

        switch ($mime_type){
            case "image/jpeg":
                $img = imagecreatefromjpeg($path);
                break;
            case "image/png":
                $img = imagecreatefrompng($path);
                $background = imagecolorallocate($img, 1, 1, 1);
                imagecolortransparent($img, $background);
                imagealphablending($img, false);
                imagesavealpha($img, true);
                break;
            case "image/gif":
                $img = imagecreatefromgif($path);
                $background = imagecolorallocate($img, 1, 1, 1);
                imagecolortransparent($img, $background);
                break;
        }

        imagecopyresized($this->thumb, $img, $left, $top, 0, 0, $width, $height, $object['width'], $object['height']);
    }

    function generatePreview($type, $side_type, $design_id = false, $default_type = false)
    {
        //$preview_path = $this->path . 'uploads/prints/' . $design_id . '/' . $this->type . '/preview/' . $side_type . '.png'; // Print image path
        //if (!file_exists($preview_path)) {
            if ($design_id)
                $this->design_id = $design_id;

            $is_default = ($type == $default_type);

            $side_type = 'front';

            $colors = $this->available_colors;
            $color = array_shift($colors);

            // Paths
            $path = $this->path . 'uploads/prints/' . $design_id . '/' . $this->type . '/' . $side_type . '.png'; // Print image path
            $save_path = $this->path . 'uploads/prints/' . $design_id . '/' . $this->type . '/' . $side_type . '-' . $color['name'] . '-preview.png'; // Preview image path
            $side_image_path = FCPATH . 'images/constructor/' . $this->type . '-' . $side_type . '.png'; // Side image path

            if ($this->background_type == 'image') {
                $side_image_path = FCPATH . 'images/constructor/' . $this->type . '/' . $side_type . '-' . $color['name'] . '.png'; // Side image path
            }

            if ($this->type == $default_type) {
                $path = $this->path . 'uploads/prints/' . $design_id . '/' . $side_type . '.png'; // Print image path
            }

            // Sizes
            $a = 4;
            if ($this->sizes['result_w'] < 2560 || $this->sizes['result_h'] < 2560) {
                $a = 1;
            }
            $width = $this->sizes['workarea_w'] * $a;
            $height = $this->sizes['workarea_h'] * $a;
            $top = ($this->preview_size[0] - $height) / 2;
            $left = ($this->preview_size[1] - $width) / 2;

            // Preview mask
            // $background = imagecreatetruecolor($this->preview_size[0], $this->preview_size[1]); // Create blank preview mask
            // $transparent = imagecolorallocatealpha($background, 255, 255, 255, 0); // Get transparent color
            // imagefill($background, 0, 0, $transparent); // Fill preview transparent
            // if($this->background_type == 'color'){
            //     if(isset($this->config['background_color'])){
            //         $color_rgb = $this->_colorName2Rgb($this->config['background_color']);
            //         $background_color = imagecolorallocate($background, $color_rgb[0], $color_rgb[1], $color_rgb[2]);
            //         imagefill($background, 0, 0, $background_color);
            //     }
            // }


            // switch($this->type){
            //     case 'sign':
            //         // Side
            //         $side = imagecreatefrompng($side_image_path); // Create side image
            //         imagealphablending($side, false);
            //         imagesavealpha($side, true);

            //         imagecopyresized($background, $print, $left, $top, 0, 0, $this->sizes['workarea_w'], $this->sizes['workarea_h'], $this->sizes['result_w'], $this->sizes['result_h']); // Add print image on preview mask
            //         imagecopyresampled($background, $side, 0, 0, 0, 0, $this->preview_size[0], $this->preview_size[1], 500, 500); // Add side image on preview mask
            //         break;
            //     default:
            //         //foreach($this->available_colors as $product_type => $color){
            //             // Side
            //             //$side_image_path = FCPATH . 'constructor/'.$type.'-'.$side_type.'-'.$color->name.'.png'; // Side image path
            //             $side = imagecreatefrompng($side_image_path); // Create side image
            //             imagealphablending($side, false);
            //             imagesavealpha($side, true);

            //             imagecopyresized($background, $side, 0, 0, 0, 0, $this->preview_size[0], $this->preview_size[1], 500, 500); // Add side image on preview mask
            //             imagecopyresampled($background, $print, $left, $top, 0, 0, $width, $height, $this->sizes['result_w'], $this->sizes['result_h']); // Add print image on preview mask
            //         //}
            //         break;
            // }

            switch ($this->type) {
                case 'sign':
                case 'mug_twotone':
                    $print = imagecreatefrompng($path); // Create print image
                    $print_bg = imagecolorallocate($print, 1, 1, 1); //  Get color
                    imagecolortransparent($print, $print_bg);

                    // Side
                    $side = imagecreatefrompng($side_image_path); // Create side image
                    imagealphablending($side, false);
                    imagesavealpha($side, true);

                    $background = $this->_generateBackground($this->config->background_color);

                    imagecopyresized($background, $print, $left, $top, 0, 0, $this->sizes['workarea_w'], $this->sizes['workarea_h'], $this->sizes['result_w'], $this->sizes['result_h']); // Add print image on preview mask
                    imagecopyresampled($background, $side, 0, 0, 0, 0, $this->preview_size[0], $this->preview_size[1], 500, 500); // Add side image on preview mask

                    // Output
                    imagepng($background, FCPATH . $save_path);
                    imagedestroy($background);
                    imagedestroy($print);
                    imagedestroy($side);

                    $result = array();
                    $result[$color["name"]] = array("front" => $save_path);
                    break;
                default:
                    $result = $this->generateSides($is_default);
                    break;
            }

            //file_put_contents($preview_path, $result);

        //} else {
        //    $result = file_get_contents($preview_path);
        //}

        return $result;
    }

    function generateSides($is_default = 0){
        $a = 4;
        if($this->sizes['result_w'] < 2560 || $this->sizes['result_h'] < 2560){$a = 1;}
        $width = $this->sizes['workarea_w'] * $a;
        $height = $this->sizes['workarea_h'] * $a;
        $top = ($this->preview_size[0] - $height) / 2;
        $left = ($this->preview_size[1] - $width) / 2;

        $result = array();

        foreach($this->available_colors as $color){
            //$background_ = $this->_generateBackground($color['code']);

            if(!isset($result[$color['name']])){
                $result[$color['name']] = array();
            }

            foreach($this->sides as $side_type => $side_object){
                //if(шы($side_object) < 1) continue;
                $background = $this->_generateBackground($color['code']);

                $path = $this->path . 'uploads/prints/'.$this->design_id . '/' . $this->type .'/' . $side_type.'.png'; // Print image path
                if($is_default)
                    $path = $this->path . 'uploads/prints/'.$this->design_id .'/' . $side_type.'.png'; // Print image path
                $save_path = 'uploads/prints/' . $this->design_id . '/' . $this->type .'/' . $side_type . '-' .$color['name']. '-preview.png'; // Preview image path
                $side_image_path = FCPATH . 'images/constructor/'.$this->type.'-'.$side_type.'.png'; // Side image path

                if($this->background_type == 'image'){
                    $side_image_path = FCPATH . 'images/constructor/'.$this->type.'/'.$side_type.'-black.png'; // Side image path
                }

                // Print
                $print = imagecreatefrompng($path); // Create print image
                $print_bg = imagecolorallocate($print, 0, 0, 0); //  Get color
                imagecolortransparent($print, $print_bg);

                // Side
                $side = imagecreatefrompng($side_image_path); // Create side image
                imagealphablending($side, false);
                imagesavealpha($side, true);

                imagecopyresampled($background, $print, $left, $top, 0, 0, $width, $height, $this->sizes['result_w'], $this->sizes['result_h']); // Add print image on preview mask
                imagecopyresized($background, $side, 0, 0, 0, 0, $this->preview_size[0], $this->preview_size[1], $this->sizes['original_w'], $this->sizes['original_h']); // Add side image on preview mask

                // Output
                imagepng($background, $this->path . $save_path);
                imagedestroy($background);
                imagedestroy($print);
                imagedestroy($side);

                $result[$color['name']][$side_type] = $save_path;
            }
            //imagedestroy($background_);
        }

        return $result;
    }

    function generatePrints($design_id, $type, $default_type = false){
        $default_sizes = $this->sizes_config[$default_type];

        if($default_type && $default_type != $type){
            $this->font_ratio = $default_sizes['font_ratio'];
            $this->image_ratio = $default_sizes['image_ratio'];
        }

        $paths = array();
        foreach($this->sides as $side_type => $side){
            $this->thumb = imagecreatetruecolor($this->sizes['result_w'], $this->sizes['result_h']);

            $transparent = imagecolorallocatealpha($this->thumb, 0, 0, 0, 127);
            imagefill($this->thumb, 0, 0, $transparent);

            imagealphablending($this->thumb, false);
            imagesavealpha($this->thumb, true);

            $objects = $side;
            @$workarea = array_shift($objects);

            $this->setWorkarea($workarea);
            if($type == $default_type){
                $this->setObjects($objects);
                $save_path = $this->path . 'uploads/prints/'.$design_id.'/'.$side_type.'.png';
            }else{
                $path = $this->path . 'uploads/prints/'.$design_id.'/' .$side_type. '.png';

                $width = $default_sizes['result_w'];
                $height = $default_sizes['result_h'];

                $new_width = $this->sizes['result_w'];
                $new_height = $this->sizes['result_h'];

                $ratio = $height / $width;



                if($new_width != $this->sizes['result_w']){
                    $new_width = $this->sizes['result_w'];
                }
                $new_height = $new_width * $ratio;

                $top = ($this->sizes['result_h'] / 2) - ($new_height / 2);
                $left = ($this->sizes['result_w'] / 2) - ($new_width / 2);

                $img = imagecreatefrompng($path);
                $background = imagecolorallocate($img, 1, 1, 1);
                imagecolortransparent($img, $background);
                imagealphablending($img, false);
                imagesavealpha($img, true);

                imagecopyresized($this->thumb, $img, $left, $top, 0, 0, $new_width, $new_height, $width, $height);
                $save_path = 'uploads/prints/'.$design_id.'/'.$type.'/'.$side_type.'.png';
                imagedestroy($img);
            }

            imagepng($this->thumb, $this->path . $save_path);
            imagedestroy($this->thumb);

            $paths[$side_type] = $save_path;
        }

        return $paths;
    }

    function _generateBackground($color){
        $background = imagecreatetruecolor($this->preview_size[0], $this->preview_size[1]); // Create blank preview mask
        $transparent = imagecolorallocatealpha($background, 255, 255, 255, 0); // Get transparent color
        imagefill($background, 0, 0, $transparent); // Fill preview transparent
        if($this->background_type == 'color'){
            if(isset($color)){
                $color_rgb = $this->_colorName2Rgb($color);
                $background_color = imagecolorallocate($background, $color_rgb[0], $color_rgb[1], $color_rgb[2]);
                imagefill($background, 0, 0, $background_color);
            }
        }

        return $background;
    }

    function _hex2rgb($hex) {
        $hex = str_replace("#", "", $hex);

        if(strlen($hex) == 3) {
          $r = hexdec(substr($hex,0,1).substr($hex,0,1));
          $g = hexdec(substr($hex,1,1).substr($hex,1,1));
          $b = hexdec(substr($hex,2,1).substr($hex,2,1));
        } else {
          $r = hexdec(substr($hex,0,2));
          $g = hexdec(substr($hex,2,2));
          $b = hexdec(substr($hex,4,2));
        }
        $rgb = array($r, $g, $b);
        //return implode(",", $rgb); // returns the rgb values separated by commas
        return $rgb; // returns an array with the rgb values
    }

    function _colorName2Rgb($name){
        $colors  =  ARRAY( 
            "black"=>array( "red"=>0x00,  "green"=>0x00,  "blue"=>0x00), 
            "maroon"=>array( "red"=>0x80,  "green"=>0x00,  "blue"=>0x00), 
            "green"=>array( "red"=>0x00,  "green"=>0x80,  "blue"=>0x00), 
            "olive"=>array( "red"=>0x80,  "green"=>0x80,  "blue"=>0x00), 
            "navy"=>array( "red"=>0x00,  "green"=>0x00,  "blue"=>0x80), 
            "purple"=>array( "red"=>0x80,  "green"=>0x00,  "blue"=>0x80), 
            "teal"=>array( "red"=>0x00,  "green"=>0x80,  "blue"=>0x80), 
            "gray"=>array( "red"=>0x80,  "green"=>0x80,  "blue"=>0x80), 
            "silver"=>array( "red"=>0xC0,  "green"=>0xC0,  "blue"=>0xC0), 
            "red"=>array( "red"=>0xFF,  "green"=>0x00,  "blue"=>0x00), 
            "lime"=>array( "red"=>0x00,  "green"=>0xFF,  "blue"=>0x00), 
            "yellow"=>array( "red"=>0xFF,  "green"=>0xFF,  "blue"=>0x00), 
            "blue"=>array( "red"=>0x00,  "green"=>0x00,  "blue"=>0xFF), 
            "fuchsia"=>array( "red"=>0xFF,  "green"=>0x00,  "blue"=>0xFF), 
            "aqua"=>array( "red"=>0x00,  "green"=>0xFF,  "blue"=>0xFF), 
            "white"=>array( "red"=>0xFF,  "green"=>0xFF,  "blue"=>0xFF), 
 
            //  Additional  colors  as  they  are  used  by  Netscape  and  IE 
            "aliceblue"=>array( "red"=>0xF0,  "green"=>0xF8,  "blue"=>0xFF), 
            "antiquewhite"=>array( "red"=>0xFA,  "green"=>0xEB,  "blue"=>0xD7), 
            "aquamarine"=>array( "red"=>0x7F,  "green"=>0xFF,  "blue"=>0xD4), 
            "azure"=>array( "red"=>0xF0,  "green"=>0xFF,  "blue"=>0xFF), 
            "beige"=>array( "red"=>0xF5,  "green"=>0xF5,  "blue"=>0xDC), 
            "blueviolet"=>array( "red"=>0x8A,  "green"=>0x2B,  "blue"=>0xE2), 
            "brown"=>array( "red"=>0xA5,  "green"=>0x2A,  "blue"=>0x2A), 
            "burlywood"=>array( "red"=>0xDE,  "green"=>0xB8,  "blue"=>0x87), 
            "cadetblue"=>array( "red"=>0x5F,  "green"=>0x9E,  "blue"=>0xA0), 
            "chartreuse"=>array( "red"=>0x7F,  "green"=>0xFF,  "blue"=>0x00), 
            "chocolate"=>array( "red"=>0xD2,  "green"=>0x69,  "blue"=>0x1E), 
            "coral"=>array( "red"=>0xFF,  "green"=>0x7F,  "blue"=>0x50), 
            "cornflowerblue"=>array( "red"=>0x64,  "green"=>0x95,  "blue"=>0xED), 
            "cornsilk"=>array( "red"=>0xFF,  "green"=>0xF8,  "blue"=>0xDC), 
            "crimson"=>array( "red"=>0xDC,  "green"=>0x14,  "blue"=>0x3C), 
            "darkblue"=>array( "red"=>0x00,  "green"=>0x00,  "blue"=>0x8B), 
            "darkcyan"=>array( "red"=>0x00,  "green"=>0x8B,  "blue"=>0x8B), 
            "darkgoldenrod"=>array( "red"=>0xB8,  "green"=>0x86,  "blue"=>0x0B), 
            "darkgray"=>array( "red"=>0xA9,  "green"=>0xA9,  "blue"=>0xA9), 
            "darkgreen"=>array( "red"=>0x00,  "green"=>0x64,  "blue"=>0x00), 
            "darkkhaki"=>array( "red"=>0xBD,  "green"=>0xB7,  "blue"=>0x6B), 
            "darkmagenta"=>array( "red"=>0x8B,  "green"=>0x00,  "blue"=>0x8B), 
            "darkolivegreen"=>array( "red"=>0x55,  "green"=>0x6B,  "blue"=>0x2F), 
            "darkorange"=>array( "red"=>0xFF,  "green"=>0x8C,  "blue"=>0x00), 
            "darkorchid"=>array( "red"=>0x99,  "green"=>0x32,  "blue"=>0xCC), 
            "darkred"=>array( "red"=>0x8B,  "green"=>0x00,  "blue"=>0x00), 
            "darksalmon"=>array( "red"=>0xE9,  "green"=>0x96,  "blue"=>0x7A), 
            "darkseagreen"=>array( "red"=>0x8F,  "green"=>0xBC,  "blue"=>0x8F), 
            "darkslateblue"=>array( "red"=>0x48,  "green"=>0x3D,  "blue"=>0x8B), 
            "darkslategray"=>array( "red"=>0x2F,  "green"=>0x4F,  "blue"=>0x4F), 
            "darkturquoise"=>array( "red"=>0x00,  "green"=>0xCE,  "blue"=>0xD1), 
            "darkviolet"=>array( "red"=>0x94,  "green"=>0x00,  "blue"=>0xD3), 
            "deeppink"=>array( "red"=>0xFF,  "green"=>0x14,  "blue"=>0x93), 
            "deepskyblue"=>array( "red"=>0x00,  "green"=>0xBF,  "blue"=>0xFF), 
            "dimgray"=>array( "red"=>0x69,  "green"=>0x69,  "blue"=>0x69), 
            "dodgerblue"=>array( "red"=>0x1E,  "green"=>0x90,  "blue"=>0xFF), 
            "firebrick"=>array( "red"=>0xB2,  "green"=>0x22,  "blue"=>0x22), 
            "floralwhite"=>array( "red"=>0xFF,  "green"=>0xFA,  "blue"=>0xF0), 
            "forestgreen"=>array( "red"=>0x22,  "green"=>0x8B,  "blue"=>0x22), 
            "gainsboro"=>array( "red"=>0xDC,  "green"=>0xDC,  "blue"=>0xDC), 
            "ghostwhite"=>array( "red"=>0xF8,  "green"=>0xF8,  "blue"=>0xFF), 
            "gold"=>array( "red"=>0xFF,  "green"=>0xD7,  "blue"=>0x00), 
            "goldenrod"=>array( "red"=>0xDA,  "green"=>0xA5,  "blue"=>0x20), 
            "greenyellow"=>array( "red"=>0xAD,  "green"=>0xFF,  "blue"=>0x2F), 
            "honeydew"=>array( "red"=>0xF0,  "green"=>0xFF,  "blue"=>0xF0), 
            "hotpink"=>array( "red"=>0xFF,  "green"=>0x69,  "blue"=>0xB4), 
            "indianred"=>array( "red"=>0xCD,  "green"=>0x5C,  "blue"=>0x5C), 
            "indigo"=>array( "red"=>0x4B,  "green"=>0x00,  "blue"=>0x82), 
            "ivory"=>array( "red"=>0xFF,  "green"=>0xFF,  "blue"=>0xF0), 
            "khaki"=>array( "red"=>0xF0,  "green"=>0xE6,  "blue"=>0x8C), 
            "lavender"=>array( "red"=>0xE6,  "green"=>0xE6,  "blue"=>0xFA), 
            "lavenderblush"=>array( "red"=>0xFF,  "green"=>0xF0,  "blue"=>0xF5), 
            "lawngreen"=>array( "red"=>0x7C,  "green"=>0xFC,  "blue"=>0x00), 
            "lemonchiffon"=>array( "red"=>0xFF,  "green"=>0xFA,  "blue"=>0xCD), 
            "lightblue"=>array( "red"=>0xAD,  "green"=>0xD8,  "blue"=>0xE6), 
            "lightcoral"=>array( "red"=>0xF0,  "green"=>0x80,  "blue"=>0x80), 
            "lightcyan"=>array( "red"=>0xE0,  "green"=>0xFF,  "blue"=>0xFF), 
            "lightgoldenrodyellow"=>array( "red"=>0xFA,  "green"=>0xFA,  "blue"=>0xD2), 
            "lightgreen"=>array( "red"=>0x90,  "green"=>0xEE,  "blue"=>0x90), 
            "lightgrey"=>array( "red"=>0xD3,  "green"=>0xD3,  "blue"=>0xD3), 
            "lightpink"=>array( "red"=>0xFF,  "green"=>0xB6,  "blue"=>0xC1), 
            "lightsalmon"=>array( "red"=>0xFF,  "green"=>0xA0,  "blue"=>0x7A), 
            "lightseagreen"=>array( "red"=>0x20,  "green"=>0xB2,  "blue"=>0xAA), 
            "lightskyblue"=>array( "red"=>0x87,  "green"=>0xCE,  "blue"=>0xFA), 
            "lightslategray"=>array( "red"=>0x77,  "green"=>0x88,  "blue"=>0x99), 
            "lightsteelblue"=>array( "red"=>0xB0,  "green"=>0xC4,  "blue"=>0xDE), 
            "lightyellow"=>array( "red"=>0xFF,  "green"=>0xFF,  "blue"=>0xE0), 
            "limegreen"=>array( "red"=>0x32,  "green"=>0xCD,  "blue"=>0x32), 
            "linen"=>array( "red"=>0xFA,  "green"=>0xF0,  "blue"=>0xE6), 
            "mediumaquamarine"=>array( "red"=>0x66,  "green"=>0xCD,  "blue"=>0xAA), 
            "mediumblue"=>array( "red"=>0x00,  "green"=>0x00,  "blue"=>0xCD), 
            "mediumorchid"=>array( "red"=>0xBA,  "green"=>0x55,  "blue"=>0xD3), 
            "mediumpurple"=>array( "red"=>0x93,  "green"=>0x70,  "blue"=>0xD0), 
            "mediumseagreen"=>array( "red"=>0x3C,  "green"=>0xB3,  "blue"=>0x71), 
            "mediumslateblue"=>array( "red"=>0x7B,  "green"=>0x68,  "blue"=>0xEE), 
            "mediumspringgreen"=>array( "red"=>0x00,  "green"=>0xFA,  "blue"=>0x9A), 
            "mediumturquoise"=>array( "red"=>0x48,  "green"=>0xD1,  "blue"=>0xCC), 
            "mediumvioletred"=>array( "red"=>0xC7,  "green"=>0x15,  "blue"=>0x85), 
            "midnightblue"=>array( "red"=>0x19,  "green"=>0x19,  "blue"=>0x70), 
            "mintcream"=>array( "red"=>0xF5,  "green"=>0xFF,  "blue"=>0xFA), 
            "mistyrose"=>array( "red"=>0xFF,  "green"=>0xE4,  "blue"=>0xE1), 
            "moccasin"=>array( "red"=>0xFF,  "green"=>0xE4,  "blue"=>0xB5), 
            "navajowhite"=>array( "red"=>0xFF,  "green"=>0xDE,  "blue"=>0xAD), 
            "oldlace"=>array( "red"=>0xFD,  "green"=>0xF5,  "blue"=>0xE6), 
            "olivedrab"=>array( "red"=>0x6B,  "green"=>0x8E,  "blue"=>0x23), 
            "orange"=>array( "red"=>0xFF,  "green"=>0xA5,  "blue"=>0x00), 
            "orangered"=>array( "red"=>0xFF,  "green"=>0x45,  "blue"=>0x00), 
            "orchid"=>array( "red"=>0xDA,  "green"=>0x70,  "blue"=>0xD6), 
            "palegoldenrod"=>array( "red"=>0xEE,  "green"=>0xE8,  "blue"=>0xAA), 
            "palegreen"=>array( "red"=>0x98,  "green"=>0xFB,  "blue"=>0x98), 
            "paleturquoise"=>array( "red"=>0xAF,  "green"=>0xEE,  "blue"=>0xEE), 
            "palevioletred"=>array( "red"=>0xDB,  "green"=>0x70,  "blue"=>0x93), 
            "papayawhip"=>array( "red"=>0xFF,  "green"=>0xEF,  "blue"=>0xD5), 
            "peachpuff"=>array( "red"=>0xFF,  "green"=>0xDA,  "blue"=>0xB9), 
            "peru"=>array( "red"=>0xCD,  "green"=>0x85,  "blue"=>0x3F), 
            "pink"=>array( "red"=>0xFF,  "green"=>0xC0,  "blue"=>0xCB), 
            "plum"=>array( "red"=>0xDD,  "green"=>0xA0,  "blue"=>0xDD), 
            "powderblue"=>array( "red"=>0xB0,  "green"=>0xE0,  "blue"=>0xE6), 
            "rosybrown"=>array( "red"=>0xBC,  "green"=>0x8F,  "blue"=>0x8F), 
            "royalblue"=>array( "red"=>0x41,  "green"=>0x69,  "blue"=>0xE1), 
            "saddlebrown"=>array( "red"=>0x8B,  "green"=>0x45,  "blue"=>0x13), 
            "salmon"=>array( "red"=>0xFA,  "green"=>0x80,  "blue"=>0x72), 
            "sandybrown"=>array( "red"=>0xF4,  "green"=>0xA4,  "blue"=>0x60), 
            "seagreen"=>array( "red"=>0x2E,  "green"=>0x8B,  "blue"=>0x57), 
            "seashell"=>array( "red"=>0xFF,  "green"=>0xF5,  "blue"=>0xEE), 
            "sienna"=>array( "red"=>0xA0,  "green"=>0x52,  "blue"=>0x2D), 
            "skyblue"=>array( "red"=>0x87,  "green"=>0xCE,  "blue"=>0xEB), 
            "slateblue"=>array( "red"=>0x6A,  "green"=>0x5A,  "blue"=>0xCD), 
            "slategray"=>array( "red"=>0x70,  "green"=>0x80,  "blue"=>0x90), 
            "snow"=>array( "red"=>0xFF,  "green"=>0xFA,  "blue"=>0xFA), 
            "springgreen"=>array( "red"=>0x00,  "green"=>0xFF,  "blue"=>0x7F), 
            "steelblue"=>array( "red"=>0x46,  "green"=>0x82,  "blue"=>0xB4), 
            "tan"=>array( "red"=>0xD2,  "green"=>0xB4,  "blue"=>0x8C), 
            "thistle"=>array( "red"=>0xD8,  "green"=>0xBF,  "blue"=>0xD8), 
            "tomato"=>array( "red"=>0xFF,  "green"=>0x63,  "blue"=>0x47), 
            "turquoise"=>array( "red"=>0x40,  "green"=>0xE0,  "blue"=>0xD0), 
            "violet"=>array( "red"=>0xEE,  "green"=>0x82,  "blue"=>0xEE), 
            "wheat"=>array( "red"=>0xF5,  "green"=>0xDE,  "blue"=>0xB3), 
            "whitesmoke"=>array( "red"=>0xF5,  "green"=>0xF5,  "blue"=>0xF5), 
            "yellowgreen"=>array( "red"=>0x9A,  "green"=>0xCD,  "blue"=>0x32));
        
        $color = $colors['black'];
        if(isset($colors[$name])){
            $color = $colors[$name];
        }else{
            if(mb_substr($name, 0, 1) == '#'){
                $color = array();
                $color["red"] = hexdec(mb_substr($name, 1, 2));
                $color["green"] = hexdec(mb_substr($name, 3, 2));
                $color["blue"] = hexdec(mb_substr($name, 5, 2));
            }
        }

        return array($color["red"], $color["green"], $color["blue"]);
    }
}