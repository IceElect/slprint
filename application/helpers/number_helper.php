<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function num_decline($text, $number, $eArray)
{
    $text = preg_replace('%\%n\%%', $number, $text);
    if(isset($eArray[0])){
        $text = preg_replace('%\%o\%%', num_getEnding($number, $eArray), $text);
    }else{
        $text = preg_replace('%\%o1\%%', num_getEnding($number, array('й', 'я', 'ев')), $text);
        $text = preg_replace('%\%o2\%%', num_getEnding($number, array('ый', 'ых', 'ых')), $text);
        $text = preg_replace('%\%o3\%%', num_getEnding($number, array('ка', 'ок', 'ых')), $text);
        $text = preg_replace('%\%o4\%%', num_getEnding($number, array('ая', 'ых', 'ых')), $text);
        $text = preg_replace('%\%o5\%%', num_getEnding($number, array('ь', 'и', 'ей')), $text);
    }
    return $text;
}

function num_getEnding($number, $endingArray)
{
    $number = $number % 100;
    if ($number>=11 && $number<=19) {
        $ending=$endingArray[2];
    }
    else {
        $i = $number % 10;
        switch ($i)
        {
            case (1): $ending = $endingArray[0]; break;
            case (2):
            case (3):
            case (4): $ending = $endingArray[1]; break;
            default: $ending=$endingArray[2];
        }
    }
    return $ending;
}