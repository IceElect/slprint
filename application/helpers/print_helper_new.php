<?php

class Print_object{

    private $type;
    private $side;
    private $config;
    private $workarea = false;
    private $size = array();
    private $workarea_size = array();
    private $preview_size = array();

    private $path = '';
    private $thumb;

    private $sizes = false;
    private $design_id = false;
    private $background_type = 'color';

    private $sizes_config = array(
        0 => array(),
        'manshort' => array(
            'background_type' => 'color', 
            'result_w' => 3508, 
            'result_h' => 4961,
            'original_w' => 500,
            'original_h' => 500,
            'workarea_w' => 264, 
            'workarea_h' => 372.5, 
            'image_ratio' => 13.28931297709924, 
            'font_ratio' => 13.28931297709924,
        ),
        'manshort' => array(
            'background_type' => 'color', 
            'result_w' => 3508, 
            'result_h' => 4961,
            'original_w' => 500,
            'original_h' => 500,
            'workarea_w' => 220, 
            'workarea_h' => 311, 
            'image_ratio' => 15.9454545, 
            'font_ratio' => 15.9454545,
        ),
        'womanshort' => array(
            'background_type' => 'color', 
            'result_w' => 3508, 
            'result_h' => 4961, 
            'original_w' => 500,
            'original_h' => 500,
            'workarea_w' => 220, 
            'workarea_h' => 311, 
            'image_ratio' => 15.9454545, 
            'font_ratio' => 15.9454545,
        ),
        'mug_white' => array(
            'background_type' => 'color', 
            'result_w' => 2362, 
            'result_h' => 1122, 
            'original_w' => 500,
            'original_h' => 500,
            'workarea_w' => 400.5, 
            'workarea_h' => 190, 
            'image_ratio' => 5.89762797, 
            'font_ratio' => 5.89762797,
        ),
        'mug_twotone' => array(
            'background_type' => 'image', 
            'result_w' => 2362, 
            'result_h' => 1122, 
            'original_w' => 500,
            'original_h' => 500,
            'workarea_w' => 402, 
            'workarea_h' => 191, 
            'image_ratio' => 5.87562189, 
            'font_ratio' => 5.87562189,
        ),
        'sign' => array(
            'background_type' => 'color',
            'result_w' => 568,
            'result_h' => 568,
            'original_w' => 568,
            'original_h' => 568,
            'workarea_w' => 568,
            'workarea_h' => 568,
            'image_ratio' => 1,
            'font_ratio' => 1,
        ),
        'man_sweatshirt' => array(
            'background_type' => 'color', 
            'result_w' => 3508, 
            'result_h' => 4961, 
            'original_w' => 1000,
            'original_h' => 1000,
            'workarea_w' => 206, 
            'workarea_h' => 290, 
            'image_ratio' => 17.0291262, 
            'font_ratio' => 17.0291262,
        ),  
    );

    function __construct($design_id, $type, $config){
        $this->type = $type;
        $this->initSizes($type);
        $this->font_ratio = $this->sizes['font_ratio'];
        $this->image_ratio = $this->sizes['image_ratio'];
        $this->config = (object) $config;
        $this->design_id = $design_id;

        if (!isset($_SERVER) || !isset($_SERVER['HTTP_HOST']) || '' == $_SERVER['HTTP_HOST']) {
            $g_sHostname = getenv("HOSTNAME");
        } else {
            $g_sHostname = $_SERVER['HTTP_HOST'];
        }

        if (preg_match("`test\.print\.slto\.ru$`i", $g_sHostname)) {
            $this->path = '/var/www/print.slto.ru/public_html/';
        }

        $this->font_ratio = $this->font_ratio / $this->config->mobile_scale;
        $this->image_ratio = $this->image_ratio / $this->config->mobile_scale;
    }

    function initSizes($type = 1){
        $data = $this->sizes_config[$type];

        $this->preview_size = array(2560, 2560);

        if($data['result_w'] < 2560 || $data['result_h'] < 2560){ 
            $this->preview_size = array(640, 640);
        }
        if($type == 'mug'){
            $this->preview_size = array(2000, 2000);
        }

        $this->sizes = $data;
        $this->background_type = $data['background_type'];
    }

    function setWorkarea($workarea){
        $this->workarea = (array) $workarea;
    }
    function setObjects($objects){
        if(!$objects || $objects == "false") return false;

        foreach($objects as $key => $object){
            $object = (array) $object;
            switch($object['type']){
                case 'image':
                    $this->addImage($object);
                break;
                case 'text':
                    $this->addText($object);
                break;
            }
        }
    }
    function setSides($sides){
        $this->sides = (array) $sides;
    }
    function setColors($colors){
        $this->available_colors = $colors;
    }

    function generatePrints($design_id, $type, $default_type = false){

    }

}