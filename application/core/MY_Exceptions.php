<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions {

    function __construct(){
        parent::__construct();
        $this->CI =& get_instance();
    }

    public function show_404($page = '', $log_error = TRUE)
    {
        $this->CI->output->set_status_header('404');
        $this->CI->load->library('MY_Smarty');
        $this->CI->load->library('Frontend');
        //echo $this->CI->output->get_output();
        $this->CI->frontend->view('404');
        exit(4);
    }
}