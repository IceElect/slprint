function datatable_render_date(data, type, full, meta){
    var result = data;
    if(type == "display"){
        if (data == null){
            return '';
        }
        if (result != null && result != ''){
            return result.substr(0, 10);/*
             var date_string = result.substr(0, 10) + ' 00:00:00';
             var c = new Date(date_string.replace(/-/g, "/"));
             return jQuery.datepicker.formatDate('yy-mm-dd', c);*/
        }
    }
    return result;
}
function datatable_render_order_status(data, type, full, meta){
    var status_list = {
        0: 'Новый',
        1: 'Принят',
        2: 'Подтвержден',
        3: 'В производстве',
        4: 'Отправлен',
        5: 'Получен',
        6: 'Выдан',
        7: 'Завершен',
        8: 'Отменен'
    };

    return status_list[data];
}
