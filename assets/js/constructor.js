function observeBoolean(property) {
    document.getElementById(property).onclick = function() {
        canvas.item(0)[property] = this.checked;
        canvas.renderAll();
    };
}

function observeNumeric(property) {
    document.getElementById(property).oninput = function() {
        canvas.item(0)[property] = parseFloat(this.value);
        canvas.renderAll();
    };
}

function observeValue(property) {
    document.getElementById(property).oninput = function() {
        canvas.item(0)[property] = this.value;
        canvas.renderAll();
    };
}

function observeRadio(property) {
    document.getElementById(property).onchange = function() {
        var name = document.getElementById(this.id).name;
        canvas.item(0)[name] = this.value;
        canvas.renderAll();
    };
}

function observeOptionsList(property) {
    var list = document.querySelectorAll('#' + property +
    ' [type="checkbox"]');
    for (var i = 0, len = list.length; i < len; i++) {
        list[i].onchange = function() {
        canvas.item(0)[property](this.name, this.checked);
        canvas.renderAll();
        };
    };
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

var workspace_top;
var workspace_left;
var canvas_size = [640, 640];
var workspace_size = false;

var center = {x:canvas_size[0] / 2, y:canvas_size[1] / 2};

var Constuct = function(){
    var self = {canvas: false};
    self.config = {mobile_scale: 1, background_color: "#ffffff", background_color_name: "white"};
    self.props_config = {
        'manshort': {background_type: 'color', sides: {'front': {}, 'back': {}}},
        'womanshort': {background_type: 'color', sides: {'front': {}, 'back': {}}},
        'sign': {background_type: 'color', sides: {'front': {}}},
        'mug_white': {background_type: 'color', sides: {'front': {}}},
        'mug_twotone': {background_type: 'image', sides: {'front': {}}},
        'man_sweatshirt': {background_type: 'color', sides: {'front': {}, 'back': {}}},
    };

    self.init = function(type){
        self.type = type;

        var holder_width = $(".page-layout__main").outerWidth();
        self.config.mobile_scale = holder_width / 640;

        self.config.canvas_size = 640 * self.config.mobile_scale;
        center = {x:self.config.canvas_size / 2, y:self.config.canvas_size / 2};

        self.side = 'front';
        self.props = self.props_config[type];

        self.ratio = 13.38931297709924;

        self.canvases = {};

        if(Object.size(self.props.sides) < 2){
            $(".construct-panel").addClass('only-one-side');
        }

        $.each(self.props.sides, function(key, value){
            $(".construct-panel-sides").append('<div class="construct-panel-side" data-side="'+key+'" onclick="construct.setSide(\''+key+'\')"><img src="/images/constructor/'+type+'-'+key+'.png" style="background-color:#ffffff;"></div>');
            $(".product-constructor-holder").append('<canvas id="product-custructor__'+key+'" class="upper-canvas product-constructor__canvas" width="'+self.config.canvas_size+'" height="'+self.config.canvas_size+'"></canvas>');
            self.canvases[key] = new fabric.Canvas('product-custructor__'+key);
            self.canvases[key].selection = false;
            self.canvases[key]._init = false;

            $("#product-custructor__"+key).parent().hide();
        })

        self.initSizes(type);

        // picker init
        self.picker = new CP(document.querySelector('#text-control-text-color'), false);
        
        self.picker_code = document.createElement('input');
        self.picker_code.className = 'color-code';
        self.picker_code.pattern = '^#[A-Fa-f0-9]{6}$';
        self.picker_code.type = 'text';

        self.picker.self.appendChild(self.picker_code);

        self.setSide('front');
        //self.addTestRect();
    }

    self.initSizes = function(type){
        var sizes = {
            'manshort': {width: 220, height: 311, ratio: 15.9454545},
            'womanshort': {width: 220, height: 311, ratio: 15.9454545},
            'sign': {width: 568, height: 568, ratio: 1},
            'mug_white': {width: 400.5, height: 190, ratio: 5.89762797},
            'mug_twotone': {width: 402, height: 191, ratio: 5.87562189},
            'man_sweatshirt': {width: 206, height: 290, ratio: 17.029},

            // 324 137
        };
        workspace_size = [sizes[type].width * self.config.mobile_scale, sizes[type].height * self.config.mobile_scale];
        self.ratio = sizes[type].ratio / self.config.mobile_scale;
    }

    self.initHandlers = function(){
        self.canvas.on('mouse:up', function(e, selected){
            if('preventDefault' in e)
                e.preventDefault();
            self._setControls(e.target);
        })

        self.canvas.on('mouse:down', (event) => {
            if(event.target !== null){
                //Ignore this event, we are clicking on an object (event.target is clicked object)
                return;
            }
            //... your canvas event handling code...
        })

        self.picker.on("enter", function() {
            self.picker_code.value = '#' + CP._HSV2HEX(this.get());
        });

        self.picker.on("change", function(color) {
            obj = self.canvas.getActiveObject();
            if(obj){
                obj.set('fill', '#' + color);
                $("#text-control-text-color").css('background-color', '#' + color).html("");
                self.canvas.renderAll();
                this.source.value = '#' + color;
                self.picker_code.value = '#' + color;
            }
        });

        $("#text-control-text-color").unbind().on("click", function(e) {
            self.picker[self.picker.visible ? 'exit' : 'enter']();
            self.picker_code.value = '#' + CP._HSV2HEX(self.picker.get());
        });

        self.picker_code.oncut = self._pickerUpdate;
        self.picker_code.onpaste = self._pickerUpdate;
        self.picker_code.onkeyup = self._pickerUpdate;
        self.picker_code.oninput = self._pickerUpdate;

        $(".fonts-select__list-item").unbind().on("click", function(e) {
            var name = $(this).css('font-family');
            $("#text-control-text-font").val(name);
            self._selectTextFont(name);
        })

        // $("#text-control-text-font").unbind().on("change", function(e){
        //    var name = $(this).val();
        //    self._selectTextFont(name);
        // });
    }

    self._pickerUpdate = function(){
        if (this.value.length) {
            self.picker.set(this.value);
            self.picker.fire("change", [this.value.slice(1)]);
        }
    }

    self.changeColor = function(color, color_name){
        var color = $("input[name='attributes[color]']:checked").data('color');
        var color_name = $("input[name='attributes[color]']:checked").val();

        if(self.props.background_type == 'image'){
            self.config.background_color_name = color_name;
        }else{
            self.config.background_color = color;
            self.config.background_color_name = color_name;
            $(".canvas-container").css('background-color', color);
        }
    }

    self.setBackground = function(color){
        if(self.props.background_type == 'image'){
            var URL = "/images/constructor/" + self.type + "/" + self.side + "-" + color + ".png";
        }else{
            var URL = "/images/constructor/" + self.type + "-" + self.side + ".png";
        }

        fabric.Image.fromURL(URL, function (oImg) {
            oImg.scaleToWidth(self.config.canvas_size);

            self.canvas.setOverlayImage(oImg, function() {
                self.canvas.renderAll();
            });
        });
    }

    self.setBackgroundFromImage = function(color){
        if(self.background_type == 'image'){
            var URL = "/images/constructor/" + self.type + "/" + self.side + "-" + color + ".png";
        }else{
            var URL = "/images/constructor/" + self.type + "-" + self.side + ".png";
        }

        fabric.Image.fromURL(URL, function (oImg) {
            oImg.scaleToWidth(self.config.canvas_size);

            self.canvas.setOverlayImage(oImg, function() {
                self.canvas.renderAll();
            });
        });
    }

    self.disableBackground = function(){
        self.current_background = self.canvas.overlayImage;
        self.canvas.setOverlayImage(null, function() {
            self.canvas.renderAll();
        });
    }

    self.enableBackground = function(){
        self.canvas.setOverlayImage(self.current_background, function() {
            self.canvas.renderAll();
        });
    }

    self.setSide = function(side){

        if(!side){
            $(".construct-panel-sides").hide();
        }else{
            $(".construct-panel-sides").show();
            if(self.side != side){
                $("#product-custructor__"+side).parent().show();
                $("#product-custructor__"+self.side).parent().hide();
            }
            if(self.canvas){
                self.canvases[self.side] = self.canvas;
            }
            $("#product-custructor__"+side).parent().show();

            $(".construct-panel-sides .construct-panel-side").removeClass('selected')
            $(".construct-panel-side[data-side='"+side+"']").addClass('selected');

            self.canvas = self.canvases[side];
        }

        self.side = side;

        if(!self.canvas._init){
            self.initHandlers();

            self.setBackground("black");
            self.setWorkspace();

            self.canvas._init = true;
        }
    }

    self.setWorkspace = function(){
        workspace_top = center.x - (workspace_size[1] / 2);
        workspace_left = center.y - (workspace_size[0] / 2);

        var rectObject = {
            stroke: '#606060',
            top: workspace_top,
            left: workspace_left,
            width: workspace_size[0],
            height: workspace_size[1],
            strokeDashArray: [5, 5],
            hasControls: false,
            fill: 'transparent',
            selectable: false,
            clipFor: 'workspace',
        }

        switch(self.type){
            case 'sign':
                rectObject.top;
                rectObject.left;
                rectObject.rx = workspace_size[1]/2;
                rectObject.ry = workspace_size[0]/2;
                self.workspace = new fabric.Ellipse(rectObject);
                break;
            case 'mug':
                rectObject.stroke = false;
                self.workspace = new fabric.Rect(rectObject);
                break;
            default:
                self.workspace = new fabric.Rect(rectObject);
                break;
        }

        self.canvas.add(self.workspace);
    }

    self.addImg = function(URL){
        fabric.Image.fromURL(URL, function (oImg) {
            var maxScaleX = (oImg.width / 100) / (oImg.width / self.ratio);
            var maxScaleY = (oImg.height / 100) / (oImg.height / self.ratio);

            var maxWidthPercent = oImg.width / (workspace_size[0] * self.ratio);
            var maxHeightPercent = oImg.height / (workspace_size[1] * self.ratio);

            var maxScaleX = (workspace_size[0] * maxWidthPercent) / oImg.width;
            var maxScaleY = (workspace_size[1] * maxHeightPercent) / oImg.height;
            //var maxScaleY = oImg.height / (workspace_size[0] * self.ratio);

            var scaleForFullX =  (workspace_size[0] * self.ratio) / oImg.width;
            var scaleForFullY =  (workspace_size[1] * self.ratio) / oImg.height;

            if(maxScaleY > scaleForFullY){
                var scaleY = scaleForFullY;
                var scaleX = scaleForFullY;
            }else{
                if(maxScaleX > scaleForFullX){
                    var scaleX = scaleForFullX;
                    var scaleY = scaleForFullX;
                }else{
                    var scaleX = (maxScaleX > scaleForFullX)?scaleForFullX:maxScaleX;
                    var scaleY = (maxScaleY > scaleForFullY)?scaleForFullY:maxScaleY;
                }
            }

            maxScaleX = maxScaleX * 1.2;
            maxScaleY = maxScaleY * 1.2;

            oImg.set({
                src: URL,
                type: 'image',
                scaleX: financial(scaleX),
                scaleY: financial(scaleY),
                clipName: 'workspace',
                top : center.y - (oImg.height * scaleY / 2),
                left : center.x - (oImg.width * scaleX / 2),
                lastGoodTop: center.y - (oImg.height * scaleY / 2),
                lastGoodLeft: center.y - (oImg.height * scaleY / 2),
            });

            oImg.set({
                clipTo: function(t) {
                    var e = findByClipName(this.clipName);
                    this.setCoords();
                    var r = 1 / this.scaleX,
                        n = 1 / this.scaleY;
                    t.save();
                    var i = -this.width / 2 - e.strokeWidth,
                        o = -this.height / 2 - e.strokeWidth;
                    if (t.translate(i, o), t.rotate(-1 * this.angle * (Math.PI / 180)), t.scale(r, n), t.beginPath(), "rect" === e.type) t.rect(e.left - this.oCoords.tl.x, e.top - this.oCoords.tl.y, e.width + e.strokeWidth, e.height + e.strokeWidth);
                    else if ("ellipse" === e.type) {
                        var a = e.left + e.rx - this.oCoords.tl.x,
                            s = e.top + e.ry - this.oCoords.tl.y;
                        t.moveTo(a, s - e.ry), t.bezierCurveTo(a + 1.33 * e.rx + 1, s - e.ry, a + 1.33 * e.rx + 1, s + e.ry + 1, a, s + e.ry + 1), t.bezierCurveTo(a - 1.33 * e.rx, s + e.ry + 1, a - 1.33 * e.rx, s - e.ry, a, s - e.ry)
                    } else if ("polygon" === e.type) {
                        var c = e.points;
                        t.moveTo(c[0].x - this.oCoords.tl.x, c[0].y - this.oCoords.tl.y);
                        for (var l = 1; l < c.length; l++) t.lineTo(c[l].x - this.oCoords.tl.x, c[l].y - this.oCoords.tl.y)
                    }
                    t.closePath(), t.restore()
                }
            });

            oImg.on('scaling', function() {
                if(this.scaleX > maxScaleX) {
                    this.scaleX = financial(maxScaleX);
                    this.left = this.lastGoodLeft;
                    this.top = this.lastGoodTop;
                }
                if(this.scaleY > maxScaleY) {
                    this.scaleY = financial(maxScaleY);
                    this.left = this.lastGoodLeft;
                    this.top = this.lastGoodTop;
                }
                this.lastGoodTop = this.top;
                this.lastGoodLeft = this.left;
            })

            self.canvas.add(oImg);
            self.canvas.setActiveObject(oImg);

            self._setControls(oImg);

            cart.calcPrice();
        })
    }

    self.addText = function(e){
        e.preventDefault();
        var a = self.canvas.width / 640;
        
        var oText = new fabric.Text('', {
            fontSize: 19 * self.config.mobile_scale,
            type: 'text',
            fill: '#000000',
            textAlign: "center",
            lineHeight: 1,
            clipName: 'workspace',
            fontFamily: 'Arial',
            padding: 0,
            centering_x: true,
            centering_y: true,
        });

        oText.setControlsVisibility({
            ml: false,
            mt: false,
            mr: false,
            mb: false,
        });

        oText.set({
            clipTo: function(ctx) {
                var e = findByClipName(this.clipName);
                this.setCoords();
                var r = 1 / this.scaleX,
                    n = 1 / this.scaleY;
                ctx.save();
                var i = -this.width / 2 - e.strokeWidth,
                    o = -this.height / 2 - e.strokeWidth;
                if (ctx.translate(i, o), ctx.rotate(-1 * this.angle * (Math.PI / 180)), ctx.scale(r, n), ctx.beginPath(), "rect" === e.type) ctx.rect(e.left - this.oCoords.tl.x, e.top - this.oCoords.tl.y, e.width + e.strokeWidth, e.height + e.strokeWidth);
                else if ("ellipse" === e.type) {
                    var a = e.left + e.rx + 2 - this.oCoords.tl.x,
                        s = e.top + e.ry + 2 - this.oCoords.tl.y;
                    ctx.moveTo(a, s - e.ry), ctx.bezierCurveTo(a + 1.33 * e.rx + 1, s - e.ry, a + 1.33 * e.rx + 1, s + e.ry + 1, a, s + e.ry + 1), ctx.bezierCurveTo(a - 1.33 * e.rx, s + e.ry + 1, a - 1.33 * e.rx, s - e.ry, a, s - e.ry)
                } else if ("polygon" === e.type) {
                    var c = e.points;
                    ctx.moveTo(c[0].x - this.oCoords.tl.x, c[0].y - this.oCoords.tl.y);
                    for (var l = 1; l < c.length; l++) ctx.lineTo(c[l].x - this.oCoords.tl.x, c[l].y - this.oCoords.tl.y)
                }
                ctx.closePath(), ctx.restore()
            }
        });

        oText.set({
            left : center.x - (oText.width / 2),
            top : center.y - (oText.height / 2),
        });

        oText.on('moving', function(){
            this.set('centering_x', false);
            this.set('centering_y', false);
        })

        self.canvas.add(oText);
        self.canvas.setActiveObject(oText);

        self._setControls(oText);

        cart.calcPrice();
    }

    self.degToRad = function(degrees) {
        return degrees * (Math.PI / 180);
    }

    self.clipByName = function (ctx) {
        var clipRect = self.findByClipName(this.clipName);
        var scaleXTo1 = (1 / this.scaleX);
        var scaleYTo1 = (1 / this.scaleY);
        ctx.save();
        ctx.translate(0,0);
        ctx.rotate(self.degToRad(this.angle * -1));
        ctx.scale(scaleXTo1, scaleYTo1);
        ctx.beginPath();
        ctx.rect(
            clipRect.left - this.left,
            clipRect.top - this.top,
            clipRect.width,
            clipRect.height
        );
        ctx.closePath();
        ctx.restore();
    }

    self.onUploadComplete = function(data){
        if(data.response == true){
            popup.hide('construct_upload');
            self.addImg(data.src);
        }else{

        }
    }

    self._setControls = function(obj){
        var obj = self.canvas.getActiveObject();

        self.layer = $(".layer-controls");
        self.layer_text = self.layer.find(".text-controls");
        self.layer_image = self.layer.find(".image-controls");

        self.constructor_controls = $(".constructor-controls");
        self.constructor_delete_layer = self.constructor_controls.find('#constructor-control-delete-layer');
        self.constructor_center_horizontal = self.constructor_controls.find('#constructor-control-center-horizontal');
        self.constructor_center_vertical = self.constructor_controls.find('#constructor-control-center-vertical');

        self.layer.hide();
        self.layer_text.hide();
        self.layer_image.hide();
        self.constructor_controls.hide();

        if(self.picker){
            self.picker.exit();
        }

        if(obj){

            $(self.constructor_delete_layer).unbind('click');
            $(self.constructor_delete_layer).on('click', function(e){
                e.preventDefault();
                self.canvas.remove(obj);
                self.canvas.renderAll();
                self._setControls(false);
            })

            switch(obj.type){
                case 'image':
                    $(self.constructor_center_horizontal).unbind('click');
                    $(self.constructor_center_horizontal).on('click', function(e){
                        e.preventDefault();
                        
                        obj.set({
                            top : center.y - (obj.height / 2),
                            centering_y: true,
                        });

                        self.canvas.renderAll();
                    })

                    $(self.constructor_center_vertical).unbind('click');
                    $(self.constructor_center_vertical).on('click', function(e){
                        e.preventDefault();
                        
                        obj.set({
                            left : center.x - (obj.width / 2),
                            centering_x: true,
                        });

                        self.canvas.renderAll();
                    })

                    self.constructor_controls.show();
                break;
                case 'text':
                    self.layer.show();
                    self.layer_text.show();

                    $(self.constructor_center_horizontal).unbind('click');
                    $(self.constructor_center_horizontal).on('click', function(e){
                        e.preventDefault();
                        
                        obj.set({
                            top : center.y - (obj.height * obj.scaleY / 2),
                            centering_y: true,
                        });

                        self.canvas.renderAll();
                    })

                    $(self.constructor_center_vertical).unbind('click');
                    $(self.constructor_center_vertical).on('click', function(e){
                        e.preventDefault();
                        
                        obj.set({
                            left : center.x - (obj.width * obj.scaleX / 2),
                            centering_x: true,
                        });

                        self.canvas.renderAll();
                    })

                    self._initTextControls(obj);
                    self.constructor_controls.show();
                break;
            }
        }else{

        }
    }

    self._initTextControls = function(obj){
        var text = self.layer.find("#text-control-text");
        var font_select = self.layer.find(".text-controls__fonts-select");
        text.val(obj.getText());
        text.focus();
        text.unbind().on('keyup paste', function(e){
            obj.set('textAlign', 'center');
            obj.setText($(this).val());
            self.canvas.renderAll();

            if(obj.centering_x === true){
                obj.set({
                    left : center.x - (obj.width / 2),
                });
            }
            if(obj.centering_y === true){
                obj.set({
                    top : center.y - (obj.height / 2),
                });
            }
        })

        font_select.removeClass('opened');
    }

    self._selectTextFont = function(name){
        var name = name.replace(/^"(.+(?="$))"$/, '$1');
        obj = self.canvas.getActiveObject();
        if(obj){
            obj.set('fontFamily', "\'" + name + "\'");
            $(".fonts-select__selected-font").css('font-family', "'" + name + "'").html(name);
            self.canvas.renderAll();
        }
    }


    self.export = function(){
        var sides = {};
        $.each(self.canvases, function(key, canvas){
            sides[key] = canvas._objects;
        })
        return {sides: sides, config: self.config};
    }

    self.exportSVG = function(){
        var sides = {};
        $.each(self.canvases, function(key, canvas){
            sides[key] = canvas.toSVG({
                viewBox:  {
                    y: workspace_top+1,
                    x: workspace_left+1,
                    width: workspace_size[0]-1,
                    height: workspace_size[1]-1,
                }
            });
        })
        return {sides: sides, config: self.config};
    }

    return self;
}

var construct = new Constuct;

var findByClipName = function(name) {
    return _(construct.canvas.getObjects()).where({
        clipFor: name
    }).first()
}

function financial(x) {
  return Number.parseFloat(x).toFixed(10);
}