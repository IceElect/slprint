document.getElementById( "header-toggle-nav" ).addEventListener( "click", function() {

    toggleMobileMenu();
});

function toggleMobileMenu(){
    var button = document.getElementById( "header-toggle-nav" );
    button.classList.toggle( "active" );

    var body = document.querySelector( "body" );
    var status = body.classList.contains('mobile-menu');

    if(status){
        animate({
            duration: 200,
            timing: function(timeFraction) {
                return timeFraction;
            },
            draw: function(progress) {
                body.style.left = (1 - progress / 271) + 'px';
                if(progress >= 1){
                    body.classList.remove( "mobile-menu" );
                }
            }
        });
    }else{
        body.classList.toggle( "mobile-menu" );
        animate({
            duration: 200,
            timing: function(timeFraction) {
                return timeFraction;
            },
            draw: function(progress) {
                body.style.left = progress * 271 + 'px';
            }
        });
    }
}

function animate(options) {

  var start = performance.now();

  requestAnimationFrame(function animate(time) {
    // timeFraction от 0 до 1
    var timeFraction = (time - start) / options.duration;
    if (timeFraction > 1) timeFraction = 1;

    // текущее состояние анимации
    var progress = options.timing(timeFraction)

    options.draw(progress);

    if (timeFraction < 1) {
      requestAnimationFrame(animate);
    }

  });
}