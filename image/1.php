<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

header('Content-Type: image/png');

$side = new Imagick();
$side->readImage(dirname(__FILE__) . '/../uploads/prints/65/front.png');
$side->thumbnailImage(200, 0);

$side_image = new Imagick();
$side_image->readImage(dirname(__FILE__) . '/../images/constructor/manshort-front.png');

$white = new ImagickPixel('#333');

$side_background = new Imagick();
$side_background->newImage(500, 500, $white);
$side_background->setImageFormat('png');

$side_background->compositeImage($side_image, imagick::COMPOSITE_OVER, 0, 0);
$side_background->compositeImage($side, imagick::COMPOSITE_OVER, 155, 120);

echo $side_background;